﻿fl.outputPanel.clear();
 
var items = fl.getDocumentDOM().library.items;
var catalog = "PowerUps/PANDORA_BOX/";
var needle = "SPACE_BLACKBOX";

if (items.length > 0) {
	var index = 1;
    for each (var item in items) {
    	if (item.name.indexOf("SPACE_BLACKBOX") > -1) {
			var arrName = item.name.split("SPACE_BLACKBOX");
			var newName = needle + index.toString();
			
			if (item.name != newName) {
				item.name = newName;
			}
			
			index++;
		}
			
    }
} else {
    alert("The current Flash document's Library is empty.");
}