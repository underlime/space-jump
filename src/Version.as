package  
{
	
	/**
	 * ...
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public final class Version 
	{
		
		static public const Major:int = 0;
		static public const Minor:int = 0;
		static public const Build:int = 0;
		static public const Revision:int = 0;
		
	}
	
}