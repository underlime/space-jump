package application.models
{
    import application.models.top.GeneralTopModel;

    public class TopGlobalModel extends GeneralTopModel
    {
        public function TopGlobalModel()
        {
            super(ModelData.TOP_GLOBAL_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['top_global'];
        }
    }
}
