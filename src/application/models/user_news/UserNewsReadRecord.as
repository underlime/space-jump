package application.models.user_news
{
    import framework.data.Data;
    import framework.data.fields.IntegerField;

    public class UserNewsReadRecord extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _news:IntegerField = new IntegerField();

        public function UserNewsReadRecord()
        {
            super('UserNewsReadRecord');
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get news():IntegerField
        {
            return _news;
        }
    }
}
