package application.models.user_news
{
    import framework.data.Data;
    import framework.data.fields.CharField;
    import framework.data.fields.DateTimeField;
    import framework.data.fields.IntegerField;

    public class UserNewsRecord extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _user:IntegerField = new IntegerField();
        private var _news_type:IntegerField = new IntegerField();
        private var _time:DateTimeField = new DateTimeField();
        private var _expires_time:DateTimeField = new DateTimeField();
        private var _additional_data:CharField = new CharField();

        public function UserNewsRecord()
        {
            super('UserNewsRecord');
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get user():IntegerField
        {
            return _user;
        }

        public function get news_type():IntegerField
        {
            return _news_type;
        }

        public function get time():DateTimeField
        {
            return _time;
        }

        public function get expires_time():DateTimeField
        {
            return _expires_time;
        }

        public function get additional_data():CharField
        {
            return _additional_data;
        }
    }
}
