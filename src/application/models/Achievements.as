package application.models
{
    import application.models.achievements.AchievementRecord;

    import framework.data.ListData;
    import framework.data.SortHelper;

    public class Achievements extends ListData
    {
        public function Achievements()
        {
            _RecordClass = AchievementRecord;
            super(ModelData.ACHIEVEMENTS_MODEL);
        }

        public function getSortedList():Vector.<AchievementRecord>
        {
            var result:Vector.<AchievementRecord> = new Vector.<AchievementRecord>();
            for (var key:String in list.value)
                result.push(list.value[key]);

            return result.sort(SortHelper.getComparison(SortHelper.ASC));
        }

        override public function getModelDataKeys():Array
        {
            return ['achievements'];
        }

        public function getAchievementByCode(code:String):AchievementRecord
        {
            for each (var achvRec:AchievementRecord in list.value) {
                if (achvRec.code.value == code)
                    return achvRec;
            }
            return null;
        }
    }
}
