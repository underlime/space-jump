/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.04.13
 * Time: 14:53
 */
package application.models
{
    import application.models.items.ItemRecord;

    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import framework.data.Data;
    import framework.data.ModelsRegistry;
    import framework.data.fields.DateTimeField;
    import framework.data.fields.IntegerField;

    public class SuiteParams extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SuiteParams()
        {
            super(ModelData.SUITE_PARAMS);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["suite_params"]
        }

        override public function destroy():void
        {
            _destroyRepairTimer();
            super.destroy();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _id:IntegerField = new IntegerField();
        private var _item:IntegerField = new IntegerField();
        private var _hp:IntegerField = new IntegerField();
        private var _hp_time:DateTimeField = new DateTimeField();
        private var _auto_repair_seconds:IntegerField = new IntegerField();
        private var _repair_price:IntegerField = new IntegerField();
        private var _free_jumps:IntegerField = new IntegerField();

        private var _repairTimer:Timer;
        private var _mSecondsRemain:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        override public function importData(data:Object):void
        {
            super.importData(data);
            _initRepairTimer();
        }

        private function _initRepairTimer():void
        {
            if (!(hp_time.value && auto_repair_seconds.value))
                return;

            _destroyRepairTimer();

            var now:Date = new Date();
            var allRightTime:Number = hp_time.value.getTime() + auto_repair_seconds.value*1000;
            _mSecondsRemain = allRightTime - now.getTime();

            if (_mSecondsRemain > 0) {
                _repairTimer = new Timer(_mSecondsRemain);
                _repairTimer.addEventListener(TimerEvent.TIMER, _reset);
                _repairTimer.start();
            }
            else {
                _reset();
            }
        }

        private function _reset(event:TimerEvent = null):void
        {
            hp_time.value = new Date();
            if (suiteItem)
                hp.value = suiteItem.max_hp.value;
            _initRepairTimer();

            var changeEvent:Event = new Event(Event.CHANGE);
            dispatchEvent(changeEvent);
        }

        private function _destroyRepairTimer():void
        {
            if (_repairTimer) {
                _repairTimer.stop();
                _repairTimer.removeEventListener(TimerEvent.TIMER, _reset);
                _repairTimer = null;
            }
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get item():IntegerField
        {
            return _item;
        }

        public function get hp():IntegerField
        {
            return _hp;
        }

        public function get hp_time():DateTimeField
        {
            return _hp_time;
        }

        public function get auto_repair_seconds():IntegerField
        {
            return _auto_repair_seconds;
        }

        public function get repair_price():IntegerField
        {
            return _repair_price;
        }

        public function get free_jumps():IntegerField
        {
            return _free_jumps;
        }

        public function get suiteItem():ItemRecord
        {
            var itemsModel:Items = ModelsRegistry.getModel(ModelData.ITEMS_MODEL) as Items;
            var itemId:String = _item.value.toString();
            return itemsModel.getRecordById(itemId) as ItemRecord;
        }

        public function get actualHp():int
        {
            if (!hp_time.value)
                return hp.value;

            var hpTimestamp:Number = hp_time.value.getTime();
            var now:Date = new Date();
            var currentTimestamp:Number = now.getTime();
            if (currentTimestamp - hpTimestamp >= _auto_repair_seconds.value*1000) {
                var item:ItemRecord = suiteItem;
                if (item)
                    return suiteItem.max_hp.value;
            }

            return hp.value;
        }

        public function get damage():int
        {
            var item:ItemRecord = suiteItem;
            if (item)
                return (item.max_hp.value - actualHp);
            return 0;
        }


    }
}
