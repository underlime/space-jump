package application.models.user_achievements
{
    import framework.data.fields.IntegerField;

    public class UserAchievementRecord
    {
        private var _id:IntegerField = new IntegerField();
        private var _achievement:IntegerField = new IntegerField();

        public function UserAchievementRecord()
        {
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function set id(value:IntegerField):void
        {
            _id = value;
        }

        public function get achievement():IntegerField
        {
            return _achievement;
        }

        public function set achievement(value:IntegerField):void
        {
            _achievement = value;
        }
    }
}
