package application.models
{
    import application.models.trophies.TrophiesCollectionRecord;
    import application.models.trophies.TrophyRecord;

    import framework.data.ListData;
    import framework.data.SortHelper;

    public class Trophies extends ListData
    {
        private var _trophiesCache:Object = {};

        public function Trophies()
        {
            _RecordClass = TrophiesCollectionRecord;
            super(ModelData.TROPHIES_MODEL);
        }

        public function getTrophyById(searchId:String):TrophyRecord
        {
            if (_trophiesCache[searchId])
                return _trophiesCache[searchId];

            var collectionId:String;
            var trophyId:String;
            var collection:TrophiesCollectionRecord;

            var trophyRecord:TrophyRecord = null;
            for (collectionId in list.value) {
                collection = list.value[collectionId];
                for (trophyId in collection.trophies_list.list.value) {
                    if (trophyId == searchId) {
                        trophyRecord = collection.trophies_list.list.value[trophyId];
                        _trophiesCache[trophyId] = trophyRecord;
                        break;
                    }
                }

                if (trophyRecord)
                    break;
            }

            return trophyRecord;
        }

        public function getSortedList():Vector.<TrophiesCollectionRecord>
        {
            var result:Vector.<TrophiesCollectionRecord> = new Vector.<TrophiesCollectionRecord>();
            for (var key:String in list.value)
                result.push(list.value[key]);

            return result.sort(SortHelper.getComparison(SortHelper.ASC));
        }

        override public function getModelDataKeys():Array
        {
            return ['trophies_collections'];
        }

        override public function importData(data:Object):void
        {
            _trophiesCache = {};
            super.importData(data);
        }
    }
}
