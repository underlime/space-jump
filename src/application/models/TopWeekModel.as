package application.models
{
    import application.models.top.GeneralTopModel;

    public class TopWeekModel extends GeneralTopModel
    {
        public function TopWeekModel()
        {
            super(ModelData.TOP_WEEK_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['top_week'];
        }
    }
}
