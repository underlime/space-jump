package application.models
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ModelData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const GAME_MODEL:String = "GameModel";
        public static const STATISTICS_GAME_MODEL:String = "StatisticsGameModel";
        public static const USER_PARAMS_MODEL:String = "UsrParamsModel";

        public static const USER_MODEL:String = 'User';
        public static const USERS_LIST_MODEL:String = 'UsersList';
        public static const SOC_NET_INFO_REGISTRY:String = 'SocNetInfoRegistry';

        public static const ITEMS_MODEL:String = 'Items';
        public static const ACHIEVEMENTS_MODEL:String = 'Achievements';
        public static const USER_ACHIEVEMENTS_MODEL:String = 'UserAchievements';
        public static const INVENTORY_MODEL:String = 'Inventory';


        public static const PATTERN_MODEL:String = "Pattern";
        public static const OBJECT_PATTERN:String = "ObjectPattern";
        public static const PATTERNS_COLLECTION:String = "PatternsCollection";

        public static const SKINS_COLLECTION:String = "SkinsCollection";
        public static const SKIN_MODEL:String = "SkinModel";

        public static const TOP_DAY_MODEL:String = "TopDayModel";
        public static const TOP_WEEK_MODEL:String = "TopWeekModel";
        public static const TOP_MONTH_MODEL:String = "TopMonthModel";
        public static const TOP_GLOBAL_MODEL:String = "TopGlobalModel";

        public static const TROPHIES_MODEL:String = "Trophies";
        public static const USERS_TROPHIES_MODEL:String = "UsersTrophies";
        public static const SUITE_PARAMS:String = "SuiteParamsModel";
        public static const GAME_SESSION:String = "GameSessionModel";
        public static const COINS_UPGRADE:String = "CoinsUpgradeRecord";
        public static const COINS_COLLECTION:String = "CoinsCollectionModel";
        public static const USER_NEWS_MODEL:String = 'UserNews';
        public static const USER_NEWS_READ_MODEL:String = 'UserNewsRead';

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}