package application.models
{
    import application.models.top.GeneralTopModel;

    public class TopDayModel extends GeneralTopModel
    {
        public function TopDayModel()
        {
            super(ModelData.TOP_DAY_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['top_day'];
        }
    }
}
