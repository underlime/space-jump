package application.models.top
{
    import application.models.Items;
    import application.models.ModelData;
    import application.models.items.ItemRecord;

    import framework.data.Data;
    import framework.data.ModelsRegistry;

    public class TopRecord extends Data
    {
        private var _user_info:TopUserInfo = new TopUserInfo();
        private var _inventory:TopInventory = new TopInventory();
        private var _itemsModel:Items;

        public function TopRecord()
        {
            super('TopRecord');
            _itemsModel = ModelsRegistry.getModel(ModelData.ITEMS_MODEL) as Items;
        }

        public function get user_info():TopUserInfo
        {
            return _user_info;
        }

        public function get inventory():TopInventory
        {
            return _inventory;
        }

        public function get itemsList():Vector.<ItemRecord>
        {
            var itemsList:Vector.<ItemRecord> = new Vector.<ItemRecord>();
            var itemRecord:ItemRecord;
            var itemId:String;

            for (var invId:String in inventory.list.value) {
                itemId = inventory.list.value[invId].item.value.toString();
                itemRecord = this._itemsModel.getRecordById(itemId) as ItemRecord;
                if (itemRecord)
                    itemsList.push(itemRecord);
            }

            return itemsList;
        }
    }
}
