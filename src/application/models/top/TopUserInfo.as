package application.models.top
{
    import framework.data.Data;
    import framework.data.fields.CharField;
    import framework.data.fields.IntegerField;

    public class TopUserInfo extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _soc_net_id:CharField = new CharField();
        private var _max_score_day:IntegerField = new IntegerField();
        private var _max_score_week:IntegerField = new IntegerField();
        private var _max_score_month:IntegerField = new IntegerField();
        private var _max_score_global:IntegerField = new IntegerField();
        private var _stars:IntegerField = new IntegerField();

        public function TopUserInfo()
        {
            super('TopUserInfo');
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get soc_net_id():CharField
        {
            return _soc_net_id;
        }

        public function get max_score_day():IntegerField
        {
            return _max_score_day;
        }

        public function get max_score_week():IntegerField
        {
            return _max_score_week;
        }

        public function get max_score_month():IntegerField
        {
            return _max_score_month;
        }

        public function get max_score_global():IntegerField
        {
            return _max_score_global;
        }

        public function get stars():IntegerField
        {
            return _stars;
        }
    }
}
