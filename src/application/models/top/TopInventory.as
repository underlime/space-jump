package application.models.top
{
    import framework.data.ListData;

    public class TopInventory extends ListData
    {
        public function TopInventory()
        {
            _RecordClass = TopInventoryRecord;
            super('TopInventoryList');
        }
    }
}
