package application.models.top
{
    import framework.data.fields.IntegerField;

    public class TopInventoryRecord
    {
        private var _id:IntegerField = new IntegerField();
        private var _item:IntegerField = new IntegerField();
        private var _count:IntegerField = new IntegerField();
        private var _equipped:IntegerField = new IntegerField();

        public function TopInventoryRecord()
        {
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get item():IntegerField
        {
            return _item;
        }

        public function get count():IntegerField
        {
            return _count;
        }

        public function get equipped():IntegerField
        {
            return _equipped;
        }
    }
}
