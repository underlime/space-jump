package application.models.top
{
    import framework.data.ListData;

    public class GeneralTopModel extends ListData
    {
        public function GeneralTopModel(name:String)
        {
            _RecordClass = TopRecord;
            super(name);
        }

        public function getSocNetIdsList():Array
        {
            var socNetIds:Array = [];
            var topRecord:TopRecord;
            for (var i:String in list.value) {
                topRecord = list.value[i];
                socNetIds.push(topRecord.user_info.soc_net_id.value);
            }

            return socNetIds;
        }
    }
}
