package application.models
{
    import application.models.trophies.UsersTrophyRecord;

    import framework.data.ListData;

    public class UsersTrophies extends ListData
    {
        public function UsersTrophies()
        {
            _RecordClass = UsersTrophyRecord;
            super(ModelData.USERS_TROPHIES_MODEL);
        }

        public function hasTrophy(trophyId:int):Boolean
        {
            for (var key:String in list.value)
                if (list.value[key].trophy.value == trophyId)
                    return true;
            return false;
        }

        override public function getModelDataKeys():Array
        {
            return ['users_trophies'];
        }
    }
}
