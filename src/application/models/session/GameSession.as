/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.04.13
 * Time: 14:58
 */
package application.models.session
{
    import application.models.ModelData;

    import framework.data.Data;
    import framework.data.fields.BooleanField;
    import framework.data.fields.CharField;
    import framework.data.fields.DateTimeField;
    import framework.data.fields.IntegerField;

    public class GameSession extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameSession()
        {
            super(ModelData.GAME_SESSION);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["game_session"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _trophy_flag:BooleanField = new BooleanField();
        private var _difficulty:CharField = new CharField();
        private var _start_time:DateTimeField = new DateTimeField();
        private var _id:IntegerField = new IntegerField();
        private var _user:IntegerField = new IntegerField();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get trophy_flag():BooleanField
        {
            return _trophy_flag;
        }

        public function get difficulty():CharField
        {
            return _difficulty;
        }

        public function get start_time():DateTimeField
        {
            return _start_time;
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get user():IntegerField
        {
            return _user;
        }
    }
}
