package application.models.achievements
{
    import framework.data.fields.CharField;
    import framework.data.fields.IntegerField;

    public class AchievementRecord
    {
        private var _id:IntegerField = new IntegerField();
        private var _code:CharField = new CharField();
        private var _sort:IntegerField = new IntegerField();
        private var _name_ru:CharField = new CharField();
        private var _name_en:CharField = new CharField();
        private var _description_ru:CharField = new CharField();
        private var _description_en:CharField = new CharField();
        private var _image:CharField = new CharField();
        private var _criteria_current_score:IntegerField = new IntegerField();
        private var _criteria_global_score:IntegerField = new IntegerField();
        private var _criteria_current_coins:IntegerField = new IntegerField();
        private var _criteria_global_coins:IntegerField = new IntegerField();

        public function AchievementRecord()
        {
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get code():CharField
        {
            return _code;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get name_en():CharField
        {
            return _name_en;
        }

        public function get description_ru():CharField
        {
            return _description_ru;
        }

        public function get description_en():CharField
        {
            return _description_en;
        }

        public function get image():CharField
        {
            return _image;
        }

        public function get criteria_current_score():IntegerField
        {
            return _criteria_current_score;
        }

        public function get criteria_global_score():IntegerField
        {
            return _criteria_global_score;
        }

        public function get criteria_current_coins():IntegerField
        {
            return _criteria_current_coins;
        }

        public function get criteria_global_coins():IntegerField
        {
            return _criteria_global_coins;
        }

        public function get sort():IntegerField
        {
            return _sort;
        }
    }
}
