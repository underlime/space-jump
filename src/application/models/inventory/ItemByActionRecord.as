package application.models.inventory
{
    import application.models.items.ItemRecord;

    public class ItemByActionRecord
    {
        public var count:int = 0;
        public var equipped:int = 0;
        public var equippedItemsList:Vector.<ItemRecord> = new Vector.<ItemRecord>();
    }
}
