package application.models.inventory
{
    import framework.data.fields.IntegerField;

    public class InventoryRecord
    {
        private var _id:IntegerField = new IntegerField();
        private var _item:IntegerField = new IntegerField();
        private var _count:IntegerField = new IntegerField();
        private var _equipped:IntegerField = new IntegerField();

        public function InventoryRecord()
        {
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function set id(value:IntegerField):void
        {
            _id = value;
        }

        public function get item():IntegerField
        {
            return _item;
        }

        public function set item(value:IntegerField):void
        {
            _item = value;
        }

        public function get count():IntegerField
        {
            return _count;
        }

        public function set count(value:IntegerField):void
        {
            _count = value;
        }

        public function get equipped():IntegerField
        {
            return _equipped;
        }

        public function set equipped(value:IntegerField):void
        {
            _equipped = value;
        }
    }
}
