package application.models
{
    import application.config.DataPhysics;
    import application.config.GameMode;
    import application.events.ApplicationEvent;
    import application.views.game.layers.Layer;

    import flash.events.Event;

    import framework.data.Data;
    import framework.data.ModelDataEvent;
    import framework.data.ModelsRegistry;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class StatisticsGameModel extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const COPPER_VALUE:int = 1;
        public static const SILVER_VALUE:int = 2;
        public static const GOLD_VALUE:int = 3;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StatisticsGameModel()
        {
            super(ModelData.STATISTICS_GAME_MODEL);
            beginFreeFall();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["statistics_data"];
        }

        override public function destroy():void
        {
            _userAchievements.removeEventListener(ModelDataEvent.ADD, _achievementAddHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var collisionType:String = "";

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function beginCollectAchievements():void
        {
            _userAchievements = ModelsRegistry.getModel(ModelData.USER_ACHIEVEMENTS_MODEL) as UserAchievements;
            _userAchievements.addEventListener(ModelDataEvent.ADD, _achievementAddHandler);
        }

        public function beginFreeFall():void
        {
            _tempDistance = this.distance;
        }

        public function endFreeFall():void
        {
            var totalFall:Number = this.distance - _tempDistance;
            if (totalFall > this.freeFall)
                this.freeFall = totalFall;

            beginFreeFall();
        }

        public function reduceHealth(damage:int):void
        {
            var curArmor:Number = this.armor;
            if (curArmor > 0) {
                if (curArmor >= damage) {
                    this.armor = curArmor - damage;
                }
                else {
                    var diff:int = damage - curArmor;
                    this.armor = 0;
                    this.health -= diff;
                }
            }
            else {
                this.health -= damage;
            }
        }

        public function setupDefaultHealth(health:Number):void
        {
            _defaultHealth = health;
            this.health = _defaultHealth;
        }

        /**
         * Сбросить здоровье на начальное значение
         */
        public function flushHealth():void
        {
            this.health = _defaultHealth;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        //Переменные, не подверженные шифрованию,
        //к ним можно обращаться напрямую
        private var Z:Number = Math.round(1 + Math.random() * 1000);
        private var ENCODED_NULL:Number = _encodeNumber(0);

        private var _defaultHeight:Number = 40000;
        private var _defaultHealth:Number = 10;
        private var _tempDistance:Number = 0;
        private var _missionsComplete:int = 0;

        private var _maxBonus:Number = DataPhysics.instance.max_bonus.value;
        private var _maxArmor:Number = DataPhysics.instance.max_armor.value;

        private var _layer:String = Layer.COSMOS_LAYER;
        private var _locked:Boolean = false;
        private var _gameMode:String = GameMode.EASY;

        private var _usedItems:Array = [];
        private var _success:Boolean = false;
        private var _trophyFound:Boolean = false;

        private var _userAchievements:UserAchievements;
        private var _user:User;

        //Шифрованные переменные,
        //к ним обращаться ТОЛЬКО через аксессоры

        private var _points:Number = ENCODED_NULL;
        private var _seconds:Number = ENCODED_NULL;
        private var _height:Number = _encodeNumber(40000);
        private var _distance:Number = ENCODED_NULL;

        private var _health:Number = _encodeNumber(DataPhysics.instance.user_default_health.value);
        private var _armor:Number = _encodeNumber(DataPhysics.instance.user_default_armor.value);

        private var _maxVelocity:Number = ENCODED_NULL;
        private var _freeFall:Number = ENCODED_NULL;
        private var _coins:Number = ENCODED_NULL;
        private var _bonus:Number = ENCODED_NULL;
        private var _maxHealth:Number = _encodeNumber(10);
        private var _cola:Number = ENCODED_NULL;

        private var _copper:Number = ENCODED_NULL;
        private var _silver:Number = ENCODED_NULL;
        private var _gold:Number = ENCODED_NULL;

        private var _booster_count:Number = ENCODED_NULL;
        private var _power_shield_count:Number = ENCODED_NULL;

        private var _heart_count:Number = ENCODED_NULL;
        private var _shield_count:Number = ENCODED_NULL;

        private var _bonus_seconds:Number = ENCODED_NULL;
        private var _pause_seconds:Number = ENCODED_NULL;

        private var _gemsTotal:int = ENCODED_NULL;
        private var _gemsMultiply:int = _encodeNumber(DataPhysics.instance.chance_multy.value);
        private var _gemsCurrent:int = _encodeNumber(1);
        private var _gemsSpent:int = ENCODED_NULL;

        private var _resurrectionCount:int = ENCODED_NULL;

        private var _physicsTicks:Number = ENCODED_NULL;
        private var _realTime:Number = ENCODED_NULL;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _encodeNumber(x:Number):Number
        {
            return (x - Z);
        }

        private function _decodeNumber(y:Number):Number
        {
            return (y + Z);
        }

        private function _addPoints(diff:Number = 1):void
        {
            this.points += int(diff);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _achievementAddHandler(event:ModelDataEvent):void
        {
            _missionsComplete++;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get points():Number
        {
            return _decodeNumber(_points);
        }

        public function set points(value:Number):void
        {
            var currentPoints:Number = _decodeNumber(_points);
            if (currentPoints != value) {
                _points = _encodeNumber(value);
                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        public function get seconds():Number
        {
            return _decodeNumber(_seconds);
        }

        public function set seconds(value:Number):void
        {
            var currentSeconds:Number = _decodeNumber(_seconds);
            if (currentSeconds != value && !_locked) {
                _seconds = _encodeNumber(value);
                super.dispatchEvent(new Event(Event.CHANGE));
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TIME_UPDATE));
            }
        }

        public function get height():Number
        {
            return _gameMode != GameMode.ENDLESS ? _decodeNumber(_height) : this.distance;
        }

        public function set height(value:Number):void
        {
            var currentHeight:Number = _decodeNumber(_height);
            if (currentHeight != value && !_locked) {
                if (value >= 0)
                    _height = _encodeNumber(value);
                else
                    _height = ENCODED_NULL;
                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        public function get distance():Number
        {
            return _decodeNumber(_distance);
        }

        public function set distance(value:Number):void
        {
            var currentDistance:Number = _decodeNumber(_distance);
            var diff:Number = value - currentDistance;

            if (currentDistance != value && !_locked) {
                _distance = _encodeNumber(value);
                _addPoints(diff);
                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        public function get health():Number
        {
            return _decodeNumber(_health);
        }

        public function set health(value:Number):void
        {
            var currentHealth:Number = _decodeNumber(_health);
            if (currentHealth != value) {
                if (value >= 0 && value <= this.maxHealth)
                    _health = _encodeNumber(value);
                else
                    if (value < 0)
                        _health = ENCODED_NULL;
                    else
                        if (value > this.maxHealth)
                            _health = _encodeNumber(this.maxHealth);

                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        public function get maxVelocity():Number
        {
            return int(_decodeNumber(_maxVelocity));
        }

        public function set maxVelocity(value:Number):void
        {
            var currentVal:Number = _decodeNumber(_maxVelocity);
            if (currentVal != value && !_locked)
                _maxVelocity = _encodeNumber(value);
        }

        public function get freeFall():Number
        {
            return _decodeNumber(_freeFall);
        }

        public function set freeFall(value:Number):void
        {
            _freeFall = _encodeNumber(value);
        }

        public function set coins(value:Number):void
        {
            var currentCoins:Number = _decodeNumber(_coins);
            if (currentCoins != value) {
                _coins = _encodeNumber(value);
                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        public function get coins():Number
        {
            return _decodeNumber(_coins);
        }

        public function get armor():Number
        {
            return _decodeNumber(_armor);
        }

        public function set armor(value:Number):void
        {
            var currentArmor:Number = _decodeNumber(_armor);
            if (currentArmor != value) {
                if (value <= _maxArmor)
                    _armor = _encodeNumber(value);
                else
                    _armor = _encodeNumber(_maxArmor);
                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        public function get bonus():Number
        {
            return _decodeNumber(_bonus);
        }

        public function set bonus(value:Number):void
        {
            ++this.cola;
            var cur:Number = _decodeNumber(_bonus);
            if (cur != value) {
                _bonus = _encodeNumber(value);
                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        public function get layer():String
        {
            return _layer;
        }

        public function set layer(value:String):void
        {
            if (_layer != value) {
                _layer = value;
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.LAYER_CHANGE));
            }
        }

        public function set maxHealth(value:Number):void
        {
            _maxHealth = _encodeNumber(value);
        }

        public function get maxHealth():Number
        {
            return _decodeNumber(_maxHealth);
        }

        public function set defaultHeight(value:Number):void
        {
            _defaultHeight = value;
            this.height = _defaultHeight;
        }

        public function get defaultHeight():Number
        {
            return _defaultHeight;
        }

        public function get locked():Boolean
        {
            return _locked;
        }

        public function set locked(value:Boolean):void
        {
            _locked = value;
        }

        public function get maxBonus():Number
        {
            return _maxBonus;
        }

        public function set gameMode(gameMode:String):void
        {
            _gameMode = gameMode;
        }

        public function get usedItems():Array
        {
            return _usedItems;
        }

        public function get trophyFound():Boolean
        {
            return _trophyFound;
        }

        public function get missionsComplete():int
        {
            return _missionsComplete;
        }

        public function get success():Boolean
        {
            return _success;
        }

        public function set success(value:Boolean):void
        {
            _success = value;
        }

        public function set trophyFound(value:Boolean):void
        {
            _trophyFound = value;
        }

        public function get cola():Number
        {
            return _decodeNumber(_cola);
        }

        public function set cola(value:Number):void
        {
            _cola = _encodeNumber(value);
        }

        public function get copper():Number
        {
            return _decodeNumber(_copper);
        }

        public function get silver():Number
        {
            return _decodeNumber(_silver);
        }

        public function get gold():Number
        {
            return _decodeNumber(_gold);
        }

        public function set copper(value:Number):void
        {
            _copper = _encodeNumber(value);
        }

        public function set silver(value:Number):void
        {
            _silver = _encodeNumber(value);
        }

        public function set gold(value:Number):void
        {
            _gold = _encodeNumber(value);
        }

        public function get booster_count():Number
        {
            return _decodeNumber(_booster_count);
        }

        public function set booster_count(value:Number):void
        {
            _booster_count = _encodeNumber(value);
        }

        public function get power_shield_count():Number
        {
            return _decodeNumber(_power_shield_count);
        }

        public function set power_shield_count(value:Number):void
        {
            _power_shield_count = _encodeNumber(value);
        }

        public function get heart_count():Number
        {
            return _decodeNumber(_heart_count);
        }

        public function set heart_count(value:Number):void
        {
            _heart_count = _encodeNumber(value);
        }

        public function get shield_count():Number
        {
            return _decodeNumber(_shield_count);
        }

        public function set shield_count(value:Number):void
        {
            _shield_count = _encodeNumber(value);
        }

        public function get bonus_seconds():Number
        {
            return _decodeNumber(_bonus_seconds);
        }

        public function set bonus_seconds(value:Number):void
        {
            _bonus_seconds = _encodeNumber(value);
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TIME_UPDATE));
        }

        public function get pause_seconds():Number
        {
            return _decodeNumber(_pause_seconds);
        }

        public function set pause_seconds(value:Number):void
        {
            _pause_seconds = _encodeNumber(value);
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TIME_UPDATE));
        }

        public function get gemsTotal():int
        {
            return _decodeNumber(_gemsTotal);
        }

        public function set gemsTotal(value:int):void
        {
            _gemsTotal = _encodeNumber(value);
        }

        public function get gemsMultiply():int
        {
            return _decodeNumber(_gemsMultiply);
        }

        public function set gemsMultiply(value:int):void
        {
            _gemsMultiply = _encodeNumber(value);
        }

        public function get gemsCurrent():int
        {
            return _decodeNumber(_gemsCurrent);
        }

        public function set gemsCurrent(value:int):void
        {
            _gemsCurrent = _encodeNumber(value);
        }

        public function get gemsSpent():int
        {
            return _decodeNumber(_gemsSpent);
        }

        public function set gemsSpent(value:int):void
        {
            _gemsSpent = _encodeNumber(value);
        }

        public function get resurrectionCount():int
        {
            return _decodeNumber(_resurrectionCount);
        }

        public function set resurrectionCount(value:int):void
        {
            _resurrectionCount = _encodeNumber(value);
        }

        public function get physicsTicks():Number
        {
            return _decodeNumber(_physicsTicks);
        }

        public function set physicsTicks(value:Number):void
        {
            _physicsTicks = _encodeNumber(value);
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.TIME_UPDATE));
        }

        public function get realTime():Number
        {
            return _decodeNumber(_realTime);
        }

        public function set realTime(value:Number):void
        {
            _realTime = _encodeNumber(value);
        }
    }

}
