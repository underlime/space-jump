package application.models
{
    import application.models.inventory.InventoryRecord;
    import application.models.inventory.ItemByActionRecord;
    import application.models.items.ItemRecord;

    import framework.data.ListData;
    import framework.data.ModelsRegistry;

    public class Inventory extends ListData
    {
        public function Inventory()
        {
            _RecordClass = InventoryRecord;
            super(ModelData.INVENTORY_MODEL);
        }

        public function getRecordByItemId(itemId:int):InventoryRecord
        {
            for (var key:String in list.value)
                if (list.value[key].item.value == itemId)
                    return list.value[key];
            return null;
        }

        public function getItemsGroupByAction():Object
        {
            var itemsModel:Items = ModelsRegistry.getModel(ModelData.ITEMS_MODEL) as Items;
            var itemsList:Object = {};

            for (var i:int=0; i<Items.ACTIONS_LIST.length; ++i)
                itemsList[Items.ACTIONS_LIST[i]] = new ItemByActionRecord();

            var itemId:String;
            var itemRecord:ItemRecord;
            for each (var inventoryRecord:InventoryRecord in list.value) {
                itemId = inventoryRecord.item.value.toString();
                itemRecord = itemsModel.getRecordById(itemId) as ItemRecord;
                if (itemRecord.action.value in itemsList)
                    _addRecordToGroupByAction(itemsList, itemRecord, inventoryRecord);
            }

            return itemsList;
        }

        private function _addRecordToGroupByAction(itemsList:Object, itemRecord:ItemRecord, inventoryRecord:InventoryRecord):void
        {
            itemsList[itemRecord.action.value].count += inventoryRecord.count.value;
            itemsList[itemRecord.action.value].equipped += inventoryRecord.equipped.value;
            if (inventoryRecord.equipped.value)
                itemsList[itemRecord.action.value].equippedItemsList.push(itemRecord);
        }

        public function getEquippedItem(action:String):ItemRecord
        {
            var item:ItemRecord;
            var itemsModel:Items = ModelsRegistry.getModel(ModelData.ITEMS_MODEL) as Items;

            for each (var record:InventoryRecord in list.value) {
                if (record.equipped.value) {
                    item = itemsModel.getRecordById(record.item.value.toString()) as ItemRecord;
                    if (item.action.value == action)
                        return item;
                }
            }
            return null;
        }

        override public function getModelDataKeys():Array
        {
            return ['inventory'];
        }
    }
}
