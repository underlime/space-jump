package application.models
{
    import application.models.coins.CoinsUpgradeCollection;
    import application.models.session.GameSession;

    import framework.data.DataBase;
    import framework.socnet.models.SocNetInfoRegistry;

    public class GameDataBase extends DataBase
    {
        public function GameDataBase()
        {
            super();
            _createDataModels();
        }

        private function _createDataModels():void
        {
            addChild(new User());
            addChild(new UserNews());
            addChild(new UserNewsRead());
            addChild(new UsersList());
            addChild(new SocNetInfoRegistry());
            addChild(new Items());
            addChild(new Achievements());
            addChild(new UserAchievements());
            addChild(new Inventory());

            addChild(new TopDayModel());
            addChild(new TopWeekModel());
            addChild(new TopMonthModel());
            addChild(new TopGlobalModel());

            addChild(new Trophies());
            addChild(new UsersTrophies());

            addChild(new GameModel());
            addChild(new SuiteParams());
            addChild(new GameSession());

            addChild(new CoinsUpgradeCollection());
        }
    }
}
