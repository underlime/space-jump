/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.02.13
 * Time: 13:00
 */
package application.models.skin
{
    import application.models.ModelData;

    import framework.data.ListData;

    public class SkinsCollection extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SkinsCollection()
        {
            _RecordClass = SkinModel;
            super(ModelData.SKINS_COLLECTION);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['skins'];
        }

        override public function importData(data:Object):void
        {
            super.importData(data);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getSkin(skinName:String):SkinModel
        {
            for (var key:String in list.value)
                if (list.value[key]['skin_name'].value == skinName)
                    return list.value[key];

            return list.value[0];
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
