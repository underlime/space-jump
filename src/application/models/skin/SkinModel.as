/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.02.13
 * Time: 13:02
 */
package application.models.skin
{
    import application.models.ModelData;

    import framework.data.Data;
    import framework.data.fields.BooleanField;
    import framework.data.fields.CharField;

    public class SkinModel extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SkinModel()
        {
            super(ModelData.SKIN_MODEL);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _skin_name:CharField = new CharField();
        private var _body:CharField = new CharField();

        private var _left_shoulder:CharField = new CharField();
        private var _left_hand:CharField = new CharField();
        private var _left_hip:CharField = new CharField();
        private var _left_leg:CharField = new CharField();
        private var _left_feet:CharField = new CharField();

        private var _right_shoulder:CharField = new CharField();
        private var _right_hand:CharField = new CharField();
        private var _right_hip:CharField = new CharField();
        private var _right_leg:CharField = new CharField();
        private var _right_feet:CharField = new CharField();

        private var _hat_enabled:BooleanField = new BooleanField(false);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get skin_name():CharField
        {
            return _skin_name;
        }

        public function get body():CharField
        {
            return _body;
        }

        public function get left_shoulder():CharField
        {
            return _left_shoulder;
        }

        public function get left_hand():CharField
        {
            return _left_hand;
        }

        public function get left_hip():CharField
        {
            return _left_hip;
        }

        public function get left_leg():CharField
        {
            return _left_leg;
        }

        public function get left_feet():CharField
        {
            return _left_feet;
        }

        public function get right_shoulder():CharField
        {
            return _right_shoulder;
        }

        public function get right_hand():CharField
        {
            return _right_hand;
        }

        public function get right_hip():CharField
        {
            return _right_hip;
        }

        public function get right_leg():CharField
        {
            return _right_leg;
        }

        public function get right_feet():CharField
        {
            return _right_feet;
        }

        public function get hat_enabled():BooleanField
        {
            return _hat_enabled;
        }
    }
}
