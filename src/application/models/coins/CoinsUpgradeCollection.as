/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.04.13
 * Time: 12:31
 */
package application.models.coins
{
    import application.models.ModelData;

    import framework.data.ListData;
    import framework.data.fields.IntegerField;

    public class CoinsUpgradeCollection extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CoinsUpgradeCollection()
        {
            _RecordClass = CoinsUpgradeRecord;
            super(ModelData.COINS_COLLECTION);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['coins_upgrade_collection'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getUpgradeDataByLevel(level:int = 0):CoinsUpgradeRecord
        {
            var coinsRecord:CoinsUpgradeRecord;

            for (var key:String in super.list.value) {
                coinsRecord = super.list.value[key];
                if (coinsRecord.upgrade.value == level)
                    return coinsRecord;
            }

            throw  new Error("Upgrade level " + level + " is not exists");
        }

        public function getCoinsChanceRecord(level:int = 0, sector:int = 1):CoinsChanceRecord
        {
            var coinsRecord:CoinsUpgradeRecord = getUpgradeDataByLevel(level);
            return coinsRecord.getCoinsChanceRecordBySector(sector);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _first_check_point:IntegerField = new IntegerField(24000);
        private var _second_check_point:IntegerField = new IntegerField(47000);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get first_check_point():IntegerField
        {
            return _first_check_point;
        }

        public function get second_check_point():IntegerField
        {
            return _second_check_point;
        }
    }
}
