/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.04.13
 * Time: 12:27
 */
package application.models.coins
{
    import application.models.ModelData;

    import framework.data.ListData;
    import framework.data.fields.IntegerField;

    public class CoinsUpgradeRecord extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CoinsUpgradeRecord()
        {
            _RecordClass = CoinsChanceRecord;
            super(ModelData.COINS_UPGRADE);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['coins_upgrade'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getCoinsChanceRecordBySector(sector:int = 1):CoinsChanceRecord
        {
            if (list.value[sector.toString()])
                return list.value[sector.toString()];

            return null;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _upgrade:IntegerField = new IntegerField(0);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get upgrade():IntegerField
        {
            return _upgrade;
        }
    }
}
