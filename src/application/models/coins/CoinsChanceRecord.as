/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 30.04.13
 * Time: 12:24
 */
package application.models.coins
{
    import framework.data.fields.IntegerField;

    public class CoinsChanceRecord
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CoinsChanceRecord()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function toString():String
        {
            return "silver_chance:" + _silver_chance.value.toString() + "\ngold_chance: " + _gold_chance.value.toString();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _silver_chance:IntegerField = new IntegerField(0);
        private var _gold_chance:IntegerField = new IntegerField(0);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get silver_chance():IntegerField
        {
            return _silver_chance;
        }

        public function get gold_chance():IntegerField
        {
            return _gold_chance;
        }
    }
}
