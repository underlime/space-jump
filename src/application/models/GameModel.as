package application.models
{
    import application.config.DataPhysics;
    import application.config.GameMode;
    import application.events.ApplicationEvent;
    import application.models.coins.CoinsUpgradeCollection;
    import application.models.pattern.PatternsCollection;

    import com.greensock.TweenLite;

    import framework.data.DataContainer;

    /**
     * Модель для хранения параметров, влияющих на игровой процесс
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class GameModel extends DataContainer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameModel()
        {
            super(ModelData.GAME_MODEL);
            _statistics = new StatisticsGameModel();
            _setup();
            addChild(_statistics);
            addChild(new PatternsCollection());
            addChild(new CoinsUpgradeCollection());
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ["game_params"];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function flushProperties():void
        {
            _tempVelocity = 100;
            _velocity = 100;
            _isBlockVelocity = false;
            _replaceStatisticsModel();
        }

        /**
         * Увеличиваем скорость
         */
            // TODO сделать по-человечески
        public function increaseVelocity():void
        {
            if (_velocity >= 1000)
                return;

            if (_gameMode != GameMode.ENDLESS) {
                if (_velocity < 250)
                    this.velocity += 9;
                else
                    if (_velocity < 500)
                        this.velocity += 5;
                    else
                        if (_velocity < 1000)
                            this.velocity += 2;
            } else {
                if (_statistics.distance > 70000) {
                    this.velocity += 5;
                } else {
                    this.velocity += 9;
                }
            }
        }

        /**
         * Установить неизменяемое значение скорости
         * @param velocity
         * @param tweenTime
         */
        public function setStaticVelocity(velocity:Number, tweenTime:int = 2):void
        {
            _tempVelocity = _velocity;
            if (tweenTime > 0) {
                TweenLite.to(this, tweenTime, {
                    velocity: velocity,
                    onComplete: blockVelocity
                });
            } else {
                this.velocity = velocity;
                this.blockVelocity();
            }
        }

        public function removeStaticVelocity(tweenTime:int = 2):void
        {
            TweenLite.killTweensOf(this);
            _isBlockVelocity = false;
            if (tweenTime > 0) {
                TweenLite.to(this, tweenTime, {velocity: _tempVelocity});
            } else {
                this.velocity = _tempVelocity;
            }
        }

        public function blockVelocity():void
        {
            _isBlockVelocity = true;
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.BLOCK));
        }

        public function setupBonusVelocity():void
        {
            this.removeStaticVelocity(0);
            _tempVelocity = _velocity;
            this.velocity = _bonusVelocity;
            _isBlockVelocity = true;
        }

        public function removeBonusVelocity():void
        {
            _isBlockVelocity = false;
            this.velocity = _tempVelocity;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _tempVelocity:Number = DataPhysics.instance.start_velocity.value;
        private var _velocity:Number = DataPhysics.instance.start_velocity.value;
        private var _bonusVelocity:Number = DataPhysics.instance.bonus_level_velocity.value;

        private var _isBlockVelocity:Boolean = false;
        private var _gameMode:String = GameMode.EASY;
        private var _gameModeHeights:Object = {};
        private var _statistics:StatisticsGameModel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            _gameModeHeights[GameMode.EASY] = 40000;
            _gameModeHeights[GameMode.NORMAL] = 70000;
            _gameModeHeights[GameMode.HARD] = 100000;
            _gameModeHeights[GameMode.ENDLESS] = int.MAX_VALUE;
        }

        private function _replaceStatisticsModel():void
        {
            removeChild(_statistics);
            _statistics.destroy();
            _statistics = new StatisticsGameModel();
            _statistics.gameMode = _gameMode;
            addChild(_statistics);
            if (_gameModeHeights[_gameMode])
                _statistics.defaultHeight = _gameModeHeights[_gameMode];
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get velocity():Number
        {
            return _velocity;
        }

        public function set velocity(value:Number):void
        {
            if (value != _velocity && !_isBlockVelocity) {
                _velocity = value;
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.VELOCITY_CHANGE));
                if (_statistics.maxVelocity < _velocity)
                    _statistics.maxVelocity = _velocity;
            }
        }

        public function set gameMode(mode:String):void
        {
            _gameMode = mode;
            if (_gameModeHeights[mode]) {
                _statistics.defaultHeight = _gameModeHeights[mode];
                _statistics.gameMode = mode;
            }
        }

        public function get gameMode():String
        {
            return _gameMode;
        }

        public function get isBlockVelocity():Boolean
        {
            return _isBlockVelocity;
        }
    }

}