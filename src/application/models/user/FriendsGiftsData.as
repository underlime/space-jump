package application.models.user
{
    import framework.data.Data;
    import framework.data.fields.IntegerField;

    public class FriendsGiftsData extends Data
    {
        private var _gems:IntegerField = new IntegerField();
        private var _coins:IntegerField = new IntegerField();
        private var _parachute:IntegerField = new IntegerField();
        private var _rocket:IntegerField = new IntegerField();
        private var _resurrection:IntegerField = new IntegerField();

        public function FriendsGiftsData()
        {
            super('FriendsGiftsData');
        }

        public function get gems():IntegerField
        {
            return _gems;
        }

        public function get coins():IntegerField
        {
            return _coins;
        }

        public function get parachute():IntegerField
        {
            return _parachute;
        }

        public function get rocket():IntegerField
        {
            return _rocket;
        }

        public function get resurrection():IntegerField
        {
            return _resurrection;
        }
    }
}
