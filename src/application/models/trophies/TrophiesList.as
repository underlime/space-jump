package application.models.trophies
{
    import framework.data.ListData;
    import framework.data.SortHelper;

    public class TrophiesList extends ListData
    {
        public function TrophiesList()
        {
            _RecordClass = TrophyRecord;
            super('TrophiesList');
        }

        public function getSortedList():Vector.<TrophyRecord>
        {
            var result:Vector.<TrophyRecord> = new Vector.<TrophyRecord>();
            for (var key:String in list.value)
                result.push(list.value[key]);

            return result.sort(SortHelper.getComparison(SortHelper.ASC));
        }
    }
}
