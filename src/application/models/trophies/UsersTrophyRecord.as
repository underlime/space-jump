package application.models.trophies
{
    import framework.data.Data;
    import framework.data.fields.IntegerField;

    public class UsersTrophyRecord extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _user:IntegerField = new IntegerField();
        private var _trophy:IntegerField = new IntegerField();

        public function UsersTrophyRecord()
        {
            super('UsersTrophyRecord');
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get user():IntegerField
        {
            return _user;
        }

        public function get trophy():IntegerField
        {
            return _trophy;
        }
    }
}
