package application.models.trophies
{
    import framework.data.fields.CharField;
    import framework.data.fields.IntegerField;

    public class TrophyRecord
    {
        private var _id:IntegerField = new IntegerField();
        private var _name_ru:CharField = new CharField();
        private var _name_en:CharField = new CharField();
        private var _sort:IntegerField = new IntegerField();
        private var _image:CharField = new CharField();
        private var _symbol:CharField = new CharField();
        private var _symbol_disabled:CharField = new CharField();

        public function TrophyRecord()
        {
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get name_en():CharField
        {
            return _name_en;
        }

        public function get sort():IntegerField
        {
            return _sort;
        }

        public function get image():CharField
        {
            return _image;
        }

        public function get symbol():CharField
        {
            return _symbol;
        }

        public function get symbol_disabled():CharField
        {
            return _symbol_disabled;
        }
    }
}
