package application.models.trophies
{
    import framework.data.Data;
    import framework.data.fields.CharField;
    import framework.data.fields.IntegerField;

    public class TrophiesCollectionRecord extends Data
    {
        private var _id:IntegerField = new IntegerField();
        private var _code:CharField = new CharField();
        private var _name_ru:CharField = new CharField();
        private var _name_en:CharField = new CharField();
        private var _sort:IntegerField = new IntegerField();

        private var _trophies_list:TrophiesList = new TrophiesList();

        public function TrophiesCollectionRecord()
        {
            super('TrophiesCollectionRecord');
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get code():CharField
        {
            return _code;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get name_en():CharField
        {
            return _name_en;
        }

        public function get sort():IntegerField
        {
            return _sort;
        }

        public function get trophies_list():TrophiesList
        {
            return _trophies_list;
        }
    }
}
