package application.models
{
    import application.models.user_news.UserNewsReadRecord;

    import framework.data.ListData;

    public class UserNewsRead extends ListData
    {
        public function UserNewsRead()
        {
            _RecordClass = UserNewsReadRecord;
            super(ModelData.USER_NEWS_READ_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['news_read'];
        }

        /**
         * Была ли новость прочитана
         * @param newsId
         * @return
         */
        public function wasRead(newsId:int):Boolean
        {
            for each (var record:UserNewsReadRecord in this.list.value) {
                if (record.news.value == newsId)
                    return true;
            }

            return false;
        }
    }
}
