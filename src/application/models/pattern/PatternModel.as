/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 17.02.13
 * Time: 10:41
 */
package application.models.pattern
{
    import application.models.ModelData;

    import framework.data.ListData;
    import framework.data.fields.BooleanField;
    import framework.data.fields.IntegerField;

    /**
     * Модель шаблона
     * @field level уровень сложности
     */
    public class PatternModel extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PatternModel()
        {
            _RecordClass = PatternObject;
            super(ModelData.PATTERN_MODEL);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['pattern'];
        }

        override public function importData(data:Object):void
        {
            super.importData(data);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _pattern_id:IntegerField = new IntegerField(0);
        private var _level:IntegerField = new IntegerField();
        private var _bonus_pattern:BooleanField = new BooleanField(false);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get pattern_id():IntegerField
        {
            return _pattern_id;
        }

        public function get level():IntegerField
        {
            return _level;
        }

        public function get bonus_pattern():BooleanField
        {
            return _bonus_pattern;
        }
    }
}
