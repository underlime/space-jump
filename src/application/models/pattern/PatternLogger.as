/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 04.05.13
 * Time: 14:50
 */
package application.models.pattern
{
    import flash.filters.GlowFilter;
    import flash.text.TextFieldAutoSize;

    import framework.view.text.SimpleTextField;

    public class PatternLogger extends SimpleTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PatternLogger()
        {
            super();
            _setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private function _setup():void
        {
            this.width = 150;
            this.size = 20;
            this.color = 0x000000;
            this.filters = [new GlowFilter(0xffffff)];
            this.x = 643;
            this.y = 50;
            this.autoSize = TextFieldAutoSize.RIGHT;
        }

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
