/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 17.02.13
 * Time: 10:47
 */
package application.models.pattern
{
    import application.models.ModelData;

    import framework.data.Data;
    import framework.data.fields.CharField;
    import framework.data.fields.FloatField;
    import framework.data.fields.IntegerField;

    /**
     * Данные для генерации объекта, входящего в шаблон.
     * @field object_alias строковой псевдоним для класса объекта
     * @field position_x координата X относительно левой границы экрана
     * @field position_y координата Y относительно начала шаблона (position_y = 0)
     * @field chance вероятность генерации объекта (0 - 100)
     * @field count количество генерируемых объектов (актуально для монет)
     * @field delta_velocity дельта скорости (величина отклонения скорости)
     * @field rotation скорость вращения (случайноая величина от 0 до указанной)
     * @field horizontal_velocity горизонтальная скорость объекта
     * @field delay_y для горизонтальных тел. (задержка в пикселях по вертикали)
     */
    public class PatternObject extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PatternObject()
        {
            super(ModelData.OBJECT_PATTERN);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _object_alias:CharField = new CharField();

        private var _position_x:FloatField = new FloatField();
        private var _position_y:FloatField = new FloatField();

        private var _chance:FloatField = new FloatField(100);
        private var _count:IntegerField = new IntegerField(1);

        private var _delta_velocity:FloatField = new FloatField(0);
        private var _rotation:FloatField = new FloatField(0);

        private var _horizontal_velocity:FloatField = new FloatField(0);
        private var _delay_y:FloatField = new FloatField(0);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get object_alias():CharField
        {
            return _object_alias;
        }

        public function get position_y():FloatField
        {
            return _position_y;
        }

        public function get chance():FloatField
        {
            return _chance;
        }

        public function get count():IntegerField
        {
            return _count;
        }

        public function get delta_velocity():FloatField
        {
            return _delta_velocity;
        }

        public function get rotation():FloatField
        {
            return _rotation;
        }

        public function get horizontal_velocity():FloatField
        {
            return _horizontal_velocity;
        }

        public function get position_x():FloatField
        {
            return _position_x;
        }

        public function get delay_y():FloatField
        {
            return _delay_y;
        }
    }
}
