/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 17.02.13
 * Time: 11:37
 */
package application.models.pattern
{
    import application.models.ModelData;

    import framework.data.ListData;

    public class PatternsCollection extends ListData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PatternsCollection()
        {
            _RecordClass = PatternModel;
            super(ModelData.PATTERNS_COLLECTION);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['patterns_collection'];
        }

        override public function importData(data:Object):void
        {
            super.importData(data);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getPatternsByLevel(level:int):Vector.<PatternModel>
        {
            var patterns:Vector.<PatternModel> = new Vector.<PatternModel>();
            for (var key:String in list.value) {

                if (list.value[key]['level'].value == level && !list.value[key]['bonus_pattern'].value)
                    patterns.push(list.value[key] as PatternModel);
            }
            return patterns;
        }

        public function getBonusLevelPatterns():Vector.<PatternModel>
        {
            var patterns:Vector.<PatternModel> = new Vector.<PatternModel>();
            for (var key:String in list.value) {

                if (list.value[key]['bonus_pattern'].value)
                    patterns.push(list.value[key] as PatternModel);
            }
            return patterns;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
