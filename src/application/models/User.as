package application.models
{
    import application.models.user.FriendsGiftsData;

    import framework.data.Data;
    import framework.data.fields.BooleanField;
    import framework.data.fields.CharField;
    import framework.data.fields.DateTimeField;
    import framework.data.fields.FloatField;
    import framework.data.fields.IntegerField;
    import framework.data.fields.StructField;

    public class User extends Data
    {
        //Сервер
        private var _id:IntegerField = new IntegerField();
        private var _soc_net_id:CharField = new CharField();
        private var _photo_album_id:FloatField = new FloatField();
        private var _register_date:DateTimeField = new DateTimeField();
        private var _max_score_global:IntegerField = new IntegerField();
        private var _max_score_day:IntegerField = new IntegerField();
        private var _max_score_week:IntegerField = new IntegerField();
        private var _max_score_month:IntegerField = new IntegerField();
        private var _jumps_count:IntegerField = new IntegerField();
        private var _landings_count:IntegerField = new IntegerField();
        private var _summary_time:IntegerField = new IntegerField();
        private var _coins:IntegerField = new IntegerField();
        private var _gems:IntegerField = new IntegerField();
        private var _gems_rate:IntegerField = new IntegerField();
        private var _stars:IntegerField = new IntegerField();
        private var _health:IntegerField = new IntegerField();
        private var _acceleration:IntegerField = new IntegerField();
        private var _immortality:IntegerField = new IntegerField();
        private var _coins_improve_level:IntegerField = new IntegerField();
        private var _place_in_top:IntegerField = new IntegerField();
        private var _place_in_top_time:DateTimeField = new DateTimeField();
        private var _favorites_prize:BooleanField = new BooleanField();
        private var _join_group_prize:BooleanField = new BooleanField();
        private var _max_capsules_to_explore:IntegerField = new IntegerField();
        private var _explored_capsules:IntegerField = new IntegerField();
        private var _last_explore_guest_time:DateTimeField = new DateTimeField();
        private var _last_explore_time:DateTimeField = new DateTimeField();
        private var _visits_count:IntegerField = new IntegerField();
        private var _day_prize_type:CharField = new CharField();
        private var _first_day_prize:IntegerField = new IntegerField();
        private var _last_day_prize:IntegerField = new IntegerField();
        private var _prizes_actions_table:StructField = new StructField();
        private var _was_friends_prizes_given:BooleanField = new BooleanField();
        private var _friends_gifts:FriendsGiftsData = new FriendsGiftsData();
        private var _news_like_coins:IntegerField = new IntegerField();

        //Соц. сеть
        private var _first_name:CharField = new CharField();
        private var _last_name:CharField = new CharField();
        private var _sex:CharField = new CharField();

        public function User()
        {
            super(ModelData.USER_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['user', 'soc_net_user_info'];
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get soc_net_id():CharField
        {
            return _soc_net_id;
        }

        public function get register_date():DateTimeField
        {
            return _register_date;
        }

        public function get max_score_global():IntegerField
        {
            return _max_score_global;
        }

        public function get max_score_day():IntegerField
        {
            return _max_score_day;
        }

        public function get max_score_week():IntegerField
        {
            return _max_score_week;
        }

        public function get max_score_month():IntegerField
        {
            return _max_score_month;
        }

        public function get jumps_count():IntegerField
        {
            return _jumps_count;
        }

        public function get landings_count():IntegerField
        {
            return _landings_count;
        }

        public function get summary_time():IntegerField
        {
            return _summary_time;
        }

        public function get coins():IntegerField
        {
            return _coins;
        }

        public function get gems():IntegerField
        {
            return _gems;
        }

        public function get stars():IntegerField
        {
            return _stars;
        }

        public function get health():IntegerField
        {
            return _health;
        }

        public function get acceleration():IntegerField
        {
            return _acceleration;
        }

        public function get immortality():IntegerField
        {
            return _immortality;
        }

        public function get coins_improve_level():IntegerField
        {
            return _coins_improve_level;
        }

        public function get first_name():CharField
        {
            return _first_name;
        }

        public function get last_name():CharField
        {
            return _last_name;
        }

        public function get sex():CharField
        {
            return _sex;
        }

        public function get place_in_top():IntegerField
        {
            return _place_in_top;
        }

        public function get place_in_top_time():DateTimeField
        {
            return _place_in_top_time;
        }

        public function get favorites_prize():BooleanField
        {
            return _favorites_prize;
        }

        public function get join_group_prize():BooleanField
        {
            return _join_group_prize;
        }

        public function get max_capsules_to_explore():IntegerField
        {
            return _max_capsules_to_explore;
        }

        public function get explored_capsules():IntegerField
        {
            return _explored_capsules;
        }

        public function get last_explore_guest_time():DateTimeField
        {
            return _last_explore_guest_time;
        }

        public function get last_explore_time():DateTimeField
        {
            return _last_explore_time;
        }

        public function get gems_rate():IntegerField
        {
            return _gems_rate;
        }

        public function get photo_album_id():FloatField
        {
            return _photo_album_id;
        }

        public function get visits_count():IntegerField
        {
            return _visits_count;
        }

        public function get day_prize_type():CharField
        {
            return _day_prize_type;
        }

        public function get first_day_prize():IntegerField
        {
            return _first_day_prize;
        }

        public function get last_day_prize():IntegerField
        {
            return _last_day_prize;
        }

        public function get prizes_actions_table():StructField
        {
            return _prizes_actions_table;
        }

        public function get was_friends_prizes_given():BooleanField
        {
            return _was_friends_prizes_given;
        }

        public function get friends_gifts():FriendsGiftsData
        {
            return _friends_gifts;
        }

        public function get news_like_coins():IntegerField
        {
            return _news_like_coins;
        }
    }
}
