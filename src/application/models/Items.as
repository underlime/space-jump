package application.models
{
    import application.models.items.ItemRecord;

    import framework.data.ListData;
    import framework.data.SortHelper;

    public class Items extends ListData
    {
        public static const RUBRIC_UPGRADES:String = 'upgrades';
        public static const RUBRIC_CONSUMABLES:String = 'consumables';
        public static const RUBRIC_SUITS:String = 'suits';
        public static const RUBRIC_HATS:String = 'hats';
        public static const RUBRIC_CAPSULES:String = 'capsule';

        public static const ACTION_HEALTH:String = 'improve_health';
        public static const ACTION_ACCELERATION:String = 'improve_acceleration';
        public static const ACTION_IMMORTALITY:String = 'improve_immortality';
        public static const ACTION_IMPROVE_COINS:String = 'improve_coins';
        public static const ACTION_PARACHUTE:String = 'parachute';
        public static const ACTION_RESURRECTION:String = 'resurrection';
        public static const ACTION_RESURRECTION_PLUS:String = 'resurrection_plus';
        public static const ACTION_ROCKET:String = 'rocket';
        public static const ACTION_MEGA_ROCKET:String = 'mega_rocket';
        public static const ACTION_SUITE:String = 'suite';
        public static const ACTION_HAT:String = 'hat';
        public static const ACTION_CROWN:String = 'crown';
        public static const ACTION_CAPSULE:String = 'capsule';

        public static const ACTIONS_LIST:Array = [
            ACTION_HEALTH, ACTION_ACCELERATION, ACTION_IMMORTALITY,
            ACTION_PARACHUTE, ACTION_IMPROVE_COINS, ACTION_RESURRECTION,
            ACTION_RESURRECTION_PLUS, ACTION_ROCKET, ACTION_MEGA_ROCKET,
            ACTION_SUITE, ACTION_HAT, ACTION_CROWN, ACTION_CAPSULE
        ];

        public function Items()
        {
            _RecordClass = ItemRecord;
            super(ModelData.ITEMS_MODEL);
        }

        /**
         * Извлечь предметы определенной рубрики
         * @param rubricName
         * @return
         */
        public function getItemsByRubric(rubricName:String):Vector.<ItemRecord>
        {
            var filteredList:Vector.<ItemRecord> = new Vector.<ItemRecord>();
            for (var key:String in list.value)
                if (list.value[key]['rubric'].value == rubricName)
                    filteredList.push(list.value[key]);

            return filteredList.sort(SortHelper.getComparison(SortHelper.ASC));
        }

        /**
         * Вернуть предмет по экшену
         * @param action
         * @return
         */
        public function getItemByAction(action:String):ItemRecord
        {
            var item:ItemRecord;
            for (var key:String in list.value)
            {
                item = list.value[key];
                if (item.action.value == action)
                    return item;
            }

            return null;
        }

        override public function getModelDataKeys():Array
        {
            return ['items'];
        }


        override public function importData(data:Object):void
        {
            super.importData(data);
        }
    }
}
