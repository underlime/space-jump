package application.models
{
    import application.models.achievements.AchievementRecord;
    import application.models.user_achievements.UserAchievementRecord;

    import framework.data.ListData;
    import framework.data.ModelsRegistry;

    public class UserAchievements extends ListData
    {
        public function UserAchievements()
        {
            _RecordClass = UserAchievementRecord;
            super(ModelData.USER_ACHIEVEMENTS_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['user_achievements'];
        }

        public function getRecordByAchievementId(id:int):UserAchievementRecord
        {
            for each (var achvRecord:UserAchievementRecord in list.value) {
                if (achvRecord.achievement.value == id)
                    return achvRecord;
            }
            return null;
        }

        public function checkAchievementByCode(code:String):Boolean
        {
            var achievementsModel:Achievements = ModelsRegistry.getModel(ModelData.ACHIEVEMENTS_MODEL) as Achievements;
            var achvRecord:AchievementRecord = achievementsModel.getAchievementByCode(code);
            return (achvRecord && getRecordByAchievementId(achvRecord.id.value));
        }
    }
}
