package application.models
{
    import application.models.user_news.UserNewsRecord;

    import framework.data.ListData;
    import framework.data.SortHelper;

    public class UserNews extends ListData
    {
        public function UserNews()
        {
            _RecordClass = UserNewsRecord;
            super(ModelData.USER_NEWS_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['user_news'];
        }

        public function getSortedList():Vector.<UserNewsRecord>
        {
            var result:Vector.<UserNewsRecord> = new Vector.<UserNewsRecord>();
            for (var key:String in list.value)
                result.push(list.value[key]);

            return result.sort(SortHelper.getTimeComparison(SortHelper.DESC));
        }
    }
}
