package application.models
{
    import framework.data.ListData;

    public class UsersList extends ListData
    {
        public function UsersList()
        {
            _RecordClass = User;
            super(ModelData.USERS_LIST_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['users_list'];
        }
    }
}
