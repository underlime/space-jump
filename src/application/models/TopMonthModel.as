package application.models
{
    import application.models.top.GeneralTopModel;

    public class TopMonthModel extends GeneralTopModel
    {
        public function TopMonthModel()
        {
            super(ModelData.TOP_MONTH_MODEL);
        }

        override public function getModelDataKeys():Array
        {
            return ['top_month'];
        }
    }
}
