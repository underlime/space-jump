package application.models.items
{
    import framework.data.fields.CharField;
    import framework.data.fields.IntegerField;

    public class ItemRecord
    {
        private var _id:IntegerField = new IntegerField();
        private var _sort:IntegerField = new IntegerField();
        private var _rubric:CharField = new CharField();
        private var _name_ru:CharField = new CharField();
        private var _name_en:CharField = new CharField();
        private var _description_ru:CharField = new CharField();
        private var _description_en:CharField = new CharField();
        private var _image:CharField = new CharField();
        private var _action:CharField = new CharField();
        private var _price:IntegerField = new IntegerField();
        private var _gems_price:IntegerField = new IntegerField();
        private var _upgrade_price:CharField = new CharField();
        private var _gems_upgrade_price:CharField = new CharField();
        private var _max_to_buy:IntegerField = new IntegerField();
        private var _symbol:CharField = new CharField();
        private var _max_hp:IntegerField = new IntegerField();

        public function ItemRecord()
        {
        }

        public function get id():IntegerField
        {
            return _id;
        }

        public function get sort():IntegerField
        {
            return _sort;
        }

        public function get rubric():CharField
        {
            return _rubric;
        }

        public function get name_ru():CharField
        {
            return _name_ru;
        }

        public function get name_en():CharField
        {
            return _name_en;
        }

        public function get description_ru():CharField
        {
            return _description_ru;
        }

        public function get description_en():CharField
        {
            return _description_en;
        }

        public function get image():CharField
        {
            return _image;
        }

        public function get action():CharField
        {
            return _action;
        }

        public function get price():IntegerField
        {
            return _price;
        }

        public function get max_to_buy():IntegerField
        {
            return _max_to_buy;
        }

        public function get upgrade_price():CharField
        {
            return _upgrade_price;
        }

        public function get symbol():CharField
        {
            return _symbol;
        }

        public function get max_hp():IntegerField
        {
            return _max_hp;
        }

        public function get gems_price():IntegerField
        {
            return _gems_price;
        }

        public function get gems_upgrade_price():CharField
        {
            return _gems_upgrade_price;
        }
    }
}
