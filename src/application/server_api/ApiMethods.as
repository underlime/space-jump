/**
 * Сигнатуры методов находятся в пакете signatures.
 * Если для данного метода нет структуры с сигнатурой,
 * значит, он не принимает параметров
 */
package application.server_api
{
    public class ApiMethods
    {
        public static const ITEMS_GET_MARKET_LIST:String = 'items/get.market.list';
        public static const ACHIEVEMENTS_GET_LIST:String = 'achievements/get.list';

        public static const ACHIEVEMENTS_CHECK_PING_EARTH:String = 'achievements/check/ping.earth';
        public static const ACHIEVEMENTS_CHECK_BE_TRAINED:String = 'achievements/check/be.trained';
        public static const ACHIEVEMENTS_CHECK_PICTURE:String = 'achievements/check/picture';
        public static const ACHIEVEMENTS_CHECK_BANKING:String = 'achievements/check/banking';

        public static const ITEMS_BUY:String = 'items/buy';
        public static const ITEMS_EQUIP:String = 'items/equip';

        public static const BANK_GET_PRICES:String = 'bank/get.prices';

        public static const USER_GET_INFO:String = 'user/get.info';
        public static const USER_GET_LIST_INFO:String = 'user/get.list.info';
        public static const USER_GET_COINS:String = 'user/get.coins';
        public static const USER_REGISTER:String = 'user/register';
        public static const USER_EXPLORE_CAPSULE:String = 'user/explore.capsule';
        public static const USER_GIVE_FRIENDS_BONUSES:String = 'user/give.friends.bonuses';
        public static const USER_SET_NEWS_READ:String = 'user/user.set.news.read';

        public static const USER_SET_PHOTO_ALBUM:String = 'user/set.photo.album';
        public static const TOP_GET_DAY:String = 'top/get.day';
        public static const TOP_GET_WEEK:String = 'top/get.week';
        public static const TOP_GET_MONTH:String = 'top/get.month';

        public static const TOP_GET_GLOBAL:String = 'top/get.global';
        public static const TROPHIES_GET_COLLECTIONS:String = 'trophies/get.collections';
        public static const TROPHIES_GET_USERS_TROPHIES:String = 'trophies/get.users.trophies';
        public static const SESSION_INIT:String = "game_sessions/init";

        public static const SESSION_FINISH:String = "game_sessions/finish";
        public static const SUITE_REPAIR:String = 'suite/repair';

        public function ApiMethods()
        {
        }
    }
}
