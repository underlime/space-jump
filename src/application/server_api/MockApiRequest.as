package application.server_api
{
    import flash.utils.ByteArray;

    import framework.underquery.ApiRequest;

    public class MockApiRequest extends ApiRequest
    {
        [Embed(source="../../../lib/config/mock-server-response.json", mimeType="application/octet-stream")]
        private var MockServerResponse:Class;

        public function MockApiRequest(methodUrl:String)
        {
            super(methodUrl);
        }

        override public function send():void
        {
            _onLoad(null);
        }

        override protected function _loadData():Object
        {
            var responseBytes:ByteArray = new MockServerResponse();
            var jsonResponse:String = responseBytes.readUTFBytes(responseBytes.length);
            return JSON.parse(jsonResponse);
        }
    }
}
