package application.server_api
{
    import application.config.ApplicationConfiguration;
    import application.config.Config;
    import application.config.DataApi;
    import application.controllers.ApplicationController;

    import framework.controller.BaseController;
    import framework.socnet.MyMailRu;
    import framework.socnet.SocNet;
    import framework.tools.SaltTool;
    import framework.underquery.ApiRequest;

    public class ServerApi extends framework.underquery.ServerApi
    {
        private var _dataApiConfig:DataApi;

        public function ServerApi(baseController:BaseController, mock:Boolean)
        {
            super(baseController);
            var applicationController:ApplicationController = _baseController as ApplicationController;
            _dataApiConfig = applicationController.config.getChildByName(Config.DATA_API_CONFIG) as DataApi;

            if (SocNet.instance().socNetName == SocNet.MY_MAIL_RU)
                _setMailRuUploadParams();

            if (mock)
                _ApiRequestClass = MockApiRequest;
        }

        private function _setMailRuUploadParams():void
        {
            var baseImageUrl:String = _dataApiConfig.protocol.value + '://' + _dataApiConfig.domain.value;
            if (_dataApiConfig.port.value != 80)
                baseImageUrl += ':' + String(_dataApiConfig.port.value);

            var shortUrl:String = _dataApiConfig.url.value + 'image/upload/';
            framework.underquery.ServerApi.uploadImageUrl = baseImageUrl + shortUrl;

            var tokenSalt:String = SaltTool.decode(ApplicationConfiguration.TOKEN_SALT, ApplicationConfiguration.Z);
            var tokens:Object = {};
            tokens[SocNet.MY_MAIL_RU] = MyMailRu(SocNet.instance()).createToken(shortUrl, tokenSalt);
            framework.underquery.ServerApi.uploadImageToken = tokens;
        }

        override protected function _createRequestUri(method:String, queryString:String = ''):String
        {
            var requestUri:String = _dataApiConfig.url.value;
            if (requestUri.charAt(0) != '/')
                requestUri = '/' + requestUri;

            if (requestUri.charAt(requestUri.length - 1) != '/')
                requestUri += '/';

            requestUri += method;
            if (requestUri.charAt(requestUri.length - 1) != '/')
                requestUri += '/';

            if (queryString)
                requestUri += '?' + queryString;

            requestUri = requestUri.replace('//', '/');
            return requestUri;
        }

        override protected function _createFullUrl(requestUri:String):String
        {
            var fullUrl:String = _dataApiConfig.protocol.value + '://' + _dataApiConfig.domain.value;
            if (_dataApiConfig.port.value != 80)
                fullUrl += ':' + String(_dataApiConfig.port.value);
            fullUrl += requestUri;
            return fullUrl;
        }

        override protected function _getTokenSalt():String
        {
            return SaltTool.decode(ApplicationConfiguration.TOKEN_SALT, ApplicationConfiguration.Z);
        }
    }

}
