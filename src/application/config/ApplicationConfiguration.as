/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.10.12
 */

package application.config
{

    import flash.system.Capabilities;

    import framework.socnet.SocNet;

    public class ApplicationConfiguration
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static var LANGUAGE:String = ApplicationConfiguration.EN;
        public static const APP_WIDTH:int = 800;
        public static const APP_HEIGHT:int = 700;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        /**
         * Debug value: true
         * Production value: false
         */
        public static const DEBUG:Boolean = false;

        /**
         * Debug value: true
         * Production value: false
         */
        public static const SHOW_PATTERNS:Boolean = false;

        public static const Z:int = 127;

        public static const APP_NAME:String = "SpaceDiver";
        public static const PLAYER_STAND_ALONE:String = "StandAlone";
        public static const LOCAL_PATH:String = "../lib/config/config.xml";
        public static const EN:String = "english";
        public static const RU:String = "russian";

        public static const RESULT_SALT:Array = [14, 10, 7, 22, 12, 16, 24, 81];
        public static const TOKEN_SALT:Array = [28, 6, 23, 6, 15, 30, 5, 32];

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private static var _flashVars:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public static function get CONFIG_PATH():String
        {
            if (Capabilities.playerType == ApplicationConfiguration.PLAYER_STAND_ALONE) {
                return ApplicationConfiguration.LOCAL_PATH;
            }
            else {
                return _flashVars['config_path'];
            }
        }

        public static function get STAND_ALONE():Boolean
        {
            return Capabilities.playerType == ApplicationConfiguration.PLAYER_STAND_ALONE;
        }

        public static function getApplicationLink(socNetName:String):String
        {
            switch (socNetName) {
                case SocNet.VK_COM:
                case SocNet.MOCK:
                    return 'https://vk.com/kosmodiver';
                    break;
                case SocNet.FACEBOOK_COM:
                    return 'https://apps.facebook.com/148797118628286/';
                case SocNet.ODNOKLASSNIKI_RU:
                    return 'http://www.odnoklassniki.ru/game/cosmodiver';
                    break;
                default:
                    return null;
            }
        }

        public static function set flashVars(vars:Object):void
        {
            _flashVars = vars;
        }
    }
}
