/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 28.06.13
 * Time: 20:07
 */
package application.config
{
    import flash.ui.Keyboard;

    import org.casalib.ui.KeyCombo;

    /**
     * При изменении горячих клавиш необходимо подправить надписи в классе
     * application.views.game.layers.panel.buttons.ButtonLayer
     *
     * line 117
     * line 146
     * line 174
     * line 132
     */
    public class HotKeyConfig
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const RESURRECTION_HOT_KEY:KeyCombo = new KeyCombo([Keyboard.SHIFT, Keyboard.R]);
        public static const PARACHUTE_HOT_KEY:KeyCombo = new KeyCombo([Keyboard.SHIFT, Keyboard.P]);
        public static const SCREEN_SHOT_HOT_KEY:KeyCombo = new KeyCombo([Keyboard.SHIFT, Keyboard.S]);

        public static const TIMER_DEBUG_HOT_KEY:KeyCombo = new KeyCombo([Keyboard.T, Keyboard.I, Keyboard.M, Keyboard.E]);
        public static const PAUSE_HOT_KEY:KeyCombo = new KeyCombo([Keyboard.SHIFT, Keyboard.Z]);

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
