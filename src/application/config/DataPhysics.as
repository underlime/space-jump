package application.config
{
    import framework.data.Data;
    import framework.data.fields.BooleanField;
    import framework.data.fields.FloatField;
    import framework.data.fields.IntegerField;

    /**
     * Настройки игры
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     *
     * @var angular_drag Угловое торможение для всех объектов (> 0) (не используется)
     * @var linear_drag Линецное торможение для всех объектов (> 0) (не используется)
     * @var free_acceleration Ускорение свободного падения (не используется, ускорение рассчитывается в GameModel -> increaseVelocity)
     * @var start_velocity Стартовая скорость падения объектов.
     *
     * @var hero_max_velocity Максимальная скорость героя (px/sec)
     * @var hero_acceleration Ускорение героя (px/Нажатие клавиши)
     * @var hero_max_angular_velocity Максимальная угловая скорость героя
     * @var hero_angular_acceleration Угловое ускорение героя
     * @var hero_damping Линейное торможение героя (срабатывает при отпускании всех клавиш, > 0. Чем меньше значение, тем больше ускорение)
     * @var hero_angular_damping Угловое торможение героя (срабатывает при отпускании всех клавиш, > 0. Чем меньше значение, тем больше ускорение)
     * @var hero_inertia - если флаг установлен, то при резкой смене направления героя, он сначала гасит предыдущую скорость, затем набирает текущую
     *
     * @var wood_box_damage - урон от деревянного ящика
     * @var wood_box_speed_reduce_percent - процент, на который гасится скорость падения при столкновении с деревянным ящиком (0-100)
     * @var wood_box_chance (не используется)
     *
     * @var metal_box_damage - урон от металлического ящика
     * @var metal_box_speed_reduce_percent - процент, на который гасится скорость падения при столкновении с металлическим ящиком (0-100)
     * @var metal_box_chance (не используется)
     *
     * @var tnt_box_damage - урон от взрывающегося ящика
     * @var tnt_box_speed_reduce_percent - процент, на который гасится скорость падения при столкновении с взрывающимся ящиком (0-100)
     * @var tnt_box_chance (не используется)
     *
     * @var birds_damage урон от столкновения с птичками
     * @var jet_damage урон от столкновения с реактивным самолетом
     * @var ufo_damage урон от столкновения с НЛО
     * @var plane_damage урон от столкновения с пассажирским самолетом
     * @var satellite_damage урон от столкновения со спутником
     *
     * @var coins_max_count (не используется)
     * @var coins_min_count (не используется)
     *
     * @var copper_coin_money - количество денег за 1 бронзовую монету
     * @var silver_coin_money - количество денег за 1 серебрянную монету
     * @var gold_coin_money - количество денег за 1 золотую монету
     *
     * @var heart_regeneration - количество здоровья за 1 сердце
     * @var shield_armor - количество брони за 1 щит
     *
     * @var birds_velocity - горизонтальная скорость птиц (px/sec), может переопределяться параметрами шаблона
     * @var ufo_velocity - горизонтальная скорость НЛО (px/sec), может переопределяться параметрами шаблона
     * @var jet_velocity - горизонтальная скорость Реактивного самолета (px/sec), может переопределяться параметрами шаблона
     * @var plane_velocity - горизонтальная скорость Пассажирского самолета (px/sec), может переопределяться параметрами шаблона
     * @var satellite_velocity - горизонтальная скорость спутника (px/sec), может переопределяться параметрами шаблона
     *
     * @var user_default_health начальное здоровье героя
     * @var user_default_armor начальная броня героя
     *
     * @var max_armor Максимальное значение брони
     * @var resurrection_seconds Время действия бафа Воскрешение (сек)
     *
     * @var bonus_level_velocity скорость падения на бонусном уровне (px/sec)
     * @var bonus_level_acceleration ускорение на бонусном уровне (не используется)
     * @var max_bonus количество бутылок, необходимое для бонусного уровня
     *
     * @var booster_seconds время действия Ускорителя (сек)
     * @var rocket_seconds время действия стартовой ракеты (сек)
     * @var mega_rocket_seconds время действия стартовой МЕГАракеты (сек)
     * @var defence_field_seconds Время действия защитного поля (сек)
     *
     * @var booster_level_chance Процент, на который возрастает шанс выпадения Ускорителя с уровнем прокачки
     * @var shield_level_chance Процент, на который возрастает шанс выпадения Защитного поля с уровнем прокачки
     */
    public class DataPhysics extends Data
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static var _instance:DataPhysics = null;

        public static const TIME_STEP:Number = 1 / 30;
        public static const WORLD_WIDTH:int = 800;
        public static const WORLD_HEIGHT:int = 700;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DataPhysics()
        {
            super(Config.DATA_PHYSICS_CONFIG);
            if (DataPhysics._instance)
                throw new Error("use instance property");
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getModelDataKeys():Array
        {
            return ['data_physics'];
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _angular_drag:FloatField = new FloatField(0);
        private var _linear_drag:FloatField = new FloatField(0);
        private var _free_acceleration:FloatField = new FloatField(6);
        private var _start_velocity:FloatField = new FloatField(100);

        private var _hero_max_velocity:FloatField = new FloatField(200);
        private var _hero_acceleration:FloatField = new FloatField(50);
        private var _hero_max_angular_velocity:FloatField = new FloatField(8);
        private var _hero_angular_acceleration:FloatField = new FloatField(.5);
        private var _hero_damping:FloatField = new FloatField(0.5);
        private var _hero_angular_damping:FloatField = new FloatField(.5);
        private var _hero_inertia:BooleanField = new BooleanField(true);

        private var _wood_box_damage:FloatField = new FloatField(1);
        private var _wood_box_speed_reduce_percent:FloatField = new FloatField(10);
        private var _wood_box_chance:FloatField = new FloatField(100);

        private var _metal_box_damage:FloatField = new FloatField(2);
        private var _metal_box_speed_reduce_percent:FloatField = new FloatField(10);
        private var _metal_box_chance:FloatField = new FloatField(30);

        private var _tnt_box_damage:FloatField = new FloatField(3);
        private var _tnt_box_speed_reduce_percent:FloatField = new FloatField(10);
        private var _tnt_box_chance:FloatField = new FloatField(10);

        private var _birds_damage:FloatField = new FloatField(1);
        private var _jet_damage:FloatField = new FloatField(1);
        private var _ufo_damage:FloatField = new FloatField(1);
        private var _plane_damage:FloatField = new FloatField(1);
        private var _satellite_damage:FloatField = new FloatField(1);

        private var _coins_max_count:IntegerField = new IntegerField(8);
        private var _coins_min_count:IntegerField = new IntegerField(1);
        private var _copper_coin_money:IntegerField = new IntegerField(1);
        private var _silver_coin_money:IntegerField = new IntegerField(2);
        private var _gold_coin_money:IntegerField = new IntegerField(3);

        private var _heart_regeneration:FloatField = new FloatField(1);
        private var _shield_armor:IntegerField = new IntegerField(1);

        private var _birds_velocity:FloatField = new FloatField(100);
        private var _ufo_velocity:FloatField = new FloatField(100);
        private var _jet_velocity:FloatField = new FloatField(100);
        private var _plane_velocity:FloatField = new FloatField(100);
        private var _satellite_velocity:FloatField = new FloatField(100);

        private var _user_default_health:IntegerField = new IntegerField(10);
        private var _user_default_armor:IntegerField = new IntegerField(0);

        private var _max_armor:IntegerField = new IntegerField(10);
        private var _resurrection_seconds:IntegerField = new IntegerField(10);

        private var _bonus_level_velocity:IntegerField = new IntegerField(500);
        private var _bonus_level_acceleration:IntegerField = new IntegerField(5);

        private var _max_bonus:IntegerField = new IntegerField(21);

        private var _booster_seconds:IntegerField = new IntegerField(5);
        private var _rocket_seconds:IntegerField = new IntegerField(5);
        private var _mega_rocket_seconds:IntegerField = new IntegerField(10);
        private var _defence_field_seconds:IntegerField = new IntegerField(6);

        private var _booster_level_chance:IntegerField = new IntegerField(5);
        private var _shield_level_chance:IntegerField = new IntegerField(5);

        private var _chance_multy:IntegerField = new IntegerField(2);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get shield_armor():IntegerField
        {
            return _shield_armor;
        }

        public function get heart_regeneration():FloatField
        {
            return _heart_regeneration;
        }

        public function get gold_coin_money():IntegerField
        {
            return _gold_coin_money;
        }

        public function get silver_coin_money():IntegerField
        {
            return _silver_coin_money;
        }

        public function get copper_coin_money():IntegerField
        {
            return _copper_coin_money;
        }

        public function get coins_min_count():IntegerField
        {
            return _coins_min_count;
        }

        public function get coins_max_count():IntegerField
        {
            return _coins_max_count;
        }

        public function get plane_damage():FloatField
        {
            return _plane_damage;
        }

        public function get ufo_damage():FloatField
        {
            return _ufo_damage;
        }

        public function get jet_damage():FloatField
        {
            return _jet_damage;
        }

        public function get angular_drag():FloatField
        {
            return _angular_drag;
        }

        public function get linear_drag():FloatField
        {
            return _linear_drag;
        }

        public function get free_acceleration():FloatField
        {
            return _free_acceleration;
        }

        public function get start_velocity():FloatField
        {
            return _start_velocity;
        }

        public function get hero_max_velocity():FloatField
        {
            return _hero_max_velocity;
        }

        public function get hero_acceleration():FloatField
        {
            return _hero_acceleration;
        }

        public function get hero_max_angular_velocity():FloatField
        {
            return _hero_max_angular_velocity;
        }

        public function get hero_angular_acceleration():FloatField
        {
            return _hero_angular_acceleration;
        }


        public function get hero_damping():FloatField
        {
            return _hero_damping;
        }

        public function get hero_angular_damping():FloatField
        {
            return _hero_angular_damping;
        }

        public function get wood_box_damage():FloatField
        {
            return _wood_box_damage;
        }

        public function get wood_box_speed_reduce_percent():FloatField
        {
            return _wood_box_speed_reduce_percent;
        }

        public function get wood_box_chance():FloatField
        {
            return _wood_box_chance;
        }

        public function get metal_box_damage():FloatField
        {
            return _metal_box_damage;
        }

        public function get metal_box_speed_reduce_percent():FloatField
        {
            return _metal_box_speed_reduce_percent;
        }

        public function get metal_box_chance():FloatField
        {
            return _metal_box_chance;
        }

        public function get tnt_box_damage():FloatField
        {
            return _tnt_box_damage;
        }

        public function get tnt_box_speed_reduce_percent():FloatField
        {
            return _tnt_box_speed_reduce_percent;
        }

        public function get tnt_box_chance():FloatField
        {
            return _tnt_box_chance;
        }

        public function get birds_damage():FloatField
        {
            return _birds_damage;
        }

        static public function get instance():DataPhysics
        {
            if (DataPhysics._instance == null)
                DataPhysics._instance = new DataPhysics();

            return DataPhysics._instance;
        }

        public function get satelite_damage():FloatField
        {
            return _satellite_damage;
        }

        public function get birds_velocity():FloatField
        {
            return _birds_velocity;
        }

        public function get ufo_velocity():FloatField
        {
            return _ufo_velocity;
        }

        public function get jet_velocity():FloatField
        {
            return _jet_velocity;
        }

        public function get plane_velocity():FloatField
        {
            return _plane_velocity;
        }

        public function get satellite_velocity():FloatField
        {
            return _satellite_velocity;
        }

        public function get user_default_health():IntegerField
        {
            return _user_default_health;
        }

        public function get user_default_armor():IntegerField
        {
            return _user_default_armor;
        }

        public function get max_armor():IntegerField
        {
            return _max_armor;
        }

        public function get resurrection_seconds():IntegerField
        {
            return _resurrection_seconds;
        }

        public function get bonus_level_velocity():IntegerField
        {
            return _bonus_level_velocity;
        }

        public function get bonus_level_acceleration():IntegerField
        {
            return _bonus_level_acceleration;
        }

        public function get max_bonus():IntegerField
        {
            return _max_bonus;
        }

        public function get booster_seconds():IntegerField
        {
            return _booster_seconds;
        }

        public function get rocket_seconds():IntegerField
        {
            return _rocket_seconds;
        }

        public function get mega_rocket_seconds():IntegerField
        {
            return _mega_rocket_seconds;
        }

        public function get defence_field_seconds():IntegerField
        {
            return _defence_field_seconds;
        }

        public function get booster_level_chance():IntegerField
        {
            return _booster_level_chance;
        }

        public function get shield_level_chance():IntegerField
        {
            return _shield_level_chance;
        }

        public function get hero_inertia():BooleanField
        {
            return _hero_inertia;
        }

        public function get chance_multy():IntegerField
        {
            return _chance_multy;
        }
    }

}