package application.config
{
    import framework.data.Data;
    import framework.data.fields.BooleanField;
    import framework.data.fields.CharField;
    import framework.data.fields.IntegerField;

    public class DataApi extends Data
    {
        private var _protocol:CharField = new CharField();
        private var _domain:CharField = new CharField();
        private var _port:IntegerField = new IntegerField();
        private var _url:CharField = new CharField();
        private var _image_upload_url:CharField = new CharField();
        private var _mock:BooleanField = new BooleanField();

        public function DataApi()
        {
            super(Config.DATA_API_CONFIG);
            _protocol.value = 'http';
            _domain.value = '127.0.0.1';
            _port.value = 80;
            _url.value = '/';
            _mock.value = false;
        }

        override public function getModelDataKeys():Array
        {
            return ['data_api'];
        }

        public function get protocol():CharField
        {
            return _protocol;
        }

        public function get domain():CharField
        {
            return _domain;
        }

        public function get port():IntegerField
        {
            return _port;
        }

        public function get url():CharField
        {
            return _url;
        }

        public function get mock():BooleanField
        {
            return _mock;
        }

        public function get image_upload_url():CharField
        {
            return _image_upload_url;
        }
    }
}
