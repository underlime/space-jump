package application.config
{
    import framework.data.DataContainer;

    public class Config extends DataContainer
    {
        public static const MAIN_CONFIG:String = 'Config';
        public static const DATA_API_CONFIG:String = 'DataApi';
        public static const DATA_PHYSICS_CONFIG:String = "DataPhysics";


        public function Config()
        {
            super(MAIN_CONFIG);
            addChild(new DataApi());
            addChild(DataPhysics.instance);
        }

        override public function getModelDataKeys():Array
        {
            return [];
        }
    }
}
