/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.03.13
 * Time: 12:10
 */
package application.sound.helpers
{
    import flash.events.Event;
    import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.media.SoundTransform;

    public class SoundControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SoundControl(sound:Sound)
        {
            _sound = sound;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var soundTransform:SoundTransform = new SoundTransform(.4);

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function playLoop():void
        {
            _playSound();
            _channel.addEventListener(Event.SOUND_COMPLETE, _repeatSound);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _sound:Sound;
        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _playSound():void
        {
            _channel = _sound.play(0, 0, soundTransform);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _repeatSound(event:Event):void
        {
            _playSound();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
