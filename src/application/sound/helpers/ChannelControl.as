/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.03.13
 * Time: 14:32
 */
package application.sound.helpers
{
    import com.greensock.TweenLite;

    import flash.media.SoundChannel;
    import flash.media.SoundTransform;

    public class ChannelControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChannelControl(channel:SoundChannel)
        {
            super();
            _channel = channel;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function reduceAndStop(seconds:int):void
        {
            _transform = new SoundTransform(_channel.soundTransform.volume);
            _channel.soundTransform = _transform;

            var tweenParams:Object = {
                "volume": 0,
                "onUpdate": _updateChannel,
                "onComplete": _stopChannel
            };
            TweenLite.to(_transform, seconds, tweenParams);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _channel:SoundChannel;
        private var _transform:SoundTransform;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateChannel():void
        {
            _channel.soundTransform = _transform;
        }

        private function _stopChannel():void
        {
            _channel.stop();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
