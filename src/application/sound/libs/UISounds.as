/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.02.13
 * Time: 11:53
 */
package application.sound.libs
{
    public class UISounds
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BUTTON_CONFIRM:String = "BTN_CONFIRM_SOUND";
        public static const BUTTON_CLOSE:String = "CLOSE_BTN_PRESS";
        public static const SHARE_BUTTON_OVER:String = "SHARE_BUTTON_HOVER_SOUND";
        public static const ACHIEVEMENT_UNLOCK:String = "ACHIEVEMENT_UNLOCKED_SOUND";
        public static const REPAIR:String = "REPAIR_SOUND_2";
        public static const BUY:String = "SHOP_BUY_SOUND";
        public static const SHOP_EQUIP:String = "SHOP_EQUIP_SOUND";

        public static const SUCCESS:String = "SUCCES_SOUND";
        public static const FAIL:String = "DEATH_SOUND";

        public static const RESURRECTION_INIT:String = "RESSURECTION_INIT_SOUND";
        public static const RESURRECTION:String = "RESURRECTION_SOUND";
        public static const RESURRECTION_PLUS:String = "RESURRECTION_PLUS_SOUND";


        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
