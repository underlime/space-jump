/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 10.03.13
 * Time: 13:27
 */
package application.sound.libs
{
    public class CollisionSounds
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BIRD_DEATH:Array = ["BIRD_DEATH_SOUND_1", "BIRD_DEATH_SOUND_2"];
        public static const IMPACT_SOUND:Array = ["IMPACT_HEAVY_SOUND", "IMPACT_NORMAL_SOUND"];
        public static const METAL_BOX_IMPACT:Array = ["METAL_BOX_IMPACT_1", "METAL_BOX_IMPACT_2"];
        public static const TNT_BOX_IMPACT:Array = ["TNT_BOX_EXPLODE_1", "TNT_BOX_EXPLODE_2"];
        public static const WOOD_BOX_IMPACT:Array = ["WOOD_BOX_IMPACT"];
        public static const FIREWORK_IMPACT:Array = ["PETTARD_EXPLODE_1"];
        public static const HOG_IMPACT:Array = ["HOG_IMPACT"];
        public static const HOLE_TOUCH:Array = ["BLACK_HOLE_TOUCH"];

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
