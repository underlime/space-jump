/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.03.13
 * Time: 16:52
 */
package application.sound.libs
{
    public class ThemeSounds
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const MENU_THEME:Array = ["MenuTheme_1"];
        public static const GAME_THEME:Array = ["GameTheme_1", "GameTheme_2", "GameTheme_3"];
        public static const BONUS_THEME:Array = ["BonusTheme"];

        public static const MENU_URLS_SUITE:Array = ["../mp3/Broke_For_Free_-_06_-_Only_Instrumental.mp3"];
        public static const GAME_URLS_SUITE:Array = ["../mp3/Broke_For_Free_-_03_-_Day_Bird.mp3", "../mp3/Chris_Zabriskie_-_06_-_Divider.mp3", "../mp3/Jahzzar_-_05_-_Traffic.mp3"];
        public static const BONUS_URLS_SUITE:Array = ["../mp3/Kevin_MacLeod_-_06_-_Hustle.mp3"];

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
