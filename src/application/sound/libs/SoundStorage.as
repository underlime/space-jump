/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.02.13
 * Time: 12:31
 */
package application.sound.libs
{
    import flash.media.Sound;
    import flash.system.ApplicationDomain;

    public class SoundStorage extends Object
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SoundStorage(appDomain:ApplicationDomain)
        {
            _appDomain = appDomain;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        public function getSound(definition:String):Sound
        {
            if (!_soundHash[definition])
                _addSoundData(definition);

            return _soundHash[definition] as Sound;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _soundHash:Array = [];
        private var _appDomain:ApplicationDomain;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addSoundData(definition:String):void
        {
            if (!_appDomain.hasDefinition(definition))
                throw new Error("Definition " + definition + " not found");

            var SoundClass:Class = _appDomain.getDefinition(definition) as Class;
            _soundHash[definition] = new SoundClass();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
