/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.03.13
 * Time: 12:05
 */
package application.sound.libs
{
    public class LoopSounds
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const FORCE_FIELD:String = "FORCE_FIELD_LOOP";
        public static const AIRBUS_FLIGHT:String = "AIRBUS_FLIGHT_LOOP";
        public static const UFO_FLIGHT:String = "UFO_FLIGHT_LOOP";
        public static const SATELLITE_FLIGHT:String = "SPUTNIK_FLIGHT_LOOP";
        public static const JET_FLIGHT:String = "JET_FLIGHT_SOUND_LOOP";
        public static const HOG_FLY:String = "HOG_FLIGHT_LOOP";
        public static const ROCKER_FLY:String = "MEGASTART_FLIGHT_LOOP";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
