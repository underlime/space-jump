/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.03.13
 * Time: 11:09
 */
package application.sound.libs
{
    public class PickupSounds
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const COIN_PICK_UP:String = "COINS_PICK_UP";
        public static const HEART_PICK_UP:String = "HEART_PICK_UP";
        public static const COLA_PICK_UP:String = "JET_COLA_PICK_UP";
        public static const POWERUP_PICK_UP:String = "POWERUP_PICK_UP";
        public static const SHIELD_PICK_UP:String = "SMALL_SHIELD_PICK_UP";
        public static const PARACHUTE_OPEN:String = "PARASHUT_OPEN_SOUND_2";
        public static const TROPHY_PICKUP:String = "TROPHY_PICKUP_DONE";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
