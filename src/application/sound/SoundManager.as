/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.02.13
 * Time: 10:50
 */
package application.sound
{
    import application.config.ApplicationConfiguration;
    import application.sound.helpers.ChannelControl;
    import application.sound.libs.SoundStorage;
    import application.sound.libs.ThemeSounds;

    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.media.Sound;
    import flash.media.SoundChannel;
    import flash.media.SoundTransform;
    import flash.net.SharedObject;
    import flash.net.URLRequest;
    import flash.system.ApplicationDomain;

    import framework.helpers.MathHelper;

    import org.casalib.events.RemovableEventDispatcher;

    public class SoundManager extends RemovableEventDispatcher
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static var _instance:SoundManager = null;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SoundManager()
        {
            super();
            if (_instance != null)
                throw new Error("Use instance property");
            _setupParams();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function playUISound(soundData:String):void
        {
            if (_soundMute)
                return;

            var sound:Sound = _soundStorage.getSound(soundData);
            var channel:SoundChannel = sound.play(0, 0, _uiSoundTransform);
            channel.addEventListener(Event.SOUND_COMPLETE, _uiSoundComplete);
        }

        public function playCollisionSound(soundVariants:Array):void
        {
            if (_soundMute)
                return;

            var randomIndex:int = MathHelper.random(0, soundVariants.length - 1);
            var soundData:String = soundVariants[randomIndex];
            var soundChannel:SoundChannel;

            if (_collisionChannels.length < _maxCollisionSounds) {
                var sound:Sound = _soundStorage.getSound(soundData);
                soundChannel = sound.play(0, 0, _collisionSoundTransform);
                soundChannel.addEventListener(Event.SOUND_COMPLETE, _collisionComplete);
                _collisionChannels.push(soundChannel);
            }
            else {
                soundChannel = _collisionChannels[0];
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, _collisionComplete);
                soundChannel.stop();
                _collisionChannels.splice(0, 1);
            }
        }

        public function playPickupSound(soundData:String):void
        {
            if (_soundMute)
                return;

            var soundChannel:SoundChannel;

            if (_pickupChannels.length < _maxPickupSounds) {
                var sound:Sound = _soundStorage.getSound(soundData);
                soundChannel = sound.play(0, 0, _pickupSoundTransform);
                soundChannel.addEventListener(Event.SOUND_COMPLETE, _pickupSoundComplete);
                _pickupChannels.push(soundChannel);
            }
            else {
                soundChannel = _pickupChannels[0];
                soundChannel.removeEventListener(Event.SOUND_COMPLETE, _pickupSoundComplete);
                soundChannel.stop();
                _pickupChannels.splice(0, 1);
            }
        }

        public function playLoopSound(soundData:String):SoundChannel
        {
            if (_soundMute)
                return null;

            var sound:Sound = _soundStorage.getSound(soundData);
            var channel:SoundChannel = sound.play(0, int.MAX_VALUE, _itemsTransform);

            _loopSounds.push(channel);
            return channel;
        }

        public function tweenChannelStop(channel:SoundChannel, seconds:int = 1.5):ChannelControl
        {
            var channelControl:ChannelControl = new ChannelControl(channel);
            channelControl.reduceAndStop(seconds);
            return channelControl;
        }

        public function playMenuTheme():void
        {
            _themeChannel.stop();
            var randomIndex:int = MathHelper.random(0, ThemeSounds.MENU_URLS_SUITE.length - 1);
            var sound:Sound = new Sound(new URLRequest(ThemeSounds.MENU_URLS_SUITE[randomIndex]));

            sound.addEventListener(IOErrorEvent.IO_ERROR, _ioErrorHandler);
            sound.addEventListener(Event.COMPLETE, _loadCompleteHandler);

            _themeChannel = sound.play(0, int.MAX_VALUE, _themeTransform);

            if (_musicMute)
                _themeChannel.soundTransform = new SoundTransform(0);
        }

        public function playGameTheme():void
        {
            _themeChannel.stop();

            var randomIndex:int = MathHelper.random(0, ThemeSounds.GAME_URLS_SUITE.length - 1);
            var sound:Sound = new Sound(new URLRequest(ThemeSounds.GAME_URLS_SUITE[randomIndex]));

            sound.addEventListener(IOErrorEvent.IO_ERROR, _ioErrorHandler);
            sound.addEventListener(Event.COMPLETE, _loadCompleteHandler);

            _themeChannel = sound.play(0, int.MAX_VALUE, _themeTransform);
            if (_musicMute)
                _themeChannel.soundTransform = new SoundTransform(0);
        }

        public function playBonusTheme():void
        {
            _themeChannel.stop();
            var randomIndex:int = MathHelper.random(0, ThemeSounds.BONUS_URLS_SUITE.length - 1);
            var sound:Sound = new Sound(new URLRequest(ThemeSounds.BONUS_URLS_SUITE[randomIndex]));

            sound.addEventListener(IOErrorEvent.IO_ERROR, _ioErrorHandler);
            sound.addEventListener(Event.COMPLETE, _loadCompleteHandler);

            _themeChannel = sound.play(0, int.MAX_VALUE, _themeTransform);
            if (_musicMute)
                _themeChannel.soundTransform = new SoundTransform(0);
        }

        public function destroyLoopSounds():void
        {
            _disableLoopSounds();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _appDomain:ApplicationDomain = new ApplicationDomain();
        private var _soundStorage:SoundStorage = new SoundStorage(_appDomain);

        private var _uiSoundTransform:SoundTransform;
        private var _collisionSoundTransform:SoundTransform;
        private var _pickupSoundTransform:SoundTransform;
        private var _itemsTransform:SoundTransform;

        private var _collisionChannels:Vector.<SoundChannel> = new Vector.<SoundChannel>();
        private var _maxCollisionSounds:int = 7;

        private var _pickupChannels:Vector.<SoundChannel> = new Vector.<SoundChannel>();
        private var _maxPickupSounds:int = 7;

        private var _themeChannel:SoundChannel;
        private var _themeTransform:SoundTransform;

        private var _musicMute:Boolean = false;
        private var _soundMute:Boolean = false;

        private var _storage:SharedObject = SharedObject.getLocal(ApplicationConfiguration.APP_NAME, '/');
        private var _loopSounds:Vector.<SoundChannel> = new Vector.<SoundChannel>();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupParams():void
        {
            _uiSoundTransform = new SoundTransform(.4);
            _collisionSoundTransform = new SoundTransform(.4);
            _pickupSoundTransform = new SoundTransform(.4);
            _itemsTransform = new SoundTransform(.4);

            _themeChannel = new SoundChannel();
            _themeTransform = new SoundTransform(.3);

            _checkSettings();
        }

        private function _deleteChannel(channel:SoundChannel, struct:Vector.<SoundChannel>):void
        {
            var index:int = struct.indexOf(channel);
            channel.stop();

            if (index > -1) {
                struct.splice(index, 1);
            }
        }

        private function _enableMusic():void
        {
            _themeChannel.soundTransform = _themeTransform;
        }

        private function _disableMusic():void
        {
            _themeChannel.soundTransform = new SoundTransform(0);
        }

        private function _disableLoopSounds():void
        {
            for (var i:int = 0; i < _loopSounds.length; i++)
                tweenChannelStop(_loopSounds[i]);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _collisionComplete(event:Event):void
        {
            var channel:SoundChannel = event.target as SoundChannel;
            channel.removeEventListener(Event.SOUND_COMPLETE, _collisionComplete);
            _deleteChannel(channel, _collisionChannels);
        }

        private function _pickupSoundComplete(event:Event):void
        {
            var channel:SoundChannel = event.target as SoundChannel;
            channel.removeEventListener(Event.SOUND_COMPLETE, _pickupSoundComplete);
            _deleteChannel(channel, _pickupChannels);
        }

        private function _uiSoundComplete(event:Event):void
        {
            var channel:SoundChannel = event.target as SoundChannel;
            channel.removeEventListener(Event.SOUND_COMPLETE, _uiSoundComplete);
            channel.stop();
        }

        private function _checkSettings():void
        {
            if (_storage.data.sound_enabled == null) {
                _storage.data.sound_enabled = 1;
                this.soundMute = false;
            }
            else {
                var soundEnabled:int = int(_storage.data.sound_enabled);
                if (soundEnabled < 1)
                    this.soundMute = true;
            }

            if (_storage.data.music_enabled == null) {
                _storage.data.music_enabled = 1;
                this.musicMute = false;
            }
            else {
                var musicEnabled:int = int(_storage.data.music_enabled);
                if (musicEnabled < 1)
                    this.musicMute = true;
            }
            _storage.flush();
        }

        private function _ioErrorHandler(event:IOErrorEvent):void
        {
            trace((event.currentTarget as Sound).url, "Ошибка загрузки звука", event.text);
            var sound:Sound = event.currentTarget as Sound;
            _destroySound(sound);
        }

        private function _loadCompleteHandler(event:Event):void
        {
            var sound:Sound = event.currentTarget as Sound;
            _destroySound(sound);
        }

        private function _destroySound(sound:Sound):void
        {
            sound.removeEventListener(IOErrorEvent.IO_ERROR, _ioErrorHandler);
            sound.removeEventListener(Event.COMPLETE, _loadCompleteHandler);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public static function get instance():SoundManager
        {
            if (_instance == null)
                _instance = new SoundManager();
            return _instance;
        }

        public function get appDomain():ApplicationDomain
        {
            return _appDomain;
        }

        public function get musicMute():Boolean
        {
            return _musicMute;
        }

        public function set musicMute(value:Boolean):void
        {
            _musicMute = value;
            if (_musicMute)
                _disableMusic();
            else
                _enableMusic();
        }

        public function get soundMute():Boolean
        {
            return _soundMute;
        }

        public function set soundMute(value:Boolean):void
        {
            _soundMute = value;
            _disableLoopSounds();
        }
    }
}
