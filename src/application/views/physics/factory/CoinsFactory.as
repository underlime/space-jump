package application.views.physics.factory
{
    import application.views.physics.bodies.coins.BaseCoins;
    import application.views.physics.bodies.coins.CopperCoins;
    import application.views.physics.bodies.coins.GoldCoins;
    import application.views.physics.bodies.coins.SilverCoins;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class CoinsFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        /**
         * Возвращает последовательность монет
         * level:
         * 0 - Генерятся только медные монетки
         * 1 - Генерятся медные и серебрянные монетки
         * 2 - Генерятся медные, серебрянные и золотые монетки
         * @param    level Тип генерируемых монет (0, 1, 2)
         * @param    count Количество генерируемых монет
         * @return  Вектор с монетами
         */
        public static function getCoinsGroup(level:int = 0, count:int = 6):Vector.<BaseCoins>
        {
            if (level > 2 || level < 0) {
                level = 0;
            }

            var copperCoinsCount:int = CoinsFactory._getCopperCoinsCount(level, count);
            var silverCoinsCount:int = CoinsFactory._getSilverCoinsCount(level, count);
            var goldCoinsCount:int = CoinsFactory._getGoldCoinsCount(level, count);

            var result:Vector.<BaseCoins> = new Vector.<BaseCoins>();

            for (var i:int = 0; i < copperCoinsCount; i++) {
                result.push(new CopperCoins());
            }

            for (i = 0; i < silverCoinsCount; i++) {
                result.push(new SilverCoins());
            }

            for (i = 0; i < goldCoinsCount; i++) {
                result.push(new GoldCoins());
            }

            return result;
        }

        // PRIVATE CLASS METHODS ---------------------------------------------------------------/

        private static function _getCopperCoinsCount(level:int, count:int):int
        {
            var result:int = int(Math.ceil(count / (level + 1)));

            if (result < 1) {
                result = 0;
            }

            return result;
        }

        private static function _getSilverCoinsCount(level:int, count:int):int
        {
            var result:int;
            var copper:int = CoinsFactory._getCopperCoinsCount(level, count);
            if (level == 0) {
                return 0;
            }

            if (level == 1) {
                result = count - copper;
            }

            if (level == 2) {
                var total:int = count - copper;
                result = int(Math.floor(total / 2));
            }

            if (result < 1) {
                result = 0;
            }
            return result;
        }

        private static function _getGoldCoinsCount(level:int, count:int):int
        {
            var copper:int = CoinsFactory._getCopperCoinsCount(level, count);
            var silver:int = CoinsFactory._getSilverCoinsCount(level, count);
            var result:int = count - copper - silver;
            if (result < 1) {
                result = 0;
            }
            return result;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}