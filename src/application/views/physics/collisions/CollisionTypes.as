/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.03.13
 * Time: 16:04
 */
package application.views.physics.collisions
{
    public class CollisionTypes
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public static const WOOD_BOX_COLLISION:String = "WoodBoxCollision";
        public static const METAL_BOX_COLLISION:String = "MetalBoxCollision";
        public static const TNT_BOX_COLLISION:String = "TntBoxCollision";
        public static const UFO_COLLISION:String = "UfoCollision";
        public static const PLANE_COLLISION:String = "PlaneCollision";
        public static const JET_COLLISION:String = "JetCollision";
        public static const SATELLITE_COLLISION:String = "SatelliteCollision";
        public static const BIRD_COLLISION:String = "BirdCollision";
        public static const FIREWORK_COLLISION:String = "FireWorkCollision";

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
