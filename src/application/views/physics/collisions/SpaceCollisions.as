/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.11.12
 */

/**
 * Здесь регистрируются и обрабатываются все столкновения
 */
package application.views.physics.collisions
{

    import nape.callbacks.CbEvent;
    import nape.callbacks.CbType;
    import nape.callbacks.InteractionListener;
    import nape.callbacks.InteractionType;
    import nape.callbacks.PreListener;
    import nape.space.Space;

    import org.casalib.core.IDestroyable;

    public class SpaceCollisions implements IDestroyable
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpaceCollisions(space:Space)
        {
            super();
            _space = space;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function destroy():void
        {
            if (_destroyed)
                return;

            for (var i:int = 0; i < _interactionHash.length; i++) {
                _interactionHash[i].space = null;
                _interactionHash[i] = null;
            }

            for (var j:int = 0; j < _preHash.length; j++) {
                _preHash[j].space = null;
                _preHash[j] = null;
            }

            _interactionHash = null;
            _preHash = null;
            _destroyed = true;
        }

        public function registerCallback(type1:CbType, type2:CbType, callback:Function):void
        {
            var listener:InteractionListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, type1, type2, callback);
            listener.space = _space;
            _interactionHash.push(listener);
        }

        public function registerPreCallback(type1:CbType, type2:CbType, callback:Function):void
        {
            var listener:PreListener = new PreListener(InteractionType.COLLISION, type1, type2, callback);
            listener.space = _space;
            _preHash.push(listener);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _space:Space;
        private var _destroyed:Boolean = false;
        private var _interactionHash:Vector.<InteractionListener> = new Vector.<InteractionListener>();
        private var _preHash:Vector.<PreListener> = new Vector.<PreListener>();

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get destroyed():Boolean
        {
            return _destroyed;
        }

    }
}
