/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.10.12
 */

package application.views.physics
{

    import application.config.DataPhysics;
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.border.CosmoBorder;
    import application.views.physics.collisions.SpaceCollisions;

    import flash.display.DisplayObjectContainer;

    import framework.view.View;

    import nape.callbacks.Listener;
    import nape.phys.Body;
    import nape.space.Space;
    import nape.util.Debug;
    import nape.util.ShapeDebug;

    /**
     * Класс для инициализации физического мира
     */
    public class SpaceWorld extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpaceWorld(model:GameModel)
        {
            _model = model;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _space = new Space();
            _collisions = new SpaceCollisions(_space);
            _space.worldAngularDrag = dataPhysics.angular_drag.value;
            _space.worldLinearDrag = dataPhysics.linear_drag.value;

            _setupBorder();
        }

        override public function destroy():void
        {
            if (destroyed || !_space)
                return;

            _collisions.destroy();
            _destroyObjects();
            _destroyListeners();
            if (_debug) {
                _destroyDebug();
            }
            _space.clear();
            _space = null;
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            _space.step(DataPhysics.TIME_STEP);
            updateDebug();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.UPDATE));
        }

        public function updateDebug():void
        {
            if (_debug) {
                _updateDebug();
            }

        }

        public function addDebugDraw(debugContainer:DisplayObjectContainer):void
        {
            _debugContainer = debugContainer;
            _debug = true;
            _debugDraw();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:GameModel;
        private var _space:Space;
        private var _debug:Boolean = false;
        private var _debugShape:Debug;
        private var _collisions:SpaceCollisions;
        private var _dataPhysics:DataPhysics = DataPhysics.instance;
        private var _debugContainer:DisplayObjectContainer;

        private var _topBorder:CosmoBorder;
        private var _bottomBorder:CosmoBorder;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _debugDraw():void
        {
            _debugShape = new ShapeDebug(DataPhysics.WORLD_WIDTH, DataPhysics.WORLD_HEIGHT);
            if (_debugContainer)
                _debugContainer.addChild(_debugShape.display);
            else
                addChild(_debugShape.display);
        }

        private function _updateDebug():void
        {
            if (_debugShape) {
                _debugShape.clear();
                _debugShape.draw(_space);
                _debugShape.flush();
            }
        }

        private function _destroyDebug():void
        {
            if (_debugContainer && _debugShape && _debugContainer.contains(_debugShape.display))
                _debugContainer.removeChild(_debugShape.display);
            else
                removeChild(_debugShape.display);
            _debugShape = null;
        }

        private function _destroyListeners():void
        {
            if (!_space)
                return;

            for (var i:int = 0; i < space.listeners.length; ++i) {
                var listener:Listener = space.listeners.at(i);
                listener.space = null;
                listener = null;
            }
        }

        private function _destroyObjects():void
        {
            if (!_space)
                return;

            for (var i:int = 0; i < space.bodies.length; ++i) {
                var body:Body = space.bodies.at(i);
                if (body.userData.wrapper && (body.userData.wrapper as SpaceBody)) {
                    (body.userData.wrapper as SpaceBody).destroy();
                } else {
                    body.space = null;
                }
            }
        }

        private function _setupBorder():void
        {
            _topBorder = new CosmoBorder();
            _topBorder.body.position.setxy(CosmoBorder.BORDER_WIDTH / 2 - 100, CosmoBorder.BORDER_HEIGHT / 2);
            _topBorder.body.space = _space;
            addChild(_topBorder);


            _bottomBorder = new CosmoBorder();
            _bottomBorder.body.position.setxy(CosmoBorder.BORDER_WIDTH / 2 - 100, DataPhysics.WORLD_HEIGHT + CosmoBorder.BORDER_HEIGHT / 2);
            _bottomBorder.body.space = _space;
            addChild(_bottomBorder);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get dataPhysics():DataPhysics
        {
            return _dataPhysics;
        }

        public function get model():GameModel
        {
            return _model;
        }

        public function get space():Space
        {
            return _space;
        }

        public function get collisions():SpaceCollisions
        {
            return _collisions;
        }
    }
}
