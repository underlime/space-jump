package application.views.physics.callbacks
{

    import nape.callbacks.CbType;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class CallbackTypes
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ABYSS_TYPE:CbType = new CbType();
        public static const COSMONAUT_CBTYPE:CbType = new CbType();
        public static const WOODBOX_CBTYPE:CbType = new CbType();
        public static const METALBOX_CBTYPE:CbType = new CbType();
        public static const TNT_BOX_CBTYPE:CbType = new CbType();
        public static const COIN_CBTYPE:CbType = new CbType();
        public static const HEART_TYPE:CbType = new CbType();
        public static const SHIELD_TYPE:CbType = new CbType();
        public static const COLA_TYPE:CbType = new CbType();
        public static const GARBAGE_TYPE:CbType = new CbType();
        public static const NO_HERO_TYPE:CbType = new CbType();
        public static const EXPLOSION_TYPE:CbType = new CbType();
        public static const PETARDA_TYPE:CbType = new CbType();
        public static const JET_PLANE_TYPE:CbType = new CbType();
        public static const PLANE_TYPE:CbType = new CbType();
        public static const UFO_TYPE:CbType = new CbType();
        public static const BIRD_TYPE:CbType = new CbType();
        public static const BIG_ROCKET_TYPE:CbType = new CbType();
        public static const BIG_SHIELD_TYPE:CbType = new CbType();
        public static const EXPLOSION_OBSTACLE_TYPE:CbType = new CbType();
        public static const KINEMATIC_TYPE:CbType = new CbType();
        public static const SATELLITE_TYPE:CbType = new CbType();

        public static const GREED_PIG_TYPE:CbType = new CbType();
        public static const TROPHY_TYPE:CbType = new CbType();
        public static const BLACK_HOLE_TYPE:CbType = new CbType();
        public static const BORDER_TYPE:CbType = new CbType();

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}