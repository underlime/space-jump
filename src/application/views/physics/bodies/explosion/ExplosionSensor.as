package application.views.physics.bodies.explosion
{
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;
    import nape.dynamics.InteractionFilter;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ExplosionSensor extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ExplosionSensor(radius:Number = 100)
        {
            this._radius = radius;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            this._body.type = BodyType.STATIC;
            var shape:Circle = new Circle(this._radius, null, this._material, this._filter);
            this._body.shapes.add(shape);
            this._body.graphic = this._wrapper;
            this._body.cbTypes.add(CallbackTypes.EXPLOSION_TYPE);
        }

        override protected function _defineFilter():void
        {
            this._filter = new InteractionFilter(0, 0, 1);
            //this._filter.collisionGroup = 0x0000; // первая группа объектов
            //this._filter.collisionMask = 0x0000; // сталкиваемся только с космонавтом
            //this._filter.sensorGroup = 0x1111;
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _radius:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}