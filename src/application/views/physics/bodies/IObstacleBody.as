package application.views.physics.bodies
{

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public interface IObstacleBody
    {
        function get damage():Number;

        function get speedReduce():Number;

        function get collisionType():String;
    }

}