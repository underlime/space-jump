package application.views.physics.bodies.plane
{
    import application.views.particular.plane.PlaneData;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class YellowPlane extends SimplePlane
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function YellowPlane()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(PlaneData.YELLOW_PLANE, true);
            this._wrapper.addChild(this._graphic);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}