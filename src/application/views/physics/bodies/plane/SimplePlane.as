package application.views.physics.bodies.plane
{

    import application.sound.SoundManager;
    import application.sound.libs.LoopSounds;
    import application.views.physics.bodies.base.ObstacleBody;
    import application.views.physics.callbacks.CallbackTypes;
    import application.views.physics.collisions.CollisionTypes;

    import flash.media.SoundChannel;

    import nape.dynamics.InteractionFilter;
    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class SimplePlane extends ObstacleBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimplePlane()
        {
            super();
            super.horizontalBody = true;
            super.bodyWidth = 677;
            super.bodyHeight = 281;
            super._collisionType = CollisionTypes.PLANE_COLLISION;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _damage = this.dataPhysics.plane_damage.value;
            super.setup();
        }

        override public function render():void
        {
            super.render();
            _channel = SoundManager.instance.playLoopSound(LoopSounds.AIRBUS_FLIGHT);
        }

        override public function destroy():void
        {
            if (_channel)
                SoundManager.instance.tweenChannelStop(_channel);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function flipBody():void
        {
            _body.scaleShapes(-1, 1);
            this.graphic.scaleX = -1;
        }

        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.KINEMATIC;
            _setupShape();

            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.JET_PLANE_TYPE);
            _body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            _body.cbTypes.add(CallbackTypes.KINEMATIC_TYPE);
        }

        override protected function _addGraphics():void
        {
            throw new Error("method must be overriden!");
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x0100; // третья группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupShape():void
        {
            _body.shapes.add(new Polygon([Vec2.weak(674, 148.5), Vec2.weak(635.5, 124), Vec2.weak(610, 143.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(635.5, 124), Vec2.weak(620.5, 95), Vec2.weak(530, 186.5), Vec2.weak(610, 143.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(-0.5, 155), Vec2.weak(19.5, 182), Vec2.weak(74, 200.5), Vec2.weak(306.5, 202), Vec2.weak(434, 195.5), Vec2.weak(534, 84.5), Vec2.weak(78, 87.5), Vec2.weak(34.5, 111)]));
            _body.shapes.add(new Polygon([Vec2.weak(338.5, 232), Vec2.weak(338.5, 256), Vec2.weak(371, 279.5), Vec2.weak(443, 277.5), Vec2.weak(464, 265.5), Vec2.weak(434, 195.5), Vec2.weak(352, 223.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(629.5, 7), Vec2.weak(599, -0.5), Vec2.weak(598, -0.5), Vec2.weak(580.5, 6), Vec2.weak(534, 84.5), Vec2.weak(530, 186.5), Vec2.weak(620.5, 95)]));
            _body.shapes.add(new Polygon([Vec2.weak(544, 275.5), Vec2.weak(549, 257.5), Vec2.weak(434, 195.5), Vec2.weak(464, 265.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(352, 223.5), Vec2.weak(434, 195.5), Vec2.weak(306.5, 202)]));
            _body.shapes.add(new Polygon([Vec2.weak(434, 195.5), Vec2.weak(530, 186.5), Vec2.weak(534, 84.5)]));
            _body.setShapeFilters(_filter);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}