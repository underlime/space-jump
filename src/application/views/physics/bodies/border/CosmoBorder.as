/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.04.13
 * Time: 10:32
 */
package application.views.physics.bodies.border
{
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import nape.dynamics.InteractionFilter;
    import nape.phys.BodyType;
    import nape.shape.Polygon;
    import nape.shape.Shape;

    public class CosmoBorder extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const BORDER_WIDTH:int = 1000;
        public static const BORDER_HEIGHT:int = 50;

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CosmoBorder()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.STATIC;
            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.BORDER_TYPE);

            var bodyShape:Shape = new Polygon(Polygon.box(BORDER_WIDTH, BORDER_HEIGHT), _material, _filter);
            _body.shapes.add(bodyShape);
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x1000; // 4-я группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
