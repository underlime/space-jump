/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.03.13
 * Time: 11:21
 */
package application.views.physics.bodies.pig
{
    import application.sound.SoundManager;
    import application.sound.libs.LoopSounds;
    import application.views.particular.pig.PigAnimation;
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import flash.events.Event;
    import flash.media.SoundChannel;

    import framework.view.animation.Animation;

    import nape.dynamics.InteractionFilter;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    public class GreedPig extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GreedPig()
        {
            super();
            super.bodyWidth = 64;
            super.bodyHeight = 64;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _margin = _body.position.x;
            _body.velocity.setxy(0, -300);

            addEventListener(Event.ENTER_FRAME, _updatePosition);
            _channel = SoundManager.instance.playLoopSound(LoopSounds.HOG_FLY);
        }


        override public function destroy():void
        {
            if (_channel)
                SoundManager.instance.tweenChannelStop(_channel);
            removeEventListener(Event.ENTER_FRAME, _updatePosition);
            super.destroy();
        }

        private function _updatePosition(event:Event):void
        {
            _body.position.x = Math.sin(_body.position.y / 100) * 75 + _margin;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.DYNAMIC;

            var shape:Circle = new Circle(BodiesConfiguration.PIG_RADIUS, null, _material, _filter);
            _body.shapes.add(shape);

            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            _body.cbTypes.add(CallbackTypes.GREED_PIG_TYPE);
        }

        override protected function _addGraphics():void
        {
            _createAnimationVariant();
            _animation.x = -1 * BodiesConfiguration.PIG_RADIUS - 34;
            _animation.y = -1 * BodiesConfiguration.PIG_RADIUS - 41;
            _wrapper.addChild(_animation);
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x0001; // первая группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        override protected function _modelChangeHandler(e:Event):void
        {

        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:Animation;
        private var _margin:Number = 0;
        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _createAnimationVariant():void
        {
            _animation = new PigAnimation();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
