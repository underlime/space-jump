package application.views.physics.bodies.base
{
    import application.config.DataPhysics;
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.views.physics.bodies.GameBody;
    import application.views.physics.graphics.GraphicsWrapper;

    import flash.display.DisplayObject;
    import flash.events.Event;

    import framework.view.View;

    import nape.dynamics.InteractionFilter;
    import nape.phys.Material;
    import nape.space.Space;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class SpaceBody extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpaceBody()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupBody();
            _defineMaterial();
            _defineFilter();
            _createWrapper();
            _createBody();
            _addGraphics();
            _updateAlign();
        }

        override public function destroy():void
        {
            if (_model) {
                _model.removeEventListener(ApplicationEvent.VELOCITY_CHANGE, _modelChangeHandler);
            }
            _destroyBody();

            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.DESTROY));
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var checkPosition:Boolean = true;
        public var bodyWidth:Number = 0;
        public var bodyHeight:Number = 0;
        public var horizontalBody:Boolean = false;
        public var delay_y:Number = 0;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _space:Space;
        protected var _body:GameBody;
        protected var _wrapper:GraphicsWrapper;
        protected var _material:Material;
        protected var _filter:InteractionFilter;
        protected var _graphic:DisplayObject;
        protected var _model:GameModel;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _createWrapper():void
        {
            _wrapper = new GraphicsWrapper();
            addChild(_wrapper);
        }

        protected function _createBody():void
        {

        }

        protected function _addGraphics():void
        {

        }

        protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
        }

        protected function _defineMaterial():void
        {
            _material = new Material();
        }

        protected function _destroyBody():void
        {
            _body.space = null;
            _wrapper.destroy();
        }

        protected function _updateAlign():void
        {

        }

        /**
         * Линейное торможение тела
         * @param value (0-1)
         * 0 - Максимальное единовременное торможение (полное отсутствие инерции)
         * 1 - Полное отсутствие торможения
         */
        protected function _linearDamping(value:Number):void
        {
            _body.velocity.muleq(Math.pow(value, DataPhysics.TIME_STEP));
        }

        /**
         * Угловое торможение тела
         * @param value (0-1)
         * 0 - Максимальное единовременное торможение (полное отсутствие инерции)
         * 1 - Полное отсутствие торможения
         */
        protected function _angularDamping(value:Number):void
        {
            _body.angularVel *= Math.pow(value, DataPhysics.TIME_STEP);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _dataPhysics:DataPhysics = DataPhysics.instance;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBody():void
        {
            _body = new GameBody();
            _body.userData.wrapper = this;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _modelChangeHandler(e:Event):void
        {

        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get dataPhysics():DataPhysics
        {
            return _dataPhysics;
        }

        public function set model(value:GameModel):void
        {
            _model = value;
            _model.addEventListener(ApplicationEvent.VELOCITY_CHANGE, _modelChangeHandler);
        }

        public function get graphic():DisplayObject
        {
            return _graphic;
        }

        public function set space(value:Space):void
        {
            _space = value;
            if (_body) {
                _body.space = value;
            }
        }

        public function get body():GameBody
        {
            return _body;
        }

        public function get wrapper():GraphicsWrapper
        {
            return _wrapper;
        }
    }

}