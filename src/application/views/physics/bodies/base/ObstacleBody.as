/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 08.12.12
 */

package application.views.physics.bodies.base
{
    import application.views.physics.bodies.IObstacleBody;

    public class ObstacleBody extends SpaceBody implements IObstacleBody
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ObstacleBody()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        /**
         * Урон при столкновении
         */
        protected var _damage:Number = 0;

        /**
         * Процент уменьшения скорости при столкновении
         */
        protected var _speedReduce:Number = 0;

        /**
         * Тип столкновения
         */
        protected var _collisionType:String;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get damage():Number
        {
            return this._damage;
        }

        public function get speedReduce():Number
        {
            return this._speedReduce;
        }

        public function get collisionType():String
        {
            return _collisionType;
        }
    }
}
