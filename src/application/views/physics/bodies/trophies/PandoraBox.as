/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.03.13
 * Time: 15:30
 */
package application.views.physics.bodies.trophies
{
    import application.views.particular.trophies.PandoraBoxAnimation;
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import flash.events.Event;

    import framework.view.animation.Animation;

    import nape.dynamics.InteractionFilter;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    public class PandoraBox extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PandoraBox()
        {
            super();
            super.bodyWidth = 64;
            super.bodyHeight = 64;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.DYNAMIC;

            var shape:Circle = new Circle(BodiesConfiguration.PANDORA_BOX_RADIUS, null, _material, _filter);
            _body.shapes.add(shape);

            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            _body.cbTypes.add(CallbackTypes.TROPHY_TYPE);
        }

        override protected function _addGraphics():void
        {
            _createAnimationVariant();
            _animation.x = -1 * BodiesConfiguration.PANDORA_BOX_RADIUS;
            _animation.y = -1 * BodiesConfiguration.PANDORA_BOX_RADIUS;
            _wrapper.addChild(_animation);
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x1001; // первая группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        override protected function _modelChangeHandler(e:Event):void
        {
            var velocity:Number = ( -1) * this._model.velocity;
            this._body.velocity.y = velocity;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:Animation;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _createAnimationVariant():void
        {
            _animation = new PandoraBoxAnimation();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
