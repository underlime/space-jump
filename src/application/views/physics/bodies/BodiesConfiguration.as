/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.11.12
 */

package application.views.physics.bodies
{
    public class BodiesConfiguration
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const HERO_WIDTH:Number = 66;
        public static const HERO_HEIGHT:Number = 93;
        public static const COIN_RADIUS:int = 15;
        public static const HEART_RADIUS:int = 15;
        public static const SIMPLE_GARBAGE_RADIUS:int = 7;
        public static const PETARDA_WIDTH:Number = 32;
        public static const PETARDA_HEIGHT:Number = 32;
        public static const BIRD_RADIUS:Number = 24;
        public static const BIG_ROCKET_RADIUS:Number = 32;
        public static const BIG_SHIELD_RADIUS:Number = 32;
        public static const SHIELD_RADIUS:Number = 16;
        public static const COLA_RADIUS:Number = 16;
        public static const SATELLITE_RADIUS:Number = 25;
        public static const DEFENSE_SHIELD_RADIUS:Number = 85;
        public static const PIG_RADIUS:int = 30;
        public static const PANDORA_BOX_RADIUS:int = 32;
        public static const BLACK_HOLE_RADIUS:int = 65;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
