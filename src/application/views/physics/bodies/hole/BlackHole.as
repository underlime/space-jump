/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.04.13
 * Time: 15:23
 */
package application.views.physics.bodies.hole
{
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.helpers.MathHelper;

    import nape.dynamics.InteractionFilter;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    public class BlackHole extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BlackHole()
        {
            super();
            super.bodyWidth = 130;
            super.bodyHeight = 130;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.KINEMATIC;

            var shape:Circle = new Circle(BodiesConfiguration.BLACK_HOLE_RADIUS, null, _material, _filter);
            _body.shapes.add(shape);

            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            _body.cbTypes.add(CallbackTypes.BLACK_HOLE_TYPE);
        }

        override protected function _addGraphics():void
        {
            var arr:Array = ["WARP_1", "WARP_2", "WARP_3", "WARP_4"];
            var randomIndex:int = MathHelper.random(0, arr.length - 1);
            var _animation:Bitmap = super.source.getBitmap(arr[randomIndex]);

            _animation.x = -1 * BodiesConfiguration.BLACK_HOLE_RADIUS;
            _animation.y = -1 * BodiesConfiguration.BLACK_HOLE_RADIUS;
            _wrapper.addChild(_animation);
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x0001; // первая группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        override protected function _modelChangeHandler(e:Event):void
        {

        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
