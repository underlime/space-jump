package application.views.physics.bodies
{
    import application.views.physics.bodies.base.SpaceBody;

    import nape.geom.Vec2;
    import nape.phys.Body;
    import nape.phys.BodyType;

    /**
     * Содержит ссылку на свой визуальный контейнер
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class GameBody extends Body
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameBody(type:BodyType = null, position:Vec2 = null)
        {
            super(type, position);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var wrapper:SpaceBody;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}