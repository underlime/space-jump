/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 09.02.13
 * Time: 15:16
 */
package application.views.physics.bodies.satellite
{
    import application.config.DataPhysics;
    import application.sound.SoundManager;
    import application.sound.libs.LoopSounds;
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.base.ObstacleBody;
    import application.views.physics.callbacks.CallbackTypes;
    import application.views.physics.collisions.CollisionTypes;

    import flash.events.Event;
    import flash.media.SoundChannel;

    import nape.dynamics.InteractionFilter;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    public class BaseSatellite extends ObstacleBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseSatellite()
        {
            super();
            super.horizontalBody = true;
            super.bodyWidth = 100;
            super.bodyHeight = 50;
            super._collisionType = CollisionTypes.SATELLITE_COLLISION;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _totalMargin = _body.position.y;
            _direction_k = _body.position.y > DataPhysics.WORLD_HEIGHT / 2 ? 1 : -1;
            addEventListener(Event.ENTER_FRAME, _updateSatellite);

            _channel = SoundManager.instance.playLoopSound(LoopSounds.SATELLITE_FLIGHT);
        }

        override public function setup():void
        {
            _damage = this.dataPhysics.satelite_damage.value;
            super.setup();
        }


        override public function destroy():void
        {
            removeEventListener(Event.ENTER_FRAME, _updateSatellite);
            if (_channel)
                SoundManager.instance.tweenChannelStop(_channel);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function flipBody():void
        {
            super.body.scaleShapes(-1, 1);
            this.graphic.scaleX = -1;

            _graphic.x *= -1;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.KINEMATIC;
            _setupShape();

            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.SATELLITE_TYPE);
            _body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            _body.cbTypes.add(CallbackTypes.KINEMATIC_TYPE);
        }

        override protected function _addGraphics():void
        {
            throw new Error("method must be overriden!");
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x0100; // третья группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        protected function _setupShape():void
        {
            var shape:Circle = new Circle(BodiesConfiguration.SATELLITE_RADIUS, null, _material, _filter);
            _body.shapes.add(shape);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _totalMargin:Number = 0;
        private var _direction_k:Number = 1;
        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateSatellite(event:Event):void
        {
            var x:Number = _body.position.x;
            var xMargin:Number = DataPhysics.WORLD_WIDTH / 2;
            _body.position.y = _direction_k * (Math.pow(x - xMargin, 2) / 1000) + _totalMargin;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
