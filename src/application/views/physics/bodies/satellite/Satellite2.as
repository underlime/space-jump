/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 09.02.13
 * Time: 15:27
 */
package application.views.physics.bodies.satellite
{
    import application.views.physics.bodies.BodiesConfiguration;

    public class Satellite2 extends BaseSatellite
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Satellite2()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addGraphics():void
        {
            _graphic = super.source.getBitmap(SatelliteData.SATTELITE_2);
            _graphic.x = (-1) * BodiesConfiguration.SATELLITE_RADIUS;
            _graphic.y = (-1) * BodiesConfiguration.SATELLITE_RADIUS - 7;
            super._wrapper.addChild(_graphic);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
