/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 09.02.13
 * Time: 20:35
 */
package application.views.physics.bodies.satellite
{
    import application.views.physics.bodies.BodiesConfiguration;

    public class Satellite1 extends BaseSatellite
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Satellite1()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addGraphics():void
        {
            _graphic = super.source.getBitmap(SatelliteData.SATTELITE_1);
            _graphic.x = (-1) * BodiesConfiguration.SATELLITE_RADIUS + 2;
            _graphic.y = (-1) * BodiesConfiguration.SATELLITE_RADIUS - 8;
            super._wrapper.addChild(_graphic);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
