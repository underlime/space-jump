package application.views.physics.bodies
{

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public interface IExplosion extends IObstacleBody
    {
        function explode():void;
    }

}