package application.views.physics.bodies.boxes.tnt
{
    import application.views.particular.boxes.BoxesData;
    import application.views.particular.fitil.FitilAnimation;
    import nape.geom.Vec2;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class TNTBox2 extends TNTBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TNTBox2()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            this._fitil.x += this._graphic.x;
            this._fitil.y += this._graphic.y;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(BoxesData.TNT_BOX_2, true);
            this._wrapper.addChild(this._graphic);
            this._fitil = new FitilAnimation();
            this._wrapper.addChild(this._fitil);
            this._fitil.x = 74;
            this._fitil.y = -5;
        }

        override protected function _setBodyShape():void
        {
            var polygon:Array = [Vec2.weak(120, 44), Vec2.weak(77, 18), Vec2.weak(29, 0), Vec2.weak(0, 64), Vec2.weak(45, 101), Vec2.weak(90, 104)];
            var shape:Polygon = new Polygon(polygon, this._material);
            this._body.shapes.add(shape);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}