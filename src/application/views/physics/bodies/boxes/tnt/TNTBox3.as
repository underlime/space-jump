package application.views.physics.bodies.boxes.tnt
{
    import application.views.particular.boxes.BoxesData;
    import application.views.particular.fitil.FitilAnimation;
    import nape.geom.Vec2;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class TNTBox3 extends TNTBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TNTBox3()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            this._fitil.x += this._graphic.x;
            this._fitil.y += this._graphic.y;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(BoxesData.TNT_BOX_3, true);
            this._wrapper.addChild(this._graphic);
            this._fitil = new FitilAnimation();
            this._wrapper.addChild(this._fitil);
            this._fitil.x = 20;
            this._fitil.y = -2;
        }

        override protected function _setBodyShape():void
        {
            var polygon:Array = [Vec2.weak(105, 88), Vec2.weak(98, 56), Vec2.weak(66, 0), Vec2.weak(9, 31), Vec2.weak(0, 61), Vec2.weak(40, 118)];
            var shape:Polygon = new Polygon(polygon, this._material);
            this._body.shapes.add(shape);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}