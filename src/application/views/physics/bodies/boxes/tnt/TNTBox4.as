package application.views.physics.bodies.boxes.tnt
{
    import application.views.particular.boxes.BoxesData;
    import application.views.particular.fitil.FitilAnimation;
    import nape.geom.Vec2;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class TNTBox4 extends TNTBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TNTBox4()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            this._fitil.x += this._graphic.x;
            this._fitil.y += this._graphic.y;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(BoxesData.TNT_BOX_4, true);
            this._wrapper.addChild(this._graphic);
            this._fitil = new FitilAnimation();
            this._wrapper.addChild(this._fitil);
            this._fitil.x = 22;
            this._fitil.y = -7;
        }

        override protected function _setBodyShape():void
        {
            var polygon:Array = [Vec2.weak(110, 70), Vec2.weak(85, 0), Vec2.weak(15, 24), Vec2.weak(0, 35), Vec2.weak(24, 97), Vec2.weak(42, 100)];
            var shape:Polygon = new Polygon(polygon, this._material);
            this._body.shapes.add(shape);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}