package application.views.physics.bodies.boxes.wood
{
    import application.views.particular.boxes.BoxesData;
    import nape.geom.Vec2;
    import nape.phys.Material;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class WoodBox4 extends WoodBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function WoodBox4()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(BoxesData.WOOD_BOX_4, true);
            this._wrapper.addChild(this._graphic);
        }

        override protected function _setBodyShape():void
        {
            var material:Material = new Material();
            var polygon:Array = [Vec2.weak(99, 5), Vec2.weak(30, 0), Vec2.weak(0, 7), Vec2.weak(0, 77), Vec2.weak(27, 84), Vec2.weak(99, 80)];
            var shape:Polygon = new Polygon(polygon, material);
            this._body.shapes.add(shape);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}