package application.views.physics.bodies.boxes.wood
{
    import application.views.particular.boxes.BoxesData;
    import nape.geom.Vec2;
    import nape.phys.Material;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class WoodBox1 extends WoodBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function WoodBox1()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDS PROTECTED METHODS ----------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(BoxesData.WOOD_BOX_1, true);
            this._wrapper.addChild(this._graphic);
        }

        override protected function _setBodyShape():void
        {
            var material:Material = new Material();
            var polygon:Array = [Vec2.weak(100, 14), Vec2.weak(63, 0), Vec2.weak(0, 7), Vec2.weak(3, 73), Vec2.weak(31, 99), Vec2.weak(97, 84)];
            var shape:Polygon = new Polygon(polygon, material);
            this._body.shapes.add(shape);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}