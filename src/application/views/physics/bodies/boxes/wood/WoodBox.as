package application.views.physics.bodies.boxes.wood
{
    import application.sound.SoundManager;
    import application.sound.libs.CollisionSounds;
    import application.views.particular.explode.WoodBoxDust;
    import application.views.physics.bodies.IExplosion;
    import application.views.physics.bodies.base.ObstacleBody;
    import application.views.physics.callbacks.CallbackTypes;
    import application.views.physics.collisions.CollisionTypes;

    import flash.events.Event;

    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.phys.Material;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class WoodBox extends ObstacleBody implements IExplosion
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function WoodBox()
        {
            super();
            super.bodyWidth = 106;
            super.bodyHeight = 112;
            super._collisionType = CollisionTypes.WOOD_BOX_COLLISION;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._damage = this.dataPhysics.wood_box_damage.value;
            this._speedReduce = this.dataPhysics.wood_box_speed_reduce_percent.value;
            super.setup();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Разрушить ящик
         */
        public function explode():void
        {
            var dx:Number = this.graphic.width - 25;
            var dy:Number = this.graphic.height - 25;
            var explode:WoodBoxDust = new WoodBoxDust();
            explode.x = this.body.worldCOM.x - dx;
            explode.y = this.body.worldCOM.y - dy;
            explode.addEventListener(Event.COMPLETE, this._destruct);
            this.addChild(explode);

            this._destroyBody();

            SoundManager.instance.playCollisionSound(CollisionSounds.WOOD_BOX_IMPACT);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            this._body.type = BodyType.DYNAMIC;
            this._body.graphic = this._wrapper;
            this._body.cbTypes.add(CallbackTypes.WOODBOX_CBTYPE);
            this._body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            this._body.cbTypes.add(CallbackTypes.EXPLOSION_OBSTACLE_TYPE);
            this._setBodyShape();
        }

        override protected function _defineMaterial():void
        {
            this._material = Material.wood();
        }

        override protected function _updateAlign():void
        {
            this._body.align();
            this._graphic.x = (-1) * this._graphic.width / 2;
            this._graphic.y = (-1) * this._graphic.height / 2;
        }

        override protected function _modelChangeHandler(e:Event):void
        {
            var velocity:Number = ( -1) * this._model.velocity;
            this._body.velocity = new Vec2(0, velocity);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setBodyShape():void
        {

        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _destruct(event:Event):void
        {
            super.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}