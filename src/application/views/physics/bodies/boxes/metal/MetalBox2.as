package application.views.physics.bodies.boxes.metal
{
    import application.views.particular.boxes.BoxesData;

    import nape.geom.Vec2;
    import nape.phys.Material;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MetalBox2 extends MetalBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MetalBox2()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(BoxesData.METAL_BOX_2, true);
            this._wrapper.addChild(this._graphic);
        }

        override protected function _setBodyShape():void
        {
            var material:Material = new Material();
            var polygon:Array = [Vec2.weak(98, 5), Vec2.weak(29, 0), Vec2.weak(1, 8), Vec2.weak(1, 80), Vec2.weak(28, 85), Vec2.weak(97, 80)];
            var shape:Polygon = new Polygon(polygon, material);
            this._body.shapes.add(shape);
            this._body.setShapeFilters(this._filter);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}