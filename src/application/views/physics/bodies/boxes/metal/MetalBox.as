package application.views.physics.bodies.boxes.metal
{
    import application.views.physics.bodies.IExplosion;
    import application.views.physics.bodies.base.ObstacleBody;
    import application.views.physics.callbacks.CallbackTypes;
    import application.views.physics.collisions.CollisionTypes;

    import flash.events.Event;

    import nape.dynamics.InteractionFilter;
    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.phys.Material;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MetalBox extends ObstacleBody implements IExplosion
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MetalBox()
        {
            super();
            super.bodyWidth = 106;
            super.bodyHeight = 112;
            super._collisionType = CollisionTypes.METAL_BOX_COLLISION;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._damage = this.dataPhysics.metal_box_damage.value;
            this._speedReduce = this.dataPhysics.metal_box_speed_reduce_percent.value;
            super.setup();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function explode():void
        {
            this.destroy();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            this._body.type = BodyType.KINEMATIC;
            this._body.graphic = this._wrapper;
            this._body.cbTypes.add(CallbackTypes.METALBOX_CBTYPE);
            this._body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            this._body.cbTypes.add(CallbackTypes.KINEMATIC_TYPE);
            this._setBodyShape();
        }

        override protected function _defineMaterial():void
        {
            this._material = new Material(0, 10, 10, 100);
        }

        override protected function _defineFilter():void
        {
            this._filter = new InteractionFilter();
            this._filter.collisionGroup = 0x0100; // третья группа объектов
            this._filter.collisionMask = 0x0010;
        }

        override protected function _updateAlign():void
        {
            this._body.align();
            this._graphic.x = (-1) * this._graphic.width / 2;
            this._graphic.y = (-1) * this._graphic.height / 2;
        }

        override protected function _modelChangeHandler(e:Event):void
        {
            var velocity:Number = ( -1) * this._model.velocity;
            this._body.velocity = new Vec2(0, velocity);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setBodyShape():void
        {

        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}