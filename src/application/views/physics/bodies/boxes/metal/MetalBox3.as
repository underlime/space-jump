package application.views.physics.bodies.boxes.metal
{
    import application.views.particular.boxes.BoxesData;

    import nape.geom.Vec2;
    import nape.phys.Material;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MetalBox3 extends MetalBox
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MetalBox3()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(BoxesData.METAL_BOX_3, true);
            this._wrapper.addChild(this._graphic);
        }

        override protected function _setBodyShape():void
        {
            var material:Material = new Material();
            var polygon:Array = [Vec2.weak(89, 0), Vec2.weak(24, 14), Vec2.weak(0, 36), Vec2.weak(24, 105), Vec2.weak(95, 95), Vec2.weak(106, 64)];
            var shape:Polygon = new Polygon(polygon, material);
            this._body.shapes.add(shape);
            this._body.setShapeFilters(this._filter);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}