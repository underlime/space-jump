package application.views.physics.bodies.boxes
{
    import application.views.physics.bodies.boxes.metal.MetalBox;
    import application.views.physics.bodies.boxes.metal.MetalBox1;
    import application.views.physics.bodies.boxes.metal.MetalBox2;
    import application.views.physics.bodies.boxes.metal.MetalBox3;
    import application.views.physics.bodies.boxes.metal.MetalBox4;
    import application.views.physics.bodies.boxes.tnt.TNTBox;
    import application.views.physics.bodies.boxes.tnt.TNTBox1;
    import application.views.physics.bodies.boxes.tnt.TNTBox2;
    import application.views.physics.bodies.boxes.tnt.TNTBox3;
    import application.views.physics.bodies.boxes.tnt.TNTBox4;
    import application.views.physics.bodies.boxes.wood.WoodBox;
    import application.views.physics.bodies.boxes.wood.WoodBox1;
    import application.views.physics.bodies.boxes.wood.WoodBox2;
    import application.views.physics.bodies.boxes.wood.WoodBox3;
    import application.views.physics.bodies.boxes.wood.WoodBox4;

    import framework.helpers.MathHelper;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BoxFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static var woodBoxes:Array = [WoodBox1, WoodBox2, WoodBox3, WoodBox4];
        public static var metalBoxes:Array = [MetalBox1, MetalBox2, MetalBox3, MetalBox4];
        public static var tntBoxes:Array = [TNTBox1, TNTBox2, TNTBox3, TNTBox4];

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getWoodBox():WoodBox
        {
            var index:int = MathHelper.random(0, BoxFactory.woodBoxes.length - 1);
            return new BoxFactory.woodBoxes[index]();
        }

        public static function getMetalBox():MetalBox
        {
            var index:int = MathHelper.random(0, BoxFactory.metalBoxes.length - 1);
            return new BoxFactory.metalBoxes[index]();
        }

        public static function getTNTBox():TNTBox
        {
            var index:int = MathHelper.random(0, BoxFactory.tntBoxes.length - 1);
            return new BoxFactory.tntBoxes[index]();
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}