/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.12
 */
package application.views.physics.bodies.coins
{

    import application.views.particular.coins.SilverCoinAnimation;
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import flash.events.Event;

    import framework.view.animation.Animation;

    import nape.dynamics.InteractionFilter;
    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    public class BaseCoins extends SpaceBody implements ICoin
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseCoins()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            super.bodyWidth = 30;
            super.bodyHeight = 30;
            super.setup();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _money:int = 1;

        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            this._body.type = BodyType.DYNAMIC;
            var shape:Circle = new Circle(BodiesConfiguration.COIN_RADIUS, null, this._material, this._filter);
            this._body.shapes.add(shape);
            this._body.graphic = this._wrapper;
            this._body.cbTypes.add(CallbackTypes.COIN_CBTYPE);
            this._body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
        }

        override protected function _addGraphics():void
        {
            this._createAnimationVariant();
            this._animation.x = -1 * BodiesConfiguration.COIN_RADIUS;
            this._animation.y = -1 * BodiesConfiguration.COIN_RADIUS;
            this._wrapper.addChild(this._animation);
        }

        override protected function _defineFilter():void
        {
            this._filter = new InteractionFilter();
            this._filter.collisionGroup = 0x1101; // первая и третья группа объектов
            this._filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        override protected function _modelChangeHandler(e:Event):void
        {
            var velocity:Number = ( -1) * this._model.velocity;
            this._body.velocity = new Vec2(0, velocity);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _createAnimationVariant():void
        {
            this._animation = new SilverCoinAnimation();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:Animation;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get money():int
        {
            return _money;
        }

        public function get animation():Animation
        {
            return this._animation;
        }

        public function set animation(value:Animation):void
        {
            this._animation = value;
        }
    }
}
