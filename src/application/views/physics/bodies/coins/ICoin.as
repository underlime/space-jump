package application.views.physics.bodies.coins 
{
	import org.casalib.core.IDestroyable;
	
	/**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public interface ICoin extends IDestroyable
	{
		function get money():int;
	}
	
}