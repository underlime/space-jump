/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.12
 */

package application.views.physics.bodies.coins
{
    import application.views.particular.coins.GoldCoinsAnimation;

    public class GoldCoins extends BaseCoins
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GoldCoins()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function setup():void 
		{
			this._money = this.dataPhysics.gold_coin_money.value;
			super.setup();
		}
		
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createAnimationVariant():void
        {
            this.animation = new GoldCoinsAnimation();
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
