/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.12
 */

package application.views.physics.bodies.coins
{

    import application.views.particular.coins.CopperCoinsAnimation;

    public class CopperCoins extends BaseCoins
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CopperCoins()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function setup():void 
		{
			this._money = this.dataPhysics.copper_coin_money.value;
			super.setup();
		}
		
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createAnimationVariant():void
        {
            this.animation = new CopperCoinsAnimation();
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
