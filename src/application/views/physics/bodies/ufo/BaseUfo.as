/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.03.13
 * Time: 16:08
 */
package application.views.physics.bodies.ufo
{
    import application.sound.SoundManager;
    import application.sound.libs.LoopSounds;
    import application.views.physics.bodies.base.ObstacleBody;
    import application.views.physics.collisions.CollisionTypes;

    import flash.events.Event;
    import flash.media.SoundChannel;

    public class BaseUfo extends ObstacleBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseUfo()
        {
            super();
            super._collisionType = CollisionTypes.UFO_COLLISION;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _moveUfo();
            _channel = SoundManager.instance.playLoopSound(LoopSounds.UFO_FLIGHT);
        }


        override public function destroy():void
        {
            removeEventListener(Event.ENTER_FRAME, _updateUfo);
            if (_channel)
                SoundManager.instance.tweenChannelStop(_channel);
            super.destroy();
        }

        private function _moveUfo():void
        {
            _margin = _body.position.y;
            addEventListener(Event.ENTER_FRAME, _updateUfo);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _margin:Number = 0;
        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _updateUfo(event:Event):void
        {
            _body.position.y = Math.sin(_body.position.x * 500) * 75 + _margin;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
