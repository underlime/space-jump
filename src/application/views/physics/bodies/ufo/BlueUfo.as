package application.views.physics.bodies.ufo
{
    import application.config.DataPhysics;
    import application.views.particular.ufo.BlueUfoAnimation;
    import application.views.physics.callbacks.CallbackTypes;

    import framework.view.animation.Animation;

    import nape.dynamics.InteractionFilter;
    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BlueUfo extends BaseUfo
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BlueUfo()
        {
            super();
            super.horizontalBody = true;
            super.bodyWidth = 165;
            super.bodyHeight = 107;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._damage = this.dataPhysics.ufo_damage.value;
            super.setup();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            this._body.type = BodyType.KINEMATIC;
            this._body.graphic = this._wrapper;
            this._body.cbTypes.add(CallbackTypes.UFO_TYPE);
            this._body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            this._body.cbTypes.add(CallbackTypes.KINEMATIC_TYPE);
            this._setupPolygon();
        }

        override protected function _addGraphics():void
        {
            this._animation = new BlueUfoAnimation();
            this._wrapper.addChild(this._animation);
            this._startPosition = DataPhysics.WORLD_HEIGHT / 2 - this.height / 2;
        }

        override protected function _defineFilter():void
        {
            this._filter = new InteractionFilter();
            this._filter.collisionGroup = 0x0100; // третья группа объектов
            this._filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:Animation;
        private var _startPosition:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupPolygon():void
        {
            this._body.shapes.add(new Polygon([Vec2.weak(47.5, 12), Vec2.weak(48, 42.5), Vec2.weak(101, 29.5), Vec2.weak(90, 7.5), Vec2.weak(72, -0.5), Vec2.weak(71, -0.5)]));
            this._body.shapes.add(new Polygon([Vec2.weak(6.5, 99), Vec2.weak(36, 106.5), Vec2.weak(96, 99.5), Vec2.weak(160, 62.5), Vec2.weak(156, 34.5), Vec2.weak(101, 29.5), Vec2.weak(48, 42.5), Vec2.weak(1.5, 75)]));
            this._body.shapes.add(new Polygon([Vec2.weak(164.5, 48), Vec2.weak(156, 34.5), Vec2.weak(160, 62.5)]));
            this._body.setShapeFilters(this._filter);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get animation():Animation
        {
            return _animation;
        }
    }

}