package application.views.physics.bodies.ufo
{
    import application.views.particular.ufo.RedUfoAnimation;
    import application.views.physics.callbacks.CallbackTypes;

    import framework.view.animation.Animation;

    import nape.dynamics.InteractionFilter;
    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class RedUfo extends BaseUfo
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RedUfo()
        {
            super();
            super.horizontalBody = true;
            super.bodyWidth = 182;
            super.bodyHeight = 99;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._damage = this.dataPhysics.ufo_damage.value;
            super.setup();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            this._body.type = BodyType.KINEMATIC;
            this._body.graphic = this._wrapper;
            this._body.cbTypes.add(CallbackTypes.UFO_TYPE);
            this._body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            this._body.cbTypes.add(CallbackTypes.KINEMATIC_TYPE);
            this._setupPolygon();
        }

        override protected function _addGraphics():void
        {
            this._animation = new RedUfoAnimation();
            this._wrapper.addChild(this._animation);
        }

        override protected function _defineFilter():void
        {
            this._filter = new InteractionFilter();
            this._filter.collisionGroup = 0x0100; // третья группа объектов
            this._filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:Animation;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupPolygon():void
        {
            this._body.shapes.add(new Polygon([Vec2.weak(181.5, 69), Vec2.weak(174, 59.5), Vec2.weak(121.5, 48), Vec2.weak(75, 97.5), Vec2.weak(135, 93.5), Vec2.weak(178, 76.5)]));
            this._body.shapes.add(new Polygon([Vec2.weak(109.5, 6), Vec2.weak(86, 0.5), Vec2.weak(85, 0.5), Vec2.weak(70.5, 7), Vec2.weak(59, 49.5), Vec2.weak(75, 97.5), Vec2.weak(121.5, 48)]));
            this._body.shapes.add(new Polygon([Vec2.weak(0.5, 66), Vec2.weak(0.5, 74), Vec2.weak(23, 87.5), Vec2.weak(75, 97.5), Vec2.weak(59, 49.5), Vec2.weak(10.5, 58)]));
            this._body.setShapeFilters(this._filter);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get animation():Animation
        {
            return _animation;
        }
    }

}