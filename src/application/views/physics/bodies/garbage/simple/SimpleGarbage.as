/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.11.12
 */

package application.views.physics.bodies.garbage.simple
{

    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.garbage.GarbageData;
    import application.views.physics.callbacks.CallbackTypes;
    import framework.helpers.MathHelper;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    public class SimpleGarbage extends SpaceBody
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimpleGarbage()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            this._body.type = BodyType.DYNAMIC;
            this._body.graphic = this._wrapper;
            this._body.cbTypes.add(CallbackTypes.GARBAGE_TYPE);
            this._body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);

            var shape:Circle = new Circle(BodiesConfiguration.SIMPLE_GARBAGE_RADIUS);
            this._body.shapes.add(shape);
        }

        override protected function _addGraphics():void
        {
            var rand:int = MathHelper.random(1, GarbageData.SIMPLE_GARBAGE_COUNT);
            this._graphic = this.source.getBitmap(GarbageData.SIMPLE_GARBAGE_DATA + rand.toString());
            this._graphic.x = (-1) * BodiesConfiguration.SIMPLE_GARBAGE_RADIUS;
            this._graphic.y = (-1) * BodiesConfiguration.SIMPLE_GARBAGE_RADIUS;
            this._wrapper.addChild(this._graphic);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
