/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 14.04.13
 * Time: 19:08
 */
package application.views.physics.bodies.cosmonaut.controls
{
    import application.views.physics.bodies.cosmonaut.PowerupCosmonaut;
    import application.views.physics.bodies.cosmonaut.powerups.BaseStartRocket;
    import application.views.physics.bodies.cosmonaut.powerups.StartRocketBody;

    import flash.events.Event;

    public class RocketControl extends BasePowerupControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RocketControl(cosmonaut:PowerupCosmonaut, rocket:BaseStartRocket, seconds:int = 6)
        {
            super(cosmonaut);
            _rocket = rocket;
            _seconds = seconds;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super._block();
            _cosmonaut.jetFall();
            _setupRocketObject();
        }

        override public function destroy():void
        {
            if (!_completed)
                _completeRocket();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _rocket:BaseStartRocket;
        private var _seconds:int = 6;
        private var _rocketObject:StartRocketBody;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupRocketObject():void
        {
            _rocketObject = new StartRocketBody(_cosmonaut.body, _rocket, _seconds);
            _rocketObject.space = _cosmonaut.body.space;
            _rocketObject.addEventListener(Event.COMPLETE, _rocketCompleteHandler);
            _rocketObject.body.allowRotation = false;
            _cosmonaut.addChild(_rocketObject);
            _rocketObject.start();
        }

        private function _completeRocket():void
        {
            _rocketObject.removeEventListener(Event.COMPLETE, _rocketCompleteHandler);
            _rocketObject.destroy();

            super._unblock();
            super._cosmonaut.updateGraphic();

            _completed = true;

            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _rocketCompleteHandler(event:Event):void
        {
            _completeRocket();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
