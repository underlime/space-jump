/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 14.04.13
 * Time: 17:59
 */
package application.views.physics.bodies.cosmonaut.controls
{
    import application.views.physics.bodies.cosmonaut.PowerupCosmonaut;

    import org.casalib.events.RemovableEventDispatcher;

    public class BasePowerupControl extends RemovableEventDispatcher
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BasePowerupControl(cosmonaut:PowerupCosmonaut)
        {
            super();
            _cosmonaut = cosmonaut;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function execute():void
        {

        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _immortalBitMask:uint = 0x1000;
        protected var _simpleBitMask:uint = 0x1111;
        protected var _completed:Boolean = false;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _block():void
        {
            _cosmonaut.body.angularVel = 0;
            _cosmonaut.body.rotation = 0;
            _cosmonaut.body.allowRotation = false;
            _cosmonaut.blockRotation = true;
            _cosmonaut.bodyShape.filter.collisionMask = _immortalBitMask;
            _cosmonaut.blocked = true;
        }

        protected function _unblock():void
        {
            _cosmonaut.body.allowRotation = true;
            _cosmonaut.blockRotation = false;
            _cosmonaut.bodyShape.filter.collisionMask = _simpleBitMask;
            _cosmonaut.blocked = false;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        protected var _cosmonaut:PowerupCosmonaut;

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
