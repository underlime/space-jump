/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 14.04.13
 * Time: 20:20
 */
package application.views.physics.bodies.cosmonaut.controls
{
    import application.views.physics.bodies.cosmonaut.DefenseAnimation;
    import application.views.physics.bodies.cosmonaut.PowerupCosmonaut;
    import application.views.physics.bodies.cosmonaut.powerups.BodyDefense;

    import flash.events.Event;

    public class DefenceControl extends BasePowerupControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DefenceControl(cosmonaut:PowerupCosmonaut, seconds:int = 6)
        {
            _seconds = seconds;
            super(cosmonaut);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupAnimation();
        }

        override public function destroy():void
        {
            if (!_completed)
                _completeBuff();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update(seconds:int):void
        {
            _bodyDefense.updateSeconds(seconds);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _seconds:int = 6;
        private var _animation:DefenseAnimation;
        private var _bodyDefense:BodyDefense;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupAnimation():void
        {
            super._block();
            _animation = new DefenseAnimation(_seconds);
            _bodyDefense = new BodyDefense(_cosmonaut.body, _animation, _seconds);
            _bodyDefense.space = _cosmonaut.body.space;
            _bodyDefense.addEventListener(Event.COMPLETE, _shieldCompleteHandler);
            _cosmonaut.immortal = true;
            _cosmonaut.addChild(_bodyDefense);

            _cosmonaut.body.allowRotation = true;
            _cosmonaut.blockRotation = false;
        }

        private function _completeBuff():void
        {
            super._unblock();
            _bodyDefense.removeEventListener(Event.COMPLETE, _shieldCompleteHandler);
            _bodyDefense.destroy();
            _cosmonaut.immortal = false;

            _completed = true;
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _shieldCompleteHandler(event:Event):void
        {
            _completeBuff();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
