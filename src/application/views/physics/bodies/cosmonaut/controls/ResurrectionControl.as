/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 14.04.13
 * Time: 18:02
 */
package application.views.physics.bodies.cosmonaut.controls
{
    import application.views.particular.explode.EndingAnimation;
    import application.views.physics.bodies.cosmonaut.PowerupCosmonaut;
    import application.views.physics.bodies.cosmonaut.powerups.ResurrectionObject;

    import com.greensock.TweenLite;
    import com.greensock.plugins.ColorTransformPlugin;
    import com.greensock.plugins.TweenPlugin;

    import flash.events.Event;

    public class ResurrectionControl extends BasePowerupControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ResurrectionControl(cosmonaut:PowerupCosmonaut, animation:EndingAnimation)
        {
            _animation = animation;
            super(cosmonaut);
            TweenPlugin.activate([ColorTransformPlugin]);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupAnimation();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupAnimation():void
        {
            _resurrectionObject = new ResurrectionObject(_animation, super._cosmonaut.body);
            _resurrectionObject.addEventListener(Event.COMPLETE, _completeHandler);
            super._cosmonaut.addChild(_resurrectionObject);
            super._block();
            super._cosmonaut.jetFall();
            super._cosmonaut.addChild(_animation);

            var params:Object = {
                colorTransform: {
                    brightness: 2
                },
                onComplete: _backTween
            };
            TweenLite.to(super._cosmonaut, .9, params);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:EndingAnimation;
        private var _resurrectionObject:ResurrectionObject;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _backTween():void
        {
            var params:Object = {
                colorTransform: {
                    brightness: 1
                },
                onComplete: _backTween
            };
            TweenLite.to(super._cosmonaut, .9, params);
        }

        private function _completeHandler(event:Event):void
        {
            _resurrectionObject.removeEventListener(Event.COMPLETE, _completeHandler);
            _resurrectionObject.destroy();
            super._cosmonaut.updateGraphic();
            super._unblock();
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
