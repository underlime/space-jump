/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 14.04.13
 * Time: 19:31
 */
package application.views.physics.bodies.cosmonaut.controls
{
    import application.views.particular.powerup.BoostAnimation;
    import application.views.physics.bodies.cosmonaut.PowerupCosmonaut;
    import application.views.physics.bodies.cosmonaut.powerups.BodyDefense;

    import flash.events.Event;

    public class BoosterControl extends BasePowerupControl
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BoosterControl(cosmonaut:PowerupCosmonaut, seconds:int = 6)
        {
            _seconds = seconds;
            super(cosmonaut);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupAnimation();
            _addBooster();
            _setupDefence();
        }

        override public function destroy():void
        {
            if (!_completed)
                _completeBooster();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:BoostAnimation;
        private var _seconds:int = 6;
        private var _bodyDefense:BodyDefense;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupAnimation():void
        {
            _animation = new BoostAnimation(_seconds);
        }

        private function _addBooster():void
        {
            super._block();
            _cosmonaut.jetFall();
            _cosmonaut.immortal = true;
        }

        private function _setupDefence():void
        {
            _bodyDefense = new BodyDefense(_cosmonaut.body, _animation, _seconds);
            _bodyDefense.space = _cosmonaut.body.space;
            _bodyDefense.addEventListener(Event.COMPLETE, _boostCompleteHandler);
            _cosmonaut.addChild(_bodyDefense);
        }

        private function _completeBooster():void
        {
            super._unblock();
            _cosmonaut.immortal = true;
            _cosmonaut.updateGraphic();

            _bodyDefense.removeEventListener(Event.COMPLETE, _boostCompleteHandler);
            _bodyDefense.destroy();

            _completed = true;

            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _boostCompleteHandler(event:Event):void
        {
            _completeBooster();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
