/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 14.04.13
 * Time: 19:47
 */
package application.views.physics.bodies.cosmonaut.controls
{
    import application.sound.SoundManager;
    import application.sound.libs.PickupSounds;
    import application.views.particular.powerup.ParachuteData;
    import application.views.physics.bodies.cosmonaut.PowerupCosmonaut;
    import application.views.physics.bodies.cosmonaut.powerups.ParachuteObject;

    import flash.events.Event;

    public class ParachuteControl extends BasePowerupControl
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ParachuteControl(cosmonaut:PowerupCosmonaut, seconds:int = 6)
        {
            _seconds = seconds;
            super(cosmonaut);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupParachute();
            _setupCosmonaut();
        }

        override public function destroy():void
        {
            if (!_completed)
                _completeParachute();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _seconds:int = 6;
        private var _parachute:ParachuteObject;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupCosmonaut():void
        {
            super._block();
            _cosmonaut.immortal = true;
        }

        private function _setupParachute():void
        {
            _parachute = new ParachuteObject(_seconds);

            _parachute.addEventListener(Event.COMPLETE, _parachuteCompleteHandler);
            _cosmonaut.wrapper.addChildAt(_parachute, 0);

            _parachute.x = (-1) * ParachuteData.PARACHUTE_WIDTH / 2;
            _parachute.y = -60 - ParachuteData.PARACHUTE_HEIGHT;

            _parachute.open();

            SoundManager.instance.playPickupSound(PickupSounds.PARACHUTE_OPEN);
        }

        private function _completeParachute():void
        {
            _parachute.removeEventListener(Event.COMPLETE, _parachuteCompleteHandler);
            _parachute.destroy();

            super._unblock();
            _cosmonaut.immortal = false;

            _completed = true;

            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        private function _parachuteCompleteHandler(event:Event):void
        {
            _completeParachute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
