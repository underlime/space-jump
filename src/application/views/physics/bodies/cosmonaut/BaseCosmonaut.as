package application.views.physics.bodies.cosmonaut
{
    import application.models.Inventory;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.items.ItemRecord;
    import application.models.skin.SkinModel;
    import application.models.skin.SkinsCollection;
    import application.views.construct.Constructor;
    import application.views.construct.body.InGameJumper;
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import framework.data.ModelsRegistry;

    import nape.dynamics.InteractionFilter;
    import nape.phys.BodyType;
    import nape.phys.Material;
    import nape.shape.Polygon;

    public class BaseCosmonaut extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseCosmonaut()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            super.checkPosition = false;
            super.setup();
        }

        override public function render():void
        {
            if (!_skins)
                throw new Error("Setup skin collection by [skins] property!");

            var inventory:Inventory = ModelsRegistry.getModel(ModelData.INVENTORY_MODEL) as Inventory;
            var item:ItemRecord = inventory.getEquippedItem(Items.ACTION_SUITE);
            var skinData:String = item.symbol.value ? item.symbol.value : "default";
            var skin:SkinModel = _skins.getSkin(skinData);

            _jumperSkin = Constructor.getFlyingCosmonaut(skin);
            var crownItem:ItemRecord = inventory.getEquippedItem(Items.ACTION_CROWN);
            var hatItem:ItemRecord = inventory.getEquippedItem(Items.ACTION_HAT);

            if (crownItem && super.source.has(crownItem.symbol.value)) {
                _jumperSkin.addHat(crownItem.symbol.value);
            } else if (hatItem && super.source.has(hatItem.symbol.value)) {
                _jumperSkin.addHat(hatItem.symbol.value);
            }

            super.render();

            _setupJumperSkin();
        }

        override public function destroy():void
        {
            _jumperSkin.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function updateGraphic():void
        {
            _jumperSkin.update();
        }

        public function jetFall():void
        {
            _jumperSkin.jet();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _bodyShape:Polygon;
        protected var _simpleCollisionBitGroup:uint = 0x0010;
        protected var _simpleCollisionBitMask:uint = 0x1111;

        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.DYNAMIC;
            _bodyShape = new Polygon(Polygon.box(BodiesConfiguration.HERO_WIDTH, BodiesConfiguration.HERO_HEIGHT), _material, _filter);
            _body.shapes.add(_bodyShape);
            _body.graphic = super._wrapper;
            _body.cbTypes.add(CallbackTypes.COSMONAUT_CBTYPE);
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = _simpleCollisionBitGroup; // вторая группа объектов
            _filter.collisionMask = _simpleCollisionBitMask; // сталкиваемся со всеми объектами
        }

        override protected function _defineMaterial():void
        {
            _material = new Material();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _jumperSkin:InGameJumper;
        private var _skins:SkinsCollection;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupJumperSkin():void
        {
            _jumperSkin.scaleX = .45;
            _jumperSkin.scaleY = .45;
            _jumperSkin.x = -33;
            _jumperSkin.y = -47;
            _wrapper.addChild(_jumperSkin);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set skins(value:SkinsCollection):void
        {
            _skins = value;
        }

        public function get bodyShape():Polygon
        {
            return _bodyShape;
        }

        public function set bodyShape(value:Polygon):void
        {
            _bodyShape = value;
        }
    }
}
