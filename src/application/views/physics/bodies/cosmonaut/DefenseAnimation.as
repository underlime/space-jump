package application.views.physics.bodies.cosmonaut
{
    import application.sound.SoundManager;
    import application.sound.libs.LoopSounds;
    import application.views.particular.factory.FramesFactory;

    import com.greensock.TweenLite;

    import flash.media.SoundChannel;

    import framework.view.animation.BlinkAnimation;

    public class DefenseAnimation extends BlinkAnimation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DefenseAnimation(seconds:int)
        {
            super(FramesFactory.getAnimation(CosmonautData.DEFENSE_SHIELD, CosmonautData.DEFENSE_SHIELD_COUNT), seconds);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function render():void
        {
            super.render();
            _setNullSizes();
            _tweenToNormalSize();
            _channel = SoundManager.instance.playLoopSound(LoopSounds.FORCE_FIELD);
        }

        override public function destroy():void
        {
            if (_channel)
                SoundManager.instance.tweenChannelStop(_channel);
            super.destroy();
        }

        private function _setNullSizes():void
        {
            _needX = this.x;
            _needY = this.y;

            this.x = _needX + super.maxWidth / 2;
            this.y = _needY + super.maxHeight / 2;

            this.width = 0;
            this.height = 0;
        }

        private function _tweenToNormalSize():void
        {
            var params:Object = {
                width: super.maxWidth,
                height: super.maxHeight,
                x: _needX,
                y: _needY,
                onComplete: _tweenComplete
            };
            TweenLite.to(this, .2, params);
        }

        private function _tweenComplete():void
        {
            TweenLite.killTweensOf(this);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _needX:Number;
        protected var _needY:Number;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/


    }
}
