package application.views.physics.bodies.cosmonaut
{
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.game.layers.panel.buffs.BuffType;
    import application.views.particular.resurrection.ResurrectionAnimation;
    import application.views.particular.resurrection.ResurrectionPlusAnimation;
    import application.views.physics.bodies.cosmonaut.controls.BasePowerupControl;
    import application.views.physics.bodies.cosmonaut.controls.BoosterControl;
    import application.views.physics.bodies.cosmonaut.controls.DefenceControl;
    import application.views.physics.bodies.cosmonaut.controls.ParachuteControl;
    import application.views.physics.bodies.cosmonaut.controls.ResurrectionControl;
    import application.views.physics.bodies.cosmonaut.controls.RocketControl;
    import application.views.physics.bodies.cosmonaut.powerups.BigRocketObject;
    import application.views.physics.bodies.cosmonaut.powerups.SmallRocketObject;

    import flash.events.Event;

    import org.casalib.core.IDestroyable;

    public class PowerupCosmonaut extends MovableCosmonaut
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PowerupCosmonaut()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function removeAllBuffs():void
        {
            var arr:Array = [_defenceControl, _parachuteControl, _boosterControl, _rocketControl];
            for (var i:int = 0; i < arr.length - 1; i++) {
                var control:IDestroyable = (arr[i] as IDestroyable);
                if (control && !control.destroyed)
                    control.destroy();
            }
        }

        public function enableDefenseShield(seconds:int):void
        {
            if (!_blocked || (_blocked && _immortal)) {
                if (_immortal && _defenceControl && !_defenceControl.destroyed) {
                    _defenceControl.update(seconds);
                } else {
                    _defenceControl = new DefenceControl(this, seconds);
                    _defenceControl.addEventListener(Event.COMPLETE, _controlCompleteHandler);
                    _defenceControl.execute();
                }
                _buffType = BuffType.IMMORTAL_BUFF;
            }
        }

        public function openParachute(seconds:int):void
        {
            if (!_blocked) {
                _parachuteControl = new ParachuteControl(this, seconds);
                _parachuteControl.addEventListener(Event.COMPLETE, _controlCompleteHandler);
                _parachuteControl.execute();
                _buffType = BuffType.PARACHUTE_BUFF;
            }
        }

        /***
         * РАКЕТЫ
         */

        public function enableBooster(seconds:int):void
        {
            if (!_blocked) {
                _boosterControl = new BoosterControl(this, seconds);
                _boosterControl.addEventListener(Event.COMPLETE, _controlCompleteHandler);
                _boosterControl.execute();
                _buffType = BuffType.BOOSTER_BUFF;
            }
        }

        public function enableSmallRocket(seconds:int):void
        {
            if (!_blocked) {
                _rocketControl = new RocketControl(this, new SmallRocketObject(), seconds);
                _rocketControl.addEventListener(Event.COMPLETE, _controlCompleteHandler)
                _rocketControl.execute();
                _buffType = BuffType.ROCKET_BUFF;
            }
        }

        public function enableBigRocket(seconds:int):void
        {
            if (!_blocked) {
                _rocketControl = new RocketControl(this, new BigRocketObject(), seconds);
                _rocketControl.addEventListener(Event.COMPLETE, _controlCompleteHandler)
                _rocketControl.execute();
                _buffType = BuffType.MEGA_ROCKET_BUFF;
            }
        }

        /*********************************
         * РАКЕТЫ
         */


        /***
         * ВОСКРЕШЕНИЕ
         */

        public function resurrectionApply():void
        {
            if (!_blocked) {
                (new ResurrectionControl(this, new ResurrectionAnimation())).execute();
                SoundManager.instance.playUISound(UISounds.RESURRECTION);
            }
        }

        public function resurrectionPlusApply():void
        {
            if (!_blocked) {
                (new ResurrectionControl(this, new ResurrectionPlusAnimation())).execute();
                SoundManager.instance.playUISound(UISounds.RESURRECTION_PLUS);
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _rocketControl:RocketControl;
        private var _boosterControl:BoosterControl;
        private var _parachuteControl:ParachuteControl;
        private var _defenceControl:DefenceControl;
        private var _immortal:Boolean = false;
        private var _blocked:Boolean = false;
        private var _buffType:String = BuffType.NONE;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _controlCompleteHandler(event:Event):void
        {
            event.currentTarget.removeEventListener(Event.COMPLETE, _controlCompleteHandler);
            (event.currentTarget as BasePowerupControl).destroy();
            _buffType = BuffType.NONE;
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get immortal():Boolean
        {
            return _immortal;
        }

        /**
         * ежели сейчас работает powerup
         */
        public function get blocked():Boolean
        {
            return _blocked;
        }

        /**
         * тип текущего пауэрапа
         */
        public function get buffType():String
        {
            return _buffType;
        }

        public function set blocked(value:Boolean):void
        {
            _blocked = value;
        }

        public function set immortal(value:Boolean):void
        {
            _immortal = value;
        }
    }
}
