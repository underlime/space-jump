/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 14.04.13
 * Time: 15:19
 */
package application.views.physics.bodies.cosmonaut.powerups
{
    import application.views.particular.explode.EndingAnimation;

    import flash.events.Event;

    import framework.view.View;

    import nape.phys.Body;

    public class ResurrectionObject extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ResurrectionObject(resurrectionAnimation:EndingAnimation, cosmonaut:Body)
        {
            _resurrectionAnimation = resurrectionAnimation;
            _cosmonaut = cosmonaut;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _resurrectionAnimation.addEventListener(Event.COMPLETE, _animationCompleteHandler);
            addChild(_resurrectionAnimation);
            addEventListener(Event.ENTER_FRAME, _checkFrame);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _resurrectionAnimation:EndingAnimation;
        private var _cosmonaut:Body;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _animationCompleteHandler(event:Event):void
        {
            _resurrectionAnimation.removeEventListener(Event.COMPLETE, _animationCompleteHandler);
            removeEventListener(Event.ENTER_FRAME, _checkFrame);
            super.dispatchEvent(new Event(Event.COMPLETE));
            this.destroy();
        }

        private function _checkFrame(event:Event):void
        {
            _resurrectionAnimation.x = _cosmonaut.position.x - _resurrectionAnimation.maxWidth / 2;
            _resurrectionAnimation.y = _cosmonaut.position.y - _resurrectionAnimation.maxHeight / 2 - 15;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
