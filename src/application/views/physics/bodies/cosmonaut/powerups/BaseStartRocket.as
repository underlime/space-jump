/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.02.13
 * Time: 15:47
 */
package application.views.physics.bodies.cosmonaut.powerups
{
    import application.views.particular.explode.EndingAnimation;

    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import framework.view.View;
    import framework.view.animation.Animation;

    public class BaseStartRocket extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseStartRocket(flyAnimation:Animation, endAnimation:EndingAnimation)
        {
            _flyAnimation = flyAnimation;
            _endAnimation = endAnimation;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            if (_timer)
                _destroyTimer();

            if (_flyAnimation && !_flyAnimation.destroyed)
                _flyAnimation.destroy();

            if (_endAnimation && !_endAnimation.destroyed)
                _endAnimation.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function startRocket(seconds:int):void
        {
            addChild(_flyAnimation);
            _timer = new Timer(seconds * 1000, 1);
            _timer.addEventListener(TimerEvent.TIMER_COMPLETE, _onTimerComplete);
            _timer.start();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _flyAnimation:Animation;
        private var _endAnimation:EndingAnimation;
        private var _timer:Timer;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _destroyTimer():void
        {
            _timer.stop();
            _timer.removeEventListener(TimerEvent.TIMER_COMPLETE, _onTimerComplete);
            _timer = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onTimerComplete(event:TimerEvent):void
        {
            _destroyTimer();
            _flyAnimation.destroy();
            _endAnimation.addEventListener(Event.COMPLETE, _endHandler);
            addChild(_endAnimation);
        }

        private function _endHandler(event:Event):void
        {
            _endAnimation.removeEventListener(Event.COMPLETE, _endHandler);
            _endAnimation.destroy();
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
