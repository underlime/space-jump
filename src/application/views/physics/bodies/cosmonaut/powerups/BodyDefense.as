package application.views.physics.bodies.cosmonaut.powerups
{
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import flash.events.Event;

    import framework.view.animation.BlinkAnimation;

    import nape.dynamics.InteractionFilter;
    import nape.phys.Body;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    public class BodyDefense extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BodyDefense(cosmonautBody:Body, animation:BlinkAnimation, seconds:int = 10)
        {
            _cosmonautBody = cosmonautBody;
            _animation = animation;
            _seconds = seconds;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _body.angularVel = 0;
            _body.allowRotation = false;
        }

        override public function destroy():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _animation.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function updateSeconds(seconds:int):void
        {
            _animation.updateSeconds(seconds);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        override protected function _createBody():void
        {
            var shape:Circle = new Circle(_animation.maxWidth / 2, null, _material, _filter);

            _body.type = BodyType.DYNAMIC;
            _body.shapes.add(shape);

            _body.graphic = super._wrapper;
            _body.cbTypes.add(CallbackTypes.COSMONAUT_CBTYPE);
        }

        override protected function _addGraphics():void
        {
            _animation.x = -1 * _animation.maxWidth / 2;
            _animation.y = -1 * _animation.maxHeight / 2;
            _wrapper.addChild(_animation);
            _animation.addEventListener(Event.COMPLETE, _completeHandler);
        }

        private function _completeHandler(event:Event):void
        {
            _animation.removeEventListener(Event.COMPLETE, _completeHandler);
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x1111;
            _filter.collisionMask = 0x0001; // сталкиваемся со всеми кроме космонавта и кинематических тел
        }

        protected function _enterFrameHandler(event:Event):void
        {
            _body.position.setxy(
                    _cosmonautBody.position.x,
                    _cosmonautBody.position.y
            );
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:BlinkAnimation;
        private var _cosmonautBody:Body;
        private var _seconds:int = 30;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
