/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.02.13
 * Time: 11:40
 */
package application.views.physics.bodies.cosmonaut.powerups
{
    import application.views.particular.powerup.ParachuteCloseAnimation;
    import application.views.particular.powerup.ParachuteData;
    import application.views.particular.powerup.ParachuteInitAnimation;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import framework.view.View;

    public class ParachuteObject extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ParachuteObject(seconds:int)
        {
            _seconds = seconds;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _initAnimation = new ParachuteInitAnimation();
            _closeAnimation = new ParachuteCloseAnimation();
            _openedState = super.source.getBitmap(ParachuteData.OPENED_FRAME);
        }

        override public function destroy():void
        {
            if (_initAnimation && !_initAnimation.destroyed)
                _initAnimation.destroy();
            if (_closeAnimation && !_closeAnimation.destroyed)
                _closeAnimation.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function open():void
        {
            _initAnimation.addEventListener(Event.COMPLETE, _completeInitHandler);
            addChild(_initAnimation);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _initAnimation:ParachuteInitAnimation;
        private var _closeAnimation:ParachuteCloseAnimation;
        private var _openedState:Bitmap;

        private var _parachuteTimer:Timer;
        private var _seconds:int = 3;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _completeInitHandler(event:Event):void
        {
            _initAnimation.removeEventListener(Event.COMPLETE, _completeInitHandler);
            addChild(_openedState);

            _parachuteTimer = new Timer(_seconds * 1000, 1);
            _parachuteTimer.addEventListener(TimerEvent.TIMER_COMPLETE, _close);
            _parachuteTimer.start();
        }

        private function _close(event:TimerEvent):void
        {
            _parachuteTimer.stop();
            _parachuteTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, _close);
            _parachuteTimer = null;

            if (_openedState && this.contains(_openedState))
                removeChild(_openedState);

            _closeAnimation.addEventListener(Event.COMPLETE, _completeCloseHandler);
            addChild(_closeAnimation);
        }

        private function _completeCloseHandler(event:Event):void
        {
            _closeAnimation.removeEventListener(Event.COMPLETE, _completeCloseHandler);
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
