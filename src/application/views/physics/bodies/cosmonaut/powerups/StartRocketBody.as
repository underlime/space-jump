/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 16.02.13
 * Time: 12:31
 */
package application.views.physics.bodies.cosmonaut.powerups
{
    import application.sound.SoundManager;
    import application.sound.libs.LoopSounds;
    import application.views.particular.powerup.RocketsData;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import flash.events.Event;
    import flash.media.SoundChannel;

    import nape.dynamics.InteractionFilter;
    import nape.phys.Body;
    import nape.phys.BodyType;
    import nape.shape.Polygon;

    public class StartRocketBody extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StartRocketBody(cosmonautBody:Body, rocket:BaseStartRocket, seconds:int = 10)
        {
            _cosmonautBody = cosmonautBody;
            _rocket = rocket;
            _seconds = seconds;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _channel = SoundManager.instance.playLoopSound(LoopSounds.ROCKER_FLY);
        }

        override public function destroy():void
        {
            if (_channel)
                SoundManager.instance.tweenChannelStop(_channel);

            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _rocket.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function start():void
        {
            _rocket.startRocket(_seconds);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _createBody():void
        {
            var shape:Polygon = new Polygon(Polygon.box(RocketsData.ROCKET_WIDTH, RocketsData.ROCKET_HEIGHT), this._material, this._filter);

            _body.type = BodyType.DYNAMIC;
            _body.shapes.add(shape);

            _body.graphic = super._wrapper;
            _body.cbTypes.add(CallbackTypes.COSMONAUT_CBTYPE);
        }

        override protected function _addGraphics():void
        {
            _rocket.x = -1 * RocketsData.ROCKET_WIDTH / 2;
            _rocket.y = -1 * RocketsData.ROCKET_HEIGHT / 2;
            _wrapper.addChild(_rocket);
            _rocket.addEventListener(Event.COMPLETE, _completeHandler);
        }

        private function _completeHandler(event:Event):void
        {
            _rocket.removeEventListener(Event.COMPLETE, _completeHandler);
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x1111;
            _filter.collisionMask = 0x0001; // сталкиваемся со всеми кроме космонавта и кинематических тел
        }

        protected function _enterFrameHandler(event:Event):void
        {
            _body.position.setxy(
                    _cosmonautBody.position.x,
                    _cosmonautBody.position.y
            );
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _cosmonautBody:Body;
        private var _rocket:BaseStartRocket;
        private var _seconds:int;
        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
