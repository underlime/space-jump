/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 23.04.13
 * Time: 15:17
 */
package application.views.physics.bodies.cosmonaut
{
    import application.config.DataPhysics;

    import com.greensock.TweenLite;
    import com.greensock.plugins.ColorTransformPlugin;
    import com.greensock.plugins.TweenPlugin;

    import flash.events.Event;

    public class BlinkCosmonaut extends PowerupCosmonaut
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BlinkCosmonaut()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            TweenPlugin.activate([ColorTransformPlugin]);
            _brightParams = {
                colorTransform: {
                    brightness: 1.25
                }
            };
            _normalParams = {
                colorTransform: {
                    brightness: 1
                }
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function blink():void
        {
            super._bodyShape.filter.collisionMask = _immortalBitMask;
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _blinkFrames:int = 0;
        private var _seconds:int = 1.5;

        private var _blinkFrameDelay:int = 2;
        private var _blinkCurrentFrame:int = 0;

        private var _immortalBitMask:uint = 0x1000;
        private var _brightParams:Object;
        private var _normalParams:Object;

        private var _isNormal:Boolean = true;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _blink():void
        {
            _blinkCurrentFrame++;
            if (_blinkCurrentFrame >= _blinkFrameDelay) {
                TweenLite.to(this, 0, _brightParams);
                _blinkCurrentFrame = 0;
                _isNormal = false;
            } else {
                if (!_isNormal) {
                    TweenLite.to(this, 0, _normalParams);
                    _isNormal = true;
                }
            }
        }

        private function _removeBlink():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            TweenLite.to(this, 0, _normalParams);
            super._bodyShape.filter.collisionMask = _simpleCollisionBitMask;
            _blinkFrames = 0;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _enterFrameHandler(event:Event):void
        {
            if (_blinkFrames * DataPhysics.TIME_STEP >= _seconds)
                _removeBlink();
            else
                _blink();
            _blinkFrames++;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
