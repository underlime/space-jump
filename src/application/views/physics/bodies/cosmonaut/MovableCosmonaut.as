package application.views.physics.bodies.cosmonaut
{
    import application.config.DataPhysics;

    import flash.display.Stage;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;

    /**
     * Двигаемся
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MovableCosmonaut extends BaseCosmonaut
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MovableCosmonaut()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();

            _stage = this.stage;
            _stage.addEventListener(KeyboardEvent.KEY_DOWN, _keyBoardDownHandler);
            _stage.addEventListener(KeyboardEvent.KEY_UP, _keyBoardUpHandler);
            this.addEventListener(Event.ENTER_FRAME, _updateKeyBehaviour);
        }

        override public function destroy():void
        {
            _stage.removeEventListener(KeyboardEvent.KEY_DOWN, _keyBoardDownHandler);
            _stage.removeEventListener(KeyboardEvent.KEY_UP, _keyBoardUpHandler);
            this.removeEventListener(Event.ENTER_FRAME, _updateKeyBehaviour);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function isFree():Boolean
        {
            return (!_leftKey && !_rightKey && !_upKey && !_downKey);
        }

        public function blockCosmonaut():void
        {
            _block = true;
            _body.velocity.setxy(0, 0);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _blockRotation:Boolean = false;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _checkPosition():void
        {
            if (_body.position.x < 0) {
                _body.position.x = DataPhysics.WORLD_WIDTH;
            }

            if (_body.position.x > DataPhysics.WORLD_WIDTH) {
                _body.position.x = 0;
            }

            if (_body.position.y < 0) {
                _body.position.y = DataPhysics.WORLD_HEIGHT;
            }

            if (_body.position.y - this.height > DataPhysics.WORLD_HEIGHT) {
                _body.position.y = 0;
            }

        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _leftKey:Boolean = false;
        private var _rightKey:Boolean = false;
        private var _upKey:Boolean = false;
        private var _downKey:Boolean = false;
        private var _horizontalVelocity:int = 0;
        private var _verticalVelocity:int = 0;
        private var _angularVelocity:Number = 0;
        private var _stage:Stage;
        private var _block:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _leftKeyBehaviour():void
        {
            if (!super.dataPhysics.hero_inertia.value && _horizontalVelocity > 0)
                _horizontalVelocity = 0;

            _horizontalVelocity -= super.dataPhysics.hero_acceleration.value;
            _angularVelocity -= super.dataPhysics.hero_angular_acceleration.value;

            if (Math.abs(_horizontalVelocity) > super.dataPhysics.hero_max_velocity.value) {
                _horizontalVelocity = (-1) * super.dataPhysics.hero_max_velocity.value;
            }

            if (Math.abs(_angularVelocity) > super.dataPhysics.hero_max_angular_velocity.value) {
                _angularVelocity = ( -1) * super.dataPhysics.hero_max_angular_velocity.value;
            }

            _body.velocity.x = _horizontalVelocity;
            if (!_blockRotation)
                _body.angularVel = _angularVelocity;
        }

        private function _rightKeyBehaviour():void
        {
            if (!super.dataPhysics.hero_inertia.value && _horizontalVelocity < 0)
                _horizontalVelocity = 0;

            _horizontalVelocity += super.dataPhysics.hero_acceleration.value;
            _angularVelocity += super.dataPhysics.hero_angular_acceleration.value;

            if (Math.abs(_horizontalVelocity) > super.dataPhysics.hero_max_velocity.value) {
                _horizontalVelocity = super.dataPhysics.hero_max_velocity.value;
            }

            if (Math.abs(_angularVelocity) > super.dataPhysics.hero_max_angular_velocity.value) {
                _angularVelocity = super.dataPhysics.hero_max_angular_velocity.value;
            }

            _body.velocity.x = _horizontalVelocity;
            if (!_blockRotation)
                _body.angularVel = _angularVelocity;
        }

        private function _upKeyBehaviour():void
        {
            if (!super.dataPhysics.hero_inertia.value && _verticalVelocity > 0)
                _verticalVelocity = 0;

            _verticalVelocity -= super.dataPhysics.hero_acceleration.value;

            if (Math.abs(_verticalVelocity) > super.dataPhysics.hero_max_velocity.value) {
                _verticalVelocity = (-1) * super.dataPhysics.hero_max_velocity.value;
            }
            _body.velocity.y = _verticalVelocity;
        }

        private function _downKeyBehaviour():void
        {
            if (!super.dataPhysics.hero_inertia.value && _verticalVelocity < 0)
                _verticalVelocity = 0;

            _verticalVelocity += super.dataPhysics.hero_acceleration.value;
            if (Math.abs(_verticalVelocity) > super.dataPhysics.hero_max_velocity.value) {
                _verticalVelocity = super.dataPhysics.hero_max_velocity.value;
            }
            _body.velocity.y = _verticalVelocity;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _keyBoardUpHandler(event:KeyboardEvent):void
        {
            switch (event.keyCode) {
                case Keyboard.LEFT:
                    _leftKey = false;
                    break;
                case Keyboard.RIGHT:
                    _rightKey = false;
                    break;
                case Keyboard.UP:
                    _upKey = false;
                    break;
                case Keyboard.DOWN:
                    _downKey = false;
                    break;
                default:
                    break;
            }
        }

        private function _keyBoardDownHandler(event:KeyboardEvent):void
        {
            switch (event.keyCode) {
                case Keyboard.LEFT:
                    _leftKey = true;
                    break;
                case Keyboard.RIGHT:
                    _rightKey = true;
                    break;
                case Keyboard.UP:
                    _upKey = true;
                    break;
                case Keyboard.DOWN:
                    _downKey = true;
                    break;
                default:
                    break;
            }
        }

        private function _updateKeyBehaviour(event:Event):void
        {
            if (_block)
                return;

            _verticalVelocity = _body.velocity.y;
            _horizontalVelocity = _body.velocity.x;

            if (_leftKey) {
                _leftKeyBehaviour();
            }
            if (_rightKey) {
                _rightKeyBehaviour();
            }
            if (_upKey) {
                _upKeyBehaviour();
            }
            if (_downKey) {
                _downKeyBehaviour();
            }

            _checkPosition();

            if (this.isFree()) {
                super._linearDamping(dataPhysics.hero_damping.value);
                super._angularDamping(dataPhysics.hero_angular_damping.value)
            }

        }

        // ACCESSORS ---------------------------------------------------------------------------/


        public function get blockRotation():Boolean
        {
            return _blockRotation;
        }

        public function set blockRotation(value:Boolean):void
        {
            _blockRotation = value;
        }
    }

}