package application.views.physics.bodies.birds
{
    import application.views.particular.birds.WhiteBirdAnimation;
    import application.views.particular.explode.WhiteBirdExplodeAnimation;
    import application.views.physics.bodies.BodiesConfiguration;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class WhiteBird extends BirdBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function WhiteBird()
        {
            super();
            this._explodeAnimation = new WhiteBirdExplodeAnimation();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = new WhiteBirdAnimation();
            this._graphic.x = -1 * BodiesConfiguration.BIRD_RADIUS;
            this._graphic.y = -1 * BodiesConfiguration.BIRD_RADIUS;
            this._wrapper.addChild(this._graphic);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}