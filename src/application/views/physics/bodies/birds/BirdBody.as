package application.views.physics.bodies.birds
{
    import application.config.DataPhysics;
    import application.sound.SoundManager;
    import application.sound.libs.BodiesSound;
    import application.sound.libs.CollisionSounds;
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.IExplosion;
    import application.views.physics.bodies.base.ObstacleBody;
    import application.views.physics.callbacks.CallbackTypes;
    import application.views.physics.collisions.CollisionTypes;

    import flash.events.Event;

    import framework.view.animation.Animation;

    import nape.dynamics.InteractionFilter;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BirdBody extends ObstacleBody implements IExplosion
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BirdBody()
        {
            super();
            this.horizontalBody = true;
            super.bodyWidth = 48;
            super.bodyHeight = 48;
            super._collisionType = CollisionTypes.BIRD_COLLISION;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _damage = this.dataPhysics.birds_damage.value;
            addEventListener(Event.ENTER_FRAME, _checkPosition);

            super.render();
        }

        override public function destroy():void
        {
            removeEventListener(Event.ENTER_FRAME, _checkPosition);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function explode():void
        {
            var dx:Number = BodiesConfiguration.BIRD_RADIUS;
            var dy:Number = BodiesConfiguration.BIRD_RADIUS;

            _explodeAnimation.x = this.body.worldCOM.x - dx;
            _explodeAnimation.y = this.body.worldCOM.y - dy;
            _explodeAnimation.addEventListener(Event.COMPLETE, _destruct);
            this.addChild(_explodeAnimation);

            _destroyBody();

            SoundManager.instance.playCollisionSound(CollisionSounds.BIRD_DEATH);
        }

        /**
         * Отразить зеркально по горизонтали
         */
        public function flipBody():void
        {
            super.body.scaleShapes(-1, 1);
            this.graphic.scaleX = -1;
            _graphic.x = BodiesConfiguration.BIRD_RADIUS;
            _graphic.y = -1 * BodiesConfiguration.BIRD_RADIUS;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _explodeAnimation:Animation;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.DYNAMIC;
            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            _body.cbTypes.add(CallbackTypes.BIRD_TYPE);
            _body.cbTypes.add(CallbackTypes.EXPLOSION_OBSTACLE_TYPE);

            _body.shapes.add(new Circle(BodiesConfiguration.BIRD_RADIUS, null, _material, _filter));
            _body.allowRotation = false;
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x0001; // первая группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _destruct(event:Event):void
        {
            super.destroy();
        }

        private function _checkPosition(event:Event):void
        {
            if (_body.position.x > 0 && _body.position.x < DataPhysics.WORLD_WIDTH)
            {
                removeEventListener(Event.ENTER_FRAME, _checkPosition);
                SoundManager.instance.playCollisionSound(BodiesSound.BIRDS_FLY);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}