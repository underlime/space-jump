package application.views.physics.bodies.birds
{
    import application.views.particular.birds.RedBirdAnimation;
    import application.views.particular.explode.RedBirdExplodeAnimation;
    import application.views.physics.bodies.BodiesConfiguration;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class RedBird extends BirdBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        public function RedBird()
        {
            super();
            this._explodeAnimation = new RedBirdExplodeAnimation();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = new RedBirdAnimation();
            this._graphic.x = -1 * BodiesConfiguration.BIRD_RADIUS;
            this._graphic.y = -1 * BodiesConfiguration.BIRD_RADIUS;
            this._wrapper.addChild(this._graphic);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}