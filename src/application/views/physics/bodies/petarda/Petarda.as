package application.views.physics.bodies.petarda
{
    import application.config.DataPhysics;
    import application.sound.SoundManager;
    import application.sound.libs.BodiesSound;
    import application.sound.libs.CollisionSounds;
    import application.views.particular.petarda.RedPedardaAnimation;
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.IExplosion;
    import application.views.physics.bodies.base.ObstacleBody;
    import application.views.physics.callbacks.CallbackTypes;
    import application.views.physics.collisions.CollisionTypes;

    import flash.events.Event;

    import framework.view.animation.Animation;

    import nape.dynamics.InteractionFilter;
    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.shape.Polygon;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class Petarda extends ObstacleBody implements IExplosion
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Petarda()
        {
            super();
            super.bodyWidth = 32;
            super.bodyHeight = 32;
            super._collisionType = CollisionTypes.FIREWORK_COLLISION;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _damage = this.dataPhysics.birds_damage.value;
            addEventListener(Event.ENTER_FRAME, _checkPosition);
            super.render();
        }

        override public function destroy():void
        {
            removeEventListener(Event.ENTER_FRAME, _checkPosition);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function explode():void
        {
            _explodeAnimation.x = super.body.worldCOM.x - _explodeAnimation.maxWidth / 2;
            _explodeAnimation.y = super.body.worldCOM.y - _explodeAnimation.maxHeight / 2;
            _explodeAnimation.addEventListener(Event.COMPLETE, _destruct);
            addChild(_explodeAnimation);

            _destroyBody();

            SoundManager.instance.playCollisionSound(CollisionSounds.FIREWORK_IMPACT);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _explodeAnimation:Animation;

        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.KINEMATIC;
            var shape:Polygon = new Polygon(Polygon.box(BodiesConfiguration.PETARDA_WIDTH, BodiesConfiguration.PETARDA_HEIGHT), _material, _filter);
            _body.shapes.add(shape);
            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            _body.cbTypes.add(CallbackTypes.PETARDA_TYPE);
            _body.cbTypes.add(CallbackTypes.EXPLOSION_OBSTACLE_TYPE);
            _body.allowRotation = false;
        }

        override protected function _addGraphics():void
        {
            _createAnimationVariant();
            _animation.x = -1 * BodiesConfiguration.PETARDA_WIDTH;
            _animation.y = -1 * BodiesConfiguration.PETARDA_HEIGHT;
            _wrapper.addChild(_animation);
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x0001; // первая группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        override protected function _modelChangeHandler(e:Event):void
        {
            var velocity:Number = ( -1) * _model.velocity;
            _body.velocity = new Vec2(0, velocity);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _createAnimationVariant():void
        {
            _animation = new RedPedardaAnimation();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _animation:Animation;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _destruct(event:Event):void
        {
            super.destroy();
        }

        private function _checkPosition(event:Event):void
        {
            if (_body.position.y < DataPhysics.WORLD_HEIGHT)
            {
                removeEventListener(Event.ENTER_FRAME, _checkPosition);
                SoundManager.instance.playCollisionSound(BodiesSound.FIREWORK_FLY);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get animation():Animation
        {
            return _animation;
        }

        public function set animation(value:Animation):void
        {
            _animation = value;
        }
    }
}