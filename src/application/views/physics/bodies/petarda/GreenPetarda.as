package application.views.physics.bodies.petarda
{
    import application.views.particular.petarda.GreenPetardaAnimation;
    import application.views.particular.petarda.PetardaGreenExplode;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class GreenPetarda extends Petarda
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GreenPetarda()
        {
            super();
            _explodeAnimation = new PetardaGreenExplode();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createAnimationVariant():void
        {
            this._animation = new GreenPetardaAnimation();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}