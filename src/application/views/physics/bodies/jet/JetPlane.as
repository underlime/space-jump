package application.views.physics.bodies.jet
{
    import application.sound.SoundManager;
    import application.sound.libs.LoopSounds;
    import application.views.physics.bodies.base.ObstacleBody;
    import application.views.physics.callbacks.CallbackTypes;
    import application.views.physics.collisions.CollisionTypes;

    import flash.media.SoundChannel;

    import nape.dynamics.InteractionFilter;
    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.shape.Polygon;

    /**
     * Реактивный самолет
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class JetPlane extends ObstacleBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JetPlane()
        {
            super();
            super.horizontalBody = true;
            super.bodyWidth = 248;
            super.bodyHeight = 109;
            super._collisionType = CollisionTypes.JET_COLLISION;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _damage = this.dataPhysics.jet_damage.value;
            super.setup();
        }

        override public function render():void
        {
            super.render();
            _channel = SoundManager.instance.playLoopSound(LoopSounds.JET_FLIGHT);
        }

        override public function destroy():void
        {
            if (_channel)
                SoundManager.instance.tweenChannelStop(_channel);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function flipBody():void
        {
            _body.scaleShapes(-1, 1);
            this.graphic.scaleX = -1;
        }

        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            _body.type = BodyType.KINEMATIC;
            _setupShape();

            _body.graphic = _wrapper;
            _body.cbTypes.add(CallbackTypes.JET_PLANE_TYPE);
            _body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
            _body.cbTypes.add(CallbackTypes.KINEMATIC_TYPE);
        }

        override protected function _addGraphics():void
        {
            throw new Error("method must be overriden!");
        }

        override protected function _defineFilter():void
        {
            _filter = new InteractionFilter();
            _filter.collisionGroup = 0x0100; // третья группа объектов
            _filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupShape():void
        {
            _body.shapes.add(new Polygon([Vec2.weak(242, 69.5), Vec2.weak(213.5, 55), Vec2.weak(178, 83.5), Vec2.weak(213, 84.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(-0.5, 66), Vec2.weak(57, 87.5), Vec2.weak(129, 87.5), Vec2.weak(178, 83.5), Vec2.weak(103, 44.5), Vec2.weak(79, 37.5), Vec2.weak(37, 42.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(106, 97.5), Vec2.weak(116, 106.5), Vec2.weak(188.5, 105), Vec2.weak(178, 83.5), Vec2.weak(129, 87.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(108, 34.5), Vec2.weak(120.5, 38), Vec2.weak(171, 47.5), Vec2.weak(167, 29.5), Vec2.weak(131, 27.5), Vec2.weak(113, 30.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(120.5, 38), Vec2.weak(103, 44.5), Vec2.weak(178, 83.5), Vec2.weak(171, 47.5)]));
            _body.shapes.add(new Polygon([Vec2.weak(213.5, 55), Vec2.weak(247.5, 2), Vec2.weak(247.5, 1), Vec2.weak(171, 47.5), Vec2.weak(178, 83.5)]));
            _body.setShapeFilters(_filter);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}
