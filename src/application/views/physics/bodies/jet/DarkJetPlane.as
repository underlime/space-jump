package application.views.physics.bodies.jet
{
    import application.views.particular.jet.JetData;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class DarkJetPlane extends JetPlane
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DarkJetPlane()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _addGraphics():void
        {
            this._graphic = this.source.getBitmap(JetData.JET_PLANE_DARK);
            this._wrapper.addChild(this._graphic);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}