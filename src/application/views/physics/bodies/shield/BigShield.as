package application.views.physics.bodies.shield
{
    import application.views.particular.shield.BigShieldAnimation;
    import application.views.physics.bodies.BodiesConfiguration;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.callbacks.CallbackTypes;

    import flash.events.Event;

    import framework.view.animation.Animation;

    import nape.dynamics.InteractionFilter;
    import nape.geom.Vec2;
    import nape.phys.BodyType;
    import nape.shape.Circle;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BigShield extends SpaceBody
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BigShield()
        {
            super();
            super.bodyWidth = 64;
            super.bodyHeight = 64;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _createBody():void
        {
            this._body.type = BodyType.DYNAMIC;

            var shape:Circle = new Circle(BodiesConfiguration.BIG_SHIELD_RADIUS, null, this._material, this._filter);
            this._body.shapes.add(shape);

            this._body.graphic = this._wrapper;
            this._body.cbTypes.add(CallbackTypes.BIG_SHIELD_TYPE);
            this._body.cbTypes.add(CallbackTypes.NO_HERO_TYPE);
        }

        override protected function _addGraphics():void
        {
            this._createAnimationVariant();
            this._animation.x = -1 * BodiesConfiguration.BIG_SHIELD_RADIUS;
            this._animation.y = -1 * BodiesConfiguration.BIG_SHIELD_RADIUS;
            this._wrapper.addChild(this._animation);
        }

        override protected function _defineFilter():void
        {
            this._filter = new InteractionFilter();
            this._filter.collisionGroup = 0x1001; // первая группа объектов
            this._filter.collisionMask = 0x0010; // сталкиваемся только с космонавтом
        }

        override protected function _modelChangeHandler(e:Event):void
        {
            var velocity:Number = ( -1) * this._model.velocity;
            this._body.velocity = new Vec2(0, velocity);
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:Animation;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _createAnimationVariant():void
        {
            this._animation = new BigShieldAnimation();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}