/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.10.12
 */

package application.views.load
{
    import application.views.base.ApplicationView;

    import flash.display.Bitmap;

    public class LoadView extends ApplicationView
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LoadView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
        }


        override public function destroy():void
        {
            trace("destroy load view");
            _loadBar.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap = new Bitmap(new SPACE_LOADER_BACK());
        private var _loadBar:LoaderBar = new LoaderBar();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _background.y = 275;
            addChild(_background);
            addChild(_loadBar);
            _loadBar.x = 275;
            _loadBar.y = 335;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set percent(value:Number):void
        {
            _loadBar.percent = value;
        }
    }
}
