package application.views.load 
{
	import application.views.screen.elements.BitmapProgressBar;
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import flash.utils.setInterval;
	import framework.view.View;
	
	/**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class LoaderBar extends View
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		
		private static const BAR_WIDTH:Number = 224;
		private static const BAR_HEIGHT:Number = 30;
		private static const START_X:Number = 13;
		
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function LoaderBar() 
		{
			super();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function setup():void 
		{
			this._progressBar = new BitmapProgressBar(new Bitmap(new SPACE_LOADER_BAR_FILL()));
		}
		
		override public function render():void 
		{
			this.addChild(this._progressBar);
			this._progressBar.x = START_X;
			this.addChild(this._bar);
			this.percent = 0;
		}
		
		override public function destroy():void 
		{
			this.removeChild(this._bar);
			this._progressBar.destroy();
			super.destroy();
		}
		
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		
		private var _progressBar:BitmapProgressBar;
		private var _bar:Bitmap = new Bitmap(new SPACE_LOADER_BAR());
		private var _percent:Number;
		
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
		
		public function get percent():Number 
		{
			return this._percent;
		}
		
		public function set percent(value:Number):void 
		{
			if (this._percent != value)
			{
				this._percent = value;
				this._progressBar.percent = this._percent;
			}
		}
	}

}