package application.views.text
{
    import application.config.ApplicationConfiguration;

    import flash.utils.ByteArray;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class TextFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _instance:TextFactory = null;

        public static function get instance():TextFactory
        {
            if (_instance == null) _instance = new TextFactory();
            return _instance;
        }

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TextFactory()
        {
            if (_instance != null) {
                throw new Error("Use instance property");
            }
            _loadData();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
		
		[Embed(source="../../../../lib/config/text_data.json", mimeType="application/octet-stream")]
        private var JSONConfig:Class;
        private var _config:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        /**
         * Получить заголовок по ключу
         * @param key ключ из конфига
         * @return
         */
        public function getLabel(key:String):String
        {
            var label:String = "{{no data}}";
            var regionData:Object = _config[ApplicationConfiguration.LANGUAGE];
            if (regionData[key])
                label = regionData[key].toString();

            return label;
        }

        /**
         * Получить объект из конфига по ключу
         * @param key
         * @return
         */
        public function getObject(key:String):Object
        {
            var obj:Object = {};
            var regionData:Object = _config[ApplicationConfiguration.LANGUAGE];
            if (regionData[key])
                obj = regionData[key];
            return obj;
        }

        /**
         * Возвращает значение по ключу и языку
         * @param key
         * @param lang
         * @return
         */
        public function getLabelByLang(key:String, lang:String):String
        {
            var label:String = "{{no data}}";
            var regionData:Object;
            if (_config[lang])
                regionData = _config[lang];
            else
                regionData = _config[ApplicationConfiguration.LANGUAGE];

            if (regionData[key])
                label = regionData[key].toString();

            return label;
        }


        private function _loadData():void
        {
            var bytes:ByteArray = new JSONConfig();
            var json:String = bytes.readUTFBytes(bytes.length);
            _config = JSON.parse(json);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}