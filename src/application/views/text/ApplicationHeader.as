package application.views.text 
{
	import flash.text.TextFormatAlign;
	import framework.view.text.SimpleTextField;
	
	/**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class ApplicationHeader extends SimpleTextField
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function ApplicationHeader() 
		{
			super();
			this._setupStyle();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		// PRIVATE METHODS ---------------------------------------------------------------------/
		
		private function _setupStyle():void 
		{
			this.size = 36;
			this.color = 0xB8FCFF;
			this.align = TextFormatAlign.CENTER;
		}
		
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
	}

}