/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.03.13
 * Time: 13:03
 */
package application.views.screen.elements.checkbox
{
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    import framework.view.View;
    import framework.view.text.SimpleTextField;

    public class BaseCheckBox extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const CHECK_BOX_DATA:String = "CHECK_BOX";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseCheckBox(label:String)
        {
            super();
            _label = label;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.buttonMode = true;
            super.useHandCursor = true;

            _setupBitmap();
            _setupTextField();

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OVER, _outHandler);
            addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OVER, _outHandler);
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        protected function _toggleBox():void
        {
            _checked = !_checked;
            if (_checked)
                _rectangle.y = _checkedPositionY;
            else
                _rectangle.y = _emptyPositionY;

            super.dispatchEvent(new Event(Event.CHANGE));
            _checkboxBitmap.scrollRect = _rectangle;
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _checkedPositionY:Number = 0;
        protected var _emptyPositionY:Number = 42;
        protected var _label:String = "";

        protected var _checkboxBitmap:Bitmap;
        protected var _txtLabel:SimpleTextField;

        protected var _checked:Boolean = true;
        protected var _rectangle:Rectangle;
        protected var _hoverMargin:int = 21;

        protected var _buttonWidth:Number = 20;
        protected var _buttonHeight:Number = 21;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBitmap():void
        {
            _checkboxBitmap = super.source.getBitmap(CHECK_BOX_DATA);
            addChild(_checkboxBitmap);

            _rectangle = new Rectangle(0, 0, _buttonWidth, _buttonHeight);
            _rectangle.y = _checkedPositionY;

            _checkboxBitmap.scrollRect = _rectangle;
        }

        private function _setupTextField():void
        {
            _txtLabel = new SimpleTextField();
            _txtLabel.size = 10;
            _txtLabel.color = 0xc3fafd;
            _txtLabel.autoSize = TextFieldAutoSize.LEFT;
            _txtLabel.x = 26;
            _txtLabel.y = 3;

            addChild(_txtLabel);

            _txtLabel.text = _label;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _overHandler(event:MouseEvent):void
        {
            _rectangle.y += _buttonHeight;
            _checkboxBitmap.scrollRect = _rectangle;
        }

        protected function _outHandler(event:MouseEvent):void
        {
            _rectangle.y -= _buttonHeight;
            _checkboxBitmap.scrollRect = _rectangle;
        }

        protected function _clickHandler(event:MouseEvent):void
        {
            _toggleBox();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get checked():Boolean
        {
            return _checked;
        }

        public function set label(value:String):void
        {
            _label = value;
            if (_txtLabel)
                _txtLabel.text = _label;
        }

        public function set checked(value:Boolean):void
        {
            if (value != _checked) {
                _toggleBox();
            }
        }
    }
}
