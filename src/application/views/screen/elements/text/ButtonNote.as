package application.views.screen.elements.text 
{
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormatAlign;
	import framework.view.text.SimpleTextField;
	
	/**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class ButtonNote extends SimpleTextField
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function ButtonNote(color:uint) 
		{
			super();
			this._color = color;
			this._setup();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		
		private var _color:uint;
		
		// PRIVATE METHODS ---------------------------------------------------------------------/
		
		private function _setup():void 
		{
			this.size = 12;
			this.align = TextFormatAlign.CENTER;
			this.multiline = true;
			this.wordWrap = true;
			this.autoSize = TextFieldAutoSize.CENTER;
			this.color = this._color;
		}
		
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
	}

}