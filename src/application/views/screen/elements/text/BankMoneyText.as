/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.01.13
 */

package application.views.screen.elements.text
{
    import flash.text.TextFieldAutoSize;

    import framework.view.text.SimpleTextField;

    public class BankMoneyText extends SimpleTextField
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankMoneyText()
        {
            super();
            this._setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            this.size = 20;
            this.autoSize = TextFieldAutoSize.LEFT;
            this.color = 0xb8fcff;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
