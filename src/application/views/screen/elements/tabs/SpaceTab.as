/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 11:00
 */
package application.views.screen.elements.tabs
{
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.view.tabs.BaseTab;
    import framework.view.text.SimpleTextField;

    public class SpaceTab extends BaseTab
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpaceTab(header:String)
        {
            _header = header;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function render():void
        {
            _setupTextField();
        }


        override public function destroy():void
        {
            this.graphics.clear();
            removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOutHandler);

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/


        override protected function _enableTab():void
        {
            _setEnableHeader();
        }

        override protected function _disableTab():void
        {
            _setDisableHeader();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _header:String;
        private var _txtHeader:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTextField():void
        {
            _txtHeader = new SimpleTextField();
            _txtHeader.autoSize = TextFieldAutoSize.LEFT;
            _txtHeader.align = TextFormatAlign.LEFT;
            addChild(_txtHeader);

            _setDisableHeader();
        }

        private function _setDisableHeader():void
        {
            _txtHeader.htmlText = "<font color='#3090AA' size='16'>" + _header + "</font>";
            _removeUnderline();
            this.buttonMode = true;
            this.useHandCursor = true;

            addEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            addEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
        }

        private function _setEnableHeader():void
        {
            _txtHeader.htmlText = "<font color='#FFFFFF' size='16'>" + _header + "</font>";
            _addUnderline();
            this.buttonMode = false;
            this.useHandCursor = false;

            removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
        }

        private function _addUnderline():void
        {
            this.graphics.beginFill(0xffffff, 1);
            this.graphics.drawRect(2, 22, this.width - 2, 4);
            this.graphics.endFill();
        }

        private function _removeUnderline():void
        {
            this.graphics.clear();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onOutHandler(event:MouseEvent):void
        {
            _txtHeader.htmlText = "<font color='#3090AA' size='16'>" + _header + "</font>";
        }

        private function _onOverHandler(event:MouseEvent):void
        {
            _txtHeader.htmlText = "<font color='#80DEFF' size='16'>" + _header + "</font>";
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get header():String
        {
            return _header;
        }
    }
}
