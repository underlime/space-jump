/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 11:25
 */
package application.views.screen.elements.tabs
{
    import framework.view.tabs.BaseTab;
    import framework.view.tabs.BaseTabPanel;

    public class SpaceTabPanel extends BaseTabPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpaceTabPanel(tabs:Vector.<BaseTab>)
        {
            super(tabs);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function setup():void
        {
            for (var i:int = 0; i < super.tabs.length; i++) {
                super.tabs[i].tabID = i;
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _addTabs():void
        {
            var posX:Number = 0;
            for (var i:int = 0; i < super.tabs.length; i++) {
                addChild(super.tabs[i]);
                super.tabs[i].x = posX;

                posX += super.tabs[i].width + _margin;
            }
            _addUnderline();
        }

        private function _addUnderline():void
        {
            this.graphics.beginFill(0x80DEFF, 1);
            this.graphics.lineStyle(1, 0x80DEFF);
            this.graphics.moveTo(2, 26);
            this.graphics.lineTo(705, 26);
            this.graphics.endFill();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _margin:int = 35;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
