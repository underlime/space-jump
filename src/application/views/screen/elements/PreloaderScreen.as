/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.01.13
 */

package application.views.screen.elements
{
    import flash.display.Bitmap;
	import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;

    import framework.view.View;

    public class PreloaderScreen extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PreloaderScreen()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._preloader = this.source.getMovieClip(PRELOADER_DATA, true);
        }

        override public function render():void
        {
            this._drawBackground();
            this._addPreloader();
        }

        override public function destroy():void
        {
            this.removeEventListener(Event.ENTER_FRAME, this._enterFrameHandler);
            this.removeChild(this._preloader);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private const PRELOADER_DATA:String = "SPACE_SMALL_LOADER_VECTOR";

        private var _preloader:MovieClip;
        private var _deltaAngle:Number = 20;
        private var _currentAngle:Number = 5;
        private var _cont:Sprite;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _drawBackground():void
        {
            this.graphics.beginFill(0x000000,.8);
            this.graphics.drawRect(0, 0, this.stage.stageWidth, this.stage.stageHeight);
            this.graphics.endFill();
        }

        private function _addPreloader():void
        {
            this._cont = new Sprite();
            this._cont.addChild(this._preloader);
            this._cont.x = 400;
            this._cont.y = 350;

            this.addChild(this._cont);
            this.addEventListener(Event.ENTER_FRAME, this._enterFrameHandler);

            this._preloader.x = -25;
            this._preloader.y = -25;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _enterFrameHandler(event:Event):void
        {
            /*
            var transformMatrix:Matrix = this._preloaderBitmap.transform.matrix;
            var axisPoint:Point = new Point(25, 25);

            transformMatrix.rotate(PhysicsHelper.convertAngle(this._deltaAngle));

            var deltaPoint:Point = transformMatrix.transformPoint(axisPoint);
            var deltaX : Number = axisPoint.x - deltaPoint.x;
            var deltaY : Number = axisPoint.y - deltaPoint.y;

            transformMatrix.translate(deltaX, deltaY);

            this._preloaderBitmap.transform.matrix = transformMatrix;
            */
			
            this._currentAngle += this._deltaAngle;
			
            if (this._currentAngle >= 360)
                this._currentAngle -= 360;

            this._cont.rotation = (-1) * this._currentAngle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
