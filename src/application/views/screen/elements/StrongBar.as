package application.views.screen.elements
{
    import flash.display.Bitmap;
    import flash.text.TextFormatAlign;

    import framework.tools.SourceManager;
    import framework.view.View;
    import framework.view.text.SimpleTextField;

    public class StrongBar extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const PROGRESS_BAR_DATA:String = "SPACE_SOLIDITY_FILL";
        public static const PROGRESS_COLBA_DATA:String = "SPACE_SOLIDITY_BAR";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StrongBar(current:int, total:int)
        {
            _current = current;
            _total = total;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function render():void
        {
            _setupProgress();
            _setupColba();
            _setupLabel();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bitmapProgress:Bitmap = SourceManager.instance.getBitmap(StrongBar.PROGRESS_BAR_DATA);
        private var _progressBar:BitmapProgressBar;
        private var _bitmapColba:Bitmap = SourceManager.instance.getBitmap(StrongBar.PROGRESS_COLBA_DATA);
        private var _label:SimpleTextField = new SimpleTextField();

        private var _total:int = 0;
        private var _current:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupColba():void
        {
            addChild(_bitmapColba);
            _bitmapColba.y = 17;
        }

        private function _setupProgress():void
        {
            _progressBar = new BitmapProgressBar(_bitmapProgress);
            _progressBar.x = 8;
            _progressBar.y = 17;
            addChild(_progressBar);

            _progressBar.percent = Math.round((_current / _total) * 100);
        }

        private function _setupLabel():void
        {
            _label.width = 150;
            _label.align = TextFormatAlign.CENTER;
            addChild(_label);

            _label.htmlText = "<font size='9' color='#ff8f8f'>" + _current.toString() + "</font><font size='9' color=''> / " + _total.toString() + "</font>"
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get current():int
        {
            return _current;
        }

        public function get total():int
        {
            return _total;
        }
    }
}
