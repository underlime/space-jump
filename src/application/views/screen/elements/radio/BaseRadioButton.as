/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 15:45
 */
package application.views.screen.elements.radio
{
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.screen.elements.checkbox.BaseCheckBox;

    import flash.events.Event;
    import flash.events.MouseEvent;

    public class BaseRadioButton extends BaseCheckBox
    {
        public static const BUTTON_HEIGHT:int = 21;

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseRadioButton(label:String)
        {
            super(label);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _checkedPositionY = 126;
            _emptyPositionY = 84;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _clickHandler(event:MouseEvent):void
        {
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
            super.dispatchEvent(new Event(Event.CHANGE));
        }

        override protected function _overHandler(event:MouseEvent):void
        {
            if (!super.checked) {
                _rectangle.y += _buttonHeight;
                _checkboxBitmap.scrollRect = _rectangle;
            }
        }

        override protected function _outHandler(event:MouseEvent):void
        {
            if (!super.checked) {
                _rectangle.y = _emptyPositionY;
                _checkboxBitmap.scrollRect = _rectangle;
            }
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
