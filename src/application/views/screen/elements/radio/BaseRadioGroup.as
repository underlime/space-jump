/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 16:03
 */
package application.views.screen.elements.radio
{
    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.view.View;

    public class BaseRadioGroup extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseRadioGroup(radioButtons:Vector.<BaseRadioButton>)
        {
            _radioButtons = radioButtons;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setDefaultPositions();
        }

        override public function destroy():void
        {
            for (var i:int = 0; i < _radioButtons.length; i++) {
                _radioButtons[i].removeEventListener(MouseEvent.CLICK, _changeHandler);
                _radioButtons[i].destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function checkRadio(index:int):void
        {
            if (_radioButtons[index])
            {
                for (var i:int = 0; i < _radioButtons.length; i++)
                {
                    _radioButtons[i].checked = false;
                }
                _radioButtons[index].checked = true;
                _currentCheckedIndex = index;
                super.dispatchEvent(new Event(Event.CHANGE));
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _radioButtons:Vector.<BaseRadioButton>;
        private var _currentCheckedIndex:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setDefaultPositions():void
        {
            for (var i:int = 0; i < _radioButtons.length; i++) {
                addChild(_radioButtons[i]);
                _radioButtons[i].checked = false;
                _radioButtons[i].addEventListener(MouseEvent.CLICK, _changeHandler);
            }
            _radioButtons[0].checked = true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:MouseEvent):void
        {
            var target:BaseRadioButton = event.currentTarget as BaseRadioButton;

            var index:int = _radioButtons.indexOf(target);
            if (index > -1) {
                if (index != _currentCheckedIndex) {
                    _radioButtons[_currentCheckedIndex].checked = false;
                    _radioButtons[index].checked = true;
                    _currentCheckedIndex = index;
                    super.dispatchEvent(new Event(Event.CHANGE));
                }
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
