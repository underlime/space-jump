/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.02.13
 * Time: 15:09
 */
package application.views.screen.elements
{
    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.view.View;

    public class UpgradeProgress extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const PROGRESS_DATA:String = "SPACE_UPGRADE_LEVELS";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UpgradeProgress()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function setup():void
        {
            _progress = super.source.getBitmap(PROGRESS_DATA);
            _progress.scrollRect = _rectangle;
            addChild(_progress);
        }


        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _level:int = 0;
        private var _progress:Bitmap;
        private var _rectangle:Rectangle = new Rectangle(0, 0, 198, 25);
        private var _levelHeight:Number = 25;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateProgress():void
        {
            _rectangle.y = _level * _levelHeight;
            _progress.scrollRect = _rectangle;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set level(value:int):void
        {
            if (value >= 0 && value <= 5) {
                _level = value;
                _updateProgress();
            }
        }
    }
}
