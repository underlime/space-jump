package application.views.screen.elements 
{
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import framework.view.View;
	
	/**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class BitmapProgressBar extends View
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function BitmapProgressBar(progressBar:Bitmap) 
		{
			this._progressBar = progressBar;
			super();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function setup():void 
		{
			this._barWidth = this._progressBar.width;
			this._rectangle = new Rectangle(0, 0, this._barWidth, this._progressBar.height);
		}
		
		override public function render():void 
		{
			this.addChild(this._progressBar);
			this._progressBar.scrollRect = this._rectangle;
		}
		
		override public function destroy():void 
		{
			this._progressBar.scrollRect = null;
			this._rectangle = null;
			this.removeChild(this._progressBar);
			this._progressBar = null;
			super.destroy();
		}
		
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		
		private var _progressBar:Bitmap;
		private var _rectangle:Rectangle;
		private var _percent:Number = 0;
		private var _barWidth:Number;
		
		// PRIVATE METHODS ---------------------------------------------------------------------/
		
		private function _updatePosition():void 
		{
			this._rectangle.width = (this._percent / 100) * this._barWidth;
			this._progressBar.scrollRect = this._rectangle;
		}
		
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
		
		public function get percent():Number 
		{
			return this._percent;
		}
		
		public function set percent(value:Number):void 
		{
			if (value >= 0 && value <= 100)
			{
				this._percent = value;
				this._updatePosition();
			}
		}
	}

}