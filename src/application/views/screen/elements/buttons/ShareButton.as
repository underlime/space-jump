package application.views.screen.elements.buttons 
{
    import application.views.screen.result.ResultData;
    import application.views.text.TextFactory;

    /**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class ShareButton extends VerticalNoteButton
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function ShareButton() 
		{
			super(ResultData.SHARE_BUTTON, TextFactory.instance.getLabel('share_info_label'));
			super.color = 0xffffff;
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		
		override protected function _setButton():void 
		{
			_button = this.source.getBitmap(_buttonDefinition);
			_buttonWidth = 300;
			_buttonHeight = 50;
		}
		
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
	}

}