package application.views.screen.elements.buttons
{
    import application.views.screen.result.ResultData;
    import application.views.text.TextFactory;

    public class RepairButton extends VerticalNoteButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RepairButton()
        {
            super(ResultData.REPAIR_BUTTON, TextFactory.instance.getLabel('repair_label'));
            this.color = 0xB8FCFF;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/


        override protected function _setButton():void
        {
            this._button = this.source.getBitmap(this._buttonDefinition);
            this._buttonWidth = 67;
            this._buttonHeight = 25;
            this._verticalMargin = -2;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
