/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.03.13
 * Time: 15:22
 */
package application.views.screen.elements.buttons
{

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    import framework.view.View;
    import framework.view.text.SimpleTextField;

    public class UniversalButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const UNIVERSAL_DATA:String = "UNIVERSAL_BUTTON";
        public static const BUTTON_WIDTH:int = 145;
        public static const BUTTON_HEIGHT:int = 50;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UniversalButton(label:String, warning:Boolean = false)
        {
            _label = label;
            _warning = warning;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupBackground();
            _setupLabel();
        }

        override public function render():void
        {
            _renderButton();
            _renderLabel();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _button:Bitmap;
        protected var _rectangle:Rectangle;
        protected var _rectangleMargin:Number = 50;
        protected var _warningMargin:Number = 100;
        protected var _txtLabel:SimpleTextField;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupBackground():void
        {
            _button = super.source.getBitmap(UNIVERSAL_DATA);
            _rectangle = new Rectangle(0, 0, 145, 50);
            if (_warning)
                _rectangle.y = _warningMargin;
            _button.scrollRect = _rectangle;
        }

        protected function _setupLabel():void
        {
            _txtLabel = new SimpleTextField();
            _txtLabel.width = 145;
            _txtLabel.size = 16;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.height = 20;
        }

        protected function _renderButton():void
        {
            addChild(_button);
            buttonMode = true;
            useHandCursor = true;
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        protected function _renderLabel():void
        {
            addChild(_txtLabel);
            _txtLabel.text = _label;
            _updateVerticalPosition();
        }

        protected function _updateVerticalPosition():void
        {
            var height:Number = _txtLabel.textHeight;
            _txtLabel.y = _button.y + _button.scrollRect.height/2 - height/2;
        }

        protected function _unbindReactions():void
        {
            buttonMode = false;
            useHandCursor = false;
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _label:String;
        private var _warning:Boolean;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _overHandler(event:MouseEvent):void
        {
            _rectangle.y += _rectangleMargin;
            _button.scrollRect = _rectangle;
        }

        protected function _outHandler(event:MouseEvent):void
        {
            _rectangle.y -= _rectangleMargin;
            _button.scrollRect = _rectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
