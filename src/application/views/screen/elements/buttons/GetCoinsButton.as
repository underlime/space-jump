package application.views.screen.elements.buttons
{
    import application.views.screen.main.panel.start.VerticalButton;

    public class GetCoinsButton extends VerticalButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const ButtonData:String = "SPACE_GET_MORE_COINS";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GetCoinsButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function setup():void
        {
            this._button = this.source.getBitmap(GetCoinsButton.ButtonData);
            this._buttonWidth = 150;
            this._buttonHeight = 30;
            this._lanMargin = 60;
            super.setup();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
