package application.views.screen.elements.buttons
{
    import application.views.screen.elements.text.ButtonNote;
    import application.views.screen.main.panel.start.VerticalButton;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class VerticalNoteButton extends VerticalButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VerticalNoteButton(buttonDefinition:String, note:String)
        {
            _buttonDefinition = buttonDefinition;
            _note = note;

            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setButton();
            _lanMargin = 0;
            super.setup();
        }

        override public function render():void
        {
            super.render();
            _setupNote();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var color:uint = 0x000000;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _buttonDefinition:String;
        protected var _txtNote:ButtonNote;
        protected var _verticalMargin:Number = 0;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setButton():void
        {
            _button = this.source.getBitmap(_buttonDefinition);
            _buttonWidth = 145;
            _buttonHeight = 50;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _note:String;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupNote():void
        {
            _txtNote = new ButtonNote(this.color);
            _txtNote.width = _button.width;
            this.addChild(_txtNote);
            _txtNote.text = _note;
            _calculateY();
        }

        private function _calculateY():void
        {
            var height:Number = _txtNote.textHeight;
            _txtNote.y = _button.y + _button.scrollRect.height / 2 - height / 2 + _verticalMargin;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}