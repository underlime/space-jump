package application.views.screen.elements.buttons
{
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.screen.ScreenData;
    import application.views.screen.main.panel.start.VerticalButton;

    import flash.events.MouseEvent;

    /**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class CloseButton extends VerticalButton
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function CloseButton() 
		{
			super();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function setup():void 
		{
			_button = this.source.getBitmap(ScreenData.CLOSE_BUTTON);
			_buttonWidth = 106;
			_buttonHeight = 30;
			_lanMargin = 60;
			super.setup();

            addEventListener(MouseEvent.CLICK, _clickHandler);
		}

        override public function destroy():void
        {
            removeEventListener(MouseEvent.CLICK, _clickHandler);
            super.destroy();
        }

        private function _clickHandler(event:MouseEvent):void
        {
            SoundManager.instance.playUISound(UISounds.BUTTON_CLOSE);
        }
		
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
	}

}