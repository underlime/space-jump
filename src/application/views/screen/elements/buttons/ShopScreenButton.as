/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 13:41
 */
package application.views.screen.elements.buttons
{
    import application.views.screen.elements.text.ButtonNote;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;

    public class ShopScreenButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BUTTON_DATA:String = "SPACE_BUY_EQUIP_BUTTON";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShopScreenButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function setup():void
        {
            _arrColors[_normalState] = 0xffffff;
            _arrColors[_hoverState] = 0xffffff;
            _arrColors[_disableState] = 0x82a8bd;
            _arrColors[_doneState] = 0xFFF792;
        }

        override public function render():void
        {
            _setupButton();
        }

        override public function destroy():void
        {
            _disableButton();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public function disable():void
        {
            _disableButton();
            _rectangle.y = _disableState;
            _button.scrollRect = _rectangle;
        }

        public function _enable():void
        {
            _disableButton();
            _rectangle.y = _normalState;
            _button.scrollRect = _rectangle;
        }

        public function done():void
        {
            _disableButton();
            _rectangle.y = _doneState;
            _button.scrollRect = _rectangle;
        }

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _button:Bitmap;
        private var _rectangle:Rectangle = new Rectangle(0, 0, 132, 50);
        private var _normalState:Number = 50;
        private var _hoverState:Number = 100;
        private var _txtNote:ButtonNote;
        private var _doneState:Number = 150;
        private var _disableState:Number = 0;
        private var _header:String;
        private var _arrColors:Object = {};

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupButton():void
        {
            _button = super.source.getBitmap(BUTTON_DATA);
            _rectangle.y = _normalState;
            _button.scrollRect = _rectangle;
            addChild(_button);

            _enableButton();
        }

        private function _enableButton():void
        {
            _bindReactions();
            this.buttonMode = true;
            this.useHandCursor = true;
        }

        private function _bindReactions():void
        {
            addEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            addEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
        }

        private function _disableButton():void
        {
            _unbindReactions();
            this.buttonMode = false;
            this.useHandCursor = false;
        }

        private function _unbindReactions():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
        }

        private function _onOverHandler(event:MouseEvent):void
        {
            _rectangle.y = _hoverState;
            _button.scrollRect = _rectangle;
        }

        private function _onOutHandler(event:MouseEvent):void
        {
            _rectangle.y = _normalState;
            _button.scrollRect = _rectangle;
        }

        private function _setupNote():void
        {
            if (_txtNote && !_txtNote.destroyed)
                _txtNote.destroy();

            _txtNote = new ButtonNote(_arrColors[_rectangle.y]);
            _txtNote.width = _button.width;
            addChild(_txtNote);
            _txtNote.text = _header;

            _txtNote.y = _button.y + _button.scrollRect.height / 2 - _txtNote.textHeight / 2;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set header(value:String):void
        {
            _header = value;
            _setupNote();
        }
    }
}
