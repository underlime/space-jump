package application.views.screen.bank
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BankData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "SPACE_BANK_BG";
        public static const MONEY_SELECT:String = "SPACE_MONEY_SELECT";

        public static const MONEY_ICO_1:String = "SPACE_MONEY_TIER_1";
        public static const MONEY_ICO_2:String = "SPACE_MONEY_TIER_2";
        public static const MONEY_ICO_3:String = "SPACE_MONEY_TIER_3";
        public static const MONEY_ICO_4:String = "SPACE_MONEY_TIER_4";
        public static const MONEY_ICO_5:String = "SPACE_MONEY_TIER_5";
        public static const MONEY_ICO_6:String = "SPACE_MONEY_TIER_6";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}