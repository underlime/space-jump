/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.01.13
 */

package application.views.screen.bank
{
    import application.events.ApplicationEvent;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.screen.elements.text.BankMoneyText;
    import application.views.screen.elements.text.BankNoteText;
    import application.views.screen.shop.ShopData;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;

    import org.casalib.util.NumberUtil;

    public class MoneyPosition extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private const BACKGROUND_WIDTH:Number = 120;
        private const BACKGROUND_HEIGHT:Number = 150;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/


        /**
         * Плашка-варинта покупки на экране банка
         * @param money - количество покупаемой внутриигровой валюты
         * @param price - цена в валюте портала
         * @param note - подпись (например "20 голосов")
         * @param icon - иконка для плашки
         */
        public function MoneyPosition(money:int, price:int, note:String, icon:Bitmap = null)
        {
            _money = money;
            _price = price;
            _note = note;
            _icon = icon;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _setMoneyValue();
            _setNote();
            _setIcon();
            _bindReactions();
            this.buttonMode = true;
            this.useHandCursor = true;
        }

        override public function setup():void
        {
            super.setup();
            _gemsIco = super.source.getBitmap(ShopData.CURRENCY);
            _gemsIco.scrollRect = new Rectangle(0, 24, 24, 24);
        }

        override public function destroy():void
        {
            this.removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            this.removeEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            this.removeEventListener(MouseEvent.CLICK, _clickHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _money:int = 0;
        private var _note:String;
        private var _price:int;
        private var _icon:Bitmap;

        private var _background:Bitmap;
        private var _rectangle:Rectangle;

        private var _txtMoney:BankMoneyText;
        private var _txtNote:BankNoteText;

        private var _gemsIco:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _background = this.source.getBitmap(BankData.MONEY_SELECT);
            this.addChild(_background);

            _rectangle = new Rectangle(0, 0, BACKGROUND_WIDTH, BACKGROUND_HEIGHT);
            _background.scrollRect = _rectangle;
        }

        private function _setMoneyValue():void
        {
            _txtMoney = new BankMoneyText();
            this.addChild(_txtMoney);
            _txtMoney.text = NumberUtil.format(_money, " ", 3);
            _txtMoney.y = 100;

            addChild(_gemsIco);
            _gemsIco.y = 100;

            _txtMoney.x = 60 - (_txtMoney.textWidth + _gemsIco.width + 1) / 2;
            _gemsIco.x = _txtMoney.x + _txtMoney.width + 1;
        }

        private function _setNote():void
        {
            _txtNote = new BankNoteText();
            this.addChild(_txtNote);
            _txtNote.text = _note;
            _txtNote.y = 124;
        }

        private function _setIcon():void
        {
            if (_icon) {
                _icon.x = 20;
                _icon.y = 6;
                this.addChild(_icon);
            }
        }

        private function _bindReactions():void
        {
            this.addEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            this.addEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            this.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onOverHandler(event:MouseEvent):void
        {
            _rectangle.y = BACKGROUND_HEIGHT;
            _background.scrollRect = _rectangle;
        }

        private function _onOutHandler(event:MouseEvent):void
        {
            _rectangle.y = 0;
            _background.scrollRect = _rectangle;
        }

        private function _clickHandler(event:MouseEvent):void
        {
            var appEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.BUY_QUERY, true);
            appEvent.data['money'] = _money;
            appEvent.data['price'] = _price;
            super.dispatchEvent(appEvent);

            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
