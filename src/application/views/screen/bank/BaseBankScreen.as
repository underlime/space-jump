package application.views.screen.bank
{
    import application.events.ApplicationEvent;
    import application.views.base.ApplicationView;
    import application.views.screen.elements.buttons.CloseButton;
    import application.views.text.ApplicationHeader;

    import flash.display.Bitmap;
    import flash.display.Graphics;
    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BaseBankScreen extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseBankScreen(moneyVariants:Vector.<MoneyPosition>)
        {
            _moneyVariants = moneyVariants;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _drawBackground();
            _setBack();
            _setupCloseButton();
            _setupHeader();
            _renderVariants();
        }

        override public function destroy():void
        {
            graphics.clear();
            _closeButton.removeEventListener(MouseEvent.CLICK, _closeButtonClickHandler);
            _closeButton.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _closeButton:CloseButton;
        private var _background:Bitmap;
        private var _txtHeader:ApplicationHeader;
        private var _moneyVariants:Vector.<MoneyPosition>;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setBack():void
        {
            _background = super.source.getBitmap(BankData.BACKGROUND);
            _background.x = stage.stageWidth / 2 - _background.width / 2;
            _background.y = stage.stageHeight / 2 - _background.height / 2;
            addChild(_background);
        }

        private function _drawBackground():void
        {
            var gr:Graphics = this.graphics;
            gr.beginFill(0x000000, .8);
            gr.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            gr.endFill();
        }

        private function _setupCloseButton():void
        {
            _closeButton = new CloseButton();
            _closeButton.addEventListener(MouseEvent.CLICK, _closeButtonClickHandler);
            addChild(_closeButton);
            _closeButton.x = _background.x + _background.width - _closeButton.width - 20;
            _closeButton.y = _background.y + 23;
        }

        private function _renderVariants():void
        {
            var x0:Number = _background.x + 20;
            var dx:Number = 150;
            var y0:Number = _background.y + 81;
            var dy:Number = 178;

            var cx:Number = x0;
            var cy:Number = y0;

            var rawCount:int = 0;

            for (var i:int = 0; i < _moneyVariants.length; i++) {
                if (rawCount > 2) {
                    cx = x0;
                    cy += dy;
                    rawCount = 0;
                }

                addChild(_moneyVariants[i]);
                _moneyVariants[i].x = cx;
                _moneyVariants[i].y = cy;

                cx += dx;

                rawCount++;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeButtonClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        private function _setupHeader():void
        {
            _txtHeader = new ApplicationHeader();
            _txtHeader.align = TextFormatAlign.LEFT;
            _txtHeader.size = 28;
            addChild(_txtHeader);
            _txtHeader.text = super.textFactory.getLabel('bank_label');
            _txtHeader.x = _background.x + 20;
            _txtHeader.y = _background.y + 17;
            _txtHeader.width = 400;

        }
    }

}