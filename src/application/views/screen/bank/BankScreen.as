/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.01.13
 */

package application.views.screen.bank
{
    import framework.socnet.SocNet;
    import framework.socnet.SocNetMoney;
    import framework.tools.SourceManager;

    public class BankScreen extends BaseBankScreen
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankScreen(pricesData:Array, socNet:SocNet)
        {
            var variants:Vector.<MoneyPosition> = new Vector.<MoneyPosition>();
            var source:SourceManager = SourceManager.instance;

            var label:String,
                price:int;
            for (var i:int = 0; i < pricesData.length; ++i) {
                price = parseInt(pricesData[i]['price']);
                label = String(pricesData[i]['price']) + ' ' + SocNetMoney.getMoneyLabel(price, socNet);
                variants.push(
                        new MoneyPosition(
                                parseInt(pricesData[i]['gems']),
                                price,
                                label,
                                source.getBitmap(String(pricesData[i]['image']))
                        )
                );
            }

            super(variants);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
