package application.views.screen.main
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.UserNews;
    import application.models.UserNewsRead;
    import application.views.base.ApplicationView;

    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.filters.GlowFilter;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    import framework.data.ModelsRegistry;
    import framework.view.text.SimpleTextField;

    public class NewsButton extends ApplicationView
    {
        public static const BUTTON_WIDTH:int = 82;
        public static const BUTTON_HEIGHT:int = 91;

        [Embed(source="../../../../../lib/news/NOTIFY_ICO.png")]
        private var _ButtonBackground:Class;

        private var _buttonBg:DisplayObject;
        private var _newsCount:SimpleTextField;
        private var _userNews:UserNews;
        private var _userNewsRead:UserNewsRead;

        public function NewsButton()
        {
        }

        override public function render():void
        {
            _setup();
        }

        private function _setup():void
        {
            _userNews = ModelsRegistry.getModel(ModelData.USER_NEWS_MODEL) as UserNews;
            _userNewsRead = ModelsRegistry.getModel(ModelData.USER_NEWS_READ_MODEL) as UserNewsRead;

            _userNews.addEventListener(Event.CHANGE, _onNewsChange);
            _userNewsRead.addEventListener(Event.CHANGE, _onNewsChange);

            _setupViewRectangle();
            _setupBackground(false);
            _setupCountText();

            this.buttonMode = true;
            this.useHandCursor = true;
            this.addEventListener(MouseEvent.MOUSE_OVER, _onMouseOver);
            this.addEventListener(MouseEvent.MOUSE_OUT, _onMouseOut);
            this.addEventListener(MouseEvent.CLICK, _onClick);
        }

        private function _onNewsChange(e:Event):void
        {
            _setupCountText();
        }

        private function _setupViewRectangle():void
        {
            this.scrollRect = new Rectangle(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
        }

        private function _setupBackground(hover:Boolean):void
        {
            _destroyBackground();
            _buttonBg = new _ButtonBackground();
            var buttonRectangle:Rectangle = new Rectangle(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
            if (hover)
                buttonRectangle.y = BUTTON_HEIGHT;
            _buttonBg.scrollRect = buttonRectangle;
            addChildAt(_buttonBg, 0);
        }

        private function _onMouseOver(event:MouseEvent):void
        {
            _setupBackground(true);
        }

        private function _onMouseOut(event:MouseEvent):void
        {
            _setupBackground(false);
        }

        private function _destroyBackground():void
        {
            if (_buttonBg)
                removeChild(_buttonBg);
            _buttonBg = null;
        }

        private function _setupCountText():void
        {
            _destroyCountText();

            _newsCount = new SimpleTextField();
            _newsCount.align = TextFormatAlign.CENTER;
            _newsCount.width = 30;
            _newsCount.size = 14;
            _newsCount.x = (BUTTON_WIDTH - _newsCount.width)/2;
            _newsCount.y = (BUTTON_HEIGHT - 23);
            _newsCount.color = 0xC2FFFF;
            addChild(_newsCount);

            var newsUnreadCount:int = _userNews.length - _userNewsRead.length;
            _newsCount.htmlText = newsUnreadCount.toString();

            if (newsUnreadCount) {
                var filter:GlowFilter = new GlowFilter(0xFFFFFF, 0.15, 10, 10);
                _newsCount.filters = [filter];
            }
        }

        private function _destroyCountText():void
        {
            if (_newsCount)
                _newsCount.destroy();
            _newsCount = null;
        }

        private function _onClick(event:MouseEvent):void
        {
            var callNews:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_NEWS, true);
            this.dispatchEvent(callNews);
        }

        override public function destroy():void
        {
            _destroyBackground();
            _destroyCountText();
            super.destroy();
        }
    }
}
