package application.views.screen.main.panel.ping
{
    import application.views.screen.main.MainScreenData;
    import application.views.screen.main.panel.start.VerticalButton;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class PingButton extends VerticalButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PingButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._button = this.source.getBitmap(MainScreenData.PING_BUTTON);
            this._buttonWidth = 224;
            this._buttonHeight = 50;
            this._lanMargin = 100;

            super.setup();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}