package application.views.screen.main.panel.field
{
    import application.views.screen.main.MainScreenData;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MoneyField extends IconTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MoneyField(value:Number)
        {
            super(value);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _updateIconSource();
            _rectangle = new Rectangle(0, 0, 35, 35);
            _icon.scrollRect = _rectangle;
            _addSupplyIcon();

            super.setup();
        }

        override public function render():void
        {
            super.render();
            addChild(_iconSupply);
        }

        override public function destroy():void
        {
            this.removeChild(_icon);
            this.removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            this.removeEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _rectangle:Rectangle;

        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupIcon():void
        {
            this.addEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            this.addEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            this.buttonMode = true;
            this.useHandCursor = true;
            this.addChild(_icon);
            _icon.x = _rightEdge.x + _rightEdge.width;
        }

        protected function _updateIconSource():void
        {
            _icon = this.source.getBitmap(MainScreenData.MONEY_ICON);
        }

        override protected function _addBackground():void
        {
            super._addBackground();
            _updateSupplyPosition();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _iconSupply:Bitmap;
        private var _supplyRectangle:Rectangle = new Rectangle(0, 0, 19, 19);

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addSupplyIcon():void
        {
            _iconSupply = super.source.getBitmap(MainScreenData.ADD_SUPPLY);
            _iconSupply.scrollRect = _supplyRectangle;

            _iconSupply.x = int((-1) * _iconSupply.width / 2);
            _iconSupply.y = int(_rectangle.height - _supplyRectangle.height * 2 / 3) - 3;
        }

        private function _updateSupplyPosition():void
        {
            if (_iconSupply) {
                _iconSupply.x = Math.ceil(super.valueText.x - _leftEdge.width) - _iconSupply.width * 2 / 3;
                addChild(_iconSupply);
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onOverHandler(e:MouseEvent):void
        {
            _rectangle.y = 35;
            _icon.scrollRect = _rectangle;

            _supplyRectangle.y = 19;
            _iconSupply.scrollRect = _supplyRectangle;
        }

        private function _onOutHandler(e:MouseEvent):void
        {
            _rectangle.y = 0;
            _icon.scrollRect = _rectangle;

            _supplyRectangle.y = 0;
            _iconSupply.scrollRect = _supplyRectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}