package application.views.screen.main.panel.field
{
    import application.views.screen.main.MainScreenData;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;

    import framework.view.View;
    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class IconTextField extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function IconTextField(value:Number)
        {
            _value = value;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupTextField();
            _background = this.source.getBitmap(MainScreenData.TEXT_BACKGROUND);
            _leftEdge = this.source.getBitmap(MainScreenData.TEXT_BACK);
            _rightEdge = this.source.getBitmap(MainScreenData.TEXT_FORWARD);
            _setupIcon();
        }

        override public function render():void
        {
            _addTextField();
            _addBackground();
        }

        override public function destroy():void
        {
            this.graphics.clear();
            _valueText.destroy();
            this.removeChild(_leftEdge);
            this.removeChild(_rightEdge);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public function set value(value:Number):void
        {
            _value = value;
            if (_valueText) {
                _setNewTextValue();
            }
        }

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _icon:Bitmap;
        protected var _leftEdge:Bitmap;
        protected var _rightEdge:Bitmap;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupIcon():void
        {

        }

        protected function _addBackground():void
        {
            this.graphics.clear();
            this.addChild(_leftEdge);
            _leftEdge.x = Math.ceil(_valueText.x - _leftEdge.width);

            this.addChild(_rightEdge);
            _rightEdge.x = ( -1) * _rightEdge.width + _margin;

            this.graphics.beginBitmapFill(_background.bitmapData);
            this.graphics.drawRect(_valueText.x, 0, _valueText.width, 35);
            this.graphics.endFill();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _value:Number;
        private var _valueText:SimpleTextField;
        private var _margin:Number = 6;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addTextField():void
        {
            this.addChild(_valueText);
            _setNewTextValue();
            _updateTextPosition();
        }

        private function _setNewTextValue():void
        {
            _valueText.text = NumberUtil.format(_value, " ", 3);
            _updateTextPosition();
            _addBackground();
        }

        private function _updateTextPosition():void
        {
            _valueText.x = ( -1) * _valueText.width - _rightEdge.width + _margin;
        }

        private function _setupTextField():void
        {
            _valueText = new SimpleTextField();
            _valueText.color = 0xB8FCFF;
            _valueText.size = 24;
            _valueText.autoSize = TextFieldAutoSize.LEFT;
            _valueText.y = 3;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get valueText():SimpleTextField
        {
            return _valueText;
        }
    }
}