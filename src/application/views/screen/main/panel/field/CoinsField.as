/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.06.13
 * Time: 10:41
 */
package application.views.screen.main.panel.field
{
    import application.views.screen.main.MainScreenData;

    import flash.geom.Rectangle;

    public class CoinsField extends IconTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CoinsField(value:Number)
        {
            super(value);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _icon = this.source.getBitmap(MainScreenData.MONEY_ICON);
            _icon.scrollRect = new Rectangle(0, 0, 35, 35);
            super.setup();
        }

        override public function destroy():void
        {
            this.removeChild(_icon);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupIcon():void
        {
            this.addChild(_icon);
            _icon.x = _rightEdge.x + _rightEdge.width;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
