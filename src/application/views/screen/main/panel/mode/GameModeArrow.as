package application.views.screen.main.panel.mode
{
    import application.views.screen.main.MainScreenData;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class GameModeArrow extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameModeArrow()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._button = this.source.getBitmap(MainScreenData.GAME_MODE_ARROW);
            this._rectangle = new Rectangle(0, 0, this._buttonWidth, this._buttonHeight);
        }

        override public function render():void
        {
            this.addChild(this._button);
            this._button.scrollRect = this._rectangle;
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function enable():void
        {
            this._rectangle.y = this._buttonHeight;
            this._button.scrollRect = this._rectangle;
            this._bindButton();
            this._enabled = true;
        }

        public function disable():void
        {
            this._rectangle.y = 0;
            this._button.scrollRect = this._rectangle;
            this._unbindButton();
            this._enabled = false;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _button:Bitmap;
        private var _buttonWidth:Number = 50;
        private var _buttonHeight:Number = 50;
        private var _rectangle:Rectangle;

        private var _enabled:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _bindButton():void
        {
            this.addEventListener(MouseEvent.ROLL_OVER, this._onOverHandler);
            this.addEventListener(MouseEvent.ROLL_OUT, this._onOutHandler);

            this.buttonMode = true;
            this.useHandCursor = true;
        }

        private function _unbindButton():void
        {
            this.removeEventListener(MouseEvent.ROLL_OVER, this._onOverHandler);
            this.removeEventListener(MouseEvent.ROLL_OUT, this._onOutHandler);

            this.buttonMode = false;
            this.useHandCursor = false;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onOutHandler(e:MouseEvent):void
        {
            this._rectangle.y = this._buttonHeight;
            this._button.scrollRect = this._rectangle;
        }

        private function _onOverHandler(e:MouseEvent):void
        {
            this._rectangle.y = this._buttonHeight * 2;
            this._button.scrollRect = this._rectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get enabled():Boolean
        {
            return this._enabled;
        }
    }

}