package application.views.screen.main.panel.mode
{
    import application.config.ApplicationConfiguration;
    import application.config.GameMode;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.base.ApplicationView;
    import application.views.screen.main.MainScreenData;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class GameModeSelect extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameModeSelect()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _translateObject = super.textFactory.getObject("game_mode_label");
            _setupBackground();
            _setLabel();
            _setRightArrow();
            _setLeftArrow();
        }

        override public function render():void
        {
            addChild(_back);

            addChild(_modeLabel);
            _modeLabel.text = _translateObject[GameMode.VARIANTS[_currentModeIndex]];

            addChild(_rightArrow);
            _rightArrow.addEventListener(MouseEvent.CLICK, _slideForward);
            _rightArrow.enable();

            addChild(_leftArrow);
            _leftArrow.addEventListener(MouseEvent.CLICK, _slideBack);
        }

        override public function destroy():void
        {
            removeChild(_back);
            _leftArrow.removeEventListener(MouseEvent.CLICK, _slideBack);
            _rightArrow.removeEventListener(MouseEvent.CLICK, _slideForward);
            _rightArrow.destroy();
            _leftArrow.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _back:Bitmap;
        private var _backWidth:Number = 266;
        private var _backHeight:Number = 68;
        private var _modeLabel:SimpleTextField;

        private var _currentModeIndex:int = 0;
        private var _rightArrow:GameModeArrow;
        private var _leftArrow:GameModeArrow;

        private var _translateObject:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _checkButtons():void
        {
            if (_currentModeIndex == 0) {
                _leftArrow.disable();
            }

            if (_currentModeIndex == GameMode.VARIANTS.length - 1) {
                _rightArrow.disable();
            }
        }

        private function _setLeftArrow():void
        {
            _leftArrow = new GameModeArrow();
            _leftArrow.x = 50;
            _leftArrow.y = 16;
            _leftArrow.scaleX = -1;
        }

        private function _setRightArrow():void
        {
            _rightArrow = new GameModeArrow();
            _rightArrow.x = 216;
            _rightArrow.y = 16;
        }

        private function _setLabel():void
        {
            _modeLabel = new SimpleTextField();
            _modeLabel.size = 20;
            _modeLabel.color = 0xC2FFFF;
            _modeLabel.width = _backWidth;
            _modeLabel.y = 28;
            _modeLabel.align = TextFormatAlign.CENTER;
        }

        private function _setupBackground():void
        {
            _back = super.source.getBitmap(MainScreenData.GAME_MODE_BACKGROUND);
            var y:Number = ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU ? 0 : _backHeight;
            _back.scrollRect = new Rectangle(0, y, _backWidth, _backHeight);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _slideForward(e:MouseEvent):void
        {
            if (!_rightArrow.enabled) {
                return;
            }
            if (_currentModeIndex < GameMode.VARIANTS.length - 1) {
                _currentModeIndex++;
                _modeLabel.text = _translateObject[GameMode.VARIANTS[_currentModeIndex]];
            }

            if (!_leftArrow.enabled) {
                _leftArrow.enable();
            }

            _checkButtons();
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _slideBack(e:MouseEvent):void
        {
            if (!_leftArrow.enabled) {
                return;
            }

            if (_currentModeIndex > 0) {
                _currentModeIndex--;
                _modeLabel.text = _translateObject[GameMode.VARIANTS[_currentModeIndex]];
            }

            if (!_rightArrow.enabled) {
                _rightArrow.enable();
            }

            _checkButtons();
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get mode():String
        {
            return GameMode.VARIANTS[_currentModeIndex];
        }
    }

}