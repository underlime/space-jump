package application.views.screen.main.panel.start
{
    import application.config.ApplicationConfiguration;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class VerticalButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function VerticalButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _rectangle = new Rectangle(0, 0, _buttonWidth, _buttonHeight);
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.EN) {
                _rectangle.y = _lanMargin;
            }
            _button.scrollRect = _rectangle;
        }

        override public function render():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;
            addEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            addEventListener(MouseEvent.ROLL_OUT, _onOutHandler);
            addChild(this._button);
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOutHandler);

            if (_button && contains(_button)) {
                removeChild(_button);
                _button = null;
            }

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var soundEnabled:Boolean = true;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _button:Bitmap;
        protected var _rectangle:Rectangle;
        protected var _buttonWidth:Number = 270;
        protected var _buttonHeight:Number = 100;
        protected var _lanMargin:Number = 200;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onOutHandler(e:MouseEvent):void
        {
            _rectangle.y -= _buttonHeight;
            _button.scrollRect = _rectangle;
        }

        private function _onOverHandler(e:MouseEvent):void
        {
            _rectangle.y += _buttonHeight;
            _button.scrollRect = _rectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}