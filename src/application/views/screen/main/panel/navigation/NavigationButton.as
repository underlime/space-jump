package application.views.screen.main.panel.navigation
{
    import application.config.ApplicationConfiguration;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class NavigationButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function NavigationButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;
            this._rectangle = new Rectangle(0, 0, this._buttonWidth, this._buttonHeight);
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.EN) {
                this._rectangle.x = this._buttonWidth;
            }
        }

        override public function render():void
        {
            if (!this._button) {
                throw new Error("_button data is not defined");
            }

            this.addEventListener(MouseEvent.ROLL_OVER, this._onOverHandler);
            this.addEventListener(MouseEvent.ROLL_OUT, this._onOutHandler);
            this.addChild(this._button);
            this._button.scrollRect = this._rectangle;
        }

        override public function destroy():void
        {
            this.removeEventListener(MouseEvent.ROLL_OVER, this._onOverHandler);
            this.removeEventListener(MouseEvent.ROLL_OUT, this._onOutHandler);
            this.removeChild(this._button);
            this._button.scrollRect = null;
            this._rectangle = null;
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var soundEnabled:Boolean = true;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _button:Bitmap;
        protected var _rectangle:Rectangle;

        protected var _buttonWidth:Number = 100;
        protected var _buttonHeight:Number = 100;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onOutHandler(e:MouseEvent):void
        {
            this._rectangle.y = 0;
            this._button.scrollRect = this._rectangle;
        }

        private function _onOverHandler(e:MouseEvent):void
        {
            this._rectangle.y = this._buttonHeight;
            this._button.scrollRect = this._rectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}