package application.views.screen.main.panel.tools
{
    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ToolButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ToolButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._rectangle = new Rectangle(0, 0, this._buttonWidth, this._buttonHeight);
        }

        override public function render():void
        {
            if (!this._button) {
                throw new Error("_button data is not defined");
            }

            this.addEventListener(MouseEvent.ROLL_OVER, this._onOverHandler);
            this.addEventListener(MouseEvent.ROLL_OUT, this._onOutHandler);

            this.addChild(this._button);
            this._button.scrollRect = this._rectangle;

            this.buttonMode = true;
            this.useHandCursor = true;
        }

        override public function destroy():void
        {
            this.removeChild(this._button);
            this.removeEventListener(MouseEvent.ROLL_OVER, this._onOverHandler);
            this.removeEventListener(MouseEvent.ROLL_OUT, this._onOutHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _button:Bitmap;
        protected var _rectangle:Rectangle;
        protected var _buttonWidth:Number = 50;
        protected var _buttonHeight:Number = 50;
        protected var _enableSound:Boolean = true;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onOutHandler(e:MouseEvent):void
        {
            this._rectangle.y -= this._buttonHeight;
            this._button.scrollRect = this._rectangle;
        }

        private function _onOverHandler(e:MouseEvent):void
        {
            this._rectangle.y += this._buttonHeight;
            this._button.scrollRect = this._rectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}