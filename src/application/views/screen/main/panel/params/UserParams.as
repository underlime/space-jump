package application.views.screen.main.panel.params
{
    import application.models.User;
    import application.views.base.ApplicationView;

    import flash.events.Event;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.helpers.MathHelper;
    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class UserParams extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UserParams(model:User)
        {
            _model = model;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupTextField();
            _setupNameField();
        }

        override public function render():void
        {
            addChild(_nameField);
            addChild(_paramsField);
            _paramsField.y = 40;
            _fillData();
            _model.addEventListener(Event.CHANGE, _fillData);
        }

        override public function destroy():void
        {
            _paramsField.destroy();
            _nameField.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _paramsField:SimpleTextField;
        private var _nameField:SimpleTextField;

        private var _model:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _fillData(e:Event = null):void
        {
            _addParamsData();
            _addNameData();
        }

        private function _addParamsData():void
        {
            var paramsData:String = "<p><font color='#83DBD8' size='14'>" + super.textFactory.getLabel('max_points_label') + " </font><font color='#C2FFFF' size='14'>" + NumberUtil.format(_model.max_score_global.value, " ", 6, "0") + "</font></p>";
            paramsData += "<p><font color='#83DBD8' size='14'>" + super.textFactory.getLabel('jump_count_label') + " </font><font color='#C2FFFF' size='14'>" + NumberUtil.format(_model.jumps_count.value, " ", 3, "0") + "</font></p>";
            paramsData += "<p><font color='#83DBD8' size='14'>" + super.textFactory.getLabel('landing_count_label') + " </font><font color='#C2FFFF' size='14'>" + NumberUtil.format(_model.landings_count.value, " ", 3, "0") + "</font></p>";

            var timeObject:Object = MathHelper.splitSeconds(_model.summary_time.value);
            var hours:String = NumberUtil.format(timeObject.hours, "", 2, "0");
            var minutes:String = NumberUtil.format(timeObject.minutes, "", 2, "0");
            var seconds:String = NumberUtil.format(timeObject.seconds, "", 2, "0");

            paramsData += "<p><font color='#83DBD8' size='14'>" + super.textFactory.getLabel('flying_time_label') + " </font><font color='#C2FFFF' size='14'>" + hours + super.textFactory.getLabel('hours_label') + " " + minutes + this.textFactory.getLabel('minutes_label') + " " + seconds + this.textFactory.getLabel('seconds_label') + "</font></p>";

            _paramsField.htmlText = paramsData;
        }

        private function _addNameData():void
        {
            var nameData:String = "<p><font color='#C2FFFF' size='14'><b>" + _model.first_name.value + "</b></font></p>";
            nameData += "<p><font color='#C2FFFF' size='14'><b>" + _model.last_name.value + "</b></font></p><br>";

            _nameField.htmlText = nameData;
        }

        private function _setupTextField():void
        {
            _paramsField = new SimpleTextField();
            _paramsField.autoSize = TextFieldAutoSize.RIGHT;
            _paramsField.align = TextFormatAlign.RIGHT;
            _paramsField.width = 245;
            _paramsField.multiline = true;
        }

        private function _setupNameField():void
        {
            _nameField = new SimpleTextField();

            _nameField.autoSize = TextFieldAutoSize.RIGHT;
            _nameField.align = TextFormatAlign.RIGHT;
            _nameField.width = 245;
            _nameField.multiline = true;
            _nameField.systemFamily = "Arial";
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}