/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.04.13
 * Time: 12:03
 */
package application.views.screen.main
{
    import application.views.base.ApplicationView;
    import application.views.construct.body.MainScreenJumper;

    import flash.display.Bitmap;
    import flash.display.DisplayObject;
    import flash.events.MouseEvent;
    import flash.utils.Dictionary;

    public class CosmosLayer extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CosmosLayer()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupBackground();
            _setSun();
            _setMoon();
            _setEarth();
            _setParallaxEffect();
        }

        override public function destroy():void
        {
            super.stage.removeEventListener(MouseEvent.MOUSE_MOVE, _mouseMoveHandler);
            if (_cosmonaut && !_cosmonaut.destroyed)
                _cosmonaut.destroy();
            if (_capsule && this.contains(_capsule))
                removeChild(_capsule);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _sun:Bitmap;
        private var _moon:Bitmap;
        private var _earth:Bitmap;

        private var _capsule:DisplayObject;
        private var _cosmonaut:MainScreenJumper;

        private var _parallax:Dictionary;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            var background:Bitmap = super.source.getBitmap(MainScreenData.BACKGROUND);
            addChild(background);
        }

        private function _setSun():void
        {
            _sun = super.source.getBitmap(MainScreenData.SUN);
            _sun.y = 100;
            addChild(_sun);
        }

        private function _setMoon():void
        {
            _moon = super.source.getBitmap(MainScreenData.MOON);
            _moon.x = 271;
            _moon.y = 49;
            addChild(_moon);
        }

        private function _setEarth():void
        {
            _earth = super.source.getBitmap(MainScreenData.EARTH);
            _earth.x = 22;
            _earth.y = 315;
            addChild(_earth);
        }

        private function _setParallaxEffect():void
        {
            _setupParallaxData();
            super.stage.addEventListener(MouseEvent.MOUSE_MOVE, _mouseMoveHandler);
        }

        private function _setupParallaxData():void
        {
            _parallax = new Dictionary(false);

            _parallax[_earth] = {
                "default_x": _earth.x,
                "coefficient_x": 0.015,
                "default_y": _earth.y,
                "coefficient_y": 0.015
            };

            _parallax[_moon] = {
                "default_x": _moon.x,
                "coefficient_x": 0.01,
                "default_y": _moon.y,
                "coefficient_y": 0.01
            };

            _parallax[_sun] = {
                "default_x": _sun.x,
                "coefficient_x": -0.01,
                "default_y": _sun.y,
                "coefficient_y": -0.01
            };
        }

        private function _addCapsule():void
        {
            addChild(_capsule);
            _parallax[_capsule] = {
                "default_x": 360,
                "coefficient_x": 0.04,
                "default_y": -47,
                "coefficient_y": 0.04
            };
            _capsule.x = _parallax[_capsule].default_x;
            _capsule.y = _parallax[_capsule].default_y;
            _definePositions();
        }

        private function _removeCapsule():void
        {
            if (this.contains(_capsule))
                removeChild(_capsule);
            delete _parallax[_capsule];
        }

        private function _addCosmonaut():void
        {
            addChild(_cosmonaut);
            _parallax[_cosmonaut] = {
                "default_x": 390,
                "coefficient_x": 0.04,
                "default_y": 212,
                "coefficient_y": 0.04
            };
            _cosmonaut.x = _parallax[_cosmonaut].default_x;
            _cosmonaut.y = _parallax[_cosmonaut].default_y;
            _definePositions();
        }

        private function _removeCosmonaut():void
        {
            if (this.contains(_cosmonaut))
                _cosmonaut.destroy();
            delete _parallax[_cosmonaut];
        }

        private function _definePositions():void
        {
            var mouseX:Number = super.stage.mouseX;
            var standard_x:Number = super.stage.stageWidth / 2;
            var deltaX:Number = mouseX - standard_x;

            var mouseY:Number = super.stage.mouseY;
            var standard_y:Number = super.stage.stageHeight / 2;
            var deltaY:Number = mouseY - standard_y;

            for (var index:* in _parallax) {
                (index as DisplayObject).x = _parallax[index].default_x + deltaX * _parallax[index].coefficient_x;
                (index as DisplayObject).y = _parallax[index].default_y + deltaY * _parallax[index].coefficient_y;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _mouseMoveHandler(event:MouseEvent):void
        {
            _definePositions();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set capsule(value:DisplayObject):void
        {
            _capsule = value;
            if (_capsule && this.contains(_capsule))
                _removeCapsule();
            _addCapsule();
        }

        public function set cosmonaut(value:MainScreenJumper):void
        {
            _cosmonaut = value;
            if (_cosmonaut)
                _removeCosmonaut();
            _addCosmonaut();
        }

        public function get cosmonaut():MainScreenJumper
        {
            return _cosmonaut;
        }
    }
}
