/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.10.12
 */
package application.views.screen.main
{

    public class MainScreenData
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SUN:String = "SPACE_SunData";
        public static const MOON:String = "SPACE_MoonData";
        public static const EARTH:String = "SPACE_EarthData";
        public static const CAPSULA:String = "SPACE_CapsulaData";
        public static const BACKGROUND:String = "Space_BackgroundData";

        public static const MISSION_BUTTON:String = "SPACE_MISSIONS_BUTTON";
        public static const RANK_BUTTON:String = "SPACE_RANKS_BUTTON";
        public static const SHOP_BUTTON:String = "SPACE_SHOP_BUTTON";

        public static const START_BUTTON:String = "SPACE_START_BUTTON";
        public static const PING_BUTTON:String = "SPACE_PING_BUTTON";

        public static const HELP_BUTTON:String = "SPACE_HELP_BUTTON";
        public static const SOUND_BUTTON:String = "SPACE_SOUND_BUTTON";
        public static const OPTIONS_BUTTON:String = "SPACE_OPTIONS_BUTTON";

        public static const GAME_MODE_BACKGROUND:String = "SPACE_GAME_MODE_SELECT";
        public static const GAME_MODE_ARROW:String = "SPACE_GAME_MODE_ARROW";

        public static const TEXT_BACKGROUND:String = "SPACE_MONEY_FIELD_VAR_2_Middle";
        public static const TEXT_BACK:String = "SPACE_MONEY_FIELD_VAR_2_LeftEdge";
        public static const TEXT_FORWARD:String = "SPACE_MONEY_FIELD_VAR_2_RightEdge";

        public static const MONEY_ICON:String = "SPACE_MONEY_ICON";
        public static const OBJECTIVES_ICON:String = "SPACE_MULTIPLIER_ICON";
        public static const TROPHY_BUTTON:String = "SPACE_TROPHIE_BUTTON";

        public static const BANK_SQUARE_BUTTON:String = "BANK_SQUARE_BUTTON";
        public static const VOENKOM:String = "VOENKOM";
        public static const GEMS_ICON:String = "GEMS_ICON";

        public static const PROFILE_BUTTON:String = "PROFILE_BUTTON";
        public static const ADD_SUPPLY:String = "ADD_SUPPLY";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
