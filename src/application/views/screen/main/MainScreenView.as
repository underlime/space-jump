/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.10.12
 */
package application.views.screen.main
{

    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.models.Inventory;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.models.skin.SkinModel;
    import application.models.skin.SkinsCollection;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.base.ApplicationView;
    import application.views.construct.Constructor;
    import application.views.construct.body.MainScreenJumper;
    import application.views.screen.dialog.AlertWindow;
    import application.views.screen.main.panel.params.UserParams;
    import application.views.screen.sound.SoundSettingsView;

    import com.greensock.TweenLite;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.data.ModelsRegistry;
    import framework.socnet.SocNet;
    import framework.view.View;

    public class MainScreenView extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MainScreenView(model:User, skins:SkinsCollection)
        {
            _userModel = model;
            _skins = skins;
            super();

        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setLayers();
            _setupUserParams();
            _setInventory();
            _setupReactions();
        }

        override public function render():void
        {
            SoundManager.instance.playMenuTheme();
        }

        override public function destroy():void
        {
            _inventory.removeEventListener(Event.CHANGE, _setupInventoryItems);
            _userModel.removeEventListener(Event.CHANGE, _updateOneValueFields);

            _controlsLayer.soundButton.removeEventListener(MouseEvent.CLICK, _showSettings);
            _controlsLayer.startButton.removeEventListener(MouseEvent.CLICK, _startButtonClickHandler);

            _cosmonaut.destroy();
            _userParamsField.destroy();
            _cosmosLayer.destroy();
            _controlsLayer.destroy();

            if (_alertWindow) {
                _alertWindow.destroy();
                _alertWindow = null;
            }

            super.destroy()
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            if (_inventory && !_inventory.isEmpty)
                _setCosmonaut();
        }

        public function startGame():void
        {
            _controlsLayer.startButton.removeEventListener(MouseEvent.CLICK, _startButtonClickHandler);
            _cosmonaut.addEventListener(Event.COMPLETE, _callGameHandler);
            _cosmonaut.jump();
        }

        public function showMessage(text:String):void
        {
            if (_alertWindow)
                _alertWindow.destroy();
            _alertWindow = new AlertWindow(text);
            this.addChild(_alertWindow);
            _alertWindow.addEventListener(ApplicationEvent.CLICK_OK, _closeMessage);
            _alertWindow.render();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/


        private var _skins:SkinsCollection;

        private var _cosmosLayer:CosmosLayer;
        private var _controlsLayer:ControlLayer;
        private var _capsule:Bitmap;

        private var _cosmonaut:MainScreenJumper;
        private var _userParamsField:UserParams;

        private var _userModel:User;
        private var _inventory:Inventory;
        private var _settingsWindow:SoundSettingsView;
        private var _alertWindow:AlertWindow;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setLayers():void
        {
            _cosmosLayer = new CosmosLayer();
            _controlsLayer = new ControlLayer(_userModel);

            addChild(_cosmosLayer);
            addChild(_controlsLayer);
        }

        private function _setupUserParams():void
        {
            _userParamsField = new UserParams(_userModel);
            _controlsLayer.addChild(_userParamsField);
            _userParamsField.x = 30;
            _userParamsField.y = 200;
        }

        private function _setInventory():void
        {
            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY_MODEL) as Inventory;

            if (!_inventory.isEmpty)
                _setupInventoryItems();
            _inventory.addEventListener(Event.CHANGE, _setupInventoryItems)
        }

        private function _setupReactions():void
        {
            _controlsLayer.soundButton.addEventListener(MouseEvent.CLICK, _showSettings);
            _controlsLayer.startButton.addEventListener(MouseEvent.CLICK, _startButtonClickHandler);

            _userModel.addEventListener(Event.CHANGE, _updateOneValueFields);
        }

        private function _setCapsule():void
        {
            var item:ItemRecord = _inventory.getEquippedItem(Items.ACTION_CAPSULE);
            var capsuleData:String = (item && item.symbol.value) ? item.symbol.value : MainScreenData.CAPSULA;

            _capsule = super.source.getBitmap(capsuleData);
            _cosmosLayer.capsule = _capsule;
        }

        private function _setCosmonaut():void
        {
            var item:ItemRecord = _inventory.getEquippedItem(Items.ACTION_SUITE);
            var skinData:String = (item && item.symbol.value) ? item.symbol.value : "default";
            var skin:SkinModel = _skins.getSkin(skinData);
            var crownItem:ItemRecord = _inventory.getEquippedItem(Items.ACTION_CROWN);
            var hatItem:ItemRecord = _inventory.getEquippedItem(Items.ACTION_HAT);

            _cosmonaut = Constructor.getMainScreenJumper(skin);
            _cosmonaut.removeHat();

            if (crownItem && super.source.has(crownItem.symbol.value)) {
                _cosmonaut.addHat(crownItem.symbol.value);
            }
            else {
                if (hatItem && super.source.has(hatItem.symbol.value))
                    _cosmonaut.addHat(hatItem.symbol.value);
            }

            _cosmosLayer.cosmonaut = _cosmonaut;
            if (SocNet.instance().capabilities['set_avatar'])
                _cosmonaut.addEventListener(MouseEvent.CLICK, _cosmonautClickHandler);
        }


        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _setupInventoryItems(event:Event = null):void
        {
            _setCapsule();
            _setCosmonaut();
        }

        private function _showSettings(event:MouseEvent):void
        {
            if (_settingsWindow) {
                _settingsWindow.destroy();
                _settingsWindow = null;
            }
            else {
                _settingsWindow = new SoundSettingsView();
                addChild(_settingsWindow);
                _settingsWindow.x = 63;
                _settingsWindow.y = 562;
                _settingsWindow.alpha = 0;
                TweenLite.to(_settingsWindow, .3, {"alpha": 1});
            }

            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _updateOneValueFields(e:Event):void
        {
            _controlsLayer.objectivesField.value = _userModel.stars.value;
            _controlsLayer.moneyField.value = _userModel.coins.value;
            _controlsLayer.gemsField.value = _userModel.gems.value;
        }

        private function _startButtonClickHandler(event:MouseEvent):void
        {
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);

            // записываем в модель выбранную сложность игры
            var gameModel:GameModel = ModelsRegistry.getModel(ModelData.GAME_MODEL) as GameModel;
            gameModel.gameMode = _controlsLayer.gameMode.mode;

            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.SESSION_INIT));
        }

        private function _callGameHandler(event:Event):void
        {
            _cosmonaut.removeEventListener(Event.COMPLETE, _callGameHandler);
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_GAME));
        }

        private function _cosmonautClickHandler(event:MouseEvent):void
        {
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_AVATAR_GENERATOR));
        }

        private function _closeMessage(e:ApplicationEvent):void
        {
            if (_alertWindow) {
                _alertWindow.destroy();
                _alertWindow = null;
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get cosmosLayer():CosmosLayer
        {
            return _cosmosLayer;
        }

        public function get controlsLayer():View
        {
            return _controlsLayer;
        }

        public function get cosmonaut():MainScreenJumper
        {
            return _cosmonaut;
        }
    }
}
