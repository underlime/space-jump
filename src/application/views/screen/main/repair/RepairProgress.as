/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.04.13
 * Time: 14:44
 */
package application.views.screen.main.repair
{
    import flash.display.Bitmap;
    import flash.display.Shape;

    import framework.view.View;
    import framework.view.text.SimpleTextField;

    public class RepairProgress extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "REPAIR_BAR_BG";
        public static const BLUE_FILL:String = "REPAIR_BAR_FILL";
        public static const YELLOW_FILL:String = "REPAIR_BAR_BONUS_FILL";

        public static const BAR_WIDTH:int = 102;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RepairProgress()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBack();
            _setupShape();
            _setupTextField();
        }

        override public function destroy():void
        {
            _shape.graphics.clear();
            _txtVal.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function setYellowBar():void
        {
            _fillData = YELLOW_FILL;
            _updateBar();
        }

        public function setBlueBar():void
        {
            _fillData = BLUE_FILL;
            _updateBar();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtVal:SimpleTextField;

        private var _current:int = 0;
        private var _total:int = 1;
        private var _shape:Shape;

        private var _fillData:String = BLUE_FILL;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBack():void
        {
            var back:Bitmap = super.source.getBitmap(BACK);
            addChild(back);
        }

        private function _setupShape():void
        {
            _shape = new Shape();
            addChild(_shape);
        }

        private function _setupTextField():void
        {
            _txtVal = new SimpleTextField();
            _txtVal.width = 100;
            _txtVal.color = 0xffffff;
            _txtVal.size = 11;
            _txtVal.x = 6;
            _txtVal.y = 4;
            addChild(_txtVal);
        }

        private function _updateVal():void
        {
            _txtVal.text = _current.toString() + " / " + _total.toString();
            _updateBar();
        }

        private function _updateBar():void
        {
            var fillWidth:Number = _current * BAR_WIDTH / _total;

            _shape.graphics.clear();
            _shape.graphics.beginBitmapFill(super.source.getBitmapData(_fillData));
            _shape.graphics.drawRect(4, 4, fillWidth, 17);

            _shape.graphics.endFill();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set current(value:int):void
        {
            _current = value;
            _updateVal();
        }

        public function set total(value:int):void
        {
            _total = value;
            _updateVal();
        }
    }
}
