/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.04.13
 * Time: 14:38
 */
package application.views.screen.main.repair
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.SuiteParams;
    import application.models.User;
    import application.views.screen.elements.buttons.RepairButton;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.data.ModelsRegistry;
    import framework.view.View;

    public class MainScreenRepairBlock extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const ICON:String = "REPAIR_ICO";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MainScreenRepairBlock()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _userModel = ModelsRegistry.getModel(ModelData.USER_MODEL) as User;

            _setupIcon();
            _setupBar();
            _setupButton();
            _setupModel();
            _updateData();
        }

        override public function destroy():void
        {
            _suiteParams.removeEventListener(Event.CHANGE, _changeHandler);
            _bar.destroy();
            _button.removeEventListener(MouseEvent.CLICK, _clickHandler);
            _button.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;
        private var _button:RepairButton;
        private var _bar:RepairProgress;

        private var _suiteParams:SuiteParams;
        private var _userModel:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupModel():void
        {
            _suiteParams = ModelsRegistry.getModel(ModelData.SUITE_PARAMS) as SuiteParams;
            _suiteParams.addEventListener(Event.CHANGE, _changeHandler);
        }

        private function _setupIcon():void
        {
            _icon = super.source.getBitmap(ICON);
            addChild(_icon);
        }

        private function _setupBar():void
        {
            _bar = new RepairProgress();
            _bar.x = 28;
            addChild(_bar);
        }

        private function _setupButton():void
        {
            _button = new RepairButton();
            _button.x = 142;

            _button.addEventListener(MouseEvent.CLICK, _clickHandler);
            addChild(_button);
        }

        private function _updateData():void
        {
            if (_suiteParams.suiteItem) {
                _bar.current = _suiteParams.actualHp;
                _bar.total = _suiteParams.suiteItem.max_hp.value;
            }

            if (_userModel.jumps_count.value <= _suiteParams.free_jumps.value)
                _bar.setYellowBar();
            else
                _bar.setBlueBar();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _changeHandler(event:Event):void
        {
            _updateData();
        }

        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_REPAIR, true));
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
