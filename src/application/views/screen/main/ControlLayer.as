/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.04.13
 * Time: 14:16
 */
package application.views.screen.main
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.SuiteParams;
    import application.models.User;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.base.ApplicationView;
    import application.views.screen.help.HelpScreen;
    import application.views.screen.main.panel.field.CoinsField;
    import application.views.screen.main.panel.field.GemsField;
    import application.views.screen.main.panel.field.ObjectivesField;
    import application.views.screen.main.panel.mode.GameModeSelect;
    import application.views.screen.main.panel.navigation.BankSquareButton;
    import application.views.screen.main.panel.navigation.MissionButton;
    import application.views.screen.main.panel.navigation.RankButton;
    import application.views.screen.main.panel.navigation.ShopButton;
    import application.views.screen.main.panel.navigation.TrophyButton;
    import application.views.screen.main.panel.ping.PingButton;
    import application.views.screen.main.panel.start.StartButton;
    import application.views.screen.main.panel.tools.HelpButton;
    import application.views.screen.main.panel.tools.SoundButton;
    import application.views.screen.main.repair.MainScreenRepairBlock;
    import application.views.screen.sound.SoundSettingsView;

    import com.greensock.TweenLite;

    import flash.events.MouseEvent;

    import framework.data.ModelsRegistry;

    public class ControlLayer extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ControlLayer(model:User)
        {
            _model = model;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _suiteParams = ModelsRegistry.getModel(ModelData.SUITE_PARAMS) as SuiteParams;

            _setupMissionButton();
            _setupRankButton();
            _setupShopButton();
            _setupTrophyButton();
            _setupBankButton();
            _setupStartButton();
            _setupPingButton();
            _setupHelpButton();
            _setupSoundButtons();
            _setupGameMode();
            _setObjectivesField();
            _setMoneyField();
            _setGemsField();
            _setNews();
        }

        override public function render():void
        {
            _setRepairBlock();
        }

        override public function destroy():void
        {
            super.removeAllChildren(true);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _missionButton:MissionButton;
        private var _rankButton:RankButton;
        private var _shopButton:ShopButton;
        private var _trophyButton:TrophyButton;
        private var _bankButton:BankSquareButton;
        private var _startButton:StartButton;
        private var _pingButton:PingButton;
        private var _helpButton:HelpButton;
        private var _soundButton:SoundButton;
        private var _gameMode:GameModeSelect;
        private var _objectivesField:ObjectivesField;
        private var _moneyField:CoinsField;
        private var _settingsWindow:SoundSettingsView;
        private var _helpView:HelpScreen;
        private var _repairBlock:MainScreenRepairBlock;

        private var _model:User;
        private var _suiteParams:SuiteParams;

        private var _gemsField:GemsField;
        private var _newsButton:NewsButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupShopButton():void
        {
            _shopButton = new ShopButton();
            _shopButton.x = 230;
            _shopButton.y = 10;
            _shopButton.addEventListener(MouseEvent.CLICK, _shopButtonClickHandler);
            addChild(_shopButton);
        }

        private function _setupRankButton():void
        {
            _rankButton = new RankButton();
            _rankButton.x = 120;
            _rankButton.y = 10;
            addChild(_rankButton);
            _rankButton.addEventListener(MouseEvent.CLICK, _rateButtonClickHandler);
        }

        private function _setupMissionButton():void
        {
            _missionButton = new MissionButton();
            _missionButton.x = 10;
            _missionButton.y = 10;
            addChild(_missionButton);
            _missionButton.addEventListener(MouseEvent.CLICK, _missionButtonClickHandler);
        }

        private function _setupTrophyButton():void
        {
            _trophyButton = new TrophyButton();
            _trophyButton.x = 340;
            _trophyButton.y = 10;
            addChild(_trophyButton);
            _trophyButton.addEventListener(MouseEvent.CLICK, _trophyButtonClickHandler);
        }

        private function _setupBankButton():void
        {
            _bankButton = new BankSquareButton();
            _bankButton.x = 450;
            _bankButton.y = 10;
            addChild(_bankButton);
            _bankButton.addEventListener(MouseEvent.CLICK, _onMoneyClick);
        }

        private function _setupPingButton():void
        {
            _pingButton = new PingButton();
            _pingButton.x = 568;
            _pingButton.y = 640;
            addChild(_pingButton);
            _pingButton.addEventListener(MouseEvent.CLICK, _pingClickHandler);
        }

        private function _setupStartButton():void
        {
            _startButton = new StartButton();
            _startButton.x = 265;
            _startButton.y = 475;

            addChild(_startButton);
        }

        private function _setupGameMode():void
        {
            _gameMode = new GameModeSelect();
            addChild(_gameMode);
            _gameMode.x = 267;
            _gameMode.y = 622;
        }

        private function _setupSoundButtons():void
        {
            _soundButton = new SoundButton();
            _soundButton.x = 10;
            _soundButton.y = 580;
            addChild(_soundButton);

            _soundButton.addEventListener(MouseEvent.CLICK, _showSettings);
        }

        private function _setupHelpButton():void
        {
            _helpButton = new HelpButton();
            _helpButton.x = 10;
            _helpButton.y = 640;
            addChild(_helpButton);

            _helpButton.addEventListener(MouseEvent.CLICK, _showHelpScreen);
        }

        private function _setMoneyField():void
        {
            _moneyField = new CoinsField(_model.coins.value);
            addChild(_moneyField);
            _moneyField.x = 745;
            _moneyField.y = 20;
        }

        private function _setGemsField():void
        {
            _gemsField = new GemsField(_model.gems.value);
            addChild(_gemsField);
            _gemsField.x = 745;
            _gemsField.y = 65;
            _gemsField.addEventListener(MouseEvent.CLICK, _onMoneyClick);
        }

        private function _setObjectivesField():void
        {
            _objectivesField = new ObjectivesField(_model.stars.value);
            addChild(_objectivesField);
            _objectivesField.x = 745;
            _objectivesField.y = 110;
        }

        private function _setRepairBlock():void
        {
            _repairBlock = new MainScreenRepairBlock();
            _repairBlock.x = 160;
            _repairBlock.y = 316;
            addChild(_repairBlock);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _pingClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_PING_EARTH, true));
        }

        private function _showSettings(event:MouseEvent):void
        {
            if (_settingsWindow) {
                _settingsWindow.destroy();
                _settingsWindow = null;
            }
            else {
                _settingsWindow = new SoundSettingsView();
                addChild(_settingsWindow);
                _settingsWindow.x = 63;
                _settingsWindow.y = 562;
                _settingsWindow.alpha = 0;
                TweenLite.to(_settingsWindow, .3, {"alpha": 1});
            }

            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _showHelpScreen(event:MouseEvent):void
        {
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
            _helpView = new HelpScreen();
            addChild(_helpView);
        }

        private function _trophyButtonClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_TROPHY, true));
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _missionButtonClickHandler(e:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_MISSIONS, true));
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _shopButtonClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_SHOP, true));
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _rateButtonClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_TOP, true));
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _onMoneyClick(e:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_BUY_MONEY, true));
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _setNews():void
        {
            _newsButton = new NewsButton();
            _newsButton.x = 20;
            _newsButton.y = 200;
            addChild(_newsButton);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get startButton():StartButton
        {
            return _startButton;
        }

        public function get gameMode():GameModeSelect
        {
            return _gameMode;
        }

        public function get objectivesField():ObjectivesField
        {
            return _objectivesField;
        }

        public function get moneyField():CoinsField
        {
            return _moneyField;
        }

        public function get soundButton():SoundButton
        {
            return _soundButton;
        }

        public function get helpButton():HelpButton
        {
            return _helpButton;
        }

        public function get gemsField():GemsField
        {
            return _gemsField;
        }
    }
}
