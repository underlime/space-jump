package application.views.screen.news
{
    import application.config.ApplicationConfiguration;
    import application.controllers.ApplicationController;
    import application.models.User;
    import application.models.UserNews;
    import application.models.UserNewsRead;
    import application.views.base.ApplicationView;
    import application.views.text.TextFactory;

    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    public class NewsView extends ApplicationView
    {
        public static const WINDOW_WIDTH:int = 520;
        public static const WINDOW_HEIGHT:int = 324;
        public static const TITLE_WIDTH:int = 222;
        public static const TITLE_HEIGHT:int = 52;

        [Embed(source="../../../../../lib/news/NOTIFY_BG.png")]
        private var _WindowBackground:Class;

        [Embed(source="../../../../../lib/news/NOTIFY_TITLE.png")]
        private var _TitleBackground:Class;

        private var _windowBg:DisplayObject;
        private var _titleBackground:DisplayObject;
        private var _backgroundX:Number;
        private var _backgroundY:Number;
        private var _titleX:Number;
        private var _titleY:Number;
        private var _titleText:SimpleTextField;
        private var _newsCountText:SimpleTextField;
        private var _userNews:UserNews;
        private var _userNewsRead:UserNewsRead;
        private var _newsArea:NewsArea;
        private var _userModel:User;
        private var _appCtrl:ApplicationController;

        public function NewsView(appCtrl:ApplicationController, userModel:User, userNews:UserNews, userNewsRead:UserNewsRead)
        {
            super();
            _appCtrl = appCtrl;
            _userModel = userModel;
            _userNews = userNews;
            _userNewsRead = userNewsRead;
        }

        override public function render():void
        {
            _setBgShadow();
            _setupBackground();
            _setupTitle();
            _showNews();

            _userNews.addEventListener(Event.CHANGE, _onNewsChange);
            _userNewsRead.addEventListener(Event.CHANGE, _onNewsChange);
        }

        private function _showNews():void
        {
            _setupNewsCount();
            _setupNewsArea();
        }

        private function _onNewsChange(e:Event):void
        {
            var scrollPercent:Number = _newsArea.scrollbar.percent;
            _showNews();
            _newsArea.scrollbar.percent = scrollPercent;
        }

        private function _setupBackground():void
        {
            _windowBg = new _WindowBackground();
            _backgroundX = (ApplicationConfiguration.APP_WIDTH - WINDOW_WIDTH)/2;
            _backgroundY = (ApplicationConfiguration.APP_HEIGHT - WINDOW_HEIGHT)/2;
            _windowBg.x = _backgroundX;
            _windowBg.y = _backgroundY;
            addChild(_windowBg);
        }

        private function _setupTitle():void
        {
            _titleBackground = new _TitleBackground();
            _titleX = _backgroundX + (WINDOW_WIDTH - TITLE_WIDTH)/2;
            _titleY = _backgroundY - 15;
            _titleBackground.x = _titleX;
            _titleBackground.y = _titleY;
            addChild(_titleBackground);

            _titleText = new SimpleTextField();
            _titleText.x = _titleX + 20;
            _titleText.y = _titleY + 13;
            _titleText.width = TITLE_WIDTH;
            _titleText.height = TITLE_HEIGHT;
            _titleText.size = 20;
            _titleText.color = 0xFFFFFF;
            addChild(_titleText);
            _titleText.htmlText = TextFactory.instance
                .getLabel('news_label')
                .toUpperCase();
        }

        private function _setupNewsCount():void
        {
            _destroyCountText();
            _newsCountText = new SimpleTextField();
            _newsCountText.x = _titleX + TITLE_WIDTH - 56;
            _newsCountText.y = _titleY + 16;
            _newsCountText.width = 45;
            _newsCountText.size = 14;
            _newsCountText.color = 0xFFFFFF;
            _newsCountText.align = TextFormatAlign.CENTER;
            addChild(_newsCountText);
            _newsCountText.htmlText = (_userNews.length - _userNewsRead.length).toString();
        }

        private function _destroyCountText():void
        {
            if (_newsCountText)
                _newsCountText.destroy();
            _newsCountText = null;
        }

        private function _setupNewsArea():void
        {
            _destroyNewsArea();
            _newsArea = new NewsArea(_appCtrl, _userModel, _userNews, _userNewsRead);
            _newsArea.x = _backgroundX + NewsArea.NEWS_AREA_MARGIN_H;
            _newsArea.y = _backgroundY + NewsArea.NEWS_AREA_MARGIN_V + 5;
            addChild(_newsArea);
        }

        private function _destroyNewsArea():void
        {
            if (_newsArea)
                _newsArea.destroy();
            _newsArea = null;
        }

        override public function destroy():void
        {
            if (_windowBg)
                removeChild(_windowBg);
            _windowBg = null;

            if (_titleBackground)
                removeChild(_titleBackground);
            _titleBackground = null;

            if (_titleText)
                _titleText.destroy();
            _titleText = null;

            _destroyNewsArea();
            _destroyCountText();

            super.destroy();
        }
    }
}

