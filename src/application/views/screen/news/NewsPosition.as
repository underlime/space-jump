package application.views.screen.news
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;
    import application.models.Items;
    import application.models.User;
    import application.models.UserNewsRead;
    import application.models.user_news.UserNewsRecord;
    import application.views.base.ApplicationView;
    import application.views.screen.elements.checkbox.BaseCheckBox;
    import application.views.screen.shop.buttons.SlimButton;
    import application.views.text.TextFactory;

    import flash.display.DisplayObject;
    import flash.events.MouseEvent;
    import flash.events.TextEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    import framework.helpers.DateHelper;
    import framework.helpers.StringHelper;
    import framework.tools.SourceManager;
    import framework.view.text.SimpleTextField;

    public class NewsPosition extends ApplicationView
    {
        public static const NEWS_WIDTH:int = 430;
        public static const NEWS_HEIGHT:int = 151;
        public static const LABEL_WIDTH:int = 50;
        public static const LABEL_HEIGHT:int = 50;

        private static const COIN_ICO_DEFINITION:String = 'VALUTA';

        [Embed(source="../../../../../lib/news/NEWS_BG.png")]
        private var _Background:Class;

        [Embed(source="../../../../../lib/news/READ.png")]
        private var _PositionLabel:Class;

        private var _newsRecord:UserNewsRecord;
        private var _bg:DisplayObject;
        private var _dateText:SimpleTextField;
        private var _headerTextField:SimpleTextField;
        private var _newsTextField:SimpleTextField;
        private var _textFactory:TextFactory;
        private var _posLabel:DisplayObject;
        private var _userNewsRead:UserNewsRead;
        private var _shareCheckbox:BaseCheckBox;
        private var _likeButton:SlimButton;
        private var _bonusLabel:SimpleTextField;
        private var _userModel:User;
        private var _coinIco:DisplayObject;
        private var _newsWasRead:Boolean;
        private var _newsText:String;

        public function NewsPosition(userModel:User, newsRecord:UserNewsRecord, userNewsRead:UserNewsRead)
        {
            super();
            _userModel = userModel;
            _newsRecord = newsRecord;
            _userNewsRead = userNewsRead;
            _textFactory = TextFactory.instance;
            _newsWasRead = _userNewsRead.wasRead(_newsRecord.id.value);
        }

        override public function render():void
        {
            _setupBackground();
            _setupDate();
            _setupHeader();
            _setupText();
            _setupPosLabel();
            if (!_newsWasRead)
                _setupShareBlock();
        }

        private function _setupBackground():void
        {
            _bg = new _Background();
            addChild(_bg);
        }

        private function _setupDate():void
        {
            var format:String;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                format = '%d.%m.%Y';
            else
                format = '%m/%d/%Y';

            var dateString:String = DateHelper.formatDate(_newsRecord.time.value, format);

            _dateText = new SimpleTextField();
            _dateText.color = 0xC2FFFF;
            _dateText.size = 14;
            _dateText.x = 10;
            _dateText.y = 10;
            addChild(_dateText);
            _dateText.htmlText = dateString;
        }

        private function _setupHeader():void
        {
            var headerText:String = _getHeaderText();

            _headerTextField = new SimpleTextField();
            _headerTextField.color = 0xFFFFFF;
            _headerTextField.size = 16;
            _headerTextField.x = 10;
            _headerTextField.y = 32;
            _headerTextField.autoSize = TextFieldAutoSize.LEFT;
            addChild(_headerTextField);
            _headerTextField.htmlText = headerText.toUpperCase();
        }

        private function _getHeaderText():String
        {
            var headerText:String;
            switch (_newsRecord.news_type.value) {
                case 1:
                    headerText = _textFactory.getLabel('news_friend_bonus_header');
                    break;
                case 2:
                    headerText = _textFactory.getLabel('news_campaign_header');
                    break;
                default:
                    headerText = _textFactory.getLabel('news_common_header');
            }

            return headerText;
        }

        private function _setupText():void
        {
            _newsText = _getNewsText();

            _newsTextField = new SimpleTextField();
            _newsTextField.size = 12;
            _newsTextField.color = 0x2CFFFF;
            _newsTextField.x = 10;
            _newsTextField.y = 55;
            _newsTextField.multiline = true;
            _newsTextField.wordWrap = true;
            _newsTextField.autoSize = TextFieldAutoSize.LEFT;
            _newsTextField.width = NEWS_WIDTH - 20;
            _newsTextField.addEventListener(TextEvent.LINK, _onTextLinkClick);
            addChild(_newsTextField);
            _newsTextField.htmlText = _newsText;
            _newsTextField.mouseEnabled = true;
        }

        private function _onTextLinkClick(e:TextEvent):void
        {
            var friendMatches:Array = e.text.match(/^friend:(?P<id>\d+)$/i);
            if (friendMatches) {
                var callFriendEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_GUEST_SCREEN, true);
                callFriendEvent.data = {
                    'soc_net_id': friendMatches['id']
                };
                this.dispatchEvent(callFriendEvent);
            }
        }

        private function _getNewsText():String
        {
            var newsText:String;
            switch (_newsRecord.news_type.value) {
                case 1:
                    var data:Object = JSON.parse(_newsRecord.additional_data.value);
                    newsText = _textFactory.getLabel('news_friend_text')
                        .replace('{ID}', data['soc_net_id'])
                        .replace('{GIFT}', _getGiftName(data['bonus_type'], data['count']));
                    break;
                case 2:
                default:
                    newsText = _getNewsTextFromData();
            }

            return newsText;
        }

        private function _getGiftName(action:String, count:int):String
        {
            var name:String;
            var errorString:String = '(data error)';
            switch (action) {
                case 'gem':
                    name = _textFactory.getLabel('gems_label');
                    break;
                case 'coins':
                    name = _textFactory.getLabel('coins_label');
                    break;
                case Items.ACTION_PARACHUTE:
                    name = _textFactory.getLabel('parachutes_label');
                    break;
                case Items.ACTION_RESURRECTION:
                    name = _textFactory.getLabel('resurrections_label');
                    break;
                case Items.ACTION_ROCKET:
                    name = _textFactory.getLabel('rockets_label');
                    break;
                default:
                    name = errorString;
            }

            if (name != errorString) {
                name += ' x ' + count;
                name = '<font color="#ffffff">' + name + '</font>'
            }

            return name;
        }

        private function _getNewsTextFromData():String
        {
            var text:String = '';
            try {
                var data:Object = JSON.parse(_newsRecord.additional_data.value);
                text = data[ApplicationConfiguration.LANGUAGE] || '(lang error)';
            }
            catch (e:SyntaxError) {
                text = '(text error)';
            }

            return text;
        }

        private function _setupPosLabel():void
        {
            _posLabel = new _PositionLabel();
            var rectangle:Rectangle = new Rectangle(0, 0, LABEL_WIDTH, LABEL_HEIGHT);
            if (_newsWasRead)
                rectangle.y = LABEL_HEIGHT;
            _posLabel.scrollRect = rectangle;
            _posLabel.y = 0;
            _posLabel.x = NEWS_WIDTH - LABEL_WIDTH;
            addChild(_posLabel);
        }

        private function _setupShareBlock():void
        {
            _shareCheckbox = new BaseCheckBox(_textFactory.getLabel('share_label'));
            _shareCheckbox.x = 10;
            _shareCheckbox.y = NEWS_HEIGHT - 30;
            addChild(_shareCheckbox);

            _likeButton = new SlimButton(_textFactory.getLabel('like_label'));
            _likeButton.y = NEWS_HEIGHT - 31;
            _likeButton.x = 215;
            _likeButton.addEventListener(MouseEvent.CLICK, _onLikeClick);
            addChild(_likeButton);

            _bonusLabel = new SimpleTextField();
            _bonusLabel.color = 0xFFFFFF;
            _bonusLabel.x = SlimButton.BUTTON_WIDTH + 220;
            _bonusLabel.y = NEWS_HEIGHT - 30;
            _bonusLabel.size = 16;
            addChild(_bonusLabel);
            _bonusLabel.htmlText = '+' + _userModel.news_like_coins.value;

            _coinIco = SourceManager.instance.getBitmap(COIN_ICO_DEFINITION);
            _coinIco.x = SlimButton.BUTTON_WIDTH + 265;
            _coinIco.y = NEWS_HEIGHT - 30;
            _coinIco.scrollRect = new Rectangle(0, 0, 24, 24);
            addChild(_coinIco);
        }

        private function _onLikeClick(e:MouseEvent):void
        {
            var likeEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_LIKE, true);
            likeEvent.data = {
                'news_id': _newsRecord.id.value,
                'need_share': _shareCheckbox.checked,
                'text': StringHelper.stripHtmlTags(_newsText)
            };
            this.dispatchEvent(likeEvent);
        }

        override public function destroy():void
        {
            if (_bg)
                removeChild(_bg);
            _bg = null;

            if (_dateText)
                _dateText.destroy();
            _dateText = null;

            if (_headerTextField)
                _headerTextField.destroy();
            _headerTextField = null;

            if (_newsTextField)
                _newsTextField.destroy();
            _newsTextField = null;

            if (_posLabel)
                removeChild(_posLabel);
            _posLabel = null;

            if (_shareCheckbox)
                _shareCheckbox.destroy();
            _shareCheckbox = null;

            if (_likeButton)
                _likeButton.destroy();
            _likeButton = null;

            if (_bonusLabel)
                _bonusLabel.destroy();
            _bonusLabel = null;

            if (_coinIco)
                removeChild(_coinIco);
            _coinIco = null;

            super.destroy();
        }
    }
}

