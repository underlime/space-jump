package application.views.screen.news
{
    import application.controllers.ApplicationController;
    import application.models.User;
    import application.models.UserNews;
    import application.models.UserNewsRead;
    import application.models.user_news.UserNewsRecord;
    import application.views.base.ApplicationView;
    import application.views.particular.scroll.ScrollBarFuture;

    import flash.geom.Rectangle;

    import framework.view.scroll_future.ScrollContentObserver;

    public class NewsArea extends ApplicationView
    {
        public static const NEWS_AREA_MARGIN_H:int = 35;
        public static const NEWS_AREA_MARGIN_V:int = 40;

        public static const NEWS_AREA_WIDTH:int = (NewsView.WINDOW_WIDTH - NEWS_AREA_MARGIN_H*2);
        public static const NEWS_AREA_HEIGHT:int = (NewsView.WINDOW_HEIGHT - NEWS_AREA_MARGIN_V*2);

        public static const SCROLL_AREA_WIDTH:int = (NEWS_AREA_WIDTH - ScrollBarFuture.SCROLL_BAR_WIDTH);
        public static const NEWS_POSITION_MARGIN_BOTTOM:int = 5;

        private var _scrollbar:ScrollBarFuture;
        protected var _scrollArea:ApplicationView = new ApplicationView();
        private var _scrollAreaRectangle:Rectangle;
        private var _newsPositions:Vector.<NewsPosition> = new Vector.<NewsPosition>();
        private var _userNews:UserNews;
        private var _userNewsRead:UserNewsRead;
        private var _newsAreaRectangle:Rectangle;
        private var _scrollObserver:ScrollContentObserver;
        private var _userModel:User;
        private var _appCtrl:ApplicationController;

        public function NewsArea(appCtrl:ApplicationController, userModel:User, userNews:UserNews, userNewsRead:UserNewsRead)
        {
            super();
            _appCtrl = appCtrl;
            _userModel = userModel;
            _userNews = userNews;
            _userNewsRead = userNewsRead;
        }

        override public function render():void
        {
            _setupScrollBar();
            _setupScrollArea();
            _createNewsPositions();
            _showContent();
            _enableScroll();
        }

        private function _setupScrollBar():void
        {
            _scrollbar = new ScrollBarFuture(NEWS_AREA_HEIGHT - 2);
            _scrollbar.x = NEWS_AREA_WIDTH - ScrollBarFuture.SCROLL_BAR_WIDTH;
            _scrollbar.y = 0;
            addChild(_scrollbar);

            _newsAreaRectangle = new Rectangle(0, 0, NEWS_AREA_WIDTH, NEWS_AREA_HEIGHT);
            this.scrollRect = _newsAreaRectangle;
        }

        private function _setupScrollArea():void
        {
            _scrollArea = new ApplicationView();
            _scrollArea.x = 0;
            _scrollArea.y = 0;
            addChild(_scrollArea);
        }

        private function _createNewsPositions():void
        {
            var newsList:Vector.<UserNewsRecord> = _userNews.getSortedList();
            for each (var record:UserNewsRecord in newsList) {
                var newsPosition:NewsPosition = new NewsPosition(_userModel, record, _userNewsRead);
                _newsPositions.push(newsPosition);
            }
        }

        private function _showContent():void
        {
            var posY:int = 0;

            for each (var newsPosition:NewsPosition in _newsPositions) {
                newsPosition.x = 0;
                newsPosition.y = posY;
                _scrollArea.addChild(newsPosition);
                posY += NewsPosition.NEWS_HEIGHT + NEWS_POSITION_MARGIN_BOTTOM;
            }

            _scrollArea.graphics.beginFill(0xFFFFFF, 0);
            _scrollArea.graphics.drawRect(0, 0, _scrollArea.width, _scrollArea.height);
            _scrollArea.graphics.endFill();
        }

        private function _enableScroll():void
        {
            _scrollAreaRectangle = new Rectangle(0, 0, SCROLL_AREA_WIDTH, NEWS_AREA_HEIGHT);
            _scrollObserver = new ScrollContentObserver(_appCtrl, _scrollArea, _scrollbar, _scrollAreaRectangle);
        }

        override public function destroy():void
        {
            for each (var newsPosition:NewsPosition in _newsPositions)
                newsPosition.destroy();

            super.destroy();
        }

        public function get scrollbar():ScrollBarFuture
        {
            return _scrollbar;
        }
    }
}
