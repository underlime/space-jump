/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 03.05.13
 * Time: 22:44
 */
package application.views.screen.help
{
    import flash.display.Shape;

    import framework.view.View;

    public class HelpHiddenButton extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HelpHiddenButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;

            _shape = new Shape();
            addChild(_shape);
            _shape.graphics.beginFill(0xffffff);
            _shape.graphics.drawRect(0, 0, 184, 44);
            _shape.graphics.endFill();

            this.alpha = 0;
        }

        override public function destroy():void
        {
            _shape.graphics.clear();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _shape:Shape;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
