/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.03.13
 * Time: 11:49
 */
package application.views.screen.help
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.base.ApplicationView;

    import flash.display.MovieClip;
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;
    import flash.utils.ByteArray;

    import framework.view.View;
    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    public class HelpScreen extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const STEPS_COUNT:int = 7;

        public static const HELP_DATA:String = "TUTORIAL_MC";
        public static const CLOSE_BUTTON_DATA:String = "endButton";
        public static const PREV_BUTTON_DATA:String = "prevButton";
        public static const NEXT_BUTTON_DATA:String = "nextButton";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HelpScreen()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupTextData();
            _setupHelpMC();
            _setupButtons();
        }

        override public function render():void
        {
            addChild(_helpMC);
            _setupLabels();
            _addLabels();
            _updateCountLabel();
            _updateDescription();

            addChild(_nextButton);
            addChild(_prevButton);
            addChild(_closeButton);
        }

        override public function destroy():void
        {
            _destructObjects();
            _closeButton.destroy();
            _nextButton.destroy();
            _prevButton.destroy();
            if (_helpMC && this.contains(_helpMC))
                removeChild(_helpMC);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private static var _completeDispatched:Boolean = false;

        [Embed(source="../../../../../lib/config/faq.json", mimeType="application/octet-stream")]
        private var JsonClass:Class;

        private var _helpData:Object;
        private var _prevButton:HelpHiddenButton;
        private var _nextButton:HelpHiddenButton;
        private var _closeButton:HelpHiddenButton;
        private var _helpMC:MovieClip;

        private var _txtPrevLabel:SimpleTextField = new SimpleTextField();
        private var _txtNextLabel:SimpleTextField = new SimpleTextField();
        private var _txtFinishLabel:SimpleTextField = new SimpleTextField();
        private var _txtCountLabel:SimpleTextField = new SimpleTextField();
        private var _txtDescription:SimpleTextField = new SimpleTextField();

        private var _textLayout:View;
        private var _sound:SoundManager = SoundManager.instance;

        private var _stepsCount:int = 1;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTextData():void
        {
            var bytes:ByteArray = new JsonClass();
            var json:String = bytes.readUTFBytes(bytes.length);
            _helpData = JSON.parse(json);
        }

        private function _setupHelpMC():void
        {
            _helpMC = super.source.getMovieClip(HELP_DATA);
            _helpMC.gotoAndStop(1);
        }

        private function _setupButtons():void
        {
            _closeButton = new HelpHiddenButton();
            _closeButton.x = 308;
            _closeButton.y = 135;
            _closeButton.addEventListener(MouseEvent.CLICK, _closeClickHandler);

            _prevButton = new HelpHiddenButton();
            _prevButton.x = 170;
            _prevButton.y = 179;
            _prevButton.addEventListener(MouseEvent.CLICK, _prevClickHandler);

            _nextButton = new HelpHiddenButton();
            _nextButton.x = 450;
            _nextButton.y = 179;
            _nextButton.buttonMode = true;
            _nextButton.useHandCursor = true;
            _nextButton.addEventListener(MouseEvent.CLICK, _nextClickHandler);
        }

        private function _setupLabels():void
        {
            var arr:Array = [_txtPrevLabel, _txtNextLabel, _txtFinishLabel, _txtCountLabel, _txtDescription];
            for (var i:int = 0; i < arr.length; i++) {
                (arr[i] as SimpleTextField).size = 12;
                (arr[i] as SimpleTextField).height = 18;
                (arr[i] as SimpleTextField).color = 0xffffff;
                (arr[i] as SimpleTextField).align = TextFormatAlign.CENTER;
            }

            _txtDescription.size = 14;
            _txtDescription.width = 213;
            _txtDescription.x = 397;
            _txtDescription.y = 250;
            _txtDescription.wordWrap = true;
            _txtDescription.multiline = true;
            _txtDescription.autoSize = TextFieldAutoSize.LEFT;
            _txtDescription.align = TextFormatAlign.LEFT;

            _txtCountLabel.size = 16;
            _txtCountLabel.width = 83;
            _txtCountLabel.height = 22;
            _txtCountLabel.x = 359;
            _txtCountLabel.y = 189;

            _txtFinishLabel.x = 318;
            _txtFinishLabel.y = 150;
            _txtFinishLabel.width = 164;

            _txtPrevLabel.x = 198;
            _txtPrevLabel.y = 192;
            _txtPrevLabel.width = 146;

            _txtNextLabel.x = 457;
            _txtNextLabel.y = 192;
            _txtNextLabel.width = 146;

            addChild(_txtFinishLabel);
            addChild(_txtPrevLabel);
            addChild(_txtNextLabel);
            addChild(_txtCountLabel);
            addChild(_txtDescription);
        }

        private function _addLabels():void
        {
            _txtFinishLabel.text = _helpData.labels.end_label[ApplicationConfiguration.LANGUAGE].toString();
            _txtPrevLabel.text = _helpData.labels.prev_label[ApplicationConfiguration.LANGUAGE].toString();
            _txtNextLabel.text = _helpData.labels.next_label[ApplicationConfiguration.LANGUAGE].toString();
        }

        private function _updateCountLabel():void
        {
            var current:String = NumberUtil.format(_helpMC.currentFrame, "", 2);
            var total:String = NumberUtil.format(_helpMC.totalFrames, "", 2);
            _txtCountLabel.text = current + "/" + total;
        }

        private function _updateDescription():void
        {
            var data:Object = _helpData[ApplicationConfiguration.LANGUAGE];
            var html:String = data[_helpMC.currentFrame.toString()].text;
            _txtDescription.htmlText = html;

            if (data[_helpMC.currentFrame.toString()].additional_labels) {
                _setupTextLayout(data[_helpMC.currentFrame.toString()].additional_labels);
            }
        }

        private function _setupTextLayout(additional_labels:Object):void
        {
            _textLayout = new View();
            addChild(_textLayout);
            for (var index:String in additional_labels) {
                _setTextField(additional_labels[index]);
            }
        }

        private function _update():void
        {
            if (_textLayout && this.contains(_textLayout))
                removeChild(_textLayout);
            _updateCountLabel();
            _updateDescription();
        }

        private function _destructObjects():void
        {
            _closeButton.removeEventListener(MouseEvent.CLICK, _closeClickHandler);
            _prevButton.removeEventListener(MouseEvent.CLICK, _prevClickHandler);
            _nextButton.removeEventListener(MouseEvent.CLICK, _nextClickHandler);
        }

        private function _setTextField(data:Object):void
        {
            var field:SimpleTextField = new SimpleTextField();
            field.multiline = true;
            field.autoSize = TextFieldAutoSize.LEFT;

            if (data.x)
                field.x = Number(data.x);
            if (data.y)
                field.y = Number(data.y);

            _textLayout.addChild(field);

            if (data.text)
                field.htmlText = data.text;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeClickHandler(event:MouseEvent):void
        {
            _sound.playUISound(UISounds.BUTTON_CLOSE);
            if (!_completeDispatched && _stepsCount >= STEPS_COUNT) {
                _completeDispatched = true;
                var completeEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.TRAINING_COMPLETE, true);
                dispatchEvent(completeEvent);
            }
            destroy();
        }

        private function _nextClickHandler(event:MouseEvent):void
        {
            if (_helpMC.currentFrame < _helpMC.totalFrames)
                _helpMC.gotoAndStop(_helpMC.currentFrame + 1);

            _update();
            _sound.playUISound(UISounds.BUTTON_CONFIRM);
            ++_stepsCount;
        }

        private function _prevClickHandler(event:MouseEvent):void
        {
            if (_helpMC.currentFrame > 1)
                _helpMC.gotoAndStop(_helpMC.currentFrame - 1);

            _update();
            _sound.playUISound(UISounds.BUTTON_CONFIRM);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
