/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.02.13
 * Time: 12:18
 */
package application.views.screen
{
    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.view.View;

    public class BaseScreenPosition extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseScreenPosition()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _onOverHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _back:Bitmap;
        private var _hoverBack:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _back = super.source.getBitmap(ScreenData.BACKGROUND_POSITION);
            _hoverBack = super.source.getBitmap(ScreenData.BACKGROUND_POSITION_ACTIVE);
            _hoverBack.visible = false;

            addChild(_back);
            addChild(_hoverBack);

            addEventListener(MouseEvent.ROLL_OVER, _onOverHandler);
            addEventListener(MouseEvent.ROLL_OUT, _onOverHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onOverHandler(event:MouseEvent):void
        {
            _back.visible = !_back.visible;
            _hoverBack.visible = !_hoverBack.visible;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
