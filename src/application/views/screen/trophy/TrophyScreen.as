/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.03.13
 * Time: 14:59
 */
package application.views.screen.trophy
{
    import application.controllers.ApplicationController;
    import application.models.Trophies;
    import application.models.UsersTrophies;
    import application.models.trophies.TrophiesCollectionRecord;
    import application.views.screen.BaseScreen;
    import application.views.text.TextFactory;

    import flash.events.Event;

    public class TrophyScreen extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const START_CY:int = 20;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrophyScreen(appCtrl:ApplicationController, trophiesModel:Trophies, usersTrophiesModel:UsersTrophies, showShareButton:Boolean)
        {
            super(appCtrl, TextFactory.instance.getLabel('trophies_label'));
            _trophiesModel = trophiesModel;
            _usersTrophiesModel = usersTrophiesModel;
            _showShareButton = showShareButton;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _showRubrics();
            _trophiesModel.addEventListener(Event.CHANGE, _showRubrics);
            _usersTrophiesModel.addEventListener(Event.CHANGE, _showRubrics);
        }

        override public function destroy():void
        {
            _trophiesModel.removeEventListener(Event.CHANGE, _showRubrics);
            _usersTrophiesModel.removeEventListener(Event.CHANGE, _showRubrics);

            _destroyRubricsVector();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _trophiesModel:Trophies;
        private var _usersTrophiesModel:UsersTrophies;

        private var _collections:Vector.<TrophyRubric> = new Vector.<TrophyRubric>();
        private var _marginY:Number = 105;
        private var _marginX:Number = 20;
        private var _cY:Number = START_CY;
        private var _showShareButton:Boolean;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _showRubrics(e:Event=null):void
        {
            _createRubricsVector();
            super.clearScrollContent();

            _cY = START_CY;
            for (var i:int = 0; i < _collections.length; i++) {
                super.scrollContent.addChild(_collections[i]);
                _collections[i].x = _marginX;
                _collections[i].y = _cY;
                _cY += _marginY;
            }

            if (this.scrollContent.height > BaseScreen.CONTENT_WINDOW_HEIGHT)
                super.enableScroll();
        }

        private function _createRubricsVector():void
        {
            _destroyRubricsVector();
            _collections = new Vector.<TrophyRubric>();

            var collectionsList:Vector.<TrophiesCollectionRecord> = _trophiesModel.getSortedList();
            var trophiesCollection:TrophiesCollectionRecord;
            for each (trophiesCollection in collectionsList)
                _collections.push(new TrophyRubric(trophiesCollection, _usersTrophiesModel, _showShareButton));
        }

        private function _destroyRubricsVector():void
        {
            for (var i:int = 0; i < _collections.length; ++i)
                _collections[i].destroy();
            _collections = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
