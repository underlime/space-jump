/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.03.13
 * Time: 15:31
 */
package application.views.screen.trophy
{
    import flash.text.TextFieldAutoSize;

    import framework.view.text.SimpleTextField;

    public class TrophyRubricTitle extends SimpleTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrophyRubricTitle()
        {
            super();
            _setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            this.size = 12;
            this.color = 0x7bf3ff;
            this.width = 140;
            this.autoSize = TextFieldAutoSize.LEFT;
            this.wordWrap = true;
            this.multiline = true;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
