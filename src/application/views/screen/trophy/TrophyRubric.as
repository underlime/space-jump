/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.03.13
 * Time: 15:16
 */
package application.views.screen.trophy
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;
    import application.models.UsersTrophies;
    import application.models.trophies.TrophiesCollectionRecord;
    import application.models.trophies.TrophyRecord;
    import application.views.screen.elements.buttons.TellFriendsButton;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.tools.ScreenShot;
    import framework.view.View;

    public class TrophyRubric extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TROPHY_RAW_DATA:String = "SPACE_TROPHIES_COLLECTION_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrophyRubric(trophiesCollection:TrophiesCollectionRecord, usersTrophiesModel:UsersTrophies, showShareButton:Boolean)
        {
            super();

            _trophiesCollection = trophiesCollection;
            _usersTrophiesModel = usersTrophiesModel;
            _showShareButton = showShareButton;

            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                _title = trophiesCollection.name_ru.value.toUpperCase();
            else
                _title = trophiesCollection.name_en.value.toUpperCase();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _setupTitle();
            if (_showShareButton)
                _setupShareButton();
            _setupTrophiesList();
        }

        override public function destroy():void
        {
            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            _destroyTrophiesVector();

            if (_shareButton && !_shareButton.destroyed)
                _shareButton.destroy();
            _shareButton = null;
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _backRect:Rectangle;
        private var _title:String;
        private var _txtTitle:TrophyRubricTitle;
        private var _shareButton:TellFriendsButton;

        private var _trophiesVector:Vector.<TrophyPosition> = new Vector.<TrophyPosition>();
        private var _trophiesCollection:TrophiesCollectionRecord;
        private var _usersTrophiesModel:UsersTrophies;
        private var _showShareButton:Boolean;

        [Embed(source="../../../../../lib/share_pics/TROPHY_COLLECTION_EN.png", mimeType="image/png")]
        private var _ShareImageEn:Class;

        [Embed(source="../../../../../lib/share_pics/TROPHY_COLLECTION_RU.png", mimeType="image/png")]
        private var _ShareImageRu:Class;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _background = super.source.getBitmap(TROPHY_RAW_DATA);
            _backRect = new Rectangle(0, 0, 705, 100);
            _background.scrollRect = _backRect;

            addChild(_background);

            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _setupTitle():void
        {
            _txtTitle = new TrophyRubricTitle();
            addChild(_txtTitle);
            _txtTitle.x = 14;
            _txtTitle.y = 30;
            _txtTitle.text = _title;
        }

        private function _setupShareButton():void
        {
            _shareButton = new TellFriendsButton();
            _shareButton.x = 577;
            _shareButton.y = 20;
            _shareButton.addEventListener(MouseEvent.CLICK, _shareClickHandler);
            addChild(_shareButton);
        }

        private function _setupTrophiesList():void
        {
            _createTrophiesVector();

            var cx:Number = 147;
            var trophy:TrophyPosition;
            for (var i:int = 0; i < _trophiesVector.length; ++i) {
                trophy = _trophiesVector[i];
                trophy.x = cx;
                trophy.y = 10;
                addChild(trophy);
                cx += 83;
            }
        }

        private function _createTrophiesVector():void
        {
            _destroyTrophiesVector();
            _trophiesVector = new Vector.<TrophyPosition>();

            var trophiesList:Vector.<TrophyRecord> = _trophiesCollection.trophies_list.getSortedList();
            var trophyRecord:TrophyRecord;
            var opened:Boolean;
            for each (trophyRecord in trophiesList) {
                opened = _usersTrophiesModel.hasTrophy(trophyRecord.id.value);
                _trophiesVector.push(new TrophyPosition(trophyRecord, opened));
            }
        }

        private function _destroyTrophiesVector():void
        {
            for (var i:int=0; i<_trophiesVector.length; ++i)
                _trophiesVector[i].destroy();
            _trophiesVector = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            _backRect.y = 100;
            _background.scrollRect = _backRect;
        }

        private function _outHandler(event:MouseEvent):void
        {
            _backRect.y = 0;
            _background.scrollRect = _backRect;
        }

        private function _shareClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.SHARE, true);
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get shareText():String
        {
            var shareText:String = TextFactory.instance.getLabel('my_collection_label') + ': ';
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                shareText += _trophiesCollection.name_ru.value;
            else
                shareText += _trophiesCollection.name_en.value;
            return shareText;
        }

        public function get shareImage():Bitmap
        {
            var backGround:Bitmap;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                backGround = new _ShareImageRu();
            else
                backGround = new _ShareImageEn();

            var view:View = new View();
            view.addChild(backGround);

            var y:Number = 165;
            var x:Number = 40;
            var dltX:Number = 75;

            var positionImage:Bitmap;
            for (var i:int=0; i<_trophiesVector.length; ++i) {
                positionImage = _trophiesVector[i].shareImage;
                positionImage.x = x;
                positionImage.y = y;
                positionImage.width = positionImage.height = 70;
                view.addChild(positionImage);
                x += dltX;
            }

            var screenShot:ScreenShot = new ScreenShot();
            screenShot.takeShot(view);
            var image:Bitmap = new Bitmap(screenShot.rawData.clone());
            screenShot.destroy();

            return image;
        }
    }
}
