/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.03.13
 * Time: 16:08
 */
package application.views.screen.trophy
{
    import application.models.trophies.TrophyRecord;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    import framework.view.View;

    public class TrophyPosition extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrophyPosition(trophyRecord:TrophyRecord, opened:Boolean = false)
        {
            super();
            _trophyRecord = trophyRecord;
            _opened = opened;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupTrophy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _trophyRecord:TrophyRecord;
        private var _opened:Boolean;
        private var _trophy:Bitmap;
        private var _scrollRect:Rectangle;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTrophy():void
        {
            _trophy = super.source.getBitmap(_trophyRecord.image.value);
            _scrollRect = new Rectangle(0, 0, 80, 80);
            if (_opened)
                _scrollRect.y = 80;

            _trophy.scrollRect = _scrollRect;
            addChild(_trophy);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get shareImage():Bitmap
        {
            var y:Number = (_opened) ? _trophy.height : 0;
            var copyRect:Rectangle = new Rectangle(0, y, _trophy.width, _trophy.height);
            var destPoint:Point = new Point(0, 0);
            var imageData:BitmapData = new BitmapData(_trophy.width, _trophy.height);
            imageData.copyPixels(_trophy.bitmapData, copyRect, destPoint);
            return new Bitmap(imageData);
        }
    }
}
