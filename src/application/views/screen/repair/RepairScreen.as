/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.03.13
 * Time: 11:50
 */
package application.views.screen.repair
{
    import application.events.ApplicationEvent;
    import application.models.Items;
    import application.models.SuiteParams;
    import application.models.User;
    import application.views.screen.dialog.AbstractDialog;
    import application.views.screen.dialog.AlertWindow;
    import application.views.screen.dialog.ConfirmWindow;
    import application.views.text.TextFactory;

    import flash.display.Shape;

    import framework.view.View;

    public class RepairScreen extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RepairScreen(suiteParams:SuiteParams, itemsModel:Items, userModel:User)
        {
            super();
            _suiteParams = suiteParams;
            _itemsModel = itemsModel;
            _userModel = userModel;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _setupView();
        }

        override public function destroy():void
        {
            if (_repairView) {
                _repairView.destroy();
                _repairView = null;
            }

            if (_dialogWindow) {
                _dialogWindow.destroy();
                _dialogWindow = null;
            }

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _backgroundShape:Shape;
        private var _repairView:RepairView;
        private var _suiteParams:SuiteParams;
        private var _itemsModel:Items;
        private var _userModel:User;

        private var _dialogWindow:AbstractDialog;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _backgroundShape = new Shape();
            addChild(_backgroundShape);
            _backgroundShape.graphics.beginFill(0x000000, .8);
            _backgroundShape.graphics.drawRect(0, 0, super.stage.stageWidth, super.stage.stageHeight);
            _backgroundShape.graphics.endFill();
        }

        private function _setupView():void
        {
            _repairView = new RepairView(_suiteParams, _itemsModel, _userModel);
            addChild(_repairView);
            _repairView.x = 200;
            _repairView.y = 192;
            _repairView.addEventListener(ApplicationEvent.CALL_REPAIR, _onCallRepair);
            _repairView.addEventListener(ApplicationEvent.CALL_DONT_NEED_REPAIR, _onCallDontNeedRepair);
        }

        private function _onCallRepair(e:ApplicationEvent):void
        {
            e.stopPropagation();

            if (_userModel.gems.value)
                _showRepairDialog();
            else
                _showBankDialog();
        }

        private function _showRepairDialog():void
        {
            var text:String = TextFactory.instance.getLabel('repair_confirm_label');

            _dialogWindow = new ConfirmWindow(text);
            _dialogWindow.addEventListener(ApplicationEvent.CLICK_CONFIRM, _onConfirmRepair);
            _dialogWindow.addEventListener(ApplicationEvent.CLICK_CANCEL, _closeDialog);
            addChild(_dialogWindow);
        }

        private function _onConfirmRepair(e:ApplicationEvent):void
        {
            _dialogWindow.destroy();
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_REPAIR);
            dispatchEvent(event);
        }

        private function _closeDialog(e:ApplicationEvent = null):void
        {
            _dialogWindow.destroy();
        }

        private function _showBankDialog():void
        {
            var text:String = TextFactory.instance.getLabel('no_money_html_text');
            _dialogWindow = new ConfirmWindow(text);
            _dialogWindow.addEventListener(ApplicationEvent.CLICK_CONFIRM, _onConfirmBank);
            _dialogWindow.addEventListener(ApplicationEvent.CLICK_CANCEL, _closeDialog);
            addChild(_dialogWindow);
        }

        private function _onConfirmBank(e:ApplicationEvent):void
        {
            _closeDialog();
            var bankEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_BANK);
            dispatchEvent(bankEvent);
        }

        private function _onCallDontNeedRepair(e:ApplicationEvent):void
        {
            e.stopPropagation();

            var text:String = TextFactory.instance.getLabel('dont_repair_alert_label');

            _dialogWindow = new AlertWindow(text);
            _dialogWindow.addEventListener(ApplicationEvent.CLICK_OK, _closeDialog);
            addChild(_dialogWindow);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
