package application.views.screen.repair
{
    import application.events.ApplicationEvent;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.base.ApplicationView;
    import application.views.screen.elements.StrongBar;
    import application.views.screen.elements.buttons.RepairButton;

    import flash.display.Graphics;
    import flash.events.MouseEvent;

    import framework.view.text.SimpleTextField;

    public class RepairBlock extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RepairBlock(current:int, total:int)
        {
            _current = current;
            _total = total;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupLabel();
            _setupProgress();
            _setupButton();
            _addUnderLine();
        }

        override public function destroy():void
        {
            _repairButton.removeEventListener(MouseEvent.CLICK, _clickHandler);
            _strongProgress.destroy();
            _repairButton.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _strongProgress:StrongBar;
        private var _repairButton:RepairButton = new RepairButton();
        private var _label:SimpleTextField = new SimpleTextField();
        private var _total:int = 0;
        private var _current:int = 0;
        private var _verticalMargin:Number = 7;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupLabel():void
        {
            _label.size = 10;
            _label.color = 0x20D2EF;
            _label.y = _verticalMargin;
            addChild(_label);
            _label.text = super.textFactory.getLabel('strength_suite_label');
        }

        private function _setupButton():void
        {
            addChild(_repairButton);

            _repairButton.x = 233;
            _repairButton.y = _verticalMargin;
            _repairButton.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _setupProgress():void
        {
            _strongProgress = new StrongBar(_current, _total);
            _strongProgress.x = 75;
            _strongProgress.y = _verticalMargin;

            addChild(_strongProgress);
        }

        private function _addUnderLine():void
        {
            var gr:Graphics = this.graphics;
            gr.beginFill(0x134854, 1);
            gr.lineStyle(1, 0x134854);
            gr.lineTo(300, 0);
            gr.moveTo(0, 38);
            gr.lineTo(300, 38);
            gr.endFill();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            var eventType:String;
            if (_current < _total)
                eventType = ApplicationEvent.CALL_REPAIR;
            else
                eventType = ApplicationEvent.CALL_DONT_NEED_REPAIR;
            var repairEvent:ApplicationEvent = new ApplicationEvent(eventType, true);
            super.dispatchEvent(repairEvent);
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
