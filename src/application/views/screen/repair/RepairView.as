/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.03.13
 * Time: 10:43
 */
package application.views.screen.repair
{
    import application.events.ApplicationEvent;
    import application.models.Items;
    import application.models.SuiteParams;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.views.base.ApplicationView;
    import application.views.screen.elements.buttons.CloseButton;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.events.TimerEvent;
    import flash.text.TextFormatAlign;
    import flash.utils.Timer;

    import framework.helpers.MathHelper;
    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    public class RepairView extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RepairView(suiteParams:SuiteParams, itemsModel:Items, userModel:User)
        {
            super();
            _suiteParams = suiteParams;
            _itemsModel = itemsModel;
            _userModel = userModel;

            _suiteParams.addEventListener(Event.CHANGE, _updateData);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _setupCloseButton();
            _setupTitle();
            _setupDescription();
            _setupRepairBlock();
            _setupTimerLabel();
            _setupTimerValue();
        }

        override public function destroy():void
        {
            _destroyRepairBlock();
            _destroyRepairTimer();
            _closeButton.destroy();
            _suiteParams.removeEventListener(Event.CHANGE, _updateData);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _closeButton:CloseButton;
        private var _txtTitle:SimpleTextField;
        private var _txtDescription:SimpleTextField;
        private var _repairBlock:RepairBlock;
        private var _txtTimerLabel:SimpleTextField;

        private var _timerValue:SimpleTextField;

        private var _suiteParams:SuiteParams;
        private var _itemsModel:Items;
        private var _userModel:User;

        private var _repairTimer:Timer;
        private var _secondsRemain:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateData(e:Event=null):void
        {
            _setupRepairBlock();
            _initRepairTimer();
        }

        private function _setupBackground():void
        {
            _background = super.source.getBitmap(RepairData.BACKGROUND);
            addChild(_background);
        }

        private function _setupCloseButton():void
        {
            _closeButton = new CloseButton();
            addChild(_closeButton);
            _closeButton.x = 288;
            _closeButton.y = 6;
            _closeButton.addEventListener(MouseEvent.CLICK, _closeClickHandler);
        }

        private function _setupTitle():void
        {
            _txtTitle = new SimpleTextField();
            _txtTitle.width = 400;
            _txtTitle.size = 16;
            _txtTitle.color = 0xb8fcff;
            _txtTitle.align = TextFormatAlign.CENTER;
            _txtTitle.y = 52;
            addChild(_txtTitle);

            _txtTitle.text = super.textFactory.getLabel('repair_attention_label');
        }

        private function _setupDescription():void
        {
            _txtDescription = new SimpleTextField();
            _txtDescription.width = 400;
            _txtDescription.size = 12;
            _txtDescription.align = TextFormatAlign.CENTER;
            _txtDescription.color = 0x4ae5ff;
            _txtDescription.y = 87;
            _txtDescription.multiline = true;

            addChild(_txtDescription);
            _txtDescription.htmlText = super.textFactory.getLabel('repair_description_label');
        }

        private function _setupRepairBlock():void
        {
            var itemRecord:ItemRecord = _getItemRecord();
            if (!itemRecord)
                return;

            _destroyRepairBlock();
            _repairBlock = new RepairBlock(
                _suiteParams.actualHp,
                itemRecord.max_hp.value
            );

            _repairBlock.x = 50;
            _repairBlock.y = 136;
            addChild(_repairBlock);
        }

        private function _getItemRecord():ItemRecord
        {
            var id:String = _suiteParams.item.value.toString();
            var itemRecord:ItemRecord = _itemsModel.getRecordById(id) as ItemRecord;
            return itemRecord;
        }

        private function _destroyRepairBlock():void
        {
            if (_repairBlock) {
                _repairBlock.destroy();
                _repairBlock = null;
            }
        }

        private function _setupTimerLabel():void
        {
            _txtTimerLabel = new SimpleTextField();
            _txtTimerLabel.width = 400;
            _txtTimerLabel.align = TextFormatAlign.CENTER;
            _txtTimerLabel.color = 0x4ae5ff;
            _txtTimerLabel.size = 14;
            _txtTimerLabel.y = 192;

            addChild(_txtTimerLabel);
            _txtTimerLabel.text = super.textFactory.getLabel('repair_time_label');
        }

        private function _setupTimerValue():void
        {
            _timerValue = new SimpleTextField();
            _timerValue.width = 400;
            _timerValue.y = 216;
            _timerValue.color = 0xffffff;
            _timerValue.size = 16;
            _timerValue.align = TextFormatAlign.CENTER;
            addChild(_timerValue);

            _initRepairTimer();
        }

        private function _initRepairTimer():void
        {
            if (!_suiteParams.hp_time.value)
                return;

            _destroyRepairTimer();

            var now:Date = new Date();
            var allRightTime:Number = _suiteParams.hp_time.value.getTime() + _suiteParams.auto_repair_seconds.value*1000;
            _secondsRemain = (allRightTime - now.getTime())/1000;
            _setTime(_secondsRemain);

            if (_secondsRemain > 0) {
                _repairTimer = new Timer(1000, int.MAX_VALUE);
                _repairTimer.addEventListener(TimerEvent.TIMER, _onRepairTimer);
                _repairTimer.start();
            }
            else {
                _reset();
            }
        }

        private function _destroyRepairTimer():void
        {
            if (_repairTimer) {
                _repairTimer.stop();
                _repairTimer.removeEventListener(TimerEvent.TIMER, _onRepairTimer);
                _repairTimer = null;
            }
        }

        private function _setTime(seconds:Number):void
        {
            var timeObject:Object = MathHelper.splitSeconds(Math.round(seconds));
            _timerValue.text = NumberUtil.format(timeObject.hours, "", 2) + ":" + NumberUtil.format(timeObject.minutes, "", 2) + ":" + NumberUtil.format(timeObject.seconds, "", 2);
        }

        private function _onRepairTimer(e:TimerEvent):void
        {
            --_secondsRemain;
            if (_secondsRemain > 0)
                _setTime(_secondsRemain);
            else
                _reset();
        }

        private function _reset():void
        {
            var itemRecord:ItemRecord = _getItemRecord();
            _suiteParams.hp_time.value = new Date();
            if (itemRecord)
                _suiteParams.hp.value = itemRecord.max_hp.value;
            _updateData();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CLOSE, true);
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
