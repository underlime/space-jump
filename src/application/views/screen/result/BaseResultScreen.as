package application.views.screen.result
{
    import application.views.base.ApplicationView;
    import application.views.screen.elements.buttons.AgainButton;
    import application.views.screen.elements.buttons.MenuButton;
    import application.views.screen.elements.buttons.ShareButton;
    import application.views.text.ApplicationHeader;

    import flash.display.Bitmap;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BaseResultScreen extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseResultScreen(success:Boolean = true)
        {
            _success = success;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _setupBack();
            _setupHeader();
        }

        override public function destroy():void
        {
            this.menuButton.destroy();
            this.shareButton.destroy();
            this.againButton.destroy();
            _pointsBar.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function drawResults(points:int, results:Vector.<View>):void
        {
            _addPointsData(points);

            var y0:Number = _pointsBar.y + 60;
            var x0:Number = _background.x + 50;
            var dy:Number = 38;
            var cy:Number = y0;

            for (var i:int = 0; i < results.length; i++) {
                addChild(results[i]);
                results[i].y = cy;
                results[i].x = x0;
                cy += dy;
            }
        }

        public function addImage(image:Bitmap):void
        {
            image.x = _background.x + 50;
            image.y = _background.y + 58;

            addChild(image);
        }

        public function addButtons():void
        {
            _setAgainButton();
            _setMenuButton();
            _setShareButton();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _success:Boolean = true;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _header:ApplicationHeader;

        private var _againButton:AgainButton;
        private var _menuButton:MenuButton;
        private var _shareButton:ShareButton;

        private var _pointsBar:PointsResult;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addPointsData(points:int):void
        {
            _pointsBar = new PointsResult(points);
            _pointsBar.x = _background.x + 50;
            _pointsBar.y = _background.y + 188;
            addChild(_pointsBar);
        }

        private function _setShareButton():void
        {
            _shareButton = new ShareButton();
            addChild(_shareButton);
            _shareButton.x = _background.x + 50;
            _shareButton.y = _background.y + 593;
        }

        private function _setMenuButton():void
        {
            _menuButton = new MenuButton();
            addChild(_menuButton);
            _menuButton.x = _background.x + 205;
            _menuButton.y = _background.y + 533;
        }

        private function _setAgainButton():void
        {
            _againButton = new AgainButton();
            addChild(_againButton);
            _againButton.x = _background.x + 50;
            _againButton.y = _background.y + 533;
        }

        private function _setupHeader():void
        {
            _header = new ApplicationHeader();
            _header.width = 400;
            _header.x = _background.x;
            _header.y = _background.y + 10;

            addChild(_header);
            var key:String = _success ? 'success_label' : 'fail_label';
            _header.text = super.textFactory.getLabel(key);
        }

        private function _setupBack():void
        {
            _background = this.source.getBitmap(ResultData.BACKGROUND);
            addChild(_background);
            _background.x = stage.stageWidth / 2 - _background.width / 2;
            _background.y = stage.stageHeight / 2 - _background.height / 2;
        }

        private function _setupBackground():void
        {
            graphics.beginFill(0x000000, 0.8);
            graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            graphics.endFill();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get againButton():AgainButton
        {
            return _againButton;
        }

        public function get menuButton():MenuButton
        {
            return _menuButton;
        }

        public function get shareButton():ShareButton
        {
            return _shareButton;
        }
    }

}