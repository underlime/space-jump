/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.01.13
 */

package application.views.screen.result
{
    import application.config.GameMode;
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;
    import application.models.SuiteParams;
    import application.models.User;
    import application.views.physics.collisions.CollisionTypes;
    import application.views.screen.repair.RepairBlock;

    import flash.display.Bitmap;

    import framework.data.ModelsRegistry;
    import framework.helpers.MathHelper;
    import framework.view.View;

    import org.casalib.util.NumberUtil;

    public class ResultScreenView extends BaseResultScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ResultScreenView(model:GameModel, success:Boolean = true)
        {
            _model = model;
            super(success);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupRelations();
            super.setup();
        }

        override public function render():void
        {
            super.render();

            var statistics:StatisticsGameModel = _model.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;
            var userModel:User = ModelsRegistry.getModel(ModelData.USER_MODEL) as User;
            var suiteParams:SuiteParams = ModelsRegistry.getModel(ModelData.SUITE_PARAMS) as SuiteParams;

            var freeFall:String = NumberUtil.format(statistics.freeFall, " ", 2) + " " + super.textFactory.getLabel("distance_label");
            var height:String = (_model.gameMode != GameMode.ENDLESS) ? NumberUtil.format(statistics.defaultHeight, " ", 2) + " " + super.textFactory.getLabel("distance_label") : super.textFactory.getLabel("endless_mode_label");
            var timeObject:Object = MathHelper.splitSeconds(statistics.seconds);
            var flyingTime:String = NumberUtil.format(timeObject.hours, " ", 2) + ":" + NumberUtil.format(timeObject.minutes, "", 2) + ":" + NumberUtil.format(timeObject.seconds, "", 2);
            var maxSpeed:String = NumberUtil.format(statistics.maxVelocity, " ", 2) + " " + super.textFactory.getLabel("speed_label");
            var lastRecordData:String = NumberUtil.format(userModel.max_score_global.value, " ", 6, "");
            var maxHp:int = suiteParams.suiteItem.max_hp.value ? suiteParams.suiteItem.max_hp.value : 0;

            _results = new Vector.<View>();

            _results.push(
                    new ResultPosition(super.textFactory.getLabel('last_record_label').toUpperCase(), lastRecordData),
                    new ResultPosition(super.textFactory.getLabel("money_label").toUpperCase(), statistics.coins.toString()),
                    new ResultPosition(super.textFactory.getLabel('free_fall_label').toUpperCase(), freeFall),
                    new ResultPosition(super.textFactory.getLabel('height_label'), height),
                    new ResultPosition(super.textFactory.getLabel('flying_time_label').toUpperCase(), flyingTime),
                    new ResultPosition(super.textFactory.getLabel('max_speed_label'), maxSpeed),
                    new RepairBlock(suiteParams.actualHp, maxHp)
            );

            super.drawResults(int(Math.round(statistics.points * (1 + userModel.stars.value / 100))), _results);

            var source:String = ResultData.SUCCESS_IMAGE;

            if (!super._success) {
                if (_collisions[statistics.collisionType])
                    source = _collisions[statistics.collisionType];
                else
                    source = _collisions[CollisionTypes.FIREWORK_COLLISION];
            }

            var image:Bitmap = super.source.getBitmap(source);
            super.addImage(image);

            super.addButtons();
        }

        override public function destroy():void
        {
            for (var i:int = 0; i < _results.length; i++) {
                _results[i].destroy();
            }
            _model.flushProperties();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:GameModel;
        private var _results:Vector.<View>;
        private var _collisions:Array = [];

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupRelations():void
        {
            _collisions[CollisionTypes.BIRD_COLLISION] = ResultData.BIRD_COLLISION;
            _collisions[CollisionTypes.FIREWORK_COLLISION] = ResultData.FIREWORK_COLLISION;
            _collisions[CollisionTypes.JET_COLLISION] = ResultData.JET_COLLISION;
            _collisions[CollisionTypes.METAL_BOX_COLLISION] = ResultData.METAL_BOX_COLLISION;
            _collisions[CollisionTypes.PLANE_COLLISION] = ResultData.PLANE_COLLISION;
            _collisions[CollisionTypes.SATELLITE_COLLISION] = ResultData.SATELLITE_COLLISION;
            _collisions[CollisionTypes.TNT_BOX_COLLISION] = ResultData.TNT_BOX_COLLISION;
            _collisions[CollisionTypes.UFO_COLLISION] = ResultData.UFO_COLLISION;
            _collisions[CollisionTypes.WOOD_BOX_COLLISION] = ResultData.WOOD_BOX_COLLISION;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
