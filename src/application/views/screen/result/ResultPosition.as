/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 19.01.13
 */

package application.views.screen.result
{
    import flash.display.Graphics;

    import framework.view.View;
    import framework.view.text.SimpleTextField;

    public class ResultPosition extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ResultPosition(label:String, value:String, additionalInfo:String = "")
        {
            _label = label;
            _value = value;
            _additionalInfo = additionalInfo;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _txtField = new SimpleTextField();
            _txtField.width = 320;
        }

        override public function render():void
        {
            addChild(_txtField);
            _setupTextData();
            _addUnderLine();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _additionalInfo:String;
        private var _value:String;
        private var _label:String;

        private var _txtField:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTextData():void
        {
            var html:String = "<font size='9' color='#20D2EF'>" + _label + ":</font>";
            html += "<font size='20' color='#B8FCFF'>  " + _value + "</font>";
            html += "<font size='12' color='#8DFF98'>  " + _additionalInfo + "</font>";

            _txtField.htmlText = html;
            _txtField.y = 3;
        }

        private function _addUnderLine():void
        {
            var gr:Graphics = this.graphics;
            gr.beginFill(0x134854, 1);
            gr.lineStyle(1, 0x134854);
            //gr.moveTo(0, 35);
            gr.lineTo(300, 0);
            gr.endFill();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
