package application.views.screen.result
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ResultData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "SPACE_RESULT_BACK";
        public static const SHARE_BUTTON:String = "SPACE_RESULTS_SCREEN_SHARE";
        public static const AGAIN_BUTTON:String = "SPACE_BUTTON_AGAIN";
        public static const BUTTON_MENU:String = "SPACE_BUTTON_MENU";
        public static const REPAIR_BUTTON:String = "SPACE_FIX_IT";

        public static const UFO_COLLISION:String = "SPACE_FAIL_01";
        public static const PLANE_COLLISION:String = "SPACE_FAIL_02";
        public static const WOOD_BOX_COLLISION:String = "SPACE_FAIL_03";
        public static const METAL_BOX_COLLISION:String = "SPACE_FAIL_04";
        public static const SATELLITE_COLLISION:String = "SPACE_FAIL_05";
        public static const JET_COLLISION:String = "SPACE_FAIL_06";
        public static const TNT_BOX_COLLISION:String = "SPACE_FAIL_07";
        public static const FIREWORK_COLLISION:String = "SPACE_FAIL_08";
        public static const BIRD_COLLISION:String = "SPACE_FAIL_09";
        public static const SUCCESS_IMAGE:String = "SPACE_SUCESS";


        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}