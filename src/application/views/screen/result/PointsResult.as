package application.views.screen.result
{
    import application.views.base.ApplicationView;

    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    public class PointsResult extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PointsResult(points:int)
        {
            _points = points;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupLabel();
            _setupPointsText();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _label:SimpleTextField = new SimpleTextField();
        private var _pointsText:SimpleTextField = new SimpleTextField();
        private var _points:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupLabel():void
        {
            _label.size = 9;
            _label.color = 0x20D2EF;
            _label.width = 300;
            _label.align = TextFormatAlign.CENTER;

            addChild(_label);
            _label.text = super.textFactory.getLabel("points_label").toUpperCase();
        }

        private function _setupPointsText():void
        {
            _pointsText.width = 300;
            _pointsText.align = TextFormatAlign.CENTER;
            _pointsText.color = 0xffffff;
            _pointsText.size = 28;

            addChild(_pointsText);
            _pointsText.text = NumberUtil.format(_points, " ", 13, "");
            _pointsText.y = 10;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
