package application.views.screen.missions
{
    import application.controllers.ApplicationController;
    import application.models.Achievements;
    import application.models.UserAchievements;
    import application.models.achievements.AchievementRecord;
    import application.views.screen.BaseScreen;
    import application.views.text.TextFactory;

    import flash.events.Event;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MissionView extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MissionView(appCtrl:ApplicationController, achievementsModel:Achievements, userAchievementsModel:UserAchievements)
        {
            super(appCtrl, TextFactory.instance.getLabel('missions_label'));
            _achievementsModel = achievementsModel;
            _userAchievementsModel = userAchievementsModel;

            _achievementsModel.addEventListener(Event.CHANGE, _setupMissions);
            _userAchievementsModel.addEventListener(Event.CHANGE, _setupMissions);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _setupMissions();
        }

        override public function destroy():void
        {
            _achievementsModel.removeEventListener(Event.CHANGE, _setupMissions);
            _userAchievementsModel.removeEventListener(Event.CHANGE, _setupMissions);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _achievementsModel:Achievements;
        private var _userAchievementsModel:UserAchievements;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupMissions(e:Event = null):void
        {
            var posX:Number = 20;
            var posY:Number = 20;
            var mission:MissionPosition;
            var record:AchievementRecord;
            var complete:Boolean;
            var achievementsList:Vector.<AchievementRecord> = _achievementsModel.getSortedList();

            for (var i:int = 0; i < achievementsList.length; ++i) {
                record = achievementsList[i];
                complete = Boolean(_userAchievementsModel.getRecordByAchievementId(record.id.value));
                mission = new MissionPosition(record, complete);
                mission.x = posX;
                mission.y = posY;
                posY += 65;
                this.scrollContent.addChild(mission);
            }

            if (this.scrollContent.height > BaseScreen.CONTENT_WINDOW_HEIGHT)
                super.enableScroll();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}