package application.views.screen.missions
{
    import application.config.ApplicationConfiguration;
    import application.models.achievements.AchievementRecord;
    import application.views.base.ApplicationView;
    import application.views.screen.ScreenData;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.view.text.SimpleTextField;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MissionPosition extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MissionPosition(achievementRecord:AchievementRecord, completed:Boolean = false)
        {
            _achievementRecord = achievementRecord;
            _completed = completed;

            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU) {
                this.description = achievementRecord.description_ru.value;
                this.achievement_name = achievementRecord.name_ru.value;
            }
            else {
                this.description = achievementRecord.description_en.value;
                this.achievement_name = achievementRecord.name_en.value;
            }

            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setBackground();
            _setIcon();
            _setHeader();
            _setDescription();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var description:String;
        public var achievement_name:String;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;
        private var _achievementRecord:AchievementRecord;
        private var _completed:Boolean;
        private var _background:Bitmap;

        private var _txtHeader:SimpleTextField;
        private var _txtDescription:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setDescription():void
        {
            _txtDescription = new SimpleTextField();
            _txtDescription.size = 12;
            _txtDescription.width = 500;
            _txtDescription.multiline = true;
            _txtDescription.wordWrap = true;
            _txtDescription.color = 0x1697b5;

            this.addChild(_txtDescription);
            _txtDescription.x = 60;
            _txtDescription.y = 31;
            _txtDescription.text = this.description;
        }

        private function _setHeader():void
        {
            _txtHeader = new SimpleTextField();
            _txtHeader.size = 16;
            _txtHeader.color = 0x7bf3ff;
            _txtHeader.width = 500;
            this.addChild(_txtHeader);
            _txtHeader.text = this.achievement_name;
            _txtHeader.x = 60;
            _txtHeader.y = 11;
        }

        private function _setIcon():void
        {
            var posY:Number = _completed ? 50 : 0;

            _icon = this.source.getBitmap(ScreenData.ACHIEVEMENT_ICON);
            _icon.scrollRect = new Rectangle(0, posY, 50, 50);
            _icon.x = _icon.y = 5;

            this.addChild(_icon);
        }

        private function _setBackground():void
        {
            var back:String = _completed ? ScreenData.BACKGROUND_POSITION_ACTIVE : ScreenData.BACKGROUND_POSITION;
            _background = this.source.getBitmap(back);
            this.addChild(_background);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}