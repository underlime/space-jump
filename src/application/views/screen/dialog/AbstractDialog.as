package application.views.screen.dialog
{
    import application.views.base.ApplicationView;
    import application.views.screen.repair.RepairData;

    import flash.display.Bitmap;
    import flash.display.Shape;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    public class AbstractDialog extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AbstractDialog(htmlText:String)
        {
            super();
            _htmlText = htmlText;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupLabel();
        }

        override public function render():void
        {
            _setupBackground();
            _renderLabel();
        }

        override public function destroy():void
        {
            _txtLabel.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _background:Bitmap;
        protected var _txtLabel:SimpleTextField;
        protected var _shape:Shape;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupBackground():void
        {
            _shape = new Shape();
            addChild(_shape);
            _shape.graphics.beginFill(0x000000, .5);
            _shape.graphics.drawRect(0, 0, super.stage.stageWidth, super.stage.stageHeight);
            _shape.graphics.endFill();

            _background = super.source.getBitmap(RepairData.BACKGROUND);
            addChild(_background);
            _background.x = 200;
            _background.y = 193;
        }

        protected function _setupLabel():void
        {
            _txtLabel = new SimpleTextField();
            _txtLabel.width = 400;
            _txtLabel.align = TextFormatAlign.CENTER;
            _txtLabel.multiline = true;
        }

        protected function _renderLabel():void
        {
            addChild(_txtLabel);
            _txtLabel.x = _background.x;
            _txtLabel.y = _background.y + 47;
            _txtLabel.htmlText = _htmlText;
            _txtLabel.mouseEnabled = true;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _htmlText:String;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
