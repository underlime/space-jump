package application.views.screen.dialog
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;
    import application.views.text.TextFactory;

    import com.facebook.graph.Facebook;

    import flash.display.Bitmap;
    import flash.display.DisplayObjectContainer;
    import flash.events.Event;
    import flash.net.URLRequestMethod;

    import framework.socnet.SocNet;

    import org.casalib.events.RemovableEventDispatcher;

    public class FacebookPhotoWindow extends RemovableEventDispatcher
    {
        private static var _albumLink:String;

        private var _container:DisplayObjectContainer;
        private var _photo:Bitmap;
        private var _text:String;

        private var _dialog:PictureAlertWindow;

        public function FacebookPhotoWindow(container:DisplayObjectContainer, photo:Bitmap)
        {
            _container = container;
            _photo = photo;
            _text = TextFactory.instance.getLabel('add_photo_to_album_label');
        }

        override public function destroy():void
        {
            if (_dialog && !_dialog.destroyed)
                _dialog.destroy();
            _dialog = null;
            super.destroy();
        }

        public function show():void
        {
            if (_albumLink)
                _showDialog();
            else
                _requestAlbum();
        }

        private function _requestAlbum():void
        {
            //TODO заменить, если найдется способ лучше
            var method:String = '/me/albums';
            var params:Object = {
                'access_token': SocNet.instance().accessToken
            };
            Facebook.api(method, _onAlbums, params, URLRequestMethod.GET);
        }

        private function _onAlbums(...args):void
        {
            _albumLink = 'https://www.facebook.com/me/photos_albums';
            if (args.length && args[0] is Array)
                _searchAppAlbum(args[0]);
            _showDialog();
        }
        
        private function _searchAppAlbum(responseData:Array):void
        {
            for (var i:int=0; i<responseData.length; ++i) {
                if (responseData[i]['name'].indexOf(ApplicationConfiguration.APP_NAME) == 0) {
                    _albumLink = responseData[i]['link'];
                    break;
                }
            }
        }

        private function _showDialog():void
        {
            var text:String = _text.replace('{HREF}', _albumLink);
            _dialog = new PictureAlertWindow(text, _photo);
            _dialog.addEventListener(ApplicationEvent.CLICK_OK, _onDialogOk);
            _container.addChild(_dialog);

            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _onDialogOk(e:ApplicationEvent):void
        {
            if (_dialog && !_dialog.destroyed)
                _dialog.destroy();
            _dialog = null;
        }
    }

}
