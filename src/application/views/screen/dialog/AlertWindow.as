package application.views.screen.dialog
{
    import application.events.ApplicationEvent;
    import application.views.screen.elements.buttons.UniversalButton;

    import flash.events.MouseEvent;

    public class AlertWindow extends AbstractDialog
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AlertWindow(htmlText:String)
        {
            super(htmlText);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _setupOkButton();
        }

        override public function destroy():void
        {
            _okButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _okButton:UniversalButton;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupOkButton():void
        {
            _okButton = new UniversalButton('OK', true);
            addChild(_okButton);
            _okButton.x = _background.x + 128;
            _okButton.y = _background.y + 185;
            _okButton.addEventListener(MouseEvent.CLICK, _okClickHandler);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _okClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CLICK_OK);
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
