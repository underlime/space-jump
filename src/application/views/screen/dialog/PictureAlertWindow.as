package application.views.screen.dialog
{
    import application.events.ApplicationEvent;
    import application.views.screen.elements.buttons.UniversalButton;

    import flash.display.DisplayObject;
    import flash.events.MouseEvent;

    public class PictureAlertWindow extends AbstractDialog
    {
        public static const MAX_PIC_WIDTH:int = 100;
        public static const MAX_PIC_HEIGHT:int = 100;

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PictureAlertWindow(htmlText:String, picture:DisplayObject)
        {
            super(htmlText);
            _picture = picture;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _setupOkButton();
            _setupPicture();
        }

        override public function destroy():void
        {
            _okButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _okButton:UniversalButton;
        protected var _picture:DisplayObject;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupOkButton():void
        {
            _okButton = new UniversalButton('OK', true);
            addChild(_okButton);
            _okButton.x = _background.x + 128;
            _okButton.y = _background.y + 185;
            _okButton.addEventListener(MouseEvent.CLICK, _okClickHandler);
        }

        protected function _setupPicture():void
        {
            var width:int = _picture.width;
            var height:int = _picture.height;

            if (width > MAX_PIC_WIDTH) {
                width = MAX_PIC_WIDTH;
                height = width*_picture.height/_picture.width;
            }
            _picture.width = width;
            _picture.height = height;

            if (height > MAX_PIC_HEIGHT) {
                height = MAX_PIC_HEIGHT;
                height = height*_picture.width/_picture.height;
            }
            _picture.width = width;
            _picture.height = height;

            _picture.x = _background.x + (_background.width - width)/2;
            _picture.y = _background.y + 75;

            addChild(_picture);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _okClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CLICK_OK);
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
