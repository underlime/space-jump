/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.03.13
 * Time: 14:13
 */
package application.views.screen.dialog
{
    import application.events.ApplicationEvent;
    import application.views.screen.elements.buttons.UniversalButton;
    import application.views.text.TextFactory;

    import flash.events.MouseEvent;

    public class ConfirmWindow extends AbstractDialog
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ConfirmWindow(htmlText:String)
        {
            super(htmlText);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _setupCancelButton();
            _setupConfirmButton();
        }

        override public function destroy():void
        {
            _confirmButton.destroy();
            _cancelButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _cancelButton:UniversalButton;
        protected var _confirmButton:UniversalButton;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupCancelButton():void
        {
            var noLabel:String = TextFactory.instance.getLabel('no_label');
            _cancelButton = new UniversalButton(noLabel, true);
            _cancelButton.x = _background.x + 240;
            _cancelButton.y = _background.y + 185;
            _cancelButton.addEventListener(MouseEvent.CLICK, _cancelClickHandler);
            addChild(_cancelButton);
        }

        protected function _setupConfirmButton():void
        {
            var yesLabel:String = TextFactory.instance.getLabel('yes_label');
            _confirmButton = new UniversalButton(yesLabel);
            _confirmButton.x = _background.x + 14;
            _confirmButton.y = _background.y + 185;
            _confirmButton.addEventListener(MouseEvent.CLICK, _confirmClickHandler);
            addChild(_confirmButton);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _cancelClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CLICK_CANCEL);
            dispatchEvent(event);
        }

        private function _confirmClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CLICK_CONFIRM);
            dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
