/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.03.13
 * Time: 13:03
 */
package application.views.screen.alert
{
    import application.config.ApplicationConfiguration;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.geom.Point;
    import flash.geom.Rectangle;

    import framework.tools.ScreenShot;
    import framework.view.View;

    public class TrophyAlertPosition extends AlertPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrophyAlertPosition(icon:Bitmap, header:String, description:String, shareText:String)
        {
            icon.scrollRect = new Rectangle(0, 80, 80, 80);
            super(icon, header, description, shareText);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupIcon():void
        {
            addChild(_icon);
            _icon.x = 3;
            _icon.y = 0;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../../lib/share_pics/TROPHY_FOUND_EN.png", mimeType="image/png")]
        private var _ShareImageEn:Class;

        [Embed(source="../../../../../lib/share_pics/TROPHY_FOUND_RU.png", mimeType="image/png")]
        private var _ShareImageRu:Class;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        override public function get shareImage():Bitmap
        {
            var backGround:Bitmap;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                backGround = new _ShareImageRu();
            else
                backGround = new _ShareImageEn();

            var view:View = new View();
            view.addChild(backGround);

            var copyRect:Rectangle = new Rectangle(0, _icon.height, _icon.width, _icon.height);
            var destPoint:Point = new Point(0, 0);
            var croppedIconData:BitmapData = new BitmapData(_icon.width, _icon.height);
            croppedIconData.copyPixels(_icon.bitmapData, copyRect, destPoint);

            var trophyIcon:Bitmap = new Bitmap(croppedIconData);
            trophyIcon.x = 185;
            trophyIcon.y = 105;
            view.addChild(trophyIcon);

            var screenShot:ScreenShot = new ScreenShot();
            screenShot.takeShot(view);
            var image:Bitmap = new Bitmap(screenShot.rawData.clone());
            screenShot.destroy();

            return image;
        }

    }
}
