package application.views.screen.alert
{
    import application.models.ModelData;
    import application.models.Trophies;
    import application.models.trophies.TrophyRecord;
    import application.models.trophies.UsersTrophyRecord;

    import flash.events.Event;

    import framework.data.ModelsRegistry;

    public class ShowTrophiesMessages
    {
        private var _alertView:AlertView;
        private var _trophiesList:Array;
        private var _trophiesModel:Trophies;

        public function ShowTrophiesMessages(alertView:AlertView, trophiesList:Array)
        {
            _alertView = alertView;
            _trophiesList = trophiesList;
            _trophiesModel = ModelsRegistry.getModel(ModelData.TROPHIES_MODEL) as Trophies;
        }

        public function execute(e:Event=null):void
        {
            _trophiesModel.removeEventListener(Event.CHANGE, this.execute);

            var userTrophy:UsersTrophyRecord;
            var trophyId:String;
            var trophyInfo:TrophyRecord;
            for each (userTrophy in _trophiesList) {
                trophyId = userTrophy.trophy.value.toString();
                trophyInfo = _trophiesModel.getTrophyById(trophyId);
                _alertView.addTrophyComplete(trophyInfo);
            }
        }
    }
}
