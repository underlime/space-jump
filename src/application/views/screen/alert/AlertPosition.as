/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.03.13
 * Time: 12:09
 */
package application.views.screen.alert
{
    import application.events.ApplicationEvent;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.base.ApplicationView;
    import application.views.screen.elements.buttons.SquareCloseButton;
    import application.views.screen.elements.buttons.TellFriendsButton;

    import com.greensock.TweenLite;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.view.text.SimpleTextField;

    public class AlertPosition extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "SPACE_ACHIEVE_FIELD";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AlertPosition(icon:Bitmap, header:String, description:String, shareText:String)
        {
            _icon = icon;
            _header = header;
            _description = description;
            _shareText = shareText;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _setupHeader();
            _setupDescription();
            _setupIcon();
            _setupCloseButton();
            _setupTellButton();
            _setupAppearance();
        }

        override public function destroy():void
        {
            _closeButton.removeEventListener(MouseEvent.CLICK, _closeClickHandler);
            _tellButton.removeEventListener(MouseEvent.CLICK, _tellClickHandler);
            _closeButton.destroy();
            _tellButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _icon:Bitmap;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupIcon():void
        {
            addChild(_icon);
            _icon.x = 14;
            _icon.y = 15;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _txtHeader:SimpleTextField;
        private var _txtDescription:SimpleTextField;

        private var _header:String;
        private var _description:String;

        private var _closeButton:SquareCloseButton;
        private var _tellButton:TellFriendsButton;

        [Embed(source="../../../../../lib/share_pics/MISSION_COMPLETE_EN.png", mimeType="image/png")]
        private var _ShareImage:Class;

        private var _shareText:String;


        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _background = super.source.getBitmap(BACKGROUND);
            addChild(_background);
        }

        private function _setupHeader():void
        {
            _txtHeader = new SimpleTextField();
            _txtHeader.size = 14;
            _txtHeader.color = 0x7bf3ff;
            _txtHeader.width = 500;
            _txtHeader.x = 78;
            _txtHeader.y = 16;
            addChild(_txtHeader);
            _txtHeader.text = _header;
        }

        private function _setupDescription():void
        {
            _txtDescription = new SimpleTextField();
            _txtDescription.size = 10;
            _txtDescription.width = 500;
            _txtDescription.multiline = true;
            _txtDescription.wordWrap = true;
            _txtDescription.color = 0x1697b5;

            addChild(_txtDescription);
            _txtDescription.x = 78;
            _txtDescription.y = 37;
            _txtDescription.text = _description;
        }

        private function _setupCloseButton():void
        {
            _closeButton = new SquareCloseButton();
            addChild(_closeButton);
            _closeButton.x = 270;
            _closeButton.y = 21;
            _closeButton.addEventListener(MouseEvent.CLICK, _closeClickHandler);
        }

        private function _setupTellButton():void
        {
            _tellButton = new TellFriendsButton();
            addChild(_tellButton);
            _tellButton.x = 325;
            _tellButton.y = 11;
            _tellButton.addEventListener(MouseEvent.CLICK, _tellClickHandler);
        }

        private function _setupAppearance():void
        {
            this.alpha = 0;
            TweenLite.to(this, .3, {alpha:1});
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeClickHandler(event:MouseEvent):void
        {
            SoundManager.instance.playUISound(UISounds.BUTTON_CLOSE);
            var customEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CLOSE);
            super.dispatchEvent(customEvent);
        }

        private function _tellClickHandler(event:MouseEvent):void
        {
            var customEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.SHARE, true);
            customEvent.data.icon = _icon;
            customEvent.data.header = _header;
            customEvent.data.description = _description;
            super.dispatchEvent(customEvent);

            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get shareImage():Bitmap
        {
            return new _ShareImage();
        }

        public function get shareText():String
        {
            return _shareText;
        }
    }
}
