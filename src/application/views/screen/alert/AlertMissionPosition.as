/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.03.13
 * Time: 12:59
 */
package application.views.screen.alert
{
    import application.config.ApplicationConfiguration;
    import application.views.screen.ScreenData;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.tools.SourceManager;

    public class AlertMissionPosition extends AlertPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AlertMissionPosition(header:String, description:String, shareText:String)
        {
            var icon:Bitmap = SourceManager.instance.getBitmap(ScreenData.ACHIEVEMENT_ICON);
            icon.scrollRect = new Rectangle(0, 50, 50, 50);
            super(icon, header, description, shareText);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../../lib/share_pics/MISSION_COMPLETE_EN.png", mimeType="image/png")]
        private var _ShareImageEn:Class;

        [Embed(source="../../../../../lib/share_pics/MISSION_COMPLETE_RU.png", mimeType="image/png")]
        private var _ShareImageRu:Class;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        override public function get shareImage():Bitmap
        {
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                return new _ShareImageRu();
            else
                return new _ShareImageEn();
        }
    }
}
