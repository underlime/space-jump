/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 30.03.13
 * Time: 12:09
 */
package application.views.screen.alert
{
    import application.config.ApplicationConfiguration;
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.models.Achievements;
    import application.models.ModelData;
    import application.models.Trophies;
    import application.models.UserAchievements;
    import application.models.UsersTrophies;
    import application.models.achievements.AchievementRecord;
    import application.models.trophies.TrophyRecord;
    import application.models.user_achievements.UserAchievementRecord;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.data.DataBase;
    import framework.data.ModelDataEvent;
    import framework.error.ApplicationError;
    import framework.view.View;

    public class AlertView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AlertView(appController:ApplicationController)
        {
            super();
            _appController = appController;
            _dataBase = appController.dataBase;
            _setupModels();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _userAchievementsModel.removeEventListener(ModelDataEvent.ADD, _onAddMissions);
            _userTrophiesModel.removeEventListener(ModelDataEvent.ADD, _onAddTrophies);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addMissionComplete(achievementInfo:AchievementRecord):void
        {
            var title:String = TextFactory.instance.getLabel('mission_share_label');
            var name:String;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                name = achievementInfo.name_ru.value;
            else
                name = achievementInfo.name_en.value;

            var shareTitle:String = TextFactory.instance.getLabel('mission_share_full_label');
            var shareText:String = shareTitle + ': ' + name;
            var missionAlert:AlertMissionPosition = new AlertMissionPosition(title.toUpperCase() + '!', name.toUpperCase(), shareText);
            missionAlert.x = missionAlert.y = 6;
            addChild(missionAlert);
            SoundManager.instance.playUISound(UISounds.ACHIEVEMENT_UNLOCK);

            _addAlert(missionAlert);
        }

        public function addTrophyComplete(trophyInfo:TrophyRecord):void
        {
            var icon:Bitmap = super.source.getBitmap(trophyInfo.image.value);
            var title:String = TextFactory.instance.getLabel('trophy_share_label');
            var name:String;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                name = trophyInfo.name_ru.value;
            else
                name = trophyInfo.name_en.value;

            var shareTitle:String = TextFactory.instance.getLabel('trophy_share_full_label');
            var shareText:String = shareTitle + ': ' + name;
            var alert:TrophyAlertPosition = new TrophyAlertPosition(icon, title.toUpperCase() + '!', name.toUpperCase(), shareText);
            alert.x = alert.y = 6;
            addChild(alert);
            SoundManager.instance.playUISound(UISounds.ACHIEVEMENT_UNLOCK);

            _addAlert(alert);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _pool:Vector.<AlertPosition> = new Vector.<AlertPosition>();
        private var _margin:Number = 88;
        private var _appController:ApplicationController;
        private var _dataBase:DataBase;

        private var _achievementsModel:Achievements;
        private var _trophiesModel:Trophies;
        private var _userAchievementsModel:UserAchievements;
        private var _userTrophiesModel:UsersTrophies;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupModels():void
        {
            _achievementsModel = _dataBase.getChildByName(ModelData.ACHIEVEMENTS_MODEL) as Achievements;
            _trophiesModel = _dataBase.getChildByName(ModelData.TROPHIES_MODEL) as Trophies;
            _userAchievementsModel = _dataBase.getChildByName(ModelData.USER_ACHIEVEMENTS_MODEL) as UserAchievements;
            _userTrophiesModel = _dataBase.getChildByName(ModelData.USERS_TROPHIES_MODEL) as UsersTrophies;

            _userAchievementsModel.addEventListener(ModelDataEvent.ADD, _onAddMissions);
            _userTrophiesModel.addEventListener(ModelDataEvent.ADD, _onAddTrophies);
        }

        private function _onAddMissions(e:ModelDataEvent):void
        {
            var missionsList:Array = e.data as Array;
            var userAchievement:UserAchievementRecord;
            var achievementId:String;
            var achievementInfo:AchievementRecord;
            for each (userAchievement in missionsList) {
                achievementId = userAchievement.achievement.value.toString();
                achievementInfo = _achievementsModel.getRecordById(achievementId) as AchievementRecord;
                if (achievementInfo) {
                    addMissionComplete(achievementInfo);
                }
                else {
                    var error:ApplicationError = new ApplicationError();
                    error.type = ApplicationError.SEVER_ERROR;
                    error.message = 'Wrong achievement data';
                    _appController.appError(error);
                    break;
                }
            }
        }

        private function _onAddTrophies(e:ModelDataEvent):void
        {
            var trophiesList:Array = e.data as Array;
            var showTrophies:ShowTrophiesMessages = new ShowTrophiesMessages(this, trophiesList);
            if (_trophiesModel.isEmpty)
                _trophiesModel.addEventListener(Event.CHANGE, showTrophies.execute);
            else
                showTrophies.execute();
        }

        private function _updatePositions():void
        {
            var cy:Number = 8;
            for (var i:int = 0; i < _pool.length; i++)
            {
                _pool[i].y = cy;
                cy += _margin;
            }
        }

        private function _addAlert(alert:AlertPosition):void
        {
            _pool.push(alert);
            _updatePositions();
            alert.addEventListener(ApplicationEvent.CLOSE, _closeAlertHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeAlertHandler(event:ApplicationEvent):void
        {
            var alert:AlertPosition = event.currentTarget as AlertPosition;
            var index:int = _pool.indexOf(alert);
            if (index > -1)
            {
                alert.removeEventListener(ApplicationEvent.CLOSE, _closeAlertHandler);
                alert.destroy();
                _pool.splice(index, 1);
            }
            _updatePositions();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
