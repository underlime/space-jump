/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.03.13
 * Time: 14:57
 */
package application.views.screen.sound
{
    import application.config.ApplicationConfiguration;
    import application.sound.SoundManager;
    import application.views.base.ApplicationView;
    import application.views.screen.elements.checkbox.BaseCheckBox;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.net.SharedObject;

    public class SoundSettingsView extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "MUSIC_MENU_BG";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SoundSettingsView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _storage = SharedObject.getLocal(ApplicationConfiguration.APP_NAME, '/');
        }

        override public function render():void
        {
            _setupBackground();
            _setupCheckBoxes();
        }

        override public function destroy():void
        {
            _musicCheckBox.removeEventListener(Event.CHANGE, _musicChangeHandler);
            _soundCheckbox.removeEventListener(Event.CHANGE, _soundChangeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _musicCheckBox:BaseCheckBox;
        private var _soundCheckbox:BaseCheckBox;
        private var _sound:SoundManager = SoundManager.instance;

        private var _storage:SharedObject;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _background = super.source.getBitmap(BACKGROUND);
            addChild(_background);
        }

        private function _setupCheckBoxes():void
        {
            _musicCheckBox = new BaseCheckBox(super.textFactory.getLabel('music_label'));
            _soundCheckbox = new BaseCheckBox(super.textFactory.getLabel('sounds_label'));

            addChild(_musicCheckBox);
            addChild(_soundCheckbox);

            _musicCheckBox.addEventListener(Event.CHANGE, _musicChangeHandler);
            _soundCheckbox.addEventListener(Event.CHANGE, _soundChangeHandler);

            _musicCheckBox.x = 27;
            _musicCheckBox.y = 17;

            _soundCheckbox.x = 27;
            _soundCheckbox.y = 49;

            _checkSettings();
        }

        private function _checkSettings():void
        {
            if (_storage.data.sound_enabled == null) {
                _storage.data.sound_enabled = 1;
            }
            else {
                var soundEnabled:int = int(_storage.data.sound_enabled);
                if (soundEnabled < 1)
                    _soundCheckbox.checked = false;
            }

            if (_storage.data.music_enabled == null) {
                _storage.data.music_enabled = 1;
            }
            else {
                var musicEnabled:int = int(_storage.data.music_enabled);
                if (musicEnabled < 1)
                    _musicCheckBox.checked = false;
            }
            _storage.flush();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _musicChangeHandler(event:Event):void
        {
            _sound.musicMute = !_musicCheckBox.checked;
            if (_musicCheckBox.checked)
                _storage.data.music_enabled = 1;
            else
                _storage.data.music_enabled = 0;

            _storage.flush();
        }

        private function _soundChangeHandler(event:Event):void
        {
            _sound.soundMute = !_soundCheckbox.checked;
            if (_soundCheckbox.checked)
                _storage.data.sound_enabled = 1;
            else
                _storage.data.sound_enabled = 0;

            _storage.flush();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
