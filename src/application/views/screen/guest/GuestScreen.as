/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 22.04.13
 * Time: 19:50
 */
package application.views.screen.guest
{
    import application.controllers.guest_screen_controller.UserDataRecord;
    import application.models.Items;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.models.skin.SkinModel;
    import application.models.skin.SkinsCollection;
    import application.views.construct.Constructor;
    import application.views.construct.body.MainScreenJumper;
    import application.views.screen.elements.buttons.ExploreButton;
    import application.views.screen.main.CosmosLayer;

    import framework.view.View;

    public class GuestScreen extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GuestScreen(userData:UserDataRecord, skins:SkinsCollection, userModel:User)
        {
            _userData = userData;
            _skins = skins;
            _userModel = userModel;
            super();
        }

        public function update():void
        {
            if (_cosmosLayer.cosmonaut && !_cosmosLayer.cosmonaut.destroyed)
                _cosmosLayer.cosmonaut.destroy();

            _setCosmonaut();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupCosmosLayer();
            _setupControlScreen();
            _setupCapsule();
            _setCosmonaut();
        }

        override public function destroy():void
        {
            _cosmosLayer.destroy();
            _controlScreen.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _cosmosLayer:CosmosLayer;
        private var _controlScreen:GuestControlScreen;
        private var _userData:UserDataRecord;
        private var _skins:SkinsCollection;
        private var _userModel:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupCosmosLayer():void
        {
            _cosmosLayer = new CosmosLayer();
            addChild(_cosmosLayer);
        }

        private function _setupControlScreen():void
        {
            _controlScreen = new GuestControlScreen(_userData, _userModel);
            addChild(_controlScreen);
        }

        private function _setupCapsule():void
        {
            var item:ItemRecord = _userData.inventory.getEquippedItem(Items.ACTION_CAPSULE);
            if (item) {
                _cosmosLayer.capsule = super.source.getBitmap(item.symbol.value);
            }
        }

        private function _setCosmonaut():void
        {
            var item:ItemRecord = _userData.inventory.getEquippedItem(Items.ACTION_SUITE);
            if (item) {
                var skinData:String = (item && item.symbol.value) ? item.symbol.value : "default";
                var skin:SkinModel = _skins.getSkin(skinData);

                var hatItem:ItemRecord = _userData.inventory.getEquippedItem(Items.ACTION_HAT);
                var crownItem:ItemRecord = _userData.inventory.getEquippedItem(Items.ACTION_CROWN);

                var cosmonaut:MainScreenJumper = Constructor.getMainScreenJumper(skin);
                cosmonaut.removeHat();

                if (crownItem && super.source.has(crownItem.symbol.value)) {
                    cosmonaut.addHat(crownItem.symbol.value);
                } else {
                    if (hatItem && super.source.has(hatItem.symbol.value))
                        cosmonaut.addHat(hatItem.symbol.value);
                }

                _cosmosLayer.cosmonaut = cosmonaut;
                cosmonaut.disableHover();
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get exploreButton():ExploreButton
        {
            return _controlScreen.exploreButton;
        }
    }
}
