/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 22.04.13
 * Time: 20:33
 */
package application.views.screen.guest
{
    import application.controllers.guest_screen_controller.UserDataRecord;
    import application.events.ApplicationEvent;
    import application.models.User;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.screen.elements.buttons.ExploreButton;
    import application.views.screen.elements.buttons.HomeButton;
    import application.views.screen.main.panel.navigation.MissionButton;
    import application.views.screen.main.panel.navigation.ProfileButton;
    import application.views.screen.main.panel.navigation.TrophyButton;
    import application.views.screen.main.panel.params.UserParams;

    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.net.navigateToURL;

    import framework.helpers.ProfileLinkHelper;
    import framework.view.View;

    public class GuestControlScreen extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GuestControlScreen(userData:UserDataRecord, userModel:User)
        {
            super();
            _userData = userData;
            _userModel = userModel;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupHomeButton();
            if (_needExploreButton())
                _setupExploreButton();
            _setupMissionsButton();
            _setupProfileButton();
            _setupTrophyButton();
            _setupUserParams();
        }

        override public function destroy():void
        {
            _homeButton.removeEventListener(MouseEvent.CLICK, _homeClickHandler);
            _homeButton.destroy();
            if (_exploreButton) {
                _exploreButton.removeEventListener(MouseEvent.CLICK, _exploreClickHandler);
                _exploreButton.destroy();
            }
            _missionsButton.destroy();
            _trophyButton.destroy();
            _profileButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _homeButton:HomeButton;
        private var _exploreButton:ExploreButton;

        private var _missionsButton:MissionButton;
        private var _trophyButton:TrophyButton;
        private var _profileButton:ProfileButton;

        private var _userData:UserDataRecord;
        private var _userParamsField:UserParams;
        private var _userModel:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _needExploreButton():Boolean
        {
            var canById:Boolean = (_userModel.id.value != _userData.userInfo.id.value);

            var nowTime:Number = (new Date()).getTime();
            var canExploreByCount:Boolean = (
                nowTime - _userModel.last_explore_guest_time.value.getTime() > 86400000
                    || _userModel.explored_capsules.value < _userModel.max_capsules_to_explore.value
                );

            var canExploreByTime:Boolean = (nowTime - _userData.userInfo.last_explore_time.value.getTime() > 86400000);

            return (canById && canExploreByCount && canExploreByTime);
        }

        private function _setupMissionsButton():void
        {
            _missionsButton = new MissionButton();
            _missionsButton.x = 580;
            _missionsButton.y = 10;
            addChild(_missionsButton);
            _missionsButton.addEventListener(MouseEvent.CLICK, _missionsClickHandler);
        }

        private function _setupTrophyButton():void
        {
            _trophyButton = new TrophyButton();
            _trophyButton.x = 690;
            _trophyButton.y = 10;
            addChild(_trophyButton);
            _trophyButton.addEventListener(MouseEvent.CLICK, _trophiesClickHandler);
        }

        private function _setupProfileButton():void
        {
            _profileButton = new ProfileButton();
            _profileButton.x = 470;
            _profileButton.y = 10;
            addChild(_profileButton);
            _profileButton.addEventListener(MouseEvent.CLICK, _profileClickHandler);
        }

        private function _setupUserParams():void
        {
            _userParamsField = new UserParams(_userData.userInfo);
            addChild(_userParamsField);
            _userParamsField.x = 30;
            _userParamsField.y = 200;
        }

        private function _setupHomeButton():void
        {
            _homeButton = new HomeButton();
            _homeButton.x = 10;
            _homeButton.y = 10;
            _homeButton.addEventListener(MouseEvent.CLICK, _homeClickHandler);
            addChild(_homeButton);
        }

        private function _setupExploreButton():void
        {
            _exploreButton = new ExploreButton();
            _exploreButton.x = 265;
            _exploreButton.y = 533;
            _exploreButton.addEventListener(MouseEvent.CLICK, _exploreClickHandler);
            addChild(_exploreButton);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _homeClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE, true));
            SoundManager.instance.playUISound(UISounds.BUTTON_CLOSE);
        }

        private function _exploreClickHandler(event:MouseEvent):void
        {
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);

            var appEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.EXPLORE_GUEST, true);
            appEvent.data['user_id'] = _userData.userInfo.id.value;
            super.dispatchEvent(appEvent);
        }

        private function _missionsClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_MISSIONS, true);
            dispatchEvent(event);
        }

        private function _trophiesClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_TROPHY, true);
            dispatchEvent(event);
        }

        private function _profileClickHandler(event:MouseEvent):void
        {
            var url:String = ProfileLinkHelper.getLink(_userData.userInfo.soc_net_id.value);
            navigateToURL(new URLRequest(url), "_blank");
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get exploreButton():ExploreButton
        {
            return _exploreButton;
        }
    }
}
