package application.views.screen.bonus_window
{
    import application.views.screen.bonus_window.base.BaseBonusTile;

    import flash.display.DisplayObject;
    import flash.events.MouseEvent;

    public class FriendsBonusTile extends BaseBonusTile
    {
        private var _count:int;
        private var _name:String;
        private var _action:String;

        public function FriendsBonusTile(picture:DisplayObject, name:String, count:int, action:String)
        {
            super(TYPE_REGULAR);
            _picture = picture;
            _name = name;
            _count = count;
            _action = action;
        }

        override public function render():void
        {
            super.render();
            _setupText();
            _setupPicture();
            this.buttonMode = true;
            this.useHandCursor = true;
            this.addEventListener(MouseEvent.MOUSE_OVER, _onMouseOver);
            this.addEventListener(MouseEvent.MOUSE_OUT, _onMouseOut);
        }

        private function _onMouseOver(e:MouseEvent):void
        {
            this.setType(TYPE_PASSED);
        }

        private function _onMouseOut(e:MouseEvent):void
        {
            this.setType(TYPE_REGULAR);
        }

        private function _setupText():void
        {
            _headerText.htmlText = _name;
            _descriptionText.htmlText = '×' + _count;
        }

        private function _setupPicture():void
        {
            _picture.x = (TILE_WIDTH - ICON_WIDTH)/2;
            _picture.y = (TILE_HEIGHT - ICON_HEIGHT)/2;
            addChild(_picture);
        }

        public function get action():String
        {
            return _action;
        }
    }
}
