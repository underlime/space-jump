package application.views.screen.bonus_window.base
{
    import application.config.ApplicationConfiguration;
    import application.views.base.ApplicationView;

    import flash.display.DisplayObject;

    import flashx.textLayout.formats.TextAlign;

    import framework.view.text.SimpleTextField;

    public class BaseBonusWindow extends ApplicationView
    {
        public static const WINDOW_WIDTH:int = 680;
        public static const WINDOW_HEIGHT:int = 410;
        public static const LOGO_WIDTH:int = 150;
        public static const LOGO_HEIGHT:int = 36;

        [Embed(source="../../../../../../lib/daily_bonus/DAILY_BG.png")]
        protected var _WindowBackground:Class;

        [Embed(source="../../../../../../lib/daily_bonus/EN_LOGO.png")]
        protected var _LogoEn:Class;

        [Embed(source="../../../../../../lib/daily_bonus/RU_LOGO.png")]
        protected var _LogoRu:Class;

        [Embed(source="../../../../../../lib/daily_bonus/GEM.png")]
        protected var _GemIcon:Class;

        protected var _background:DisplayObject;
        protected var _stageWidth:int;
        protected var _stageHeight:int;
        protected var _logo:DisplayObject;

        protected var _backgroundX:Number;
        protected var _backgroundY:Number;
        protected var _headerText:SimpleTextField;
        protected var _bonusText:SimpleTextField;
        protected var _tilesVector:Vector.<BaseBonusTile> = new Vector.<BaseBonusTile>();
        protected var _windowWidth:int;
        protected var _windowHeight:int;

        public function BaseBonusWindow()
        {
            _stageWidth = super.stage.stageWidth;
            _stageHeight = super.stage.stageHeight;
            _windowWidth = WINDOW_WIDTH;
            _windowHeight = WINDOW_HEIGHT;
        }

        override public function render():void
        {
            super.render();
            _setBgShadow();
            _setupBackground();
            _setupTextFields();
        }

        protected function _setupBackground():void
        {
            _background = new _WindowBackground();
            addChild(_background);

            _backgroundX = (_stageWidth - _windowWidth)/2;
            _backgroundY = (_stageHeight - _windowHeight)/2;
            _background.x = _backgroundX;
            _background.y = _backgroundY;

            _setupLogo();
        }

        protected function _setupLogo():void
        {
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                _logo = new _LogoRu();
            else
                _logo = new _LogoEn();
            _logo.y = (_backgroundY + LOGO_HEIGHT + 25);
            _logo.x = (_stageWidth - LOGO_WIDTH)/2;
            addChild(_logo);
        }

        protected function _setupTextFields():void
        {
            _headerText = new SimpleTextField();
            _headerText.size = 22;
            _headerText.color = 0xb8fcff;
            _headerText.align = TextAlign.CENTER;
            _headerText.x = _backgroundX;
            _headerText.y = _backgroundY + 105;
            _headerText.width = _windowWidth;
            addChild(_headerText);

            _bonusText = new SimpleTextField();
            _bonusText.size = 14;
            _bonusText.color = 0xb8fcff;
            _bonusText.align = TextAlign.CENTER;
            _bonusText.x = _backgroundX;
            _bonusText.y = _backgroundY + 145;
            _bonusText.width = _windowWidth;
            addChild(_bonusText);
        }


        override public function destroy():void
        {
            if (_background)
                removeChild(_background);
            _background = null;

            if (_logo)
                removeChild(_logo);
            _logo = null;

            if (_headerText)
                _headerText.destroy();
            _headerText = null;

            if (_bonusText)
                _bonusText.destroy();
            _bonusText = null;

            for (var i:int = 0; i < _tilesVector.length; ++i) {
                if (_tilesVector[i])
                    _tilesVector[i].destroy();
                _tilesVector[i] = null;
            }

            super.destroy();
        }
    }
}
