package application.views.screen.bonus_window.base
{
    import application.views.base.ApplicationView;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.geom.Rectangle;

    import flashx.textLayout.formats.TextAlign;

    import framework.tools.SourceManager;
    import framework.view.text.SimpleTextField;

    public class BaseBonusTile extends ApplicationView
    {
        public static const TILE_WIDTH:int = 80;
        public static const TILE_HEIGHT:int = 110;

        public static const ICON_WIDTH:int = 50;
        public static const ICON_HEIGHT:int = 50;

        public static const TYPE_REGULAR:int = 0;
        public static const TYPE_PASSED:int = 1;
        public static const TYPE_SELECTED:int = 2;

        [Embed(source="../../../../../../lib/daily_bonus/DAY_BG.png")]
        protected var _TileBackground:Class;

        [Embed(source="../../../../../../lib/daily_bonus/GEM.png")]
        protected var _GemIcon:Class;

        protected var _background:DisplayObject;
        protected var _rectangle:Rectangle;

        protected var _type:int;
        protected var _picture:DisplayObject;
        protected var _headerText:SimpleTextField;
        protected var _descriptionText:SimpleTextField;
        protected var _shine:MovieClip;

        public function BaseBonusTile(type:int)
        {
            _type = type;
        }

        override public function render():void
        {
            super.render();
            _setupBackground();
            _setupTextFields();
            if (_type == TYPE_SELECTED)
                _setupShine();
        }

        protected function _setupBackground():void
        {
            _destroyBackground();
            _background = new _TileBackground();
            _rectangle = new Rectangle(0, 0, TILE_WIDTH, TILE_HEIGHT);
            if (_type == TYPE_PASSED)
                _rectangle.y = TILE_HEIGHT;
            else
                if (_type == TYPE_SELECTED)
                    _rectangle.y = TILE_HEIGHT*2;
            _background.scrollRect = _rectangle;
            addChildAt(_background, 0);
        }

        protected function _setupTextFields():void
        {
            _headerText = new SimpleTextField();
            _headerText.size = 12;
            _headerText.color = 0xb8fcff;
            _headerText.align = TextAlign.CENTER;
            _headerText.width = TILE_WIDTH;
            _headerText.x = 0;
            _headerText.y = 8;
            _headerText.fontFamily = SimpleTextField.PT_SANS;
            addChild(_headerText);

            _descriptionText = new SimpleTextField();
            _descriptionText.size = 16;
            _descriptionText.color = 0xffffff;
            _descriptionText.align = TextAlign.CENTER;
            _descriptionText.width = TILE_WIDTH;
            _descriptionText.x = -4;
            _descriptionText.y = TILE_HEIGHT - 30;
            _descriptionText.fontFamily = SimpleTextField.PT_SANS;
            addChild(_descriptionText);
        }

        protected function _setupShine():void
        {
            _shine = SourceManager.instance.getMovieClip('DAILY_BONUS_SHINE');
            addChild(_shine);
        }

        override public function destroy():void
        {
            _destroyBackground();

            if (_picture)
                removeChild(_picture);
            _picture = null;

            if (_headerText)
                _headerText.destroy();
            _headerText = null;

            if (_shine && this.contains(_shine))
                removeChild(_shine);
            _shine = null;

            super.destroy();
        }

        protected function _destroyBackground():void
        {
            if (_background)
                removeChild(_background);
            _background = null;
        }

        public function setType(type:int):void
        {
            _type = type;
            _setupBackground();
        }
    }
}
