package application.views.screen.bonus_window
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.User;
    import application.views.screen.bonus_window.base.BaseBonusTile;
    import application.views.screen.bonus_window.base.BaseBonusWindow;
    import application.views.text.TextFactory;

    import flash.events.MouseEvent;

    import framework.data.ModelsRegistry;
    import framework.tools.SourceManager;

    import org.casalib.util.NumberUtil;

    public class FriendsBonusesDialog extends BaseBonusWindow
    {
        public static const WINDOW_WIDTH:int = 520;
        public static const WINDOW_HEIGHT:int = 350;

        [Embed(source="../../../../../lib/daily_bonus/SPACEDIVER_START_GIFTS_2.png")]
        private var _FriendsBackground:Class;

        private var _friendsCount:int;
        private var _userModel:User;

        public function FriendsBonusesDialog(friendsCount:int)
        {
            super();
            _friendsCount = friendsCount;
            _windowWidth = WINDOW_WIDTH;
            _windowHeight = WINDOW_HEIGHT;
            _userModel = ModelsRegistry.getModel(ModelData.USER_MODEL) as User;
        }

        override public function render():void
        {
            super.render();
            _setupText();
            _setupTiles();
        }

        private function _setupText():void
        {
            _headerText.htmlText = TextFactory.instance
                .getLabel('friends_header_message')
                .replace('{COUNT}', NumberUtil.format(_friendsCount, ' '));
            _bonusText.htmlText = TextFactory.instance
                .getLabel('friends_message');
        }

        private function _setupTiles():void
        {
            var tilesData:Array = [
                {
                    'action': 'coins',
                    'name_ru': 'Монеты',
                    'name_en': 'Coins',
                    'count': _userModel.friends_gifts.coins.value,
                    'picture': SourceManager.instance.getBitmap('MONEY_UPGRADE_ICO')
                },
                {
                    'action': Items.ACTION_PARACHUTE,
                    'name_ru': 'Парашют',
                    'name_en': 'Parachute',
                    'count': _userModel.friends_gifts.parachute.value,
                    'picture': SourceManager.instance.getBitmap('SPACE_PARASHUT_ICON')
                },
                {
                    'action': Items.ACTION_RESURRECTION,
                    'name_ru': 'Воскрешение',
                    'name_en': 'Resurrection',
                    'count': _userModel.friends_gifts.resurrection.value,
                    'picture': SourceManager.instance.getBitmap('RESSURECTION_ICO')
                },
                {
                    'action': Items.ACTION_ROCKET,
                    'name_ru': 'Ракета',
                    'name_en': 'Rocket',
                    'count': _userModel.friends_gifts.rocket.value,
                    'picture': SourceManager.instance.getBitmap('ROCKET_ICO')
                },
                {
                    'action': 'gem',
                    'name_ru': 'Кристалл',
                    'name_en': 'Gem',
                    'count': _userModel.friends_gifts.gems.value,
                    'picture': new _GemIcon()
                }
            ];

            var margin:int = 10;
            var tilesAreaWidth:int = BaseBonusTile.TILE_WIDTH*tilesData.length + margin*(tilesData.length - 1);
            var posX:int = _backgroundX + (_windowWidth - tilesAreaWidth)/2;
            var posY:int = _backgroundY + 195;

            for (var i:int = 0; i < tilesData.length; ++i) {
                var data:Object = tilesData[i];
                var name:String;
                if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                    name = data['name_ru'];
                else
                    name = data['name_en'];

                var tile:FriendsBonusTile = new FriendsBonusTile(
                    data['picture'],
                    name,
                    data['count'],
                    data['action']
                );
                tile.x = posX;
                tile.y = posY;

                _tilesVector.push(tile);
                tile.addEventListener(MouseEvent.CLICK, _onTileClick);
                addChild(tile);
                tile.render();

                posX += BaseBonusTile.TILE_WIDTH + margin;
            }
        }

        private function _onTileClick(e:MouseEvent):void
        {
            this.setPreloader();
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_FRIEND_BONUS);
            event.data = {'action': e.target.action};
            this.dispatchEvent(event);
        }

        override protected function _setupBackground():void
        {
            _background = new _FriendsBackground();
            addChild(_background);

            _backgroundX = (_stageWidth - WINDOW_WIDTH)/2;
            _backgroundY = (_stageHeight - WINDOW_HEIGHT)/2;
            _background.x = _backgroundX;
            _background.y = _backgroundY;

            _setupLogo();
        }
    }
}
