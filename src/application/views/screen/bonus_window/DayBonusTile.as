package application.views.screen.bonus_window
{
    import application.views.screen.bonus_window.base.BaseBonusTile;
    import application.views.text.TextFactory;

    import framework.tools.SourceManager;

    public class DayBonusTile extends BaseBonusTile
    {
        private static const PICTURES_TABLE:Object = {
            0: 'MONEY_UPGRADE_ICO',
            1: 'ROCKET_ICO',
            2: 'RESSURECTION_ICO',
            3: 'SPACE_PARASHUT_ICON',
            4: 'MEGAROCKET_ICON',
            5: 'RESSURECTION_PLUS_ICO'
        };

        private static const DESCRIPTIONS_TABLE:Object = {
            0: '×250',
            1: '×1',
            2: '×1',
            3: '×1',
            4: '×1',
            5: '×1',
            6: '×2'
        };

        private var _dayNumber:int;

        public function DayBonusTile(dayNimber:int, type:int = TYPE_REGULAR)
        {
            super(type);
            _dayNumber = dayNimber;
        }

        override public function render():void
        {
            super.render();
            _setupPicture();
            _setupText();
        }

        private function _setupPicture():void
        {
            if (_dayNumber < 6)
                _picture = SourceManager.instance.getBitmap(PICTURES_TABLE[_dayNumber]);
            else
                _picture = new _GemIcon();
            _picture.x = (TILE_WIDTH - ICON_WIDTH)/2;
            _picture.y = (TILE_HEIGHT - ICON_HEIGHT)/2;
            addChild(_picture);
        }

        private function _setupText():void
        {
            _headerText.htmlText = TextFactory.instance.getLabel('day_label')
                + ' ' + (_dayNumber + 1).toString();
            _descriptionText.htmlText = DESCRIPTIONS_TABLE[_dayNumber];
        }
    }
}
