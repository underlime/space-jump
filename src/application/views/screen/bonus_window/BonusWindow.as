package application.views.screen.bonus_window
{
    import application.views.screen.bonus_window.base.BaseBonusTile;
    import application.views.screen.bonus_window.base.BaseBonusWindow;
    import application.views.screen.elements.buttons.UniversalButton;
    import application.views.text.TextFactory;

    import flash.events.MouseEvent;

    public class BonusWindow extends BaseBonusWindow
    {
        protected var _okButton:UniversalButton;
        protected var _prizeName:String;
        protected var _visitsCount:int;

        public function BonusWindow(visitsCount:int, prizeName:String = '')
        {
            super();
            _visitsCount = visitsCount;
            _prizeName = prizeName;
        }

        override public function render():void
        {
            super.render();

            _setupText();
            _setupTiles();
            _setupOkButton();
        }

        protected function _setupText():void
        {
            _headerText.htmlText = TextFactory.instance.getLabel('everyday_bonus_header').toUpperCase();
            _bonusText.htmlText = TextFactory.instance
                .getLabel('everyday_bonus_description')
                .replace('{PRIZE}', _prizeName)
                .toUpperCase();
        }

        protected function _setupTiles():void
        {
            var posX:Number = _backgroundX + 32;
            var posY:Number = _backgroundY + 197;

            var daysCount:int = 7;
            var tile:DayBonusTile;

            var visitsLim:int;
            if (_visitsCount > 0)
                visitsLim = _visitsCount - 1;
            else
                visitsLim = 6;

            var type:int;
            for (var i:int = 0; i < daysCount; ++i) {
                if (i < visitsLim)
                    type = BaseBonusTile.TYPE_PASSED;
                else
                    if (i == visitsLim)
                        type = BaseBonusTile.TYPE_SELECTED;
                    else
                        type = BaseBonusTile.TYPE_REGULAR;

                tile = new DayBonusTile(i, type);
                tile.x = posX;
                tile.y = posY;

                _tilesVector.push(tile);
                addChild(tile);
                tile.render();

                posX += BaseBonusTile.TILE_WIDTH + 10;
            }
        }

        protected function _setupOkButton():void
        {
            _okButton = new UniversalButton('OK');
            _okButton.x = _backgroundX + (WINDOW_WIDTH - UniversalButton.BUTTON_WIDTH)/2;
            _okButton.y = _backgroundY + WINDOW_HEIGHT - (UniversalButton.BUTTON_HEIGHT + 30);
            addChild(_okButton);
            _okButton.addEventListener(MouseEvent.CLICK, _okClickHandler);
        }

        protected function _okClickHandler(e:MouseEvent):void
        {
            destroy();
        }

        override public function destroy():void
        {
            if (_okButton)
                _okButton.destroy();
            _okButton = null;

            super.destroy();
        }
    }
}
