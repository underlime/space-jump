package application.views.screen
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.events.JsScrollEvent;
    import application.views.base.ApplicationView;
    import application.views.base.scroll.ScrollMarker;
    import application.views.base.scroll.ScrollTrack;
    import application.views.screen.elements.buttons.CloseButton;
    import application.views.text.ApplicationHeader;

    import flash.display.Bitmap;
    import flash.display.Graphics;
    import flash.display.Shape;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    import framework.view.View;
    import framework.view.scroll.Scroll;
    import framework.view.scroll.ScrollEvent;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BaseScreen extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const CONTENT_WINDOW_WIDTH:int = 760;
        public static const CONTENT_WINDOW_HEIGHT:int = 480;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseScreen(appCtrl:ApplicationController, header:String)
        {
            _appCtrl = appCtrl;
            _header = header;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _setupCloseButton();
            _setLine();
            _setupHeader();
            _setupScrollView();
        }

        override public function destroy():void
        {
            graphics.clear();
            _closeButton.destroy();
            /*
             _backgroundShape.graphics.clear();
             removeChild(_backgroundShape);
             */
            _scrollContent.destroy();
            if (_scrollbar)
                _scrollbar.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function enableScroll():void
        {
            _setupRectangles();
            _totalHeight = _scrollContent.height + 20;

            var marker:ScrollMarker = new ScrollMarker();
            var track:ScrollTrack = new ScrollTrack(_scrollerRectangle.height);

            _startY = _contentRectangle.y;

            _scrollbar = new Scroll(marker, track, _scrollerRectangle);
            _scrollbar.rectangle.height -= marker.markerHeight;

            _scrollbar.addEventListener(ScrollEvent.CHANGE, _sliderEventHandler);
            addChild(_scrollbar);

            _appCtrl.addEventListener(JsScrollEvent.SCROLL, _wheelEventHandler);

            _scrollbar.x = _back.x + 744;
            _scrollbar.y = _back.y + 3;

            // отрисовка прозрачного бэкграунда. чтобы мышь всегда ловила _scrollContent (фикс для MouseWheel)
            _scrollContent.graphics.beginFill(0xffffff, 0);
            _scrollContent.graphics.drawRect(0, 0, _scrollContent.width, _scrollContent.height);
            _scrollContent.graphics.endFill();
            //

            _scrollContent.scrollRect = _contentRectangle;
        }

        public function disableScroll():void
        {
            _scrollContent.graphics.clear();
            if (_scrollbar && !_scrollbar.destroyed) {
                _scrollbar.destroy();
            }
        }

        public function addChildToScrollContent(child:View):void
        {
            _renderedItems.push(child);
            _scrollContent.addChild(child);
        }

        public function clearScrollContent():void
        {
            for (var i:int = 0; i < _renderedItems.length; i++) {
                _renderedItems[i].destroy();
            }
            disableScroll();
            _replaceScrollContent();
        }

        public function getScroll():Number
        {
            return _contentRectangle.y;
        }

        public function scrollTo(pos:Number):void
        {
            _contentRectangle.y = pos;
            _scrollContent.scrollRect = _contentRectangle;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _appCtrl:ApplicationController;
        protected var _scrollContent:View;
        protected var _scrollerRectangle:Rectangle;
        protected var _contentRectangle:Rectangle;
        protected var _scrollbar:Scroll;
        protected var _back:Bitmap;
        protected var _closeButton:CloseButton;
        protected var _backgroundShape:Shape;
        protected var _lineShape:Shape;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupRectangles():void
        {
            _scrollerRectangle = new Rectangle(0, 0, 0, 550);
            _contentRectangle = new Rectangle(0, 0, CONTENT_WINDOW_WIDTH, CONTENT_WINDOW_HEIGHT);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _header:String;
        private var _txtHeader:ApplicationHeader;
        private var _startY:Number;
        private var _totalHeight:Number;
        private var _renderedItems:Vector.<View> = new Vector.<View>();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _replaceScrollContent():void
        {
            var x:Number = _scrollContent.x;
            var y:Number = _scrollContent.y;
            var index:Number = super.getChildIndex(_scrollContent);

            _scrollContent.destroy();
            _scrollContent = new View();

            _scrollContent.x = x;
            _scrollContent.y = y;
            super.addChildAt(_scrollContent, index);
        }

        private function _setupBackground():void
        {
            /*
             _backgroundShape = new Shape();
             addChild(_backgroundShape);
             _backgroundShape.graphics.beginFill(0x000000, .8);
             _backgroundShape.graphics.drawRect(0, 0, super.stage.stageWidth, super.stage.stageHeight);
             _backgroundShape.graphics.endFill();
             */

            _back = super.source.getBitmap(ScreenData.BACKGROUND);
            _back.x = 20;
            _back.y = 120;
            addChild(_back);
        }

        private function _setupCloseButton():void
        {
            _closeButton = new CloseButton();
            _closeButton.x = 620 + _back.x;
            _closeButton.y = 28 + _back.y;
            _closeButton.addEventListener(MouseEvent.CLICK, _closeButtonClickHandler);
            addChild(_closeButton);
        }

        private function _setLine():void
        {
            _lineShape = new Shape();
            var gr:Graphics = _lineShape.graphics;

            gr.beginFill(0xB8FCFF, 1);
            gr.lineStyle(1, 0xB8FCFF);
            gr.moveTo(20, 0);
            gr.lineTo(725, 0);
            gr.endFill();
            _lineShape.y = 75 + _back.y;
            _lineShape.x = 20;
            addChild(_lineShape);
        }

        private function _setupHeader():void
        {
            _txtHeader = new ApplicationHeader();
            _txtHeader.align = TextFormatAlign.LEFT;
            _txtHeader.width = 500;
            _txtHeader.x = _back.x + 20;
            _txtHeader.y = 20 + _back.y;
            addChild(_txtHeader);
            _txtHeader.text = _header;
        }

        private function _setupScrollView():void
        {
            _scrollContent = new View();
            addChild(_scrollContent);
            _scrollContent.x = 20;
            _scrollContent.y = 75 + _back.y;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeButtonClickHandler(e:MouseEvent):void
        {
            _closeButton.removeEventListener(MouseEvent.CLICK, _closeButtonClickHandler);
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE));
        }

        private function _wheelEventHandler(e:JsScrollEvent):void
        {
            this._scrollbar.percent += -3 * e.dir;
        }

        private function _sliderEventHandler(e:ScrollEvent):void
        {
            var percent:Number = e.percent;
            var pos:Number = ((_totalHeight - _contentRectangle.height) * (percent / 100)) + _startY;
            this.scrollTo(pos);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get scrollContent():View
        {
            return _scrollContent;
        }
    }
}