package application.views.screen
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ScreenData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND:String = "SPACE_SCREEN_BACK";
        public static const BACKGROUND_POSITION:String = "SPACE_ITEM_FIELD_STATE1";
        public static const BACKGROUND_POSITION_ACTIVE:String = "SPACE_ITEM_FIELD_STATE2";
        public static const ACHIEVEMENT_ICON:String = "SPACE_ACHIEVEMENT_UNLOCKED";
        public static const CLOSE_BUTTON:String = "SPACE_CLOSE_BTN";


        public static const ITEM_ICON_BACKGROUND:String = "SPACE_ITEM_BG";
        public static const PARASCHUT_ICON:String = "SPACE_PARASHUT_ICON";
        public static const GEOLOG_ICON:String = "SPACE_GEOLOG_SKIN";
        public static const DOODLE_HAT_ICON:String = "SPACE_HAT_01";
        public static const GOLD_CAPSULE_ICON:String = "SPACE_GOLD_CAPSULE";
        public static const HEART_ICON:String = "SPACE_HEART_UPGRADE";

        public static const TRANSITION_MASK:String = "SPACE_JUMPER_TRANSITION_MASK";
        public static const TELL_FRIEND_BUTTON:String = "SPACE_TELL_A_FRIEND";
        public static const SQUARE_CLOSE_BUTTON:String = "SPACE_CLOSE_ACHIEVE";

        public static const BONUS_LEVEL_MASK:String = "BONUS_MASK";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}