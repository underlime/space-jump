/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.02.13
 * Time: 11:35
 */
package application.views.screen.rate
{
    import application.views.screen.elements.tabs.SpaceTab;
    import application.views.screen.elements.tabs.SpaceTabPanel;
    import application.views.text.TextFactory;

    import framework.view.tabs.BaseTab;

    public class RateTabs extends SpaceTabPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RateTabs()
        {
            var tabs:Vector.<BaseTab> = new Vector.<BaseTab>();
            var factory:TextFactory = TextFactory.instance;

            tabs.push(
                    new SpaceTab(factory.getLabel('today_label')),
                    new SpaceTab(factory.getLabel('last_week_label')),
                    new SpaceTab(factory.getLabel('last_month_label')),
                    new SpaceTab(factory.getLabel('from_the_beginning_label'))
            );

            super(tabs);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
