/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.02.13
 * Time: 10:58
 */
package application.views.screen.rate
{
    import application.controllers.ApplicationController;
    import application.views.screen.BaseScreen;
    import application.views.text.TextFactory;

    import flash.geom.Rectangle;

    import framework.events.GameEvent;

    public class BaseRateScreen extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseRateScreen(appCtrl:ApplicationController)
        {
            super(appCtrl, TextFactory.instance.getLabel('rate_screen_label'));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function render():void
        {
            super.render();
            _setupTabsPanel();
            _setupScrollContent();
            super._lineShape.graphics.clear();
        }


        override public function destroy():void
        {
            _tabs.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupRectangles():void
        {
            _scrollerRectangle = new Rectangle(0, 0, 0, 550);
            _contentRectangle = new Rectangle(0, 0, 760, 439);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _tabs:RateTabs;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTabsPanel():void
        {
            _tabs = new RateTabs();
            _tabs.addEventListener(GameEvent.CHANGE, _tabsChangeHandler);
            _tabs.y = super._back.y + 90;
            _tabs.x = super._back.x + 20;
            addChild(_tabs);
        }

        private function _setupScrollContent():void
        {
            super.scrollContent.x = super._back.x + 20;
            super.scrollContent.y = super._back.y + 117;
            super._scrollContent.scrollRect = _contentRectangle;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _tabsChangeHandler(event:GameEvent):void
        {

        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
