/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.02.13
 * Time: 10:53
 */
package application.views.screen.rate
{
    import application.controllers.ApplicationController;
    import application.controllers.RateController;
    import application.events.ApplicationEvent;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.TopDayModel;
    import application.models.TopGlobalModel;
    import application.models.TopMonthModel;
    import application.models.TopWeekModel;
    import application.models.top.GeneralTopModel;
    import application.models.top.TopRecord;
    import application.views.screen.BaseScreen;

    import framework.data.DataBase;
    import framework.events.GameEvent;
    import framework.socnet.models.SocNetInfoRegistry;
    import framework.socnet.models.soc_net_registry.SocNetUserRecord;

    public class RateScreen extends BaseRateScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RateScreen(appCtrl:ApplicationController)
        {
            super(appCtrl);
            _getModelsFromDb(appCtrl.dataBase);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            showTop();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function showTop():void
        {
            var scoreField:String = _getScoreField();
            var top:Number = 10;
            var pos:int = 1;

            var topRecord:TopRecord;
            var socNetRecord:SocNetUserRecord;
            var ratePosition:RatePosition;
            var score:int;

            for (var i:String in _activeModel.list.value) {
                topRecord = _activeModel.list.value[i];
                var socNetId:String = topRecord.user_info.soc_net_id.value.toString();
                socNetRecord = _socNetRegistry.getRecordById(socNetId) as SocNetUserRecord;
                score = topRecord.user_info[scoreField].value;

                ratePosition = new RatePosition(topRecord, socNetRecord, pos, score);
                ratePosition.y = top;
                super.addChildToScrollContent(ratePosition);

                top += _margin;
                ++pos;
            }

            if (this.scrollContent.height > BaseScreen.CONTENT_WINDOW_HEIGHT)
                super.enableScroll();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _margin:Number = 63;
        private var _activeTabId:int = RateController.TAB_DAY;
        private var _activeModel:GeneralTopModel;
        private var _itemsModel:Items;
        private var _socNetRegistry:SocNetInfoRegistry;
        private var _topDayModel:TopDayModel;
        private var _topWeekModel:TopWeekModel;
        private var _topMonthModel:TopMonthModel;
        private var _topGlobalModel:TopGlobalModel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getModelsFromDb(dataBase:DataBase):void
        {
            _topDayModel = dataBase.getChildByName(ModelData.TOP_DAY_MODEL) as TopDayModel;
            _topWeekModel = dataBase.getChildByName(ModelData.TOP_WEEK_MODEL) as TopWeekModel;
            _topMonthModel = dataBase.getChildByName(ModelData.TOP_MONTH_MODEL) as TopMonthModel;
            _topGlobalModel = dataBase.getChildByName(ModelData.TOP_GLOBAL_MODEL) as TopGlobalModel;
            _activeModel = _topDayModel;

            _itemsModel = dataBase.getChildByName(ModelData.ITEMS_MODEL) as Items;
            _socNetRegistry = dataBase.getChildByName(ModelData.SOC_NET_INFO_REGISTRY) as SocNetInfoRegistry;
        }

        private function _changeActiveModel():void
        {
            switch (_activeTabId) {
                case RateController.TAB_DAY:
                    _activeModel = _topDayModel;
                    break;
                case RateController.TAB_WEEK:
                    _activeModel = _topWeekModel;
                    break;
                case RateController.TAB_MONTH:
                    _activeModel = _topMonthModel;
                    break;
                case RateController.TAB_GLOBAL:
                    _activeModel = _topGlobalModel;
                    break;
                default:
                    throw new Error('Wrong tab id');
            }
        }

        private function _getScoreField():String
        {
            var scoreField:String;
            switch (_activeTabId) {
                case RateController.TAB_DAY:
                    scoreField = 'max_score_day';
                    break;
                case RateController.TAB_WEEK:
                    scoreField = 'max_score_week';
                    break;
                case RateController.TAB_MONTH:
                    scoreField = 'max_score_month';
                    break;
                case RateController.TAB_GLOBAL:
                    scoreField = 'max_score_global';
                    break;
                default:
                    throw new Error('Wrong tab id');
            }
            return scoreField;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _tabsChangeHandler(event:GameEvent):void
        {
            var tabId:int = parseInt(event.data.tabID);
            if (tabId == _activeTabId)
                return;

            super.clearScrollContent();
            _activeTabId = tabId;
            _changeActiveModel();

            var needDataEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.NEED_DATA);
            needDataEvent.data = { 'tab_id': tabId };
            this.dispatchEvent(needDataEvent);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
