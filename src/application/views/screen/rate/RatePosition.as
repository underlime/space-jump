/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.02.13
 * Time: 12:21
 */
package application.views.screen.rate
{
    import application.events.ApplicationEvent;
    import application.models.Items;
    import application.models.items.ItemRecord;
    import application.models.top.TopRecord;
    import application.views.screen.BaseScreenPosition;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.display.DisplayObject;
    import flash.display.Loader;
    import flash.events.MouseEvent;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.helpers.MathHelper;
    import framework.socnet.models.soc_net_registry.SocNetUserRecord;
    import framework.tools.ImagesManager;
    import framework.view.text.SimpleTextField;

    import mx.utils.StringUtil;

    import org.casalib.util.NumberUtil;

    public class RatePosition extends BaseScreenPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RatePosition(record:TopRecord, socNetRecord:SocNetUserRecord, pos:int, score:int)
        {
            super();
            _topRecord = record;
            _socNetRecord = socNetRecord;
            _ratePosition = pos;
            _score = score;

            _itemRecordsList = record.itemsList;
            _setValues();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            var hash:Array = [ImageClass_1, ImageClass_2, ImageClass_3];
            var randomIndex:int = MathHelper.random(0, hash.length - 1);
            _icon = new (hash[randomIndex] as Class)();
        }

        override public function render():void
        {
            super.render();
            _setupRateText();
            _setupIcon();
            _setupNameText();
            _setupInventory();
            _setupPointsData();
            _setupObjectivesData();

            _setupReactions();
        }


        override public function destroy():void
        {
            this.removeEventListener(MouseEvent.CLICK, _clickHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../../lib/images/AVA_1.png")]
        private var ImageClass_1:Class;

        [Embed(source="../../../../../lib/images/AVA_2.png")]
        private var ImageClass_2:Class;

        [Embed(source="../../../../../lib/images/AVA_3.png")]
        private var ImageClass_3:Class;

        private var _icon:DisplayObject;

        private var _txtRateNumber:SimpleTextField;
        private var _ratePosition:int = 1;

        private var _userName:String = '%username%';
        private var _txtUserName:SimpleTextField;

        private var _suiteIcon:Bitmap;
        private var _hatIcon:Bitmap;
        private var _capsuleIcon:Bitmap;

        private var _score:int;
        private var _txtPoints:SimpleTextField;

        private var _objectives:int;
        private var _txtObjectives:SimpleTextField;

        private var _topRecord:TopRecord;
        private var _socNetRecord:SocNetUserRecord;
        private var _itemRecordsList:Vector.<ItemRecord>;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupReactions():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;
            this.addEventListener(MouseEvent.CLICK, _clickHandler);
        }

        private function _setValues():void
        {
            _objectives = _topRecord.user_info.stars.value;
        }

        private function _setupRateText():void
        {
            _txtRateNumber = new SimpleTextField();
            _txtRateNumber.size = 16;
            _txtRateNumber.color = 0xffffff;
            _txtRateNumber.width = 50;
            _txtRateNumber.align = TextFormatAlign.CENTER;
            _txtRateNumber.x = 0;
            _txtRateNumber.y = 15;

            addChild(_txtRateNumber);
            _txtRateNumber.text = _ratePosition.toString() + ".";
        }

        private function _setupIcon():void
        {
            _icon.x = 50;
            _icon.y = 5;
            addChild(_icon);
        }

        private function _setupNameText():void
        {
            _txtUserName = new SimpleTextField();
            _txtUserName.size = 14;
            _txtUserName.color = 0xffffff;
            _txtUserName.width = 180;
            _txtUserName.x = 120;
            _txtUserName.bold = true;
            _txtUserName.systemFamily = "Arial";

            addChild(_txtUserName);

            if (_socNetRecord) {
                _userName = _socNetRecord.first_name.value + ' ' + _socNetRecord.last_name.value;
                _userName = StringUtil.trim(_userName);
                if (_socNetRecord.picture.value)
                    ImagesManager.instance.getImageByUrl(_socNetRecord.picture.value, _onPhotoComplete);
            }

            _txtUserName.text = _userName;
            _txtUserName.y = 27 - _txtUserName.textHeight / 2;
        }

        private function _onPhotoComplete(loader:Loader):void
        {
            removeChild(_icon);
            _icon = loader;
            _setupIcon();
        }

        private function _setupInventory():void
        {
            var itemRecord:ItemRecord;
            for (var i:int = 0; i < _itemRecordsList.length; ++i) {
                itemRecord = _itemRecordsList[i];
                if (itemRecord.rubric.value == Items.RUBRIC_SUITS)
                    _setupSuiteIcon(itemRecord);
                else
                    if (itemRecord.rubric.value == Items.RUBRIC_HATS)
                        _setupHatIcon(itemRecord);
                    else
                        if (itemRecord.rubric.value == Items.RUBRIC_CAPSULES)
                            _setupCapsuleIcon(itemRecord);
            }
        }

        private function _setupSuiteIcon(itemRecord:ItemRecord):void
        {
            _suiteIcon = super.source.getBitmap(itemRecord.image.value);
            _suiteIcon.y = 5;
            _suiteIcon.x = 310;

            addChild(_suiteIcon);
        }

        private function _setupHatIcon(itemRecord:ItemRecord):void
        {
            _hatIcon = super.source.getBitmap(itemRecord.image.value);
            _hatIcon.x = 370;
            _hatIcon.y = 5;

            addChild(_hatIcon);
        }

        private function _setupCapsuleIcon(itemRecord:ItemRecord):void
        {
            _capsuleIcon = super.source.getBitmap(itemRecord.image.value);
            _capsuleIcon.x = 430;
            _capsuleIcon.y = 5;

            addChild(_capsuleIcon);
        }

        private function _setupPointsData():void
        {
            _txtPoints = new SimpleTextField();
            _txtPoints.autoSize = TextFieldAutoSize.RIGHT;
            _txtPoints.width = 50;
            _txtPoints.x = 695;
            _txtPoints.y = 8;

            var pointsPrefix:String = TextFactory.instance.getLabel("points_label") + ":   ";
            var pointsString:String = NumberUtil.format(_score, " ", 9, "");

            addChild(_txtPoints);
            _txtPoints.htmlText = "<font size='10' color='#20D2EF'>" + pointsPrefix + "</font>" + "<font size='20' color='#B8FCFF'>" + pointsString + "</font>";
        }

        private function _setupObjectivesData():void
        {
            _txtObjectives = new SimpleTextField();
            _txtObjectives.autoSize = TextFieldAutoSize.RIGHT;
            _txtObjectives.width = 50;
            _txtObjectives.x = 695;
            _txtObjectives.y = 30;

            var objPrefix:String = TextFactory.instance.getLabel('objectives_label') + ":   ";
            var objString:String = NumberUtil.format(_objectives, " ", 3, "");

            addChild(_txtObjectives);
            _txtObjectives.htmlText = "<font size='10' color='#E8C600'>" + objPrefix + "</font><font size='20' color='#FFFABB'>" + objString + "</font>";
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_GUEST_SCREEN, true);
            event.data['soc_net_id'] = _topRecord.user_info.soc_net_id.value;
            super.dispatchEvent(event);
        }


        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
