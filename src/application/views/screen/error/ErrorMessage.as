package application.views.screen.error 
{
    import flash.text.TextFieldAutoSize;

    import framework.error.ApplicationError;
    import framework.view.View;
    import framework.view.text.SimpleTextField;

    /**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class ErrorMessage extends View
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function ErrorMessage(error:ApplicationError)
		{
			this._header = error.type;
            this._code = error.code;
            this._message = error.message;
			super();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function render():void 
		{
			this._drawBackground();
			this._setupText();
		}
		
		override public function destroy():void 
		{
			this._txtHeader.destroy();
			this.graphics.clear();
			super.destroy();
		}
		
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		
		private var _txtHeader:SimpleTextField;
        private var _txtCode:SimpleTextField;
        private var _txtMessage:SimpleTextField;
		private var _header:String;
        private var _code:int;
        private var _message:String;
		
		// PRIVATE METHODS ---------------------------------------------------------------------/
		
		private function _setupText():void 
		{
			this._txtHeader = _createTextArea();
            this._txtHeader.size = 20;
			this.addChild(this._txtHeader);
			this._txtHeader.y = 20;
			this._txtHeader.text = this._header;

            this._txtCode = _createTextArea();
            this.addChild(this._txtCode);
            this._txtCode.y = 50;
            this._txtCode.text = 'Code: ' + this._code.toString();

            this._txtMessage = _createTextArea();
            this.addChild(this._txtMessage);
            this._txtMessage.y = 70;
            this._txtMessage.text = 'Message: ' + this._message;
		}

        private function _createTextArea():SimpleTextField
        {
            var textArea:SimpleTextField = new SimpleTextField();
            textArea.size = 12;
            textArea.width = 350;
            textArea.color = 0x20D2EF;
            textArea.wordWrap = true;
            textArea.multiline = true;
            textArea.autoSize = TextFieldAutoSize.LEFT;
            return textArea;
        }
		
		private function _drawBackground():void 
		{
			this.graphics.clear();
			this.graphics.beginFill(0x20D2EF, 1);
			this.graphics.lineStyle(1, 0x20D2EF);
			this.graphics.lineTo(350, 0);
			this.graphics.moveTo(0, 200);
			this.graphics.lineTo(350, 200);
			this.graphics.endFill();
		}
		
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
	}

}