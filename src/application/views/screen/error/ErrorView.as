/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.12.12
 */
package application.views.screen.error
{
    import application.views.base.ApplicationView;
    import application.views.text.ApplicationHeader;

    import flash.display.Bitmap;

    import framework.error.ApplicationError;

    public class ErrorView extends ApplicationView
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ErrorView(error:ApplicationError)
        {
            _error = error;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function setup():void
        {
            _errorMessage = new ErrorMessage(_error);
        }

        override public function render():void
        {
            _drawBackground();
            _setupBack();
            _setupHeader();
            addChild(_errorMessage);
            _errorMessage.x = 225;
            _errorMessage.y = 250;
        }


        override public function destroy():void
        {
            graphics.clear();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        public var errorName:String;
        public var message:String;

        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _error:ApplicationError;
        private var _back:Bitmap;
        private var _header:ApplicationHeader;
        private var _errorMessage:ErrorMessage;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupHeader():void
        {
            _header = new ApplicationHeader();
            _header.width = 800;
            _header.y = 200;
            addChild(_header);
            _header.text = super.textFactory.getLabel('error_label');
        }

        private function _setupBack():void
        {
            _back = new Bitmap(new SPACE_ERROR_BACK());
            _back.x = 200;
            _back.y = 165;
            addChild(_back);
        }

        private function _drawBackground():void
        {
            graphics.clear();
            graphics.beginFill(0x000000, .8);
            graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
            graphics.endFill();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
