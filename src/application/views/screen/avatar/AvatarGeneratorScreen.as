/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 10:53
 */
package application.views.screen.avatar
{
    import application.events.ApplicationEvent;
    import application.views.base.ApplicationView;
    import application.views.screen.elements.buttons.CloseButton;
    import application.views.screen.elements.checkbox.BaseCheckBox;
    import application.views.screen.elements.radio.BaseRadioGroup;

    import flash.display.Bitmap;
    import flash.display.DisplayObject;
    import flash.display.Graphics;
    import flash.display.Shape;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    public class AvatarGeneratorScreen extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const BACKGROUND:String = "AVA_GEN_BG";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AvatarGeneratorScreen(mainScreen:DisplayObject)
        {
            _mainScreen = mainScreen;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _addCloseButton();
            _setupTitle();
            _addUnderline();
            _setCheckBoxes();
            _setRadioGroup();
            _addAvatarVariants();
        }

        override public function destroy():void
        {
            _closeButton.destroy();
            _simpleVariant.destroy();
            _logoVariant.destroy();
            _simpleWithLogoVariant.destroy();

            _noteCheckBox.destroy();
            _linkCheckBox.destroy();
            _langRadioGroup.destroy();

            for (var i:int = 0; i < _avaCollection.length; i++)
                _avaCollection[i].destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _closeButton:CloseButton;
        private var _mainScreen:DisplayObject;

        private var _simpleVariant:BaseVariant;
        private var _logoVariant:BaseVariant;
        private var _simpleWithLogoVariant:BaseVariant;

        private var _noteCheckBox:BaseCheckBox;
        private var _linkCheckBox:BaseCheckBox;

        private var _langRadioGroup:BaseRadioGroup;

        private var _avaCollection:Vector.<BaseVariant> = new Vector.<BaseVariant>();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            this.graphics.clear();
            this.graphics.beginFill(0x000000, .8);
            this.graphics.drawRect(0, 0, 800, 700);
            this.graphics.endFill();

            _background = super.source.getBitmap(BACKGROUND);
            _background.x = 20;
            _background.y = 20;
            addChild(_background);
        }

        private function _addCloseButton():void
        {
            _closeButton = new CloseButton();
            _closeButton.x = _background.x + 633;
            _closeButton.y = _background.y + 18;
            _closeButton.addEventListener(MouseEvent.CLICK, _clickHandler);
            addChild(_closeButton);
        }

        private function _addAvatarVariants():void
        {
            _simpleVariant = new SimpleVariant(this, _mainScreen);
            _simpleVariant.x = 65;
            _simpleVariant.y = 152;
            addChild(_simpleVariant);

            _logoVariant = new LogoVariant(this, _mainScreen);
            _logoVariant.x = 295;
            _logoVariant.y = 152;
            addChild(_logoVariant);

            _simpleWithLogoVariant = new SimpleWithLogoVariant(this, _mainScreen);
            _simpleWithLogoVariant.x = 525;
            _simpleWithLogoVariant.y = 152;
            addChild(_simpleWithLogoVariant);

            _avaCollection.push(
                    _simpleVariant,
                    _logoVariant,
                    _simpleWithLogoVariant
            );

            for (var i:int = 0; i < _avaCollection.length; i++)
            {
                _avaCollection[i].addLink();
                _avaCollection[i].addNote();
                _avaCollection[i].addEventListener(MouseEvent.CLICK, _avaClickHandler);
            }
        }

        private function _setupTitle():void
        {
            var title:SimpleTextField = new SimpleTextField();
            title.y = 42;
            title.x = 20;
            title.width = 760;
            title.align = TextFormatAlign.CENTER;
            title.size = 20;
            title.color = 0xb7f7f7;
            addChild(title);
            title.text = super.textFactory.getLabel('generator_label');
        }

        private function _setCheckBoxes():void
        {
            _noteCheckBox = new BaseCheckBox(super.textFactory.getLabel('note_checkbox_label'));
            _linkCheckBox = new BaseCheckBox(super.textFactory.getLabel('link_checkbox_label'));

            _noteCheckBox.x = _background.x + 183;
            _noteCheckBox.y = _background.y + 87;

            _linkCheckBox.x = _background.x + 317;
            _linkCheckBox.y = _background.y + 87;

            addChild(_noteCheckBox);
            addChild(_linkCheckBox);

            _linkCheckBox.addEventListener(Event.CHANGE, _linkChangeHandler);
            _noteCheckBox.addEventListener(Event.CHANGE, _noteChangeHandler);
        }

        private function _setRadioGroup():void
        {
            _langRadioGroup = new LangRadioGroup();
            addChild(_langRadioGroup);
            _langRadioGroup.x = _background.x + 441;
            _langRadioGroup.y = _background.y + 87;

            _langRadioGroup.addEventListener(Event.CHANGE, _radioGroupChangeHandler);
        }

        private function _addUnderline():void
        {
            var shape:Shape = new Shape();
            var gr:Graphics = shape.graphics;
            addChild(shape);

            gr.beginFill(0x82dffe);
            gr.lineStyle(1, 0x82dffe);
            gr.moveTo(_background.x + 20, _background.y + 65);
            gr.lineTo(_background.x + 740, _background.y + 65);
            gr.endFill();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE));
        }

        private function _linkChangeHandler(event:Event):void
        {
            for (var i:int = 0; i < _avaCollection.length; i++)
            {
                if (_linkCheckBox.checked)
                    _avaCollection[i].addLink();
                else
                    _avaCollection[i].removeLink();
            }
        }

        private function _noteChangeHandler(event:Event):void
        {
            for (var i:int = 0; i < _avaCollection.length; i++)
            {
                if (_noteCheckBox.checked)
                    _avaCollection[i].addNote();
                else
                    _avaCollection[i].removeNote();
            }
        }

        private function _radioGroupChangeHandler(event:Event):void
        {
            for (var i:int = 0; i < _avaCollection.length; i++)
            {
                _avaCollection[i].translate();
            }
        }

        private function _avaClickHandler(event:MouseEvent):void
        {
            (event.currentTarget as BaseVariant).getAvatar();
            var completeEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.AVATAR_COMPLETE, true);
            dispatchEvent(completeEvent);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
