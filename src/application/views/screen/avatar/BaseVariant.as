/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 13:44
 */
package application.views.screen.avatar
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.display.Shape;
    import flash.display.Sprite;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.text.TextFormatAlign;

    import framework.helpers.ImagesHelper;
    import framework.tools.ScreenShot;
    import framework.tools.avatar.AvatarTool;
    import framework.view.View;
    import framework.view.text.SimpleTextField;

    public class BaseVariant extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseVariant(avaScreen:AvatarGeneratorScreen, mainScreen:DisplayObject)
        {
            _avaScreen = avaScreen;
            _mainScreen = mainScreen;
            super();
            _avatarPhoto = new ScreenShot();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _generateScreen();
            _drawBorder(_outColor);
            _bindHover();

            this.buttonMode = true;
            this.useHandCursor = true;

            addChild(_bitmapLayer);
            addChild(_noteLayer);
            addChild(_linkLayer);
        }

        override public function destroy():void
        {
            this.removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            this.removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
            _avatarPhoto.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getAvatar():void
        {
            _avatarPhoto.takeShot(this, 200, 400, 2, 2);
            if (ApplicationConfiguration.STAND_ALONE) {
                _avatarPhoto.saveToDisk();
            }
            else {
                _avaScreen.setPreloader();
                var data:BitmapData = _avatarPhoto.rawData.clone();

                _avatarTool = new AvatarTool(new Bitmap(data));
                _avatarTool.addEventListener(Event.COMPLETE, _onPhotoSaved);
                _avatarTool.addEventListener(ErrorEvent.ERROR, _onPhotoError);
                _avatarTool.setAvatar();
            }

        }

        public function addLink():void
        {
            if (_drawShape)
                _addShape();
            _addLinkText();
        }

        public function removeLink():void
        {
            if (_shape && _linkLayer.contains(_shape))
                _linkLayer.removeChild(_shape);
            if (_txtLink && !_txtLink.destroyed)
                _txtLink.destroy();
        }

        public function addNote():void
        {

        }

        public function removeNote():void
        {

        }

        public function translate():void
        {
            if (_currentLanguage == ApplicationConfiguration.RU) {
                _currentLanguage = ApplicationConfiguration.EN;
            } else {
                _currentLanguage = ApplicationConfiguration.RU;
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _bitmap:Bitmap;
        protected var _noteLayer:Sprite = new Sprite();
        protected var _linkLayer:Sprite = new Sprite();
        protected var _bitmapLayer:Sprite = new Sprite();

        protected var _drawShape:Boolean = true;
        protected var _currentLanguage:String = ApplicationConfiguration.LANGUAGE;

        private var _avatarTool:AvatarTool;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _generateScreen():void
        {
            var tmp:Bitmap = new Bitmap(ScreenShot.takeScreen(_mainScreen, 800, 600));
            var tmp2:BitmapData = ImagesHelper.matrixResize(tmp, 600, 525, true);
            _bitmap = new Bitmap(ScreenShot.takeScreen(new Bitmap(tmp2), 200, 400, 243, 42));
            _bitmap.x = 2;
            _bitmap.y = 2;
            _bitmapLayer.addChild(_bitmap);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _avatarPhoto:ScreenShot;

        private var _mainScreen:DisplayObject;
        private var _avaScreen:AvatarGeneratorScreen;
        private var _outColor:uint = 0xffffff;
        private var _inColor:uint = 0x05fafb;

        private var _shape:Shape;
        private var _txtLink:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _onPhotoSaved(e:Event):void
        {
            _avaScreen.removePreloader();
        }

        private function _onPhotoError(e:ErrorEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.UPLOAD_ERROR, true);
            dispatchEvent(event);
        }

        private function _bindHover():void
        {
            this.addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            this.addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _drawBorder(color:uint):void
        {
            this.graphics.clear();
            this.graphics.beginFill(color);
            this.graphics.drawRect(0, 0, 204, 404);
            this.graphics.endFill();
        }

        private function _addShape():void
        {
            _shape = new Shape();
            _linkLayer.addChild(_shape);
            _shape.graphics.clear();
            _shape.graphics.beginFill(0x000000, .8);
            _shape.graphics.drawRect(_bitmap.x, _bitmap.y + _bitmap.height - 24, _bitmap.width, 24);
            _shape.graphics.endFill();
        }

        private function _addLinkText():void
        {
            _txtLink = new SimpleTextField();
            _txtLink.x = _bitmap.x;
            _txtLink.y = _bitmap.y + _bitmap.height - 22;
            _txtLink.width = 200;
            _txtLink.align = TextFormatAlign.CENTER;
            _txtLink.size = 12;
            _txtLink.height = 20;
            _txtLink.color = 0xffffff;
            _txtLink.systemFamily = "Arial";

            _linkLayer.addChild(_txtLink);
            _txtLink.text = "VK.COM / KOSMODIVER";
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _outHandler(event:MouseEvent):void
        {
            _drawBorder(_outColor);
        }

        private function _overHandler(event:MouseEvent):void
        {
            _drawBorder(_inColor);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
