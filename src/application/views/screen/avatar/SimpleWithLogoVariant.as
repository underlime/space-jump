/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 15:04
 */
package application.views.screen.avatar
{
    import application.config.ApplicationConfiguration;

    import flash.display.Bitmap;
    import flash.display.DisplayObject;

    public class SimpleWithLogoVariant extends BaseVariant
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const NOTE_RU:String = "AVA3_LAYER_01_RU";
        public static const NOTE_EN:String = "AVA3_LAYER_01_EN";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimpleWithLogoVariant(avaScreen:AvatarGeneratorScreen, mainScreen:DisplayObject)
        {
            super(avaScreen, mainScreen);
            super._drawShape = false;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function addNote():void
        {
            _updateNote();
        }

        override public function removeNote():void
        {
            if (_note && _noteLayer.contains(_note))
                _noteLayer.removeChild(_note);
        }

        override public function translate():void
        {
            super.translate();
            if (_note && _noteLayer.contains(_note))
                _updateNote();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _note:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateNote():void
        {
            if (_note && _noteLayer && _noteLayer.contains(_note))
                _noteLayer.removeChild(_note);

            var src:String = super._currentLanguage == ApplicationConfiguration.RU ? NOTE_RU : NOTE_EN;
            _note = super.source.getBitmap(src);
            _note.x = 2;
            super._noteLayer.addChild(_note);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
