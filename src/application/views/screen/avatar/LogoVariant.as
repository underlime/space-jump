/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 15:03
 */
package application.views.screen.avatar
{
    import application.config.ApplicationConfiguration;
    import application.views.text.TextFactory;

    import flash.display.DisplayObject;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    public class LogoVariant extends BaseVariant
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const AVA_RU:String = "AVA2_LAYER_00_RU";
        public static const AVA_EN:String = "AVA2_LAYER_00_EN";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LogoVariant(avaScreen:AvatarGeneratorScreen, mainScreen:DisplayObject)
        {
            super(avaScreen, mainScreen);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function addNote():void
        {
            _setupNote();
            _addNote();
        }

        override public function removeNote():void
        {
            if (_txtNote && !_txtNote.destroyed)
                _txtNote.destroy();
        }

        override public function translate():void
        {
            super.translate();
            if (_txtNote)
                _txtNote.text = TextFactory.instance.getLabelByLang('get_in_label', super._currentLanguage);
            _updateBitmap();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _generateScreen():void
        {
            _updateBitmap();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtNote:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupNote():void
        {
            _txtNote = new SimpleTextField();
            _txtNote.fontFamily = SimpleTextField.LOBSTER_FONT;
            _txtNote.size = 30;
            _txtNote.color = 0xffffff;
            _txtNote.multiline = true;
            _txtNote.wordWrap = true;
            _txtNote.align = TextFormatAlign.CENTER;
            _txtNote.width = 200;
            _txtNote.y = super._bitmap.height - 74;
            super._noteLayer.addChild(_txtNote);
        }

        private function _addNote():void
        {
            _txtNote.text = TextFactory.instance.getLabelByLang('get_in_label', super._currentLanguage);
        }

        private function _updateBitmap():void
        {
            if (_bitmap && _bitmapLayer && _bitmapLayer.contains(_bitmap))
                _bitmapLayer.removeChild(_bitmap);

            var source:String = super._currentLanguage == ApplicationConfiguration.RU ? AVA_RU : AVA_EN;
            _bitmap = super.source.getBitmap(source);
            _bitmap.x = 2;
            _bitmap.y = 2;
            _bitmapLayer.addChild(_bitmap);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
