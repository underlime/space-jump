/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 16:08
 */
package application.views.screen.avatar
{
    import application.config.ApplicationConfiguration;
    import application.views.screen.elements.radio.BaseRadioButton;
    import application.views.screen.elements.radio.BaseRadioGroup;

    public class LangRadioGroup extends BaseRadioGroup
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LangRadioGroup()
        {
            _russianRadio = new BaseRadioButton('RU');
            _englishRadio = new BaseRadioButton('EN');

            var collection:Vector.<BaseRadioButton> = new Vector.<BaseRadioButton>();
            collection.push(
                    _russianRadio,
                    _englishRadio
            );

            super(collection);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _russianRadio.x = 0;
            _englishRadio.x = 88;

            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU) {
                super.checkRadio(0);
            } else {
                super.checkRadio(1);
            }


        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _russianRadio:BaseRadioButton;
        private var _englishRadio:BaseRadioButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get englishRadio():BaseRadioButton
        {
            return _englishRadio;
        }

        public function get russianRadio():BaseRadioButton
        {
            return _russianRadio;
        }
    }
}
