/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 15:01
 */
package application.views.screen.avatar
{
    import application.views.text.TextFactory;

    import flash.display.DisplayObject;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    public class SimpleVariant extends BaseVariant
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimpleVariant(avaScreen:AvatarGeneratorScreen, mainScreen:DisplayObject)
        {
            super(avaScreen, mainScreen);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function addNote():void
        {
            _setupNote();
            _addNote();
        }

        override public function removeNote():void
        {
            if (_txtNote && !_txtNote.destroyed)
                _txtNote.destroy();
        }

        override public function translate():void
        {
            super.translate();

            if (_txtNote)
                _txtNote.text = TextFactory.instance.getLabelByLang('iplay_label', super._currentLanguage);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtNote:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupNote():void
        {
            _txtNote = new SimpleTextField();
            _txtNote.fontFamily = SimpleTextField.PT_SANS;
            _txtNote.size = 18;
            _txtNote.color = 0xffffff;
            _txtNote.multiline = true;
            _txtNote.wordWrap = true;
            _txtNote.align = TextFormatAlign.CENTER;
            _txtNote.width = 200;
            _txtNote.y = super._bitmap.height - 77;
            super._noteLayer.addChild(_txtNote);
        }

        private function _addNote():void
        {
            _txtNote.text = TextFactory.instance.getLabelByLang('iplay_label', super._currentLanguage);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
