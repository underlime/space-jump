package application.views.screen.shop
{
    import application.controllers.ApplicationController;
    import application.controllers.ShopController;
    import application.models.Inventory;
    import application.models.Items;
    import application.models.User;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.screen.BaseScreen;
    import application.views.screen.shop.rubrics.capsules.CapsulesSuite;
    import application.views.screen.shop.rubrics.hats.HatsSuite;
    import application.views.screen.shop.rubrics.single.SingleUseItemsSuite;
    import application.views.screen.shop.rubrics.skin.SkinsSuite;
    import application.views.screen.shop.rubrics.upgrades.UpgradesSuite;

    import flash.events.Event;

    import framework.events.GameEvent;

    /**
     * Представление магазина.
     * Весь контент в экран магазина добавлять через super.addChildToScrollContent(child:View);
     * Очищать экран через super.clearScrollContent();
     */

    public class ShopScreen extends BaseShopScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShopScreen(shopController:ShopController)
        {
            super(shopController.baseController as ApplicationController);
            _shopController = shopController;
            _itemsModel = shopController.itemsModel;
            _inventoryModel = shopController.inventoryModel;
            _userModel = shopController.userModel;

            _itemsModel.addEventListener(Event.CHANGE, _updateScreen);
            _inventoryModel.addEventListener(Event.CHANGE, _updateScreen);
            _userModel.addEventListener(Event.CHANGE, _updateScreen);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function setup():void
        {
            _methodsHash = {
                0: _addAllItems,
                1: _addUpgrades,
                2: _addSingleUseItems,
                3: _addSuites,
                4: _addHats,
                5: _addCapsules
            };
        }

        override public function destroy():void
        {
            _itemsModel.removeEventListener(Event.CHANGE, _updateScreen);
            _inventoryModel.removeEventListener(Event.CHANGE, _updateScreen);
            _userModel.removeEventListener(Event.CHANGE, _updateScreen);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        private function _addAllItems():void
        {
            _addUpgrades();
            _addSingleUseItems();
            _addSuites();
            _addHats();
            _addCapsules();
        }

        private function _addUpgrades():void
        {
            this._addItemsSuite(UpgradesSuite);
        }

        private function _addItemsSuite(ItemsSuiteClass:Class):void
        {
            var items:ItemsSuite = new ItemsSuiteClass(_itemsModel, _inventoryModel, _userModel) as ItemsSuite;
            items.y = _startY;
            super.addChildToScrollContent(items);
            _startY += items.totalHeight;
        }

        private function _addSingleUseItems():void
        {
            this._addItemsSuite(SingleUseItemsSuite);
        }

        private function _addSuites():void
        {
            this._addItemsSuite(SkinsSuite);
        }

        private function _addHats():void
        {
            this._addItemsSuite(HatsSuite);
        }

        private function _addCapsules():void
        {
            this._addItemsSuite(CapsulesSuite);
        }

        private function _updateScreen(e:Event):void
        {
            var scrollPercent:Number = super._scrollbar.percent;
            _showSelectedTab();
            super._scrollbar.percent = scrollPercent;
        }

        private function _showSelectedTab():void
        {
            super.clearScrollContent();
            _startY = 10;
            if (_methodsHash[_tabId])
                (_methodsHash[_tabId] as Function).call(this);

            if (this.scrollContent.height > BaseScreen.CONTENT_WINDOW_HEIGHT)
                super.enableScroll();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _itemsModel:Items;
        private var _inventoryModel:Inventory;
        private var _userModel:User;

        private var _methodsHash:Object;
        private var _tabId:int = 0;
        private var _startY:Number = 10;
        private var _shopController:ShopController;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        override protected function _tabsChangeHandler(event:GameEvent):void
        {
            _tabId = int(event.data.tabID);
            _showSelectedTab();
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
