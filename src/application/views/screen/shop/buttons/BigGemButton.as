/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.07.13
 * Time: 12:05
 */
package application.views.screen.shop.buttons
{
    import application.views.screen.elements.buttons.UniversalButton;
    import application.views.screen.shop.ShopData;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    import framework.view.text.SimpleTextField;

    public class BigGemButton extends UniversalButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const GEMS_BUTTON:String = "BUY_GEMS_SMALL_BUTTON";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BigGemButton(price:int)
        {
            super(price.toString());
            _gemsIco = super.source.getBitmap(ShopData.CURRENCY);
            _gemsIco.scrollRect = new Rectangle(0, 24, 24, 24);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function block():void
        {
            _unbindReactions();
            _rectangle.y = 100;
            _button.scrollRect = _rectangle;
            _txtLabel.text = "";
            if (_gemsIco && this.contains(_gemsIco))
                removeChild(_gemsIco);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupBackground():void
        {
            _button = super.source.getBitmap(GEMS_BUTTON);
            _rectangle = new Rectangle(0, 0, 80, 50);
            _button.scrollRect = _rectangle;
        }

        override protected function _setupLabel():void
        {
            _txtLabel = new SimpleTextField();
            _txtLabel.size = 16;
            _txtLabel.height = 20;
            _txtLabel.autoSize = TextFieldAutoSize.LEFT;
        }

        override protected function _renderLabel():void
        {
            super._renderLabel();
            addChild(_gemsIco);
            _gemsIco.y = 15;
            _txtLabel.x = _rectangle.width / 2 - (_txtLabel.width + _margin + _gemsIco.width) / 2;
            _gemsIco.x = _txtLabel.x + _txtLabel.width + _margin;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _gemsIco:Bitmap;
        private var _margin:int = 5;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
