/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.06.13
 * Time: 12:51
 */
package application.views.screen.shop.buttons
{
    import application.config.Currency;
    import application.events.ShopEvent;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;

    import flash.events.MouseEvent;

    import framework.view.View;

    public class ButtonGroup extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ButtonGroup()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupButtons();
        }

        override public function destroy():void
        {
            _coinsButton.destroy();
            _gemsButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function disableCoins():void
        {
            if (_coinsButton)
                _coinsButton.disable();
        }

        public function disableGems():void
        {
            if (_gemsButton)
                _gemsButton.disable();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _coins:int = 0;
        private var _gems:int = 0;

        private var _coinsButton:CoinsButton;
        private var _gemsButton:GemsButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupButtons():void
        {
            _coinsButton = new CoinsButton();
            _coinsButton.addEventListener(MouseEvent.CLICK, _coinsClickHandler);
            addChild(_coinsButton);

            _gemsButton = new GemsButton();
            _gemsButton.addEventListener(MouseEvent.CLICK, _gemsClickHandler);
            addChild(_gemsButton);
            _gemsButton.y = 26;
        }

        private function _updateCoins():void
        {
            if (_coinsButton)
                _coinsButton.priceValue = _coins;
        }

        private function _updateGems():void
        {
            if (_gemsButton)
                _gemsButton.priceValue = _gems;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _gemsClickHandler(e:MouseEvent):void
        {
            var event:ShopEvent = new ShopEvent(ShopEvent.BUY);
            event.currency = Currency.GEMS;

            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
            super.dispatchEvent(event);
        }

        private function _coinsClickHandler(e:MouseEvent):void
        {
            var event:ShopEvent = new ShopEvent(ShopEvent.BUY);
            event.currency = Currency.COINS;

            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
            super.dispatchEvent(event);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get coins():int
        {
            return _coins;
        }

        public function set coins(value:int):void
        {
            _coins = value;
            _updateCoins();
        }

        public function get gems():int
        {
            return _gems;
        }

        public function set gems(value:int):void
        {
            _gems = value;
            _updateGems();
        }
    }
}
