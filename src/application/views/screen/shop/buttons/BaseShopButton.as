/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.06.13
 * Time: 12:00
 */
package application.views.screen.shop.buttons
{
    import application.views.base.ApplicationView;
    import application.views.screen.shop.ShopData;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFieldAutoSize;

    import framework.view.text.SimpleTextField;

    public class BaseShopButton extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const EQUIP_BUTTON:String = "BUY_EQUIP_BUTTON_TWO_PRICE";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseShopButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _icon = super.source.getBitmap(ShopData.CURRENCY);
            _icon.scrollRect = new Rectangle(0, 0, 24, 24);
        }

        override public function render():void
        {
            _setupBackground();
            _setupTextValue();
            _setupIcon();
            _bindReactions();
        }

        override public function destroy():void
        {
            _txtPriceValue.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function disable():void
        {
            if (!_disabled) {
                _unbindReactions();
                _disabled = true;

                _color = "#fc5863";
                _updatePrice();

                _rectangle.y = 0;
                _background.scrollRect = _rectangle;
            }
        }

        public function enable():void
        {
            if (_disabled) {
                _bindReactions();
                _disabled = false;

                _color = "#ffffff";
                _updatePrice();

                _rectangle.y = 24;
                _background.scrollRect = _rectangle;
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _icon:Bitmap;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _background:Bitmap;
        private var _rectangle:Rectangle = new Rectangle(0, 24, 132, 24);

        private var _priceValue:int = 0;
        private var _txtPriceValue:SimpleTextField;

        private var _disabled:Boolean = false;
        private var _color:String = "#ffffff";

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _background = super.source.getBitmap(EQUIP_BUTTON);
            _background.scrollRect = _rectangle;
            addChild(_background);
        }

        private function _setupTextValue():void
        {
            _txtPriceValue = new SimpleTextField();
            _txtPriceValue.size = 20;
            _txtPriceValue.autoSize = TextFieldAutoSize.RIGHT;
            _txtPriceValue.x = 100;

            addChild(_txtPriceValue);
            _updatePrice();
        }

        private function _setupIcon():void
        {
            _icon.x = 105;
            addChild(_icon);
        }

        private function _bindReactions():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;
            addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _updatePrice():void
        {
            if (_txtPriceValue)
                _txtPriceValue.htmlText = "<font size='20' color='" + _color + "'>" + _priceValue.toString() + "</font>";
        }

        private function _unbindReactions():void
        {
            this.buttonMode = false;
            this.useHandCursor = false;

            removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            removeEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            _rectangle.y = 48
            _background.scrollRect = _rectangle;
        }

        private function _outHandler(event:MouseEvent):void
        {
            _rectangle.y = 24;
            _background.scrollRect = _rectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get priceValue():int
        {
            return _priceValue;
        }

        public function set priceValue(value:int):void
        {
            _priceValue = value;
            _updatePrice();
        }

        public function get disabled():Boolean
        {
            return _disabled;
        }
    }
}
