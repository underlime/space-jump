package application.views.screen.shop.buttons
{
    import application.views.base.ApplicationView;

    import flash.display.DisplayObject;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;

    import framework.tools.SourceManager;
    import framework.view.text.SimpleTextField;

    public class SlimButton extends ApplicationView
    {

        public static const BUTTON_WIDTH:Number = 132;
        public static const BUTTON_HEIGHT:Number = 24;
        private static const BG_DFINITION:String = "BUY_EQUIP_BUTTON_TWO_PRICE";

        private var _text:String;
        private var _background:DisplayObject;
        private var _labelField:SimpleTextField;

        public function SlimButton(text:String)
        {
            super();
            _text = text;
        }

        override public function render():void
        {
            this.scrollRect = new Rectangle(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT);
            this.buttonMode = true;
            this.useHandCursor = true;

            this.addEventListener(MouseEvent.MOUSE_OVER, _onMouseOver);
            this.addEventListener(MouseEvent.MOUSE_OUT, _onMouseOut);

            _setupBackground(false);
            _setupText();
        }

        private function _onMouseOut(e:MouseEvent):void
        {
            _setupBackground(false);
        }

        private function _onMouseOver(e:MouseEvent):void
        {
            _setupBackground(true);
        }

        private function _setupBackground(hover:Boolean):void
        {
            _destroyBackground();
            var rectangle:Rectangle = new Rectangle(0, BUTTON_HEIGHT, BUTTON_WIDTH, BUTTON_HEIGHT);
            if (hover)
                rectangle.y = BUTTON_HEIGHT*2;
            _background = SourceManager.instance.getBitmap(BG_DFINITION);
            _background.scrollRect = rectangle;
            addChildAt(_background, 0);
        }

        private function _setupText():void
        {
            _labelField = new SimpleTextField();
            _labelField.width = BUTTON_WIDTH;
            _labelField.align = TextFormatAlign.CENTER;
            _labelField.color = 0xFFFFFF;
            _labelField.x = 0;
            _labelField.y = 4;
            addChild(_labelField);
            _labelField.htmlText = _text.toUpperCase();
        }

        private function _destroyBackground():void
        {
            if (_background)
                removeChild(_background);
            _background = null;
        }

        override public function destroy():void
        {
            _destroyBackground();

            if (_labelField)
                _labelField.destroy();
            _labelField = null;

            super.destroy();
        }
    }
}
