/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 11:32
 */
package application.views.screen.shop
{
    import application.views.screen.elements.tabs.SpaceTab;
    import application.views.screen.elements.tabs.SpaceTabPanel;
    import application.views.text.TextFactory;

    import framework.view.tabs.BaseTab;

    public class ShopTabs extends SpaceTabPanel
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShopTabs()
        {
            var tabs:Vector.<BaseTab> = new Vector.<BaseTab>();
            var factory:TextFactory = TextFactory.instance;

            tabs.push(
                    new SpaceTab(factory.getLabel('all_items_label')),
                    new SpaceTab(factory.getLabel('upgrades_label')),
                    new SpaceTab(factory.getLabel('single_use_label')),
                    new SpaceTab(factory.getLabel('suites_label')),
                    new SpaceTab(factory.getLabel('hats_label')),
                    new SpaceTab(factory.getLabel('capsules_label'))
            );

            super(tabs);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
