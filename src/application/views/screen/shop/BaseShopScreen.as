/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.02.13
 * Time: 10:14
 */
package application.views.screen.shop
{
    import application.controllers.ApplicationController;
    import application.events.ApplicationEvent;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.screen.BaseScreen;
    import application.views.screen.elements.buttons.GetCoinsButton;
    import application.views.text.TextFactory;

    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.events.GameEvent;

    public class BaseShopScreen extends BaseScreen
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseShopScreen(appCtrl:ApplicationController)
        {
            super(appCtrl, TextFactory.instance.getLabel('shop_label').toUpperCase());
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _setupScrollContent();
            _setupGetCoinsButton();
            _setupTabs();
            super._lineShape.graphics.clear();
        }

        override public function destroy():void
        {
            _getCoinsButton.destroy();
            _tabs.removeEventListener(GameEvent.CHANGE, _tabsChangeHandler);
            _tabs.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _tabs:ShopTabs;

        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupRectangles():void
        {
            _scrollerRectangle = new Rectangle(0, 0, 0, 550);
            _contentRectangle = new Rectangle(0, 0, 760, 439);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _getCoinsButton:GetCoinsButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupGetCoinsButton():void
        {
            _getCoinsButton = new GetCoinsButton();
            _getCoinsButton.x = super._back.x + 448;
            _getCoinsButton.y = super._closeButton.y;
            _getCoinsButton.addEventListener(MouseEvent.CLICK, _getCoinsClickHandler);

            addChild(_getCoinsButton);
        }

        private function _setupTabs():void
        {
            _tabs = new ShopTabs();
            _tabs.addEventListener(GameEvent.CHANGE, _tabsChangeHandler);
            _tabs.y = super._back.y + 90;
            _tabs.x = super._back.x + 20;
            addChild(_tabs);
        }

        private function _setupScrollContent():void
        {
            super.scrollContent.x = super._back.x + 20;
            super.scrollContent.y = super._back.y + 117;
            super._scrollContent.scrollRect = _contentRectangle;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _getCoinsClickHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_BUY_MONEY));
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        protected function _tabsChangeHandler(event:GameEvent):void
        {
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
