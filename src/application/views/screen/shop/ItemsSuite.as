/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 15:10
 */
package application.views.screen.shop
{
    import application.models.Inventory;
    import application.models.Items;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.views.screen.shop.headers.ItemsSuiteHeader;

    import framework.view.View;

    public class ItemsSuite extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ItemsSuite(itemsModel:Items, inventoryModel:Inventory, userModel:User, rubric:String, HeaderClass:Class,  PositionClass:Class)
        {
            _header = new HeaderClass() as ItemsSuiteHeader;
            _positions = new Vector.<View>();
            var itemsList:Vector.<ItemRecord> = itemsModel.getItemsByRubric(rubric);

            for (var i:int=0; i<itemsList.length; ++i)
                _positions.push(new PositionClass(itemsList[i], inventoryModel, userModel));

            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            addChild(_header);
            var top:Number = 43;
            for (var i:int = 0; i < _positions.length; i++) {
                addChild(_positions[i]);
                _positions[i].y = top;
                top += _margin;
            }
        }

        override public function destroy():void
        {
            for (var i:int = 0; i < _positions.length; i++) {
                _positions[i].destroy();
            }
            _header.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _header:ItemsSuiteHeader;
        private var _positions:Vector.<View>;
        private var _margin:Number = 63;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get totalHeight():Number
        {
            return (_positions.length * _margin) + 43;
        }

    }
}
