/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.02.13
 * Time: 15:25
 */
package application.views.screen.shop.rubrics.upgrades
{
    import application.models.Inventory;
    import application.models.Items;
    import application.models.User;
    import application.views.screen.shop.ItemsSuite;
    import application.views.screen.shop.headers.UpgradesHeader;

    public class UpgradesSuite extends ItemsSuite
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UpgradesSuite(itemsModel:Items, inventoryModel:Inventory, userModel:User)
        {
            super(itemsModel, inventoryModel, userModel, Items.RUBRIC_UPGRADES, UpgradesHeader, UpgradePosition);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
