/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.02.13
 * Time: 15:02
 */
package application.views.screen.shop.rubrics.upgrades
{
    import application.config.Currency;
    import application.events.ApplicationEvent;
    import application.events.ShopEvent;
    import application.models.Inventory;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.views.screen.elements.UpgradeProgress;
    import application.views.screen.shop.buttons.BigGemButton;
    import application.views.screen.shop.rubrics.base.ItemPosition;
    import application.views.screen.shop.rubrics.base.Price;
    import application.views.text.TextFactory;

    import flash.events.MouseEvent;

    import mx.utils.StringUtil;

    public class UpgradePosition extends ItemPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function UpgradePosition(itemRecord:ItemRecord, inventoryModel:Inventory, userModel:User)
        {
            var buttonText:String = TextFactory.instance.getLabel('shop_button_upgrade_label');
            super(itemRecord, inventoryModel, userModel, buttonText);
            if (_inventoryRecord)
                _upgradeLevel = _inventoryRecord.count.value;
            else
                _upgradeLevel = 0;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function render():void
        {
            super.render();
            _setupDescriptionText();
            _setupUpgradeBar();
        }

        override public function destroy():void
        {
            if (_gemsBigButton && !_gemsBigButton.destroyed)
                _gemsBigButton.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _getPrice():Price
        {
            var priceObject:Price = new Price();

            var index:int = 0;
            if (_inventoryRecord)
                index = _inventoryRecord.count.value;

            var coinsPricesList:Array = [_itemRecord.price.value];
            var coinsUpgradePricesList:Array = _itemRecord.upgrade_price.value.split(',');

            var gemsPricesList:Array = [_itemRecord.gems_price.value];
            var gemsUpgradePricesList:Array = _itemRecord.gems_upgrade_price.value.split(',');

            var coinsPrice:int;
            var gemsPrice:int;
            for (var i:int = 0; i < coinsUpgradePricesList.length; ++i) {
                coinsPrice = int(StringUtil.trim(coinsUpgradePricesList[i]));
                coinsPricesList.push(coinsPrice);

                gemsPrice = int(StringUtil.trim(gemsUpgradePricesList[i]));
                gemsPricesList.push(gemsPrice);
            }

            if (index >= coinsPricesList.length || index >= gemsPricesList.length)
                index = 0;

            priceObject.coins = coinsPricesList[index];
            priceObject.gems = gemsPricesList[index];

            return priceObject;
        }

        override protected function _addButtonGroup():void
        {
            _gemsBigButton = new BigGemButton(_getPrice().gems);
            addChild(_gemsBigButton);

            _gemsBigButton.x = 620;
            _gemsBigButton.y = 5;
            _gemsBigButton.addEventListener(MouseEvent.CLICK, _gemsClickHandler);
        }

        override protected function _addDone():void
        {
            _gemsBigButton = new BigGemButton(0);
            addChild(_gemsBigButton);

            _gemsBigButton.x = 620;
            _gemsBigButton.y = 5;
            _gemsBigButton.block();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _upgradeLevel:int = 0;
        private var _upgradeProgress:UpgradeProgress;

        private var _gemsBigButton:BigGemButton;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupDescriptionText():void
        {
            super._txtDescription.x = 287;
            super._txtDescription.y = 10;
            super._txtDescription.width = 260;
        }

        private function _setupUpgradeBar():void
        {
            _upgradeProgress = new UpgradeProgress();
            _upgradeProgress.level = _upgradeLevel;
            _upgradeProgress.x = 62;
            _upgradeProgress.y = 26;
            addChild(_upgradeProgress);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _gemsClickHandler(event:MouseEvent):void
        {
            var buyEvent:ShopEvent = new ShopEvent(ShopEvent.BUY, true);
            buyEvent.currency = Currency.GEMS;
            buyEvent.item = _itemRecord;

            var priceObject:Price = _getPrice();
            if (_userModel.gems.value < priceObject.gems)
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_BANK, true));
            else
                super.dispatchEvent(buyEvent);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set upgradeLevel(value:int):void
        {
            _upgradeLevel = value;
            if (_upgradeProgress)
                _upgradeProgress.level = _upgradeLevel;
        }
    }
}
