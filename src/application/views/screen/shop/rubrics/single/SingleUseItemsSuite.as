/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 13:12
 */
package application.views.screen.shop.rubrics.single
{
    import application.models.Inventory;
    import application.models.Items;
    import application.models.User;
    import application.views.screen.shop.ItemsSuite;
    import application.views.screen.shop.headers.SingleUseItemsHeader;

    public class SingleUseItemsSuite extends ItemsSuite
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SingleUseItemsSuite(itemsModel:Items, inventoryModel:Inventory, userModel:User)
        {
            super(itemsModel, inventoryModel, userModel, Items.RUBRIC_CONSUMABLES, SingleUseItemsHeader, SingleUseItemPosition);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
