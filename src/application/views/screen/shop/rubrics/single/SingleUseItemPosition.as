/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 14:39
 */
package application.views.screen.shop.rubrics.single
{
    import application.models.Inventory;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.views.screen.shop.rubrics.base.ItemPosition;

    import flash.text.TextFieldAutoSize;

    import framework.view.text.SimpleTextField;

    public class SingleUseItemPosition extends ItemPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SingleUseItemPosition(itemRecord:ItemRecord, inventoryModel:Inventory, userModel:User)
        {
            super(itemRecord, inventoryModel, userModel);
            if (_inventoryRecord)
                _count = _inventoryRecord.count.value;
            else
                _count = 0;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _setupCountText();
        }

        override public function destroy():void
        {
            _txtCount.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _count:int = 0;
        private var _txtCount:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupCountText():void
        {
            _txtCount = new SimpleTextField();
            _txtCount.size = 9;
            _txtCount.autoSize = TextFieldAutoSize.RIGHT;
            _txtCount.color = 0xffffff;

            _txtCount.x = 44;
            _txtCount.y = 39;

            addChild(_txtCount);
            _txtCount.text = _count.toString();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set count(value:int):void
        {
            _count = value;
            _txtCount.text = _count.toString();
        }
    }
}
