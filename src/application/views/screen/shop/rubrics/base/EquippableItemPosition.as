package application.views.screen.shop.rubrics.base
{
    import application.events.ShopEvent;
    import application.models.Inventory;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.views.text.TextFactory;

    import flash.events.MouseEvent;

    public class EquippableItemPosition extends ItemPosition
    {
        public function EquippableItemPosition(itemRecord:ItemRecord, inventoryModel:Inventory, userModel:User, buttonText:String = null)
        {
            super(itemRecord, inventoryModel, userModel, buttonText);
        }

        override protected function _setupButtonState():void
        {

            if (_inventoryRecord && _inventoryRecord.count.value) {
                if (_inventoryRecord.equipped.value == 0) {
                    _setupButton();
                    _shopButton.header = TextFactory.instance.getLabel('shop_button_equip_label');
                    _shopButton.addEventListener(MouseEvent.CLICK, _equipHandler);
                }
                else
                    if (_inventoryRecord.equipped.value) {
                        _setupButton();
                        _shopButton.header = TextFactory.instance.getLabel('shop_button_equipped_label');
                        _shopButton.done();
                    }
            }
            else
                if (_itemRecord.max_to_buy.value == 0) {
                    _setupButton();
                    _shopButton.header = TextFactory.instance.getLabel('shop_button_denied_buy');
                    _shopButton.disable();
                }
                else {
                    _setupButtonGroup();
                    _buttonsGroup.addEventListener(ShopEvent.BUY, _buyHandler);
                }
        }


        // event handlers

        private function _equipHandler(e:MouseEvent):void
        {
            var event:ShopEvent = new ShopEvent(ShopEvent.EQUIP, true);
            event.item = _itemRecord;

            super.dispatchEvent(event)
        }
    }
}
