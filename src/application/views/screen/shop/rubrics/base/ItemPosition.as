/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.02.13
 * Time: 14:16
 */
package application.views.screen.shop.rubrics.base
{
    import application.config.ApplicationConfiguration;
    import application.config.Currency;
    import application.events.ApplicationEvent;
    import application.events.ShopEvent;
    import application.models.Inventory;
    import application.models.User;
    import application.models.inventory.InventoryRecord;
    import application.models.items.ItemRecord;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.screen.BaseScreenPosition;
    import application.views.screen.ScreenData;
    import application.views.screen.elements.buttons.ShopScreenButton;
    import application.views.screen.shop.buttons.ButtonGroup;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.events.MouseEvent;

    import framework.tools.SourceManager;
    import framework.view.text.SimpleTextField;

    public class ItemPosition extends BaseScreenPosition
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ItemPosition(itemRecord:ItemRecord, inventoryModel:Inventory, userModel:User, buttonText:String = null)
        {
            _itemRecord = itemRecord;
            _inventoryModel = inventoryModel;
            _userModel = userModel;
            if (buttonText)
                _buttonText = buttonText;
            else
                _buttonText = TextFactory.instance.getLabel('shop_button_buy_label');

            _inventoryRecord = _inventoryModel.getRecordByItemId(_itemRecord.id.value);
            _icon = SourceManager.instance.getBitmap(itemRecord.image.value);

            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU) {
                _description = itemRecord.description_ru.value;
                _name = itemRecord.name_ru.value;
            }
            else {
                _description = itemRecord.description_en.value;
                _name = itemRecord.name_en.value;
            }

            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _setupIcon();
            _setupNameText();
            _setDescription();
            _setupShopButtonVariant();
        }

        override public function destroy():void
        {
            _txtDescription.destroy();
            _txtHeader.destroy();

            if (_shopButton && !_shopButton.destroyed)
                _shopButton.destroy();

            if (_buttonsGroup && !_buttonsGroup.destroyed)
                _buttonsGroup.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _clickButtonEvent:String;

        protected var _txtHeader:SimpleTextField;
        protected var _txtDescription:SimpleTextField;
        protected var _shopButton:ShopScreenButton;

        protected var _itemRecord:ItemRecord;
        protected var _inventoryRecord:InventoryRecord;

        protected var _inventoryModel:Inventory;
        protected var _userModel:User;

        protected var _buttonsGroup:ButtonGroup;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _getPrice():Price
        {
            return new Price(_itemRecord.gems_price.value, _itemRecord.price.value);
        }

        protected function _setupShopButtonVariant():void
        {
            //            var strPrice:String = NumberUtil.format(_getPrice(), ' ');
            //            var buttonHeader:String = _buttonText.replace('{{PRICE}}', strPrice);
            //
            //            _shopButton = new ShopScreenButton();
            //            addChild(_shopButton);
            //            _shopButton.header = buttonHeader;
            //            _shopButton.x = 568;
            //            _shopButton.y = 5;

            _setupButtonState();
        }

        protected function _setupButtonState():void
        {
            if (_inventoryRecord && _itemRecord.max_to_buy.value > 0 && _inventoryRecord.count.value >= _itemRecord.max_to_buy.value) {
                //                _shopButton.done();
                //                _shopButton.header = TextFactory.instance.getLabel('shop_button_done_text');
                _addDone();
            }
            else {
                if (_itemRecord.max_to_buy.value == 0) {
                    //                    _shopButton.disable();
                    //                    _shopButton.header = TextFactory.instance.getLabel('shop_button_denied_buy');
                    _setupButton();
                    _shopButton.disable();
                    _shopButton.header = TextFactory.instance.getLabel('shop_button_denied_buy');
                }
                else {


                    //                    if (_getPrice() > _userModel.coins.value)
                    //                        _clickButtonEvent = ApplicationEvent.CALL_BANK;
                    //                    else
                    //                        _clickButtonEvent = ApplicationEvent.CLICK_BUY_ITEM;
                    //                    _shopButton.addEventListener(MouseEvent.CLICK, _buttonClickHandler);

                    _addButtonGroup();
                }
            }
        }

        protected function _addDone():void
        {
            _setupButton();
            _shopButton.done();
            _shopButton.header = TextFactory.instance.getLabel('shop_button_done_text');
        }

        protected function _addButtonGroup():void
        {
            _setupButtonGroup();
            _buttonsGroup.addEventListener(ShopEvent.BUY, _buyHandler);
        }

        protected function _setupButtonGroup():void
        {
            var price:Price = _getPrice();

            _buttonsGroup = new ButtonGroup();
            addChild(_buttonsGroup);

            _buttonsGroup.x = 568;
            _buttonsGroup.y = 5;

            _buttonsGroup.coins = price.coins;
            _buttonsGroup.gems = price.gems;
        }

        protected function _setupButton():void
        {
            _shopButton = new ShopScreenButton();
            addChild(_shopButton);
            _shopButton.x = 568;
            _shopButton.y = 5;
        }


        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _icon:Bitmap;
        private var _iconBack:Bitmap;

        private var _name:String;
        private var _description:String;
        private var _buttonText:String;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupIcon():void
        {
            _iconBack = super.source.getBitmap(ScreenData.ITEM_ICON_BACKGROUND);
            _iconBack.x = _iconBack.y = 5;
            addChild(_iconBack);

            _icon.x = _icon.y = 5;
            addChild(_icon);
        }


        private function _setupNameText():void
        {
            _txtHeader = new SimpleTextField();
            _txtHeader.size = 16;
            _txtHeader.color = 0x7bf3ff;
            _txtHeader.width = 500;
            addChild(_txtHeader);

            _txtHeader.text = _name;
            _txtHeader.x = 60;
            _txtHeader.y = 3;
        }

        private function _setDescription():void
        {
            _txtDescription = new SimpleTextField();
            _txtDescription.width = 500;
            _txtDescription.multiline = true;
            _txtDescription.wordWrap = true;

            addChild(_txtDescription);
            _txtDescription.x = 60;
            _txtDescription.y = 23;
            _txtDescription.htmlText = _description;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _buyHandler(event:ShopEvent):void
        {
            var buyEvent:ShopEvent = new ShopEvent(ShopEvent.BUY, true);
            buyEvent.currency = event.currency;
            buyEvent.item = _itemRecord;

            var priceObject:Price = _getPrice();

            if (event.currency == Currency.GEMS) {
                if (_userModel.gems.value < priceObject.gems)
                    super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_BANK, true));
                else
                    super.dispatchEvent(buyEvent);
            } else {
                if (_userModel.coins.value < priceObject.coins)
                    super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_BANK, true));
                else
                    super.dispatchEvent(buyEvent);
            }
        }

        protected function _buttonClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(_clickButtonEvent, true);
            event.data = {'item_record': _itemRecord};
            dispatchEvent(event);
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get shopButton():ShopScreenButton
        {
            return _shopButton;
        }

    }
}
