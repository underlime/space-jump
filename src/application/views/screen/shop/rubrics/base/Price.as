/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.06.13
 * Time: 17:17
 */
package application.views.screen.shop.rubrics.base
{
    public class Price
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Price(gems:int = 0, coins:int = 0)
        {
            super();
            _gems = gems;
            _coins = coins;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _gems:int;
        private var _coins:int;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get gems():int
        {
            return _gems;
        }

        public function set gems(value:int):void
        {
            _gems = value;
        }

        public function get coins():int
        {
            return _coins;
        }

        public function set coins(value:int):void
        {
            _coins = value;
        }
    }
}
