/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.02.13
 * Time: 12:42
 */
package application.views.screen.shop.headers
{
    import application.views.base.ApplicationView;

    import flash.display.Bitmap;
    import flash.text.TextFieldAutoSize;

    import framework.view.text.SimpleTextField;

    public class ItemsSuiteHeader extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACKGROUND_DATA:String = "SPACE_SHOP_TITLE_FIELD";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ItemsSuiteHeader(header:String)
        {
            _header = header;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function render():void
        {
            _setupBackground();
            _setupHeader();
        }

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _back:Bitmap;
        private var _txtHeader:SimpleTextField;
        private var _header:String;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _back = super.source.getBitmap(ItemsSuiteHeader.BACKGROUND_DATA);
            addChild(_back);
        }

        private function _setupHeader():void
        {
            _txtHeader = new SimpleTextField();
            _txtHeader.size = 14;
            _txtHeader.color = 0xffffff;
            _txtHeader.autoSize = TextFieldAutoSize.LEFT;
            _txtHeader.x = 12;
            _txtHeader.y = 10;

            addChild(_txtHeader);
            _txtHeader.text = _header;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
