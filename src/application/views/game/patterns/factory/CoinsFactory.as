/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 04.03.13
 * Time: 15:23
 */
package application.views.game.patterns.factory
{
    import application.models.Inventory;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.coins.CoinsChanceRecord;
    import application.models.coins.CoinsUpgradeCollection;
    import application.models.pattern.PatternObject;
    import application.views.game.layers.Layer;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.coins.CopperCoins;
    import application.views.physics.bodies.coins.GoldCoins;
    import application.views.physics.bodies.coins.SilverCoins;

    import framework.data.ModelsRegistry;
    import framework.helpers.MathHelper;

    import nape.geom.Vec2;

    public class CoinsFactory extends BaseBodyFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CoinsFactory()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getBodies(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            var currentY:Number = patternObject.position_y.value;
            var deltaY:Number = 5;

            var chanceRecord:CoinsChanceRecord = _coinsCollection.getCoinsChanceRecord(_level, _defineSector());

            for (var i:int = 0; i < patternObject.count.value; i++) {

                var TempClass:Class;

                if (MathHelper.chance(chanceRecord.gold_chance.value))
                    TempClass = GoldCoins;
                else
                    if (MathHelper.chance(chanceRecord.silver_chance.value))
                        TempClass = SilverCoins;
                    else
                        TempClass = CopperCoins;

                var spaceBody:SpaceBody = new TempClass();
                result.push(spaceBody);

                spaceBody.body.velocity = new Vec2(0, (-1) * _gameModel.velocity);
                spaceBody.body.position.setxy(patternObject.position_x.value, currentY);
                spaceBody.model = super._gameModel;

                currentY += spaceBody.bodyHeight + deltaY;
            }

            return result;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setup():void
        {
            _fillHash();
            _defineUpgradeLevel();
            _setupModel();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _level:int = 0;
        private var _coinsCollection:CoinsUpgradeCollection;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _fillHash():void
        {
            _hash[Layer.COSMOS_LAYER] = [CopperCoins];
            _hash[Layer.DARK_LAYER] = [CopperCoins, SilverCoins];
            _hash[Layer.BRIGHT_LAYER] = [CopperCoins, SilverCoins, GoldCoins];
        }

        private function _defineUpgradeLevel():void
        {
            var inventory:Inventory = ModelsRegistry.getModel(ModelData.INVENTORY_MODEL) as Inventory;
            var data:Object = inventory.getItemsGroupByAction()[Items.ACTION_IMPROVE_COINS];
            _level = data.count ? int(data.count) : 0;
        }

        private function _setupModel():void
        {
            _coinsCollection = _gameModel.getChildByName(ModelData.COINS_COLLECTION) as CoinsUpgradeCollection;
        }

        private function _defineSector():int
        {
            if (_statistics.distance < _coinsCollection.first_check_point.value)
                return 1;
            if (_statistics.distance < _coinsCollection.second_check_point.value)
                return 2;

            return 3;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
