/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 03.03.13
 * Time: 17:12
 */
package application.views.game.patterns.factory
{
    import application.config.DataPhysics;
    import application.models.pattern.PatternObject;
    import application.views.game.layers.Layer;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.birds.BirdBody;
    import application.views.physics.bodies.birds.BlackBird;
    import application.views.physics.bodies.birds.RedBird;
    import application.views.physics.bodies.birds.WhiteBird;

    import framework.helpers.MathHelper;

    import nape.geom.Vec2;

    public class BirdsFactory extends BaseBodyFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BirdsFactory()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getBodies(patternObject:PatternObject):Vector.<SpaceBody>
        {
            if (patternObject.object_alias.value == "birds_direct_flock")
                return _generateDirectFlock(patternObject);
            else
                return _generateAngleFlock(patternObject);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setup():void
        {
            _hash[Layer.COSMOS_LAYER] = [RedBird];
            _hash[Layer.DARK_LAYER] = [WhiteBird, BlackBird];
            _hash[Layer.BRIGHT_LAYER] = [WhiteBird, BlackBird];
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _methodHash:Array;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _generateAngleFlock(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            var variants:Array = _hash[_statistics.layer];
            var middleIndex:int = Math.ceil(patternObject.count.value / 2);
            var deltaVerticalVelocity:Number = DataPhysics.instance.birds_velocity.value / patternObject.count.value;
            var startIndex:int = middleIndex - patternObject.count.value;
            var verticalVelocity:Array = [];
            var scale:Boolean = patternObject.position_x.value < DataPhysics.WORLD_WIDTH / 2;

            for (startIndex; startIndex <= patternObject.count.value; startIndex++) {
                verticalVelocity.push(startIndex * deltaVerticalVelocity);
            }

            for (var i:int = 0; i < patternObject.count.value; i++) {
                var randomIndex:int = MathHelper.random(0, variants.length - 1);
                var TempClass:Class = variants[randomIndex] as Class;
                var spaceBody:SpaceBody = new TempClass();

                result.push(spaceBody);
                spaceBody.body.velocity = new Vec2(DataPhysics.instance.birds_velocity.value, verticalVelocity[i]);
                spaceBody.body.position.setxy(patternObject.position_x.value, patternObject.position_y.value);
                spaceBody.delay_y = patternObject.delay_y.value;

                if (scale) {
                    (spaceBody as BirdBody).flipBody();
                } else {
                    spaceBody.body.velocity.x *= -1;
                }
            }

            return result;
        }

        private function _generateDirectFlock(patternObject:PatternObject):Vector.<SpaceBody>
        {
            // TODO нужно сделать. (пока не удалось победить косяк птиц)
            return _generateAngleFlock(patternObject);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
