/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 03.03.13
 * Time: 16:04
 */
package application.views.game.patterns.factory
{
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;
    import application.models.pattern.PatternObject;
    import application.models.session.GameSession;
    import application.views.physics.bodies.base.SpaceBody;

    import framework.data.ModelsRegistry;

    public class BaseBodyFactory implements IBodiesFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseBodyFactory()
        {
            super();
            _gameModel = ModelsRegistry.getModel(ModelData.GAME_MODEL) as GameModel;
            _statistics = _gameModel.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;
            _session = ModelsRegistry.getModel(ModelData.GAME_SESSION) as GameSession;
            _setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getBodies(patternObject:PatternObject):Vector.<SpaceBody>
        {
            throw new Error("Method must be overridden!")
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _hash:Array = [];
        protected var _gameModel:GameModel;
        protected var _statistics:StatisticsGameModel;
        protected var _session:GameSession;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setup():void
        {
            throw new Error("Method must be overridden!")
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
