/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 03.03.13
 * Time: 15:45
 */
package application.views.game.patterns.factory
{
    import application.models.pattern.PatternObject;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.boxes.metal.MetalBox1;
    import application.views.physics.bodies.boxes.metal.MetalBox2;
    import application.views.physics.bodies.boxes.metal.MetalBox3;
    import application.views.physics.bodies.boxes.metal.MetalBox4;
    import application.views.physics.bodies.boxes.tnt.TNTBox1;
    import application.views.physics.bodies.boxes.tnt.TNTBox2;
    import application.views.physics.bodies.boxes.tnt.TNTBox3;
    import application.views.physics.bodies.boxes.tnt.TNTBox4;
    import application.views.physics.bodies.boxes.wood.WoodBox1;
    import application.views.physics.bodies.boxes.wood.WoodBox2;
    import application.views.physics.bodies.boxes.wood.WoodBox3;
    import application.views.physics.bodies.boxes.wood.WoodBox4;
    import application.views.physics.bodies.cola.JetCola;
    import application.views.physics.bodies.heart.Heart;
    import application.views.physics.bodies.hole.BlackHole;
    import application.views.physics.bodies.petarda.GreenPetarda;
    import application.views.physics.bodies.petarda.PurplePetarda;
    import application.views.physics.bodies.petarda.RedPetarda;
    import application.views.physics.bodies.pig.GreedPig;
    import application.views.physics.bodies.shield.Shield;

    import framework.helpers.MathHelper;

    public class SimpleBodiesFactory extends BaseBodyFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimpleBodiesFactory()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getBodies(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            var variants:Array = _hash[patternObject.object_alias.value];
            var cy:Number = patternObject.position_y.value;

            for (var i:int = 0; i < patternObject.count.value; i++) {
                var randomIndex:int = MathHelper.random(0, variants.length - 1);
                var TempClass:Class = variants[randomIndex] as Class;
                var spaceBody:SpaceBody = new TempClass();
                result.push(spaceBody);

                spaceBody.body.position.setxy(
                        patternObject.position_x.value,
                        cy
                );

                spaceBody.body.angularVel = MathHelper.random(0, int(patternObject.rotation.value));
                spaceBody.body.velocity.y = super._gameModel.velocity * (-1);
                spaceBody.model = super._gameModel;

                cy += spaceBody.bodyHeight + 10;
            }

            return result;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setup():void
        {
            _hash["box"] = [WoodBox1, WoodBox2, WoodBox3, WoodBox4, MetalBox1, MetalBox2, MetalBox3, MetalBox4, TNTBox1, TNTBox2, TNTBox3, TNTBox4];
            _hash["wood_box"] = [WoodBox1, WoodBox2, WoodBox3, WoodBox4];
            _hash["metal_box"] = [MetalBox1, MetalBox2, MetalBox3, MetalBox4];
            _hash["tnt_box"] = [TNTBox1, TNTBox2, TNTBox3, TNTBox4];
            _hash["cola"] = [JetCola];
            _hash["heart"] = [Heart];
            _hash["shield"] = [Shield];
            _hash["pig"] = [GreedPig];
            _hash["firework"] = [RedPetarda, GreenPetarda, PurplePetarda];
            _hash["hole"] = [BlackHole];
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
