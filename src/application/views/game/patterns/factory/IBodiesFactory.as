/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 03.03.13
 * Time: 15:47
 */
package application.views.game.patterns.factory
{
    import application.models.pattern.PatternObject;
    import application.views.physics.bodies.base.SpaceBody;

    public interface IBodiesFactory
    {
        function getBodies(patternObject:PatternObject):Vector.<SpaceBody>;
    }
}
