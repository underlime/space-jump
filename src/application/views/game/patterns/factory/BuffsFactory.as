/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 28.04.13
 * Time: 15:00
 */
package application.views.game.patterns.factory
{
    import application.config.DataPhysics;
    import application.models.Inventory;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.pattern.PatternObject;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.rocket.BigRocket;
    import application.views.physics.bodies.shield.BigShield;
    import application.views.physics.bodies.trophies.PandoraBox;

    import framework.data.ModelsRegistry;
    import framework.helpers.MathHelper;

    /**
     * Фабрика генерации трофеев, неуязвимости и бустера.
     * Трофей генерится следующим образом: если с сервера пришел флаг показать трофей (_session.trophy_flag.value),
     * и в текущем шаблоне есть трофей, то он появляется с 50% вероятностью.
     *
     * Бустер и защитное поле генерятся по одному и тому же принципу:
     * вероятность % = уровень прокачки (0-5) * DataPhysics.instance.booster_level_chance.value (DataPhysics.instance.shield_level_chance.value);
     *
     * То есть, если DataPhysics.instance.booster_level_chance.value = 5, эти бафы будут появлять в шаблонах с прокаченной вероятностью 0-25%.
     * Если уровень прокачки, то вероятность генерации в игровой сессии 15% в каждом шаблоне, где
     * предусмотрен этот паэурап
     */
    public class BuffsFactory extends SimpleBodiesFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TROPHY:String = "trophy";
        public static const ROCKET:String = "bonus_rocket";
        public static const SHIELD:String = "bonus_shield";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BuffsFactory()
        {
            super();
            _defineBuffsChance();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getBodies(patternObject:PatternObject):Vector.<SpaceBody>
        {
            if (patternObject.object_alias.value == TROPHY)
                return _getTrophy(patternObject);
            else
                if (patternObject.object_alias.value == ROCKET)
                    return _getBooster(patternObject);
                else
                    if (patternObject.object_alias.value == SHIELD)
                        return _getShield(patternObject);
                    else
                        return new Vector.<SpaceBody>();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setup():void
        {
            _hash["trophy"] = [PandoraBox];
            _hash["bonus_shield"] = [BigShield];
            _hash["bonus_rocket"] = [BigRocket];
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _trophyGenerated:Boolean = false;
        private var _trophyPatternChance:int = 50;

        private var _shieldChance:int = 0;
        private var _boosterChance:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _defineBuffsChance():void
        {
            var inventory:Inventory = ModelsRegistry.getModel(ModelData.INVENTORY_MODEL) as Inventory;
            var dataBooster:Object = inventory.getItemsGroupByAction()[Items.ACTION_ACCELERATION];
            var boosterLevel:int = dataBooster.count ? int(dataBooster.count) : 0;
            if (boosterLevel > 5 || boosterLevel < 0)
                boosterLevel = 0;

            var dataShield:Object = inventory.getItemsGroupByAction()[Items.ACTION_IMMORTALITY];
            var shieldLevel:int = dataShield.count ? int(dataShield.count) : 0;

            if (shieldLevel > 5 || shieldLevel < 0)
                shieldLevel = 0;

            _boosterChance = boosterLevel * DataPhysics.instance.booster_level_chance.value;
            _shieldChance = shieldLevel * DataPhysics.instance.shield_level_chance.value;

            if (_boosterChance == 0)
                _boosterChance = 5;
            if (_shieldChance == 0)
                _shieldChance = 5;

        }

        private function _getTrophy(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            if (!_trophyGenerated && _session.trophy_flag.value && MathHelper.chance(_trophyPatternChance)) {
                var cy:Number = patternObject.position_y.value;
                var spaceBody:SpaceBody = new PandoraBox();

                spaceBody.body.position.setxy(
                        patternObject.position_x.value,
                        cy
                );

                spaceBody.body.velocity.y = super._gameModel.velocity * (-1);
                spaceBody.model = super._gameModel;


                result.push(spaceBody);
                _trophyGenerated = true;
            }

            return result;
        }

        private function _getBooster(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            if (MathHelper.chance(_boosterChance)) {
                var cy:Number = patternObject.position_y.value;
                var spaceBody:SpaceBody = new BigRocket();

                spaceBody.body.position.setxy(
                        patternObject.position_x.value,
                        cy
                );
                spaceBody.body.velocity.y = super._gameModel.velocity * (-1);
                spaceBody.model = super._gameModel;
                result.push(spaceBody);
            }

            return result;
        }

        private function _getShield(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            if (MathHelper.chance(_shieldChance)) {
                var cy:Number = patternObject.position_y.value;
                var spaceBody:SpaceBody = new BigShield();

                spaceBody.body.position.setxy(
                        patternObject.position_x.value,
                        cy
                );
                spaceBody.body.velocity.y = super._gameModel.velocity * (-1);
                spaceBody.model = super._gameModel;
                result.push(spaceBody);
            }

            return result;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
