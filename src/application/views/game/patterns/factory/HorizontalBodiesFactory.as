/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.03.13
 * Time: 14:11
 */
package application.views.game.patterns.factory
{
    import application.config.DataPhysics;
    import application.models.pattern.PatternObject;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.jet.BlueJetPlane;
    import application.views.physics.bodies.jet.DarkJetPlane;
    import application.views.physics.bodies.jet.JetPlane;
    import application.views.physics.bodies.jet.RedJetPlane;
    import application.views.physics.bodies.plane.SimplePlane;
    import application.views.physics.bodies.plane.WhitePlane;
    import application.views.physics.bodies.plane.YellowPlane;
    import application.views.physics.bodies.satellite.BaseSatellite;
    import application.views.physics.bodies.satellite.Satellite1;
    import application.views.physics.bodies.satellite.Satellite2;
    import application.views.physics.bodies.satellite.Satellite3;
    import application.views.physics.bodies.ufo.BlueUfo;
    import application.views.physics.bodies.ufo.RedUfo;

    import framework.helpers.MathHelper;

    import nape.geom.Vec2;

    public class HorizontalBodiesFactory extends BaseBodyFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function HorizontalBodiesFactory()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function getBodies(patternObject:PatternObject):Vector.<SpaceBody>
        {
            switch (patternObject.object_alias.value) {
                case "ufo":
                    return _getUfo(patternObject);
                    break;
                case "satellite":
                    return _getSatellite(patternObject);
                    break;
                case "jet":
                    return _getJet(patternObject);
                    break;
                case "plane":
                    return _getPlane(patternObject);
                    break;
                default:
                    return _getUfo(patternObject);
            }
        }

        private function _getSatellite(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            var variants:Array = _hash["satellite"];
            var randomIndex:int = MathHelper.random(0, variants.length - 1);
            var TempClass:Class = variants[randomIndex] as Class;
            var spaceBody:SpaceBody = new TempClass();

            result.push(spaceBody);
            spaceBody.body.position.setxy(patternObject.position_x.value, patternObject.position_y.value);
            var horVelocity:Number = patternObject.horizontal_velocity.value > 0 ? patternObject.horizontal_velocity.value : DataPhysics.instance.satellite_velocity.value;

            if (patternObject.position_x.value > DataPhysics.WORLD_WIDTH / 2) {
                horVelocity *= -1;
            } else {
                (spaceBody as BaseSatellite).flipBody();
            }

            spaceBody.body.velocity = new Vec2(horVelocity, 0)

            return result;
        }

        private function _getUfo(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            var variants:Array = _hash["ufo"];
            var randomIndex:int = MathHelper.random(0, variants.length - 1);
            var TempClass:Class = variants[randomIndex] as Class;
            var spaceBody:SpaceBody = new TempClass();

            result.push(spaceBody);
            spaceBody.body.position.setxy(patternObject.position_x.value, patternObject.position_y.value);
            var horVelocity:Number = patternObject.horizontal_velocity.value > 0 ? patternObject.horizontal_velocity.value : DataPhysics.instance.ufo_velocity.value;

            if (patternObject.position_x.value > DataPhysics.WORLD_WIDTH / 2) {
                horVelocity *= -1;
            }

            spaceBody.body.velocity = new Vec2(horVelocity, 0)

            return result;
        }

        private function _getJet(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            var variants:Array = _hash["jet"];
            var randomIndex:int = MathHelper.random(0, variants.length - 1);
            var TempClass:Class = variants[randomIndex] as Class;
            var spaceBody:SpaceBody = new TempClass();

            result.push(spaceBody);
            spaceBody.body.position.setxy(patternObject.position_x.value, patternObject.position_y.value);
            var horVelocity:Number = patternObject.horizontal_velocity.value > 0 ? patternObject.horizontal_velocity.value : DataPhysics.instance.jet_velocity.value;

            if (patternObject.position_x.value > DataPhysics.WORLD_WIDTH / 2) {
                horVelocity *= -1;
            } else {
                (spaceBody as JetPlane).flipBody();
            }

            spaceBody.body.velocity = new Vec2(horVelocity, 0)

            return result;
        }

        private function _getPlane(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();
            var variants:Array = _hash["plane"];
            var randomIndex:int = MathHelper.random(0, variants.length - 1);
            var TempClass:Class = variants[randomIndex] as Class;
            var spaceBody:SpaceBody = new TempClass();

            result.push(spaceBody);
            spaceBody.body.position.setxy(patternObject.position_x.value, patternObject.position_y.value);
            var horVelocity:Number = patternObject.horizontal_velocity.value > 0 ? patternObject.horizontal_velocity.value : DataPhysics.instance.plane_velocity.value;

            if (patternObject.position_x.value > DataPhysics.WORLD_WIDTH / 2) {
                horVelocity *= -1;
            } else {
                (spaceBody as SimplePlane).flipBody();
            }

            spaceBody.body.velocity = new Vec2(horVelocity, 0)

            return result;
        }


        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setup():void
        {
            _hash["ufo"] = [RedUfo, BlueUfo];
            _hash["jet"] = [RedJetPlane, BlueJetPlane, DarkJetPlane];
            _hash["plane"] = [WhitePlane, YellowPlane];
            _hash["satellite"] = [Satellite1, Satellite2, Satellite3];
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
