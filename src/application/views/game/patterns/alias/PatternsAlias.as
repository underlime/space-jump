/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.02.13
 * Time: 12:44
 */
package application.views.game.patterns.alias
{
    import application.models.GameModel;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.birds.BlackBird;
    import application.views.physics.bodies.birds.RedBird;
    import application.views.physics.bodies.birds.WhiteBird;
    import application.views.physics.bodies.boxes.metal.MetalBox1;
    import application.views.physics.bodies.boxes.metal.MetalBox2;
    import application.views.physics.bodies.boxes.metal.MetalBox3;
    import application.views.physics.bodies.boxes.metal.MetalBox4;
    import application.views.physics.bodies.boxes.tnt.TNTBox1;
    import application.views.physics.bodies.boxes.tnt.TNTBox2;
    import application.views.physics.bodies.boxes.tnt.TNTBox3;
    import application.views.physics.bodies.boxes.tnt.TNTBox4;
    import application.views.physics.bodies.boxes.wood.WoodBox1;
    import application.views.physics.bodies.boxes.wood.WoodBox2;
    import application.views.physics.bodies.boxes.wood.WoodBox3;
    import application.views.physics.bodies.boxes.wood.WoodBox4;
    import application.views.physics.bodies.coins.CopperCoins;
    import application.views.physics.bodies.coins.GoldCoins;
    import application.views.physics.bodies.coins.SilverCoins;
    import application.views.physics.bodies.cola.JetCola;
    import application.views.physics.bodies.heart.Heart;
    import application.views.physics.bodies.jet.BlueJetPlane;
    import application.views.physics.bodies.jet.RedJetPlane;
    import application.views.physics.bodies.petarda.GreenPetarda;
    import application.views.physics.bodies.petarda.PurplePetarda;
    import application.views.physics.bodies.petarda.RedPetarda;
    import application.views.physics.bodies.plane.WhitePlane;
    import application.views.physics.bodies.plane.YellowPlane;
    import application.views.physics.bodies.rocket.BigRocket;
    import application.views.physics.bodies.satellite.Satellite1;
    import application.views.physics.bodies.satellite.Satellite2;
    import application.views.physics.bodies.satellite.Satellite3;
    import application.views.physics.bodies.shield.BigShield;
    import application.views.physics.bodies.shield.Shield;
    import application.views.physics.bodies.ufo.BlueUfo;
    import application.views.physics.bodies.ufo.RedUfo;

    import framework.helpers.MathHelper;

    public class PatternsAlias extends Object
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _instance:PatternsAlias = null;

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PatternsAlias()
        {
            _setup();
            if (_instance != null) {
                throw new Error("Use instance property");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public function getClassByAlias(alias:String):SpaceBody
        {
            if (!_hash[alias])
                throw new Error("Wrong alias. Objects not found");
            var variants:Array = _hash[alias];
            var randomIndex:int = MathHelper.random(0, variants.length - 1);
            var SpaceBodyClass:Class = variants[randomIndex] as Class;
            return new SpaceBodyClass();
        }

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _hash:Array = [];
        private var _model:GameModel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            _hash["box"] = [WoodBox1, WoodBox2, WoodBox3, WoodBox4, MetalBox1, MetalBox2, MetalBox3, MetalBox4, TNTBox1, TNTBox2, TNTBox3, TNTBox4];
            _hash["wood_box"] = [WoodBox1, WoodBox2, WoodBox3, WoodBox4];
            _hash["metal_box"] = [MetalBox1, MetalBox2, MetalBox3, MetalBox4];
            _hash["tnt_box"] = [TNTBox1, TNTBox2, TNTBox3, TNTBox4];
            _hash["bird"] = [RedBird, BlackBird, WhiteBird];
            _hash["ufo"] = [BlueUfo, RedUfo];
            _hash["plane"] = [YellowPlane, WhitePlane];
            _hash["jet"] = [RedJetPlane, BlueJetPlane, RedJetPlane];
            _hash["firework"] = [GreenPetarda, RedPetarda, PurplePetarda];
            _hash["satellite"] = [Satellite1, Satellite2, Satellite3];
            _hash["coins"] = [CopperCoins, SilverCoins, GoldCoins];
            _hash["cola"] = [JetCola];
            _hash["heart"] = [Heart];
            _hash["shield"] = [Shield];
            _hash["bonus_shield"] = [BigShield];
            _hash["bonus_rocket"] = [BigRocket];
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public static function get instance():PatternsAlias
        {
            if (_instance == null)
                _instance = new PatternsAlias();
            return _instance;
        }

        public function set model(value:GameModel):void
        {
            _model = value;
        }
    }
}
