/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.02.13
 * Time: 12:16
 */
package application.views.game.patterns
{
    import application.config.ApplicationConfiguration;
    import application.config.DataPhysics;
    import application.events.ApplicationEvent;
    import application.models.pattern.PatternLogger;
    import application.models.pattern.PatternModel;
    import application.models.pattern.PatternsCollection;
    import application.views.game.patterns.construct.PatternConstructor;
    import application.views.physics.SpaceWorld;
    import application.views.physics.bodies.base.SpaceBody;

    import flash.events.Event;

    import framework.helpers.MathHelper;
    import framework.view.View;
    import framework.view.text.SimpleTextField;

    public class PatternsView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PatternsView(world:SpaceWorld, patterns:PatternsCollection)
        {
            _world = world;
            _patterns = patterns;
            _patternConstructor = new PatternConstructor(_world.model);
            _patternObserver = new PatternObserver();
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _patternObserver.addEventListener(Event.COMPLETE, _patternCompleteHandler);
            _world.addEventListener(ApplicationEvent.UPDATE, _physicsUpdateHandler);

            if (ApplicationConfiguration.SHOW_PATTERNS)
                _setupPatternNumber();

            addPattern();
        }

        override public function destroy():void
        {
            _world.removeEventListener(ApplicationEvent.UPDATE, _physicsUpdateHandler);
            _patternObserver.destroy();

            if (_txtPattern && !_txtPattern.destroyed)
                _txtPattern.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addPattern():void
        {
            var patternModel:PatternModel = _getPatternModel();
            var bodiesList:Vector.<SpaceBody> = _patternConstructor.getObjects(patternModel);

            _currentDistance = 0;
            _patternObserver.registerTemplate(bodiesList);
            _delayedItems = [];

            for (var i:int = 0; i < bodiesList.length; i++) {
                if (!bodiesList[i].horizontalBody) {
                    bodiesList[i].body.position.y += DataPhysics.WORLD_HEIGHT + 10;
                    bodiesList[i].body.space = _world.space;
                    addChild(bodiesList[i]);
                } else {
                    _delayedItems.push(bodiesList[i]);
                }
            }
        }

        public function pause():void
        {
            _patternObserver.removeObjects();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _patterns:PatternsCollection;
        private var _world:SpaceWorld;
        private var _level:int = 2;
        private var _currentDistance:Number = 0;
        private var _patternConstructor:PatternConstructor;
        private var _patternObserver:PatternObserver;
        private var _delayedItems:Array;
        private var _bonusEnabled:Boolean = false;

        private var _txtPattern:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupPatternNumber():void
        {
            _txtPattern = new PatternLogger();
            addChild(_txtPattern);
        }

        private function _getPatternModel():PatternModel
        {
            if (_bonusEnabled)
                return _getBonusPatternModel()
            return _getRandomPatternModel();
        }

        private function _getBonusPatternModel():PatternModel
        {
            var patternModels:Vector.<PatternModel> = _patterns.getBonusLevelPatterns();
            var randomIndex:int = MathHelper.random(0, patternModels.length - 1);
            if (_txtPattern)
                _txtPattern.text = "Pattern #" + patternModels[randomIndex].pattern_id.value.toString();

            return patternModels[randomIndex];
        }

        private function _getRandomPatternModel():PatternModel
        {
            var patternModels:Vector.<PatternModel> = _patterns.getPatternsByLevel(_level);
            var randomIndex:int = MathHelper.random(0, patternModels.length - 1);
            if (_txtPattern)
                _txtPattern.text = "Pattern #" + patternModels[randomIndex].pattern_id.value.toString();

            return patternModels[randomIndex];
        }

        private function _checkDelayedBodies():void
        {
            if (_delayedItems.length > 0) {
                var delta:Number = 0;
                for (var i:int = 0; i < _delayedItems.length; i++) {
                    delta = (_delayedItems[i] as SpaceBody).body.position.y + (_delayedItems[i] as SpaceBody).delay_y;
                    if (_currentDistance - delta >= 0) {
                        _addHorizontalBody(_delayedItems[i] as SpaceBody);
                        _delayedItems.splice(i, 1);
                    }
                }
            }
        }

        private function _addHorizontalBody(spaceBody:SpaceBody):void
        {
            spaceBody.body.space = _world.space;
            addChild(spaceBody);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _physicsUpdateHandler(event:ApplicationEvent):void
        {
            _currentDistance += _world.model.velocity * DataPhysics.TIME_STEP;
            _checkDelayedBodies();
            _patternObserver.update();
        }

        private function _patternCompleteHandler(event:Event):void
        {
            addPattern();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set level(value:int):void
        {
            _level = value;
        }

        public function set bonusEnabled(value:Boolean):void
        {
            _bonusEnabled = value;
        }
    }
}
