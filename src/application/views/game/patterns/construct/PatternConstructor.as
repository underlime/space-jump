/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.02.13
 * Time: 15:46
 */
package application.views.game.patterns.construct
{
    import application.models.GameModel;
    import application.models.pattern.PatternModel;
    import application.models.pattern.PatternObject;
    import application.views.game.patterns.BodiesFactory;
    import application.views.game.patterns.alias.PatternsAlias;
    import application.views.physics.bodies.base.SpaceBody;

    import framework.helpers.MathHelper;

    import org.casalib.core.Destroyable;

    public class PatternConstructor extends Destroyable
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PatternConstructor(model:GameModel)
        {
            super();
            _model = model;
            _patternAlias = PatternsAlias.instance;
            _patternAlias.model = _model;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getObjects(model:PatternModel):Vector.<SpaceBody>
        {
            var list:Object = model.list.value;
            var patternObject:PatternObject;
            var result:Vector.<SpaceBody> = new Vector.<SpaceBody>();

            for (var index:String in list) {
                patternObject = list[index];
                if (MathHelper.chance(patternObject.chance.value)) {
                    result = result.concat(_bodiesFactory.getBodies(patternObject));
                }
            }
            return result;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bodiesFactory:BodiesFactory = new BodiesFactory();
        private var _patternAlias:PatternsAlias;
        private var _model:GameModel;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
