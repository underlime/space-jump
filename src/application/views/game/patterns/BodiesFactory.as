/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 03.03.13
 * Time: 15:36
 */
package application.views.game.patterns
{
    import application.models.pattern.PatternObject;
    import application.views.game.patterns.factory.BirdsFactory;
    import application.views.game.patterns.factory.BuffsFactory;
    import application.views.game.patterns.factory.CoinsFactory;
    import application.views.game.patterns.factory.HorizontalBodiesFactory;
    import application.views.game.patterns.factory.IBodiesFactory;
    import application.views.game.patterns.factory.SimpleBodiesFactory;
    import application.views.physics.bodies.base.SpaceBody;

    import flash.utils.Dictionary;

    public class BodiesFactory
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BodiesFactory()
        {
            super();
            _setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function getBodies(patternObject:PatternObject):Vector.<SpaceBody>
        {
            var factory:IBodiesFactory = _getFactory(patternObject.object_alias.value);
            var bodies:Vector.<SpaceBody> = factory.getBodies(patternObject);
            return bodies;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _factoryHash:Dictionary = new Dictionary();
        private var _simpleFactory:SimpleBodiesFactory = new SimpleBodiesFactory();
        private var _birdsFactory:BirdsFactory = new BirdsFactory();
        private var _coinsFactory:CoinsFactory = new CoinsFactory();
        private var _horizontalFactory:HorizontalBodiesFactory = new HorizontalBodiesFactory();
        private var _buffsFactory:BuffsFactory = new BuffsFactory();


        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            _factoryHash[_simpleFactory] = ["box", "wood_box", "metal_box", "tnt_box", "cola", "heart", "shield", "pig", "firework", "hole"];
            _factoryHash[_birdsFactory] = ["birds_angle_flock", "birds_direct_flock"];
            _factoryHash[_coinsFactory] = ["coins"];
            _factoryHash[_horizontalFactory] = ["ufo", "satellite", "jet", "plane"];
            _factoryHash[_buffsFactory] = ["trophy", "bonus_shield", "bonus_rocket"];
        }

        private function _getFactory(alias:String):IBodiesFactory
        {
            for (var bodyFactory:* in _factoryHash) {
                var arrAlias:Array = _factoryHash[bodyFactory];
                var index:int = arrAlias.indexOf(alias);
                if (index > -1)
                    return bodyFactory as IBodiesFactory;
            }
            throw  new Error("Factory not found. Alias = " + alias);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
