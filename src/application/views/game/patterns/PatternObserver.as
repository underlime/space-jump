/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.03.13
 * Time: 12:23
 */
package application.views.game.patterns
{
    import application.config.DataPhysics;
    import application.events.ApplicationEvent;
    import application.views.physics.bodies.base.SpaceBody;

    import flash.events.Event;

    import org.casalib.events.RemovableEventDispatcher;

    public class PatternObserver extends RemovableEventDispatcher
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PatternObserver()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _destroyAll();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function registerTemplate(bodies:Vector.<SpaceBody>):void
        {
            _bodies = bodies;
            _destroyedBodies = 0;
            _totalBodies = _bodies.length;
            _registerEvents();
        }

        public function update():void
        {
            if (_bodies.length > 0) {
                for (var i:int = 0; i < _totalBodies; i++) {
                    _checkBody(_bodies[i]);
                }
            }
        }

        public function removeObjects():void
        {
            _destroyAll();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bodies:Vector.<SpaceBody>;
        private var _destroyedBodies:int = 0;
        private var _totalBodies:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _registerEvents():void
        {
            for (var i:int = 0; i < _bodies.length; i++) {
                _bodies[i].addEventListener(ApplicationEvent.DESTROY, _bodyDestroy);
            }
        }

        private function _destroyAll():void
        {
            for (var i:int = 0; i < _totalBodies; i++) {
                _bodies[i].removeEventListener(ApplicationEvent.DESTROY, _bodyDestroy);
                if (!_bodies[i].destroyed)
                    _bodies[i].destroy();
            }
        }

        private function _checkBody(spaceBody:SpaceBody):void
        {
            if (!spaceBody.destroyed && spaceBody.body.space) {
                if (spaceBody.body.velocity.y != 0 && spaceBody.body.velocity.x == 0)
                    _checkSimpleVerticalBody(spaceBody);
                else
                    _checkHorizontalBody(spaceBody);
            }
        }

        private function _checkHorizontalBody(spaceBody:SpaceBody):void
        {
            if (spaceBody.body.velocity.y < 0)
                _checkSimpleVerticalBody(spaceBody);

            if (spaceBody.body.velocity.x > 0) {
                if (spaceBody.body.position.x - spaceBody.bodyWidth > DataPhysics.WORLD_WIDTH)
                    spaceBody.destroy();
            } else {
                if (spaceBody.body.position.x < spaceBody.bodyWidth * (-1))
                    spaceBody.destroy();
            }


        }

        private function _checkSimpleVerticalBody(spaceBody:SpaceBody):void
        {
            if (spaceBody.body.position.y < spaceBody.bodyHeight * (-1))
                spaceBody.destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _bodyDestroy(event:ApplicationEvent):void
        {
            _destroyedBodies++;

            if (_destroyedBodies >= _totalBodies) {
                _destroyAll();
                super.dispatchEvent(new Event(Event.COMPLETE));
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
