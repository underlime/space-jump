/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.12.12
 */

package application.views.game
{
    import framework.view.View;

    public class BackupView extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        /*
         public function BackupView()
         {
         }

         override public function render():void
         {
         this.stage.addEventListener(MouseEvent.CLICK, this._increaseVelocity);
         this.addEventListener(Event.ENTER_FRAME, this._updatePhisics);
         this._gamePanelLayer.distance = 40000;
         this._gamePanelLayer.speed = this._model.velocity;
         }

         override public function destroy():void
         {
         this._world.destroy();
         this.graphics.clear();
         super.destroy();
         }

         // PUBLIC PROPERTIES -------------------------------------------------------------------/
         // PUBLIC METHODS ----------------------------------------------------------------------/

         public function update():void
         {
         this._iterator++;
         this._boxLayer.addBox();
         this._gamePanelLayer.seconds = this._iterator;
         this._model.velocity += this._model.acceleration;
         this._gamePanelLayer.speed = this._model.velocity;
         this._gamePanelLayer.distance -= this._model.velocity;
         }

         // PROTECTED PROPERTIES ----------------------------------------------------------------/
         // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
         // PROTECTED METHODS -------------------------------------------------------------------/
         // PRIVATE PROPERTIES ------------------------------------------------------------------/

         private var _iterator:Number = 0;
         private var _velocity:Number = 200;
         private var _background:BackgroundLayer;
         private var _model:GameModel;
         private var _world:SpaceWorld;
         private var _hero:Cosmonaut;
         private var _powerupLayer:PowerupLayer;
         private var _boxLayer:BoxLayer;
         private var _petardaLayer:PetardaLayer;
         private var _gamePanelLayer:GamePanelLayer;

         // PRIVATE METHODS ---------------------------------------------------------------------/

         private function _setupWorld():void
         {
         this.addChild(this._world);
         }

         private function _createHero():void
         {
         this._hero = new Cosmonaut();
         this.addChild(this._hero);
         this._hero.space = this._world.space;
         this._hero.body.space = this._world.space;
         this._hero.body.angularVel = 1;
         this._hero.body.position.setxy(100, 200);
         this._hero.updateGraphic();
         }

         private function _generateCoins(event:TimerEvent):void
         {
         this._powerupLayer.addCoins();
         //this._powerupLayer.addHeart();
         //this._powerupLayer.addShield();
         //this._powerupLayer.addBigShield();
         //this._powerupLayer.addBigRocket();
         this._powerupLayer.addJetCola();
         }

         private function _createPetarda(e:TimerEvent):void
         {
         this._petardaLayer.addPetarda();
         }

         private function _setupCollisions():void
         {
         this._world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.WOODBOX_CBTYPE, this._heroBoxCollisionHandler);
         this._world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.METALBOX_CBTYPE, this._heroMetalBoxCollisionHandler);
         this._world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.COIN_CBTYPE, this._heroСoinCollisionHandler);
         this._world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.HEART_TYPE, this._heroHeartCollisionHandler);
         this._world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.TNT_BOX_CBTYPE, this.__heroTntCollisionHandler);
         this._world.collisions.registerCallback(CallbackTypes.ABYSS_TYPE, CallbackTypes.NO_HERO_TYPE, this._abyssBodyCollisionHandler);
         this._world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.BIRD_TYPE, this._heroBirdCollisionHandler);
         }

         private function _heroBoxCollisionHandler(cb:InteractionCallback):void
         {
         var box:GameBody = cb.int2.castBody as GameBody;
         var woodBox:WoodBox = box.wrapper as WoodBox;


         this._hero.updateGraphic();
         this._checkLife();
         woodBox.explode();
         }

         private function _heroMetalBoxCollisionHandler(cb:InteractionCallback):void
         {
         var b1:GameBody = cb.int1.castBody as GameBody;
         var b2:GameBody = cb.int2.castBody as GameBody;

         var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;
         var position:Vec2 = carb.contacts.at(0).position;
         var impact:Impact = new Impact();
         this.addChild(impact);
         impact.x = position.x - 25;
         impact.y = position.y - 25;
         }

         private function _heroBirdCollisionHandler(cb:InteractionCallback):void
         {
         var bird:GameBody = cb.int2.castBody as GameBody;
         var birdBody:BirdBody = bird.wrapper as BirdBody;

         this._hero.updateGraphic();
         this._checkLife();
         birdBody.explode();
         }

         private function __heroTntCollisionHandler(cb:InteractionCallback):void
         {
         var box:GameBody = cb.int2.castBody as GameBody;
         var explode:TNTExplodeAnimation = new TNTExplodeAnimation();

         var dx:Number = box.wrapper.graphic.width;
         var dy:Number = box.wrapper.graphic.height;

         explode.x = box.worldCOM.x - dx;
         explode.y = box.worldCOM.y - dy;

         var sensor:ExplosionSensor = new ExplosionSensor(400);
         sensor.body.position.x = box.worldCOM.x;
         sensor.body.position.y = box.worldCOM.y;

         sensor.space = this._world.space;

         this.addChild(explode);

         this.addChild(sensor);
         this._world.update();
         this._explode(sensor, box.worldCOM);

         this._hero.updateGraphic();
         this._checkLife();
         box.wrapper.destroy();
         }

         private function _explode(sensor:ExplosionSensor, explosionPoint:Vec2):void
         {
         var arb:Arbiter;
         var body:Body;

         for (var i:int = 0; i < sensor.body.arbiters.length; ++i) {
         arb = sensor.body.arbiters.at(i);
         if (arb.isSensorArbiter()) {
         body = arb.body1 == sensor.body ? arb.body2 : arb.body1;
         if (body.isDynamic()) {
         var impulse:Vec2 = body.worldCOM.sub(explosionPoint);
         if (body == this._hero.body) {
         this._hero.body.velocity.setxy(0, 0);
         impulse.muleq(30);
         body.applyLocalImpulse(impulse);
         } else {
         impulse.muleq(2);
         body.applyLocalImpulse(impulse);
         }
         }
         }
         }
         sensor.destroy();
         }

         private function _abyssBodyCollisionHandler(cb:InteractionCallback):void
         {
         var box:GameBody = cb.int2.castBody as GameBody;
         box.wrapper.destroy();
         }

         private function _heroСoinCollisionHandler(cb:InteractionCallback):void
         {
         var coin:GameBody = cb.int2.castBody as GameBody;
         coin.wrapper.destroy();

         }

         private function _heroHeartCollisionHandler(cb:InteractionCallback):void
         {
         var heart:GameBody = cb.int2.castBody as GameBody;
         heart.wrapper.destroy();
         }

         private function _checkLife():void
         {
         this.removeEventListener(Event.ENTER_FRAME, this._updatePhisics);
         super.dispatchEvent(new ApplicationEvent(ApplicationEvent.END_GAME));
         }

         private function _addGarbage(event:TimerEvent):void
         {
         var y:int = 700;
         var x:int = MathHelper.random(20, 780);
         var targetX:Number = MathHelper.random(20, 780);
         var garbage:SimpleGarbage = new SimpleGarbage();
         garbage.space = this._world.space;
         this.addChild(garbage);
         garbage.body.position.setxy(x, y);
         if (x > PhysicsConfiguration.WORLD_WIDTH / 2) {
         targetX = targetX * (-1);
         }
         garbage.body.applyWorldForce(new Vec2(targetX, PhysicsConfiguration.OBSTACLE_FORCE / 10));
         }

         // EVENT HANDLERS ----------------------------------------------------------------------/

         private function _increaseVelocity(e:MouseEvent):void
         {
         this._velocity += 100;
         this._model.velocity = this._velocity;

         if (this._velocity == 500) {
         this._background.setDarkLayer();
         }

         if (this._velocity == 800) {
         this._background.setBrightLayer();
         }
         }

         private function _createJet(e:TimerEvent):void
         {
         var plane:YellowPlane = new YellowPlane();
         this.addChild(plane);
         plane.body.scaleShapes(-1, 1);
         plane.graphic.scaleX = -1;

         plane.space = this._world.space;
         plane.body.position.x = -700;
         plane.body.position.y = MathHelper.random(300, 500);
         plane.body.velocity = new Vec2(200, 0);

         var jet:RedJetPlane = new RedJetPlane();
         this.addChild(jet);
         jet.body.scaleShapes(-1, 1);
         jet.graphic.scaleX = -1;

         jet.space = this._world.space;
         jet.body.position.x = -300;
         jet.body.position.y = MathHelper.random(100, 300);
         jet.body.velocity = new Vec2(200, 0);


         var ufo:BlueUfo = new BlueUfo();
         this.addChild(ufo);
         ufo.space = this._world.space;
         ufo.body.position.x = -100;
         ufo.body.position.y = MathHelper.random(300, 500);
         ufo.body.velocity = new Vec2(200, MathHelper.random(-50, 50));


         var bird:WhiteBird = new WhiteBird();
         bird.space = this._world.space;
         this.addChild(bird);
         bird.body.scaleShapes(-1, 1);
         bird.graphic.scaleX = -1;

         bird.body.position.x = -100;
         bird.body.position.y = MathHelper.random(100, 500);
         bird.body.velocity = new Vec2(200, MathHelper.random(-50, 50));
         }


         private function _updatePhisics(event:Event = null):void
         {
         this._world.update();
         //this._checkVerticalPosition();
         }

         private function _checkVerticalPosition():void
         {
         if (this._hero.body.position.y > PhysicsConfiguration.WORLD_HEIGHT + 30) {
         this._hero.body.position.setxy(PhysicsConfiguration.WORLD_WIDTH / 2, 100);
         var impulse:Vec2 = new Vec2(150, 1);
         impulse.mul(10);
         }
         }

         // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
         // PUBLIC PROPERTIES -------------------------------------------------------------------/
         // PUBLIC METHODS ----------------------------------------------------------------------/
         // PROTECTED PROPERTIES ----------------------------------------------------------------/
         // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
         // PROTECTED METHODS -------------------------------------------------------------------/
         // PRIVATE PROPERTIES ------------------------------------------------------------------/
         // PRIVATE METHODS ---------------------------------------------------------------------/
         // EVENT HANDLERS ----------------------------------------------------------------------/
         // ACCESSORS ---------------------------------------------------------------------------/
         */
    }
}
