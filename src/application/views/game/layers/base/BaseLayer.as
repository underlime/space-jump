package application.views.game.layers.base 
{
	import application.config.DataPhysics;
	import framework.view.View;
	
	/**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class BaseLayer extends View
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function BaseLayer() 
		{
			super();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		
		public function update():void 
		{
			
		}
		
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		
		private var _dataPhysics:DataPhysics = DataPhysics.instance;
		
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
		
		public function get dataPhysics():DataPhysics 
		{
			return _dataPhysics;
		}
	}

}