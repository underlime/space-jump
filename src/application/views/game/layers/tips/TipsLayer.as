/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.04.13
 * Time: 11:40
 */
package application.views.game.layers.tips
{
    import application.config.ApplicationConfiguration;
    import application.views.game.layers.tips.enum.LabelsEnum;
    import application.views.game.layers.tips.enum.StatusEnum;

    import framework.view.View;

    public class TipsLayer extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const EN_POSTFIX:String = "_EN";
        public static const RU_POSTFIX:String = "_RU";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TipsLayer()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupRelations();
            _setupPostfix();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addStatus(strStatus:String, x:Number = 0, y:Number = 0):void
        {
            var status:TipStatus = new TipStatus(strStatus);
            addChild(status);
            status.x = x - 25;
            status.y = y - 25;
        }

        public function showCoins(count:int = 1, x:Number = 0, y:Number = 0):void
        {
            var realCount:int = _checkCount(count);

            if (_moneyHash[realCount]) {
                addStatus(_moneyHash[realCount], x, y);
            }
        }

        public function reduceHealth(count:int = 1, x:Number = 0, y:Number = 0):void
        {
            var realCount:int = _checkCount(count);
            if (_healthHash[realCount]) {
                addStatus(_healthHash[realCount], x, y);
            }
        }

        public function addLabel(label:LabelsEnum):void
        {
            var _x:Number = 175;
            var _y:Number = 300;

            var statusData:String = label.toString() + _langPostfix;
            var tip:TipLabel = new TipLabel(statusData);
            addChild(tip);
            tip.x = _x;
            tip.y = _y;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _langPostfix:String;
        private var _moneyHash:Object;
        private var _healthHash:Object;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupRelations():void
        {
            _moneyHash = {
                1: StatusEnum.ONE_COIN.toString(),
                2: StatusEnum.TWO_COINS.toString(),
                3: StatusEnum.THREE_COINS.toString()
            };

            _healthHash = {
                1: StatusEnum.MINUS_ONE_HP.toString(),
                2: StatusEnum.MINUS_TWO_HP.toString(),
                3: StatusEnum.MINUS_THREE_HP.toString()
            };
        }

        private function _checkCount(count:int):int
        {
            if (count >= 1 && count <= 3)
                return count;
            else
                return 1;
        }

        private function _setupPostfix():void
        {
            _langPostfix = ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU ? RU_POSTFIX : EN_POSTFIX;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
