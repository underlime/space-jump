/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.04.13
 * Time: 15:56
 */
package application.views.game.layers.tips.enum
{
    import framework.enum.BaseEnum;

    public class LabelsEnum extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BONUS:LabelsEnum = new LabelsEnum("BONUS_LEVEL");
        public static const BOOSTER:LabelsEnum = new LabelsEnum("BOOSTER");
        public static const FAIL:LabelsEnum = new LabelsEnum("FAIL");
        public static const FAST_START:LabelsEnum = new LabelsEnum("FAST_START");
        public static const FORCE_FIELD:LabelsEnum = new LabelsEnum("FORCE_FIELD");
        public static const MEGA_START:LabelsEnum = new LabelsEnum("MEGA_START");
        public static const RESURRECTION:LabelsEnum = new LabelsEnum("RESSURECTION");
        public static const RESURRECTION_PLUS:LabelsEnum = new LabelsEnum("RESSURECTION_PLUS");
        public static const SUCCESS:LabelsEnum = new LabelsEnum("SUCCESS");
        public static const PARACHUTE:LabelsEnum = new LabelsEnum("PARACHUTE");
        public static const TROPHY:LabelsEnum = new LabelsEnum("TROPHY");
        public static const PAUSE:LabelsEnum = new LabelsEnum("PAUSE");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LabelsEnum(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.game.layers.tips.enum.LabelsEnum;

LabelsEnum.lockUp();