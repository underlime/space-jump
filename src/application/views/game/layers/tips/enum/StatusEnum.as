/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.04.13
 * Time: 12:09
 */
package application.views.game.layers.tips.enum
{
    import framework.enum.BaseEnum;

    public class StatusEnum extends BaseEnum
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        private static var _lockUp:Boolean = false;

        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const HP:StatusEnum = new StatusEnum("PLUS_1HP");
        public static const COLA:StatusEnum = new StatusEnum("PLUS_1JET");
        public static const SHIELD:StatusEnum = new StatusEnum("PLUS_1SHIELD");
        public static const ONE_COIN:StatusEnum = new StatusEnum("PLUS_1COINS");
        public static const TWO_COINS:StatusEnum = new StatusEnum("PLUS_2COINS");
        public static const THREE_COINS:StatusEnum = new StatusEnum("PLUS_3COINS");
        public static const MINUS_COINS:StatusEnum = new StatusEnum("MINUS_MONEY");
        public static const MINUS_ONE_HP:StatusEnum = new StatusEnum("MINUS_1HP");
        public static const MINUS_TWO_HP:StatusEnum = new StatusEnum("MINUS_2HP");
        public static const MINUS_THREE_HP:StatusEnum = new StatusEnum("MINUS_3HP");

        // CLASS METHODS -----------------------------------------------------------------------/

        public static function lockUp():void
        {
            _lockUp = true;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function StatusEnum(num:String)
        {
            super(num);
            if (_lockUp) {
                throw new Error("This enum vas already initialized");
            }
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.views.game.layers.tips.enum.StatusEnum;

StatusEnum.lockUp();