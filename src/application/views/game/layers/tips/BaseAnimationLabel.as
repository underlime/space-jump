/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.04.13
 * Time: 11:07
 */
package application.views.game.layers.tips
{
    import flash.display.Bitmap;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.view.View;

    public class BaseAnimationLabel extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseAnimationLabel(label:String)
        {
            _label = label;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupObjects();
            _addObjects();
        }

        override public function render():void
        {
            addChild(_animation);
            _animation.gotoAndStop(1);
            _animation.addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _animation.play();
        }

        override public function destroy():void
        {
            _animation = null;
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _contAlias:String = "animation_cont";
        protected var _animationAlias:String = "ANIM_LABEL";

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupObjects():void
        {
            _animation = super.source.getMovieClip(_animationAlias, false);
            _bitmap = super.source.getBitmap(_label);
        }

        protected function _addObjects():void
        {
            var cont:DisplayObjectContainer = _animation.getChildByName(_contAlias) as DisplayObjectContainer;
            if (cont) {
                cont.addChild(_bitmap);
            }
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _label:String;
        private var _animation:MovieClip;
        private var _bitmap:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _enterFrameHandler(event:Event):void
        {
            if (_animation.currentFrame >= _animation.totalFrames) {
                _animation.stop();
                _animation.removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
                this.destroy();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
