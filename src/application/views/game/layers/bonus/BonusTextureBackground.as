/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.04.13
 * Time: 12:17
 */
package application.views.game.layers.bonus
{
    public class BonusTextureBackground extends BonusBackground
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusTextureBackground(velocity:Number)
        {
            super(velocity);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _backgroundData = "BONUS_LAYER_BACK_MASK";
            _k_velocity = 3 / 7;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
