/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 18.04.13
 * Time: 12:01
 */
package application.views.game.layers.bonus
{
    import application.config.DataPhysics;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.view.View;

    public class BonusBackground extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusBackground(velocity:Number)
        {
            super();
            _velocity = velocity;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _speed = (_velocity * DataPhysics.TIME_STEP) * _k_velocity;
            _original = super.source.getBitmap(_backgroundData);
            _copy = super.source.getBitmap(_backgroundData);
            addChild(_original);
            addChild(_copy);
            _copy.y = DataPhysics.WORLD_HEIGHT;
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        override public function destroy():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _backgroundData:String = "BONUS_LAYER_BACK";
        protected var _k_velocity:Number = 1 / 10;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _original:Bitmap;
        private var _copy:Bitmap;
        private var _speed:Number;
        private var _velocity:Number = 500;
        private var _minY:Number = DataPhysics.WORLD_HEIGHT * (-1);

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _enterFrameHandler(event:Event):void
        {
            _original.y -= _speed;
            _copy.y -= _speed;
            if (_original.y <= 0)
                _copy.y = _original.y + _original.height;
            if (_copy.y <= 0)
                _original.y = _copy.y + _copy.height;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
