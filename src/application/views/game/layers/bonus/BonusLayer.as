/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 16.04.13
 * Time: 10:39
 */
package application.views.game.layers.bonus
{
    import application.models.GameModel;
    import application.models.ModelData;
    import application.views.particular.background.layers.ParallaxBackgroundLayer;
    import application.views.particular.background.layers.ParallaxLayer2;
    import application.views.particular.background.layers.ParallaxLayer3;
    import application.views.particular.background.layers.ParallaxLayer4;

    import flash.display.DisplayObject;

    import framework.data.ModelsRegistry;
    import framework.view.View;

    public class BonusLayer extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK:String = "BONUS_LAYER_BACK";
        public static const BACK_MASK:String = "BONUS_LAYER_BACK_MASK";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusLayer()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _model = ModelsRegistry.getModel(ModelData.GAME_MODEL) as GameModel;
        }

        override public function render():void
        {
            _setupBackground();
        }

        override public function destroy():void
        {
            _background.destroy();
            _backgroundTexture.destroy();
            _layer_1.destroy();
            _layer_2.destroy();
            _layer_3.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function run():void
        {
            _setupLayers();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _layer_1:ParallaxLayer2;
        private var _layer_2:ParallaxLayer3;
        private var _layer_3:ParallaxLayer4;
        private var _model:GameModel;
        private var _background:BonusBackground;
        private var _backgroundTexture:BonusTextureBackground;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            _background = new BonusBackground(_model.velocity);
            addChild(_background);

            _backgroundTexture = new BonusTextureBackground(_model.velocity);
            addChild(_backgroundTexture);
        }

        private function _setupLayers():void
        {
            _layer_1 = new ParallaxLayer2(_model.velocity);
            _layer_2 = new ParallaxLayer3(_model.velocity);
            _layer_3 = new ParallaxLayer4(_model.velocity);

            var arr:Array = [_layer_1, _layer_2, _layer_3];
            for (var i:int = 0; i < arr.length; i++) {
                (arr[i] as ParallaxBackgroundLayer).setBonus();
                (arr[i] as ParallaxBackgroundLayer).clear();
                addChild((arr[i] as ParallaxBackgroundLayer));
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get frontLayer():DisplayObject
        {
            return _layer_3;
        }
    }
}
