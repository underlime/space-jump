package application.views.game.layers.panel.speed
{
    import application.views.base.ApplicationView;
    import application.views.game.layers.panel.PanelData;

    import flash.display.Bitmap;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class SpeedBar extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SpeedBar()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _back = super.source.getBitmap(PanelData.SPEED_DISTANCE);
            _setupTextField();
            _postfix = super.textFactory.getLabel("speed_label");
        }

        override public function render():void
        {
            addChild(_back);
            addChild(_speedField);
            _updateText();
        }

        override public function destroy():void
        {
            removeChild(_back);
            _speedField.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _back:Bitmap;
        private var _speedField:SimpleTextField;
        private var _postfix:String;

        private var _speed:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateText():void
        {
            var tempSpeed:Number = Math.round(_speed);
            _speedField.text = NumberUtil.format(tempSpeed, " ", 3, "") + " " + _postfix;
        }

        private function _setupTextField():void
        {
            _speedField = new SimpleTextField();
            _speedField.width = 90;
            _speedField.height = 30;
            _speedField.size = 14;
            _speedField.y = 5;
            _speedField.color = 0xffffff;
            _speedField.bold = true;
            _speedField.systemFamily = "Consolas";
            _speedField.align = TextFormatAlign.CENTER;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set speed(value:Number):void
        {
            if (_speed != value) {
                _speed = value;
                _updateText();
            }
        }
    }

}