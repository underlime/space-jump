package application.views.game.layers.panel.bonus
{
    import application.views.base.ApplicationView;

    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BonusPanel extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusPanel()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _bonusBar = new BonusBar();
            _bonusBar.currentBonus = 0;

            _setupText();
        }

        override public function render():void
        {
            addChild(_bonusBar);
            _bonusBar.y = 30;

            addChild(_header);
            _header.text = super.textFactory.getLabel("bonus_label");
            _header.x = 53;
        }

        override public function destroy():void
        {
            _bonusBar.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bonusBar:BonusBar;
        private var _header:SimpleTextField;
        private var _bonus:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupText():void
        {
            _header = new SimpleTextField();
            _header.size = 24;
            _header.width = 106;
            _header.height = 31;
            _header.align = TextFormatAlign.CENTER;
            _header.color = 0xB8FCFF;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get bonus():Number
        {
            return _bonus;
        }

        public function set bonus(value:Number):void
        {
            if (_bonus != value) {
                _bonus = value;
                _bonusBar.currentBonus = _bonus;
            }
        }
    }

}