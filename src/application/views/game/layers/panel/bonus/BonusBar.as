package application.views.game.layers.panel.bonus
{
    import application.views.game.layers.panel.PanelData;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BonusBar extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusBar()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            super.disableInteractive();
            this._bonusBar = this.source.getBitmap(PanelData.BONUS_BAR);
            this._rectangle = new Rectangle(0, 0, this._bonusBarWidth, this._bonusBarHeight);
        }

        override public function render():void
        {
            this.addChild(this._bonusBar);
            this._bonusBar.scrollRect = this._rectangle;
        }

        override public function destroy():void
        {
            this.removeChild(this._bonusBar);
            this._bonusBar.scrollRect = null;
            this._rectangle = null;
            this._bonusBar = null;
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _bonusBar:Bitmap;
        private var _rectangle:Rectangle;

        private var _bonusBarWidth:Number = 202;
        private var _bonusBarHeight:Number = 14;

        private var _maxBonus:int = 21;
        private var _currentBonus:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateBar():void
        {
            this._rectangle.y = this._currentBonus * this._bonusBarHeight;
            this._bonusBar.scrollRect = this._rectangle;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get currentBonus():int
        {
            return this._currentBonus;
        }

        public function set currentBonus(value:int):void
        {
            if (this._currentBonus != value && value <= this._maxBonus - 1) {
                this._currentBonus = value;
                this._updateBar();
            }
        }
    }

}