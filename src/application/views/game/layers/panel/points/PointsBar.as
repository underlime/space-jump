package application.views.game.layers.panel.points
{
    import application.views.base.ApplicationView;

    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class PointsBar extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PointsBar()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupTextField();
            _prefix = super.textFactory.getLabel("points_label") + ":";
        }

        override public function render():void
        {
            addChild(_pointsField);
            _pointsField.text = _prefix;
            addChild(_pointsValueField);
            _updateText();
        }

        override public function destroy():void
        {
            _pointsField.destroy();
            _pointsValueField.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _prefix:String;
        private var _pointsField:SimpleTextField;
        private var _pointsValueField:SimpleTextField;

        private var _points:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateText():void
        {
            _pointsValueField.x = ( -1) * _pointsValueField.width - 5;
            _pointsField.x = _pointsValueField.x - 50;
        }

        private function _setupTextField():void
        {
            _pointsField = new SimpleTextField();
            _pointsField.size = 12;
            _pointsField.color = 0x21d2ef;
            _pointsField.width = 50;
            _pointsField.y = 8;
            _pointsField.align = TextFormatAlign.RIGHT;

            _pointsValueField = new SimpleTextField();
            _pointsValueField.systemFamily = "Consolas";
            _pointsValueField.size = 26;
            _pointsValueField.bold = true;

            _pointsValueField.color = 0xb8fcff;
            _pointsValueField.autoSize = TextFieldAutoSize.LEFT;
            _pointsValueField.align = TextFormatAlign.LEFT;
            _pointsValueField.width = 100;
            _pointsValueField.height = 28;
            _pointsValueField.y = -5;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set points(value:Number):void
        {
            if (_points != value) {
                _points = value;
                _pointsValueField.text = _points.toString();
                _updateText();
            }
        }
    }

}