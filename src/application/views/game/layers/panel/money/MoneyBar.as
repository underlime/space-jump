package application.views.game.layers.panel.money
{
    import application.views.base.ApplicationView;

    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MoneyBar extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MoneyBar()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupTextField();
            _prefix = super.textFactory.getLabel("money_label") + ":";
        }

        override public function render():void
        {
            addChild(_moneyField);
            _moneyField.text = _prefix;
            addChild(_moneyValueField);
        }

        override public function destroy():void
        {
            _moneyField.destroy();
            _moneyValueField.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _prefix:String = "";
        private var _moneyField:SimpleTextField;
        private var _moneyValueField:SimpleTextField;
        private var _money:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateText():void
        {
            _moneyValueField.x = ( -1) * _moneyValueField.width - 5;
            _moneyField.x = _moneyValueField.x - 50;
        }

        private function _setupTextField():void
        {
            _moneyField = new SimpleTextField();
            _moneyField.color = 0xe8c601;
            _moneyField.size = 12;
            _moneyField.width = 50;
            _moneyField.y = 10;
            _moneyField.align = TextFormatAlign.RIGHT;

            _moneyValueField = new SimpleTextField();
            _moneyValueField.color = 0xfffabb;
            _moneyValueField.size = 26;
            _moneyValueField.systemFamily = "Consolas";
            _moneyValueField.bold = true;
            _moneyValueField.autoSize = TextFieldAutoSize.LEFT;
            _moneyValueField.align = TextFormatAlign.LEFT;
            _moneyValueField.width = 0;
            _moneyValueField.y = -3;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set money(value:Number):void
        {
            if (_money != value) {
                _money = value;
                _moneyValueField.text = _money.toString();
                _updateText();
            }
        }
    }

}