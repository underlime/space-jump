package application.views.game.layers.panel
{

    import application.config.DataPhysics;
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.models.Inventory;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;
    import application.views.game.layers.panel.bonus.BonusPanel;
    import application.views.game.layers.panel.buttons.ButtonLayer;
    import application.views.game.layers.panel.distance.DistanceBar;
    import application.views.game.layers.panel.lifebar.LifeBar;
    import application.views.game.layers.panel.money.MoneyBar;
    import application.views.game.layers.panel.points.PointsBar;
    import application.views.game.layers.panel.shieldbar.ArmorBar;
    import application.views.game.layers.panel.speed.SpeedBar;
    import application.views.game.layers.panel.time.TimeBar;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.data.ModelsRegistry;
    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class GamePanelLayer extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GamePanelLayer()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _model = ModelsRegistry.getModel(ModelData.GAME_MODEL) as GameModel;
            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY_MODEL) as Inventory;
            _statisticsModel = _model.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;

            _setupBackground();
            _setupLifeBar();
            _setupShieldBar();
            _setupBonusPanel();
            _setupSpeedBar();
            _setupDistanceBar();
            _setupTimeBar();
        }

        override public function render():void
        {
            _setupPointsBar();
            _setupMoneyBar();
            _setupButtonsLayer();
            _statisticsModel.addEventListener(Event.CHANGE, _updatePanel);
            _model.addEventListener(ApplicationEvent.VELOCITY_CHANGE, _updateVelocity);
        }

        override public function destroy():void
        {
            _statisticsModel.removeEventListener(Event.CHANGE, _updatePanel);
            _model.removeEventListener(ApplicationEvent.VELOCITY_CHANGE, _updateVelocity);

            _shieldBar.destroy();
            _lifeBar.destroy();
            _bonusPanel.destroy();
            _pointsBar.destroy();
            _moneyBar.destroy();
            _speedBar.destroy();
            _distanceBar.destroy();
            _timeBar.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _lifeBar:LifeBar;
        private var _shieldBar:ArmorBar;
        private var _bonusPanel:BonusPanel;
        private var _pointsBar:PointsBar;
        private var _moneyBar:MoneyBar;
        private var _speedBar:SpeedBar;
        private var _distanceBar:DistanceBar;
        private var _timeBar:TimeBar;
        private var _buttonsLayer:ButtonLayer;

        private var _model:GameModel;
        private var _inventory:Inventory;
        private var _statisticsModel:StatisticsGameModel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupTimeBar():void
        {
            _timeBar = new TimeBar();
            addChild(_timeBar);
            _timeBar.x = 350;
            _timeBar.y = 60;
        }

        private function _setupDistanceBar():void
        {
            _distanceBar = new DistanceBar();
            _distanceBar.y = 60;
            _distanceBar.x = 460;
            this.addChild(_distanceBar);
        }

        private function _setupSpeedBar():void
        {
            _speedBar = new SpeedBar();
            _speedBar.y = 60;
            _speedBar.x = 250;
            addChild(_speedBar);
            _speedBar.speed = _model.velocity;
        }

        private function _setupMoneyBar():void
        {
            _moneyBar = new MoneyBar();
            addChild(_moneyBar);
            _moneyBar.x = 800;
            _moneyBar.y = 20;
            _moneyBar.money = 50;
        }

        private function _setupPointsBar():void
        {
            _pointsBar = new PointsBar();
            addChild(_pointsBar);
            _pointsBar.x = 800;
            _pointsBar.points = 2;
        }

        private function _setupBonusPanel():void
        {
            _bonusPanel = new BonusPanel();
            addChild(_bonusPanel);
            _bonusPanel.x = 299;
        }

        private function _setupShieldBar():void
        {
            _shieldBar = new ArmorBar();
            addChild(_shieldBar);
            _shieldBar.x = 5;
            _shieldBar.y = 25;
        }

        private function _setupLifeBar():void
        {
            var inventory:Inventory = ModelsRegistry.getModel(ModelData.INVENTORY_MODEL) as Inventory;
            var data:Object = inventory.getItemsGroupByAction()[Items.ACTION_HEALTH];
            var count:Number = Number(data.count) * 2;
            var maxHealth:int = DataPhysics.instance.user_default_health.value + count;


            _statisticsModel.maxHealth = maxHealth;
            _statisticsModel.setupDefaultHealth(maxHealth);

            _lifeBar = new LifeBar();
            _lifeBar.maxActiveCount = maxHealth;
            _lifeBar.activateCount = maxHealth;

            addChild(_lifeBar);
            _lifeBar.x = 5;
            _lifeBar.y = 5;
        }

        private function _setupBackground():void
        {
            var backStrip:Bitmap = super.source.getBitmap(PanelData.BACK_STRIP);
            var backTop:Bitmap = super.source.getBitmap(PanelData.BACK_TOP);
            addChild(backStrip);
            addChild(backTop);
        }

        private function _setupButtonsLayer():void
        {
            _buttonsLayer = new ButtonLayer();
            addChild(_buttonsLayer);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _updateVelocity(e:ApplicationEvent):void
        {
            _speedBar.speed = _model.velocity;
        }

        private function _updatePanel(event:Event):void
        {
            _bonusPanel.bonus = _statisticsModel.bonus;
            _pointsBar.points = _statisticsModel.points;
            _moneyBar.money = _statisticsModel.coins;
            _distanceBar.distance = _statisticsModel.height;
            _timeBar.seconds = _statisticsModel.seconds;
            _lifeBar.activateCount = _statisticsModel.health;
            _shieldBar.activateCount = _statisticsModel.armor;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get buttonsLayer():ButtonLayer
        {
            return _buttonsLayer;
        }

        public function get lifeBar():LifeBar
        {
            return _lifeBar;
        }
    }

}