/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.02.13
 * Time: 16:10
 */
package application.views.game.layers.panel.buttons
{
    public class ButtonsData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SCREENSHOT_BUTTON:String = "SPACE_PRINTSCREEN_BUTTON";
        public static const RESURRECTION_BUTTON:String = "SPACE_RESSURECTION_BUTTON";
        public static const PARACHUTE_BUTTON:String = "SPACE_PARASHUT_BUTTON";
        public static const PAUSE_BUTTON:String = "PAUSE_EXIT_BUTTON";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
