/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.02.13
 * Time: 16:03
 */
package application.views.game.layers.panel.buttons
{
    import application.views.screen.main.panel.start.VerticalButton;

    import flash.text.TextFieldAutoSize;

    import framework.view.text.SimpleTextField;

    public class BasePowerUpButton extends VerticalButton
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BasePowerUpButton()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _setupButton();
            _buttonWidth = 50;
            _buttonHeight = 50;
            _lanMargin = 0;

            super.setup();
        }


        override public function render():void
        {
            super.render();
            _setupText();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _setupButton():void
        {
            throw new Error("Method must be overriden");
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _txtCount:SimpleTextField;
        private var _count:int = 1;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupText():void
        {
            _txtCount = new SimpleTextField();
            _txtCount.autoSize = TextFieldAutoSize.RIGHT;
            _txtCount.size = 10;
            _txtCount.color = 0xffffff;
            _txtCount.x = 43;
            _txtCount.y = 35;
            addChild(_txtCount);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set count(value:int):void
        {
            _count = value;
            if (_txtCount)
                _txtCount.text = _count.toString();
        }
    }
}
