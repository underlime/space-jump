/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.02.13
 * Time: 16:56
 */
package application.views.game.layers.panel.buttons
{
    import application.config.HotKeyConfig;
    import application.events.ApplicationEvent;
    import application.views.game.layers.panel.text.HotKeyButtonLabel;

    import flash.events.MouseEvent;

    import framework.socnet.OdnoklassnikiRu;
    import framework.socnet.SocNet;
    import framework.view.View;

    import org.casalib.events.KeyComboEvent;
    import org.casalib.ui.Key;
    import org.casalib.ui.KeyCombo;

    public class ButtonLayer extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ButtonLayer()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            var socNet:SocNet = SocNet.instance();
            if (socNet.socNetName == SocNet.ODNOKLASSNIKI_RU) {
                _permission = (socNet as OdnoklassnikiRu).getPhotosPermission();
            }
        }

        override public function render():void
        {
            _setupPauseButton();

            if (_permission)
                _setupScreenShotButton();

            _setupResurrectionButton();
            _setupParachuteButton();

            _addDebugCombo();

            _key.addEventListener(KeyComboEvent.DOWN, _comboDownHandler);
        }

        override public function destroy():void
        {
            _pauseButton.removeEventListener(MouseEvent.CLICK, _pauseClickHandler);
            _key.removeEventListener(KeyComboEvent.DOWN, _comboDownHandler);

            _parachuteButton.destroy();
            _parachuteButton.destroy();
            _resurrectionButton.destroy();

            if (_screenShotButton && !_screenShotButton.destroyed)
                _screenShotButton.destroy();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _screenShotButton:ScreenShotButton;
        private var _resurrectionButton:ResurrectionButton;
        private var _parachuteButton:ParachuteButton;
        private var _pauseButton:PauseButton;

        private var _key:Key = Key.getInstance();
        private var _screenCombo:KeyCombo;
        private var _resurrectionCombo:KeyCombo;
        private var _parachuteCombo:KeyCombo;
        private var _pauseCombo:KeyCombo = HotKeyConfig.PAUSE_HOT_KEY;

        private var _screenLabel:HotKeyButtonLabel;
        private var _resurrectionLabel:HotKeyButtonLabel;
        private var _parachuteLabel:HotKeyButtonLabel;

        private var _debugCombo:KeyCombo;
        private var _permission:Boolean = false;

        private var _pauseLabel:HotKeyButtonLabel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupScreenShotButton():void
        {
            _screenShotButton = new ScreenShotButton();
            _screenShotButton.x = 70;
            _screenShotButton.y = 633;
            addChild(_screenShotButton);

            _setupScreenShotLabel();
            _setupScreenShotHotKey();
        }

        private function _setupPauseButton():void
        {
            _pauseButton = new PauseButton();
            _pauseButton.x = 10;
            _pauseButton.y = 633;
            addChild(_pauseButton);

            _pauseButton.addEventListener(MouseEvent.CLICK, _pauseClickHandler);

            _addPauseLabel();
            _setupPauseHotKey();
        }

        private function _addPauseLabel():void
        {
            _pauseLabel = new HotKeyButtonLabel();
            _pauseLabel.x = 0;
            _pauseLabel.y = 683;
            addChild(_pauseLabel);
            _pauseLabel.text = "SHIFT + Z";
        }

        private function _setupPauseHotKey():void
        {
            _pauseCombo = HotKeyConfig.PAUSE_HOT_KEY;
            _key.addKeyCombo(_pauseCombo);
        }

        private function _setupScreenShotHotKey():void
        {
            _screenShotButton.addEventListener(MouseEvent.CLICK, _screenShotButtonClickHandler);
            _screenCombo = HotKeyConfig.SCREEN_SHOT_HOT_KEY;

            _key.addKeyCombo(_screenCombo);
        }

        private function _setupScreenShotLabel():void
        {
            _screenLabel = new HotKeyButtonLabel();
            addChild(_screenLabel);
            _screenLabel.text = "SHIFT + S";
            _screenLabel.x = 60;
            _screenLabel.y = 683;
        }


        private function _setupResurrectionButton():void
        {
            _resurrectionButton = new ResurrectionButton();
            _resurrectionButton.x = 680;
            _resurrectionButton.y = 633;
            addChild(_resurrectionButton);

            _setupResurrectionLabel();
            _setupResurrectionHotKey();
        }

        private function _setupResurrectionHotKey():void
        {
            _resurrectionButton.addEventListener(MouseEvent.CLICK, _resurrectionButtonClickHandler);
            _resurrectionCombo = HotKeyConfig.RESURRECTION_HOT_KEY;

            _key.addKeyCombo(_resurrectionCombo);
        }

        private function _setupResurrectionLabel():void
        {
            _resurrectionLabel = new HotKeyButtonLabel();
            addChild(_resurrectionLabel);
            _resurrectionLabel.text = "SHIFT + R";
            _resurrectionLabel.x = 670;
            _resurrectionLabel.y = 683;
        }

        private function _setupParachuteButton():void
        {
            _parachuteButton = new ParachuteButton();
            _parachuteButton.x = 740;
            _parachuteButton.y = 633;
            addChild(_parachuteButton);

            _setupParachuteLabel();
            _setupParachuteHotKey();
        }

        private function _setupParachuteHotKey():void
        {
            _parachuteButton.addEventListener(MouseEvent.CLICK, _parachuteClickHandler);
            _parachuteCombo = HotKeyConfig.PARACHUTE_HOT_KEY;

            _key.addKeyCombo(_parachuteCombo);
        }

        private function _setupParachuteLabel():void
        {
            _parachuteLabel = new HotKeyButtonLabel();
            addChild(_parachuteLabel);
            _parachuteLabel.text = "SHIFT + P";
            _parachuteLabel.x = 730;
            _parachuteLabel.y = 683;
        }

        private function _callScreenShot():void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.SCREEN_SHOT, true);
            super.dispatchEvent(event);
        }

        private function _callResurrection():void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_RESURRECTION, true);
            super.dispatchEvent(event);
        }

        private function _callParachute():void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_PARACHUTE, true);
            super.dispatchEvent(event);
        }

        private function _addDebugCombo():void
        {
            _debugCombo = HotKeyConfig.TIMER_DEBUG_HOT_KEY;
            _key.addKeyCombo(_debugCombo);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _comboDownHandler(event:KeyComboEvent):void
        {
            if (_screenCombo && _screenCombo.equals(event.keyCombo)) {
                _callScreenShot();
            }

            if (_resurrectionCombo.equals(event.keyCombo)) {
                _callResurrection();
            }

            if (_parachuteCombo.equals(event.keyCombo)) {
                _callParachute();
            }

            if (_debugCombo.equals(event.keyCombo)) {
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_DEBUG_TIMER, true));
            }

            if (_pauseCombo.equals(event.keyCombo)) {
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_PAUSE, true));
            }
        }

        private function _screenShotButtonClickHandler(e:MouseEvent):void
        {
            _callScreenShot();
        }

        private function _pauseClickHandler(e:MouseEvent):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_PAUSE, true);
            super.dispatchEvent(event);
        }

        private function _resurrectionButtonClickHandler(event:MouseEvent):void
        {
            _callResurrection();
        }

        private function _parachuteClickHandler(event:MouseEvent):void
        {
            _callParachute();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get screenShotButton():ScreenShotButton
        {
            return _screenShotButton;
        }

        public function get resurrectionButton():ResurrectionButton
        {
            return _resurrectionButton;
        }

        public function get parachuteButton():ParachuteButton
        {
            return _parachuteButton;
        }
    }
}
