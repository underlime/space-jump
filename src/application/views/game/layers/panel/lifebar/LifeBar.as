package application.views.game.layers.panel.lifebar
{
    import application.views.game.layers.panel.base.BaseBar;
    import application.views.game.layers.panel.base.PowerUpIndicator;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class LifeBar extends BaseBar
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LifeBar()
        {
            var hearts:Vector.<PowerUpIndicator> = new Vector.<PowerUpIndicator>();
            for (var i:int = 0; i < 10; i++) {
                hearts.push(new BarHeart());
            }
            super(hearts);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}