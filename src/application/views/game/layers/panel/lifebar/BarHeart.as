package application.views.game.layers.panel.lifebar
{
    import application.views.game.layers.panel.PanelData;
    import application.views.game.layers.panel.base.PowerUpIndicator;

    import flash.geom.Rectangle;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BarHeart extends PowerUpIndicator
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BarHeart()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this.indicatorWidth = 20;
            this.indicatorHeight = 18;
            this._rectangle = new Rectangle(0, 0, this.indicatorWidth, this.indicatorHeight);
            this._indicator = this.source.getBitmap(PanelData.LIFE_BAR_Heart);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}