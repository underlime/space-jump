package application.views.game.layers.panel.shieldbar
{
    import application.views.game.layers.panel.PanelData;
    import application.views.game.layers.panel.base.PowerUpIndicator;

    import flash.geom.Rectangle;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BarShield extends PowerUpIndicator
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BarShield()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this.indicatorWidth = 20;
            this.indicatorHeight = 18;
            this._rectangle = new Rectangle(0, 0, this.indicatorWidth, this.indicatorHeight);
            this._indicator = this.source.getBitmap(PanelData.BAR_SHIELD);
        }

        override public function disable():void
        {

        }

        override public function makeEmpty():void
        {
            this._rectangle.y = 0;
            this._indicator.scrollRect = this._rectangle;
        }

        override public function makeHalfFull():void
        {
            this._rectangle.y = this.indicatorHeight;
            this._indicator.scrollRect = this._rectangle;
        }

        override public function makeFull():void
        {
            this._rectangle.y = this.indicatorHeight * 2;
            this._indicator.scrollRect = this._rectangle;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}