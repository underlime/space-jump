package application.views.game.layers.panel.shieldbar
{
    import application.views.game.layers.panel.base.BaseBar;
    import application.views.game.layers.panel.base.PowerUpIndicator;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ArmorBar extends BaseBar
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ArmorBar()
        {
            var shields:Vector.<PowerUpIndicator> = new Vector.<PowerUpIndicator>();
            for (var i:int = 0; i < 5; i++) {
                shields.push(new BarShield());
            }
            super(shields);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}