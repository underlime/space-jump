package application.views.game.layers.panel.time
{
    import application.views.base.ApplicationView;
    import application.views.game.layers.panel.PanelData;

    import flash.display.Bitmap;
    import flash.text.TextFormatAlign;

    import framework.helpers.MathHelper;
    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class TimeBar extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TimeBar()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _back = super.source.getBitmap(PanelData.TIMER_FIELD);
            _setupTextField();
        }

        override public function render():void
        {
            addChild(_back);
            addChild(_timeField);
            _updateText();
        }

        override public function destroy():void
        {
            removeChild(_back);
            _timeField.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _seconds:int = 0;
        private var _timeField:SimpleTextField;
        private var _back:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _formatSeconds():String
        {
            var timeObject:Object = MathHelper.splitSeconds(_seconds);
            return NumberUtil.format(timeObject.hours, "", 2) + ":" + NumberUtil.format(timeObject.minutes, "", 2) + ":" + NumberUtil.format(timeObject.seconds, "", 2);
        }

        private function _updateText():void
        {
            var value:String = _formatSeconds();
            _timeField.text = value;
        }

        private function _setupTextField():void
        {
            _timeField = new SimpleTextField();
            _timeField.systemFamily = "Consolas";
            _timeField.width = 100;
            _timeField.height = 30;
            _timeField.size = 20;
            _timeField.y = 3;
            _timeField.color = 0xffffff;
            _timeField.bold = true;
            _timeField.align = TextFormatAlign.CENTER;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set seconds(value:int):void
        {
            if (_seconds != value) {
                _seconds = value;
                _updateText();
            }
        }
    }

}