package application.views.game.layers.panel
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class PanelData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BACK_STRIP:String = "SPACE_HUD_BG_TOP_STRIPES";
        public static const BACK_TOP:String = "SPACE_HUD_BG_TOP";

        public static const LIFE_BAR_Heart:String = "SPACE_LIFEBAR_Heart";
        public static const BAR_SHIELD:String = "SPACE_BAR_Shield";

        public static const BONUS_BAR:String = "SPACE_BonusBar";

        public static const SPEED_DISTANCE:String = "SPACE_SPEED_DISTANCE_FIELD";
        public static const TIMER_FIELD:String = "SPACE_TIMER_FIELD";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}