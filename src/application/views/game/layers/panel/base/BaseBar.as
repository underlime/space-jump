package application.views.game.layers.panel.base
{
    import framework.view.View;

    /**
     * На активацию одного элемента нужно 2 единицы activateCount
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BaseBar extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseBar(items:Vector.<PowerUpIndicator>)
        {
            _items = items;
            _totalCount = _items.length * 2;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            super.disableInteractive();
        }

        override public function render():void
        {
            var xPosition:Number = 0;
            for (var i:int = 0; i < _items.length; i++) {
                _items[i].x = xPosition;
                this.addChild(_items[i]);
                xPosition += _items[i].width;
            }
            _updateActive();
        }

        override public function destroy():void
        {
            for (var i:int = 0; i < _items.length; i++) {
                _items[i].destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _items:Vector.<PowerUpIndicator>;
        private var _activateCount:int = 0;
        private var _totalCount:Number = 0;
        private var _maxActiveCount:Number = 10;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateActive():void
        {
            var ac:Number = Math.floor(_activateCount / 2);
            var acMiddle:Number = _activateCount - (ac * 2);
            _makeEmpty();
            _makeDisable();

            for (var i:int = 0; i < ac; i++) {
                _items[i].makeFull();
            }
            var delta:Number = i;

            for (i = 0; i < acMiddle; i++) {
                _items[i + delta].makeHalfFull();
            }
        }

        private function _makeDisable():void
        {
            var start:int = Math.ceil(_maxActiveCount / 2);
            for (var i:int = start; i < _items.length; i++) {
                _items[i].disable();
            }
        }

        private function _makeEmpty():void
        {
            for (var i:int = 0; i < _items.length; i++) {
                _items[i].makeEmpty();
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get activateCount():int
        {
            return _activateCount;
        }

        public function set activateCount(value:int):void
        {
            if (_activateCount != value && (_maxActiveCount * 2 >= value)) {
                if (value < 0) {
                    value = 0;
                }
                _activateCount = value;
                _updateActive();
            }
        }

        public function set maxActiveCount(value:Number):void
        {
            _maxActiveCount = value;
        }
    }

}