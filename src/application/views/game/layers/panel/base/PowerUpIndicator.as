package application.views.game.layers.panel.base
{
    import application.views.game.layers.panel.PanelData;

    import flash.display.Bitmap;
    import flash.geom.Rectangle;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class PowerUpIndicator extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PowerUpIndicator()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _rectangle = new Rectangle(0, 0, this.indicatorWidth, this.indicatorHeight);
            _indicator = this.source.getBitmap(PanelData.LIFE_BAR_Heart);
        }

        override public function render():void
        {
            this.addChild(_indicator);
            _indicator.scrollRect = _rectangle;
        }

        override public function destroy():void
        {
            this.removeChild(_indicator);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var indicatorWidth:Number = 20;
        public var indicatorHeight:Number = 18;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function disable():void
        {
            _rectangle.y = 0;
            _indicator.scrollRect = _rectangle;
        }

        public function makeEmpty():void
        {
            _rectangle.y = this.indicatorHeight;
            _indicator.scrollRect = _rectangle;
        }

        public function makeHalfFull():void
        {
            _rectangle.y = this.indicatorHeight * 2;
            _indicator.scrollRect = _rectangle;
        }

        public function makeFull():void
        {
            _rectangle.y = this.indicatorHeight * 3;
            _indicator.scrollRect = _rectangle;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        protected var _indicator:Bitmap;
        protected var _rectangle:Rectangle;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}