package application.views.game.layers.panel.distance
{
    import application.views.base.ApplicationView;
    import application.views.game.layers.panel.PanelData;

    import flash.display.Bitmap;
    import flash.text.TextFormatAlign;

    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class DistanceBar extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DistanceBar()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _back = super.source.getBitmap(PanelData.SPEED_DISTANCE);
            _setupTextField();
            _postfix = super.textFactory.getLabel("distance_label");
        }

        override public function render():void
        {
            addChild(_back);
            addChild(_distanceField);
        }

        override public function destroy():void
        {
            removeChild(_back);
            _distanceField.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _back:Bitmap;
        private var _distanceField:SimpleTextField;
        private var _postfix:String;

        private var _distance:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateText():void
        {
            _distanceField.text = NumberUtil.format(_distance, " ", 3, "") + " " + _postfix;
        }

        private function _setupTextField():void
        {
            _distanceField = new SimpleTextField();
            _distanceField.width = 90;
            _distanceField.height = 30;
            _distanceField.size = 14;
            _distanceField.y = 5;
            _distanceField.color = 0xffffff;
            _distanceField.bold = true;
            _distanceField.systemFamily = "Consolas";
            _distanceField.align = TextFormatAlign.CENTER;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function set distance(value:Number):void
        {
            if (_distance != value) {
                _distance = value;
                _updateText();
            }
        }

    }

}