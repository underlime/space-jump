package application.views.game.layers.panel.buffs
{
    public class BuffType
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const IMMORTAL_BUFF:String = "ImmortalBuff";
        public static const MEGA_ROCKET_BUFF:String = "LongDistanceBuff";
        public static const PARACHUTE_BUFF:String = "ParachuteBuff";
        public static const RESURRECTION_BUFF:String = "RessurectionBuff";
        public static const BOOSTER_BUFF:String = "RocketBuff";
        public static const ROCKET_BUFF:String = "ShortDistanceBuff";
        public static const RESURRECTION_PLUS_BUFF:String = "ResurrectionPlusBuff";
        public static const NONE:String = "NoneBuff";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
