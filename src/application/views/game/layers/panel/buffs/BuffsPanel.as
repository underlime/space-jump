package application.views.game.layers.panel.buffs
{
    import com.greensock.TweenLite;

    import flash.events.Event;

    import framework.view.View;

    import org.casalib.util.ArrayUtil;

    public class BuffsPanel extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BuffsPanel()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
        }

        override public function destroy():void
        {
            for (var i:int = 0; i < _buffs.length; i++) {
                if (_buffs[i] && !_buffs[i].destroyed)
                    _buffs[i].destroy();
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function addBuff(buff:BaseBuff):BuffsPanel
        {
            var _identIndex:int = _getIdentIndex(buff);

            if (_identIndex >= 0)
                _deleteBuffByIndex(_identIndex);

            _buffs.push(buff);
            _updateStaticBuffsPosition();

            addChild(buff);
            buff.addEventListener(Event.COMPLETE, _buffCompleteHandler);

            return this;
        }

        public function deleteBuff(buff:BaseBuff):void
        {
            _updateBuffsList(buff);
            if (buff && !buff.destroyed){
                buff.removeEventListener(Event.COMPLETE, _buffCompleteHandler);
                buff.destroy();
                buff = null;
            }
            _updateBuffsPosition();
        }

        /**
         * Удаляет все бафы с панели
         * @param arrException исключения (BuffType)
         */
        public function deleteBuffs(arrException:Array = null):void
        {
            for (var i:int = 0; i < _buffs.length; i++) {
                if (_buffs[i] && !_buffs[i].destroyed)
                {
                    if (!arrException || (arrException && ArrayUtil.contains(arrException, _buffs[i].type) == 0))
                    {
                        _buffs[i].destroy();
                    }
                }
            }

        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _buffHeight:Number = 30;
        private var _buffMargin:Number = 10;
        private var _buffs:Vector.<BaseBuff> = new Vector.<BaseBuff>();

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateBuffsList(buff:BaseBuff):void
        {
            var nullIndex:int = _buffs.indexOf(buff);
            _buffs.splice(nullIndex, 1);
        }

        private function _updateStaticBuffsPosition():void
        {
            for (var i:int = 0; i < _buffs.length; i++) {
                _buffs[i].y = i * _buffHeight + _buffMargin;
            }
        }

        private function _updateBuffsPosition():void
        {
            for (var i:int = 0; i < _buffs.length; i++) {
                _moveBuffIcon(_buffs[i], i * _buffHeight + _buffMargin);
            }
        }

        private function _getIdentIndex(buff:BaseBuff):int
        {
            for (var i:int = 0; i < _buffs.length; i++) {
                if (buff.type == _buffs[i].type)
                    return i;
            }
            return (-1);
        }

        private function _deleteBuffByIndex(index:int):void
        {
            var buff:BaseBuff = _buffs[index];
            deleteBuff(buff);
        }

        private function _moveBuffIcon(buff:BaseBuff, targetY:Number):void
        {
            TweenLite.to(buff, 0.2, {
                y: targetY
            });
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _buffCompleteHandler(event:Event):void
        {
            var buff:BaseBuff = event.currentTarget as BaseBuff;
            var params:Object = {
                onComplete: _onTweenComplete,
                onCompleteParams: [buff],
                alpha: 0
            };
            TweenLite.to(buff, .3, params);
        }

        private function _onTweenComplete(buff:BaseBuff):void
        {
            deleteBuff(buff);
            TweenLite.killTweensOf(buff);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
