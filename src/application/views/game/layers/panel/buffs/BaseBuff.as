package application.views.game.layers.panel.buffs
{
    import application.views.base.ApplicationView;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.geom.Rectangle;
    import flash.text.TextFormatAlign;
    import flash.utils.Timer;

    import framework.view.text.SimpleTextField;

    public class BaseBuff extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const BUFF_DATA:String = "SPACE_BUFFS";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseBuff(seconds:int, rectangleY:Number)
        {
            _seconds = seconds;
            _rectangleY = rectangleY;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _currentSec = _seconds;
            _setupIcon();

            if (_seconds > 0) {
                _setupText();
                _setupTimer();
            }
        }

        override public function destroy():void
        {
            removeChild(_icon);
            _icon = null;
            if (_timer)
                _destroyTimer();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var type:String;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _icon:Bitmap;
        protected var _rectangle:Rectangle;
        protected var _txtTime:SimpleTextField;


        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _seconds:int;
        private var _timer:Timer;
        private var _currentSec:int;
        private var _rectangleY:Number;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupIcon():void
        {
            _icon = super.source.getBitmap(BaseBuff.BUFF_DATA);
            _rectangle = new Rectangle(0, _rectangleY, 81, 30);
            _icon.scrollRect = _rectangle;

            addChild(_icon);
        }

        private function _setupText():void
        {
            _txtTime = new SimpleTextField();
            _txtTime.color = 0xffffff;
            _txtTime.size = 10;
            _txtTime.width = 43;
            _txtTime.align = TextFormatAlign.RIGHT;
            _txtTime.y = 8;

            addChild(_txtTime);

            _updateData();
        }

        private function _setupTimer():void
        {
            _timer = new Timer(1000, _seconds);
            _timer.addEventListener(TimerEvent.TIMER, _timerEventHandler);
            _timer.addEventListener(TimerEvent.TIMER_COMPLETE, _timerCompleteHandler);
            _timer.start();
        }

        private function _destroyTimer():void
        {
            _timer.removeEventListener(TimerEvent.TIMER_COMPLETE, _timerCompleteHandler);
            _timer.removeEventListener(TimerEvent.TIMER, _timerEventHandler);
            _timer.stop();
            _timer = null;
        }

        private function _updateData():void
        {
            _txtTime.text = _currentSec.toString() + " " + super.textFactory.getLabel('additional_seconds_label');
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _timerEventHandler(event:TimerEvent):void
        {
            _currentSec--;
            _updateData();
        }

        private function _timerCompleteHandler(event:TimerEvent):void
        {
            _destroyTimer();
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
