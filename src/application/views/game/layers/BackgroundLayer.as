package application.views.game.layers
{
    import application.config.GameMode;
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;
    import application.views.particular.background.EarthBackground;
    import application.views.particular.background.MoonBackground;
    import application.views.particular.background.TweenBackground;
    import application.views.particular.background.layers.ParallaxBackgroundLayer;
    import application.views.particular.background.layers.ParallaxLayer1;
    import application.views.particular.background.layers.ParallaxLayer2;
    import application.views.particular.background.layers.ParallaxLayer3;
    import application.views.particular.background.layers.ParallaxLayer4;

    import flash.events.Event;

    import framework.data.ModelsRegistry;
    import framework.helpers.MathHelper;
    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BackgroundLayer extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BackgroundLayer()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _model = ModelsRegistry.getModel(ModelData.GAME_MODEL) as GameModel;
            _statisticsModel = _model.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;

            _setupVelocity();
            _setupLayers();
            _calculateHeight();
        }

        override public function render():void
        {
            addChild(_background);
            addChild(_moonLayer);
            addChild(_earthLayer);
            addChild(_cloudLayer1);
            addChild(_cloudLayer2);
            addChild(_cloudLayer3);
            addChild(_cloudLayer4);

            if (_model.gameMode == GameMode.ENDLESS) {
                _setupRandomLayer();
            }
        }

        override public function destroy():void
        {
            _earthLayer.destroy();
            _cloudLayer1.destroy();
            _cloudLayer2.destroy();
            _cloudLayer3.destroy();
            _cloudLayer4.destroy();
            _background.destroy();


            if (!_moonLayer.destroyed) {
                _moonLayer.destroy();
            }

            _model.removeEventListener(ApplicationEvent.VELOCITY_CHANGE, _updateVelocity);

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function pause():void
        {
            addChild(_cloudLayer4); // хук на случай, когда ближний слой не в этом контейнере

            for (var i:int = 0; i < _hash.length; i++) {
                _hash[i].pause();
            }
            _earthLayer.pause();
            if (!_moonLayer.destroyed)
                _moonLayer.pause();
        }

        public function resume():void
        {
            for (var i:int = 0; i < _hash.length; i++) {
                _hash[i].resume();
            }
            _earthLayer.resume();
            if (!_moonLayer.destroyed)
                _moonLayer.resume();
        }

        public function setDarkLayer():void
        {
            for (var i:int = 0; i < _hash.length; i++) {
                _hash[i].setDarkClouds();
            }
            _background.setDarkLayer();
        }

        public function setBrightLayer():void
        {
            for (var i:int = 0; i < _hash.length; i++) {
                _hash[i].setBrightClouds();
            }
            _background.setBrightLayer();
            if (_model.gameMode != GameMode.ENDLESS)
                _earthLayer.show();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:GameModel;
        private var _statisticsModel:StatisticsGameModel;
        private var _velocity:Number;
        private var _background:TweenBackground;
        private var _moonLayer:MoonBackground;
        private var _earthLayer:EarthBackground;
        private var _cloudLayer1:ParallaxBackgroundLayer;
        private var _cloudLayer2:ParallaxBackgroundLayer;
        private var _cloudLayer3:ParallaxBackgroundLayer;
        private var _cloudLayer4:ParallaxBackgroundLayer;
        private var _hash:Vector.<ParallaxBackgroundLayer> = new Vector.<ParallaxBackgroundLayer>();
        private var _darkLayerHeight:Number = 0;
        private var _brightLayerHeight:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupVelocity():void
        {
            _velocity = _model.velocity;
            _model.addEventListener(ApplicationEvent.VELOCITY_CHANGE, _updateVelocity);
        }

        private function _setupLayers():void
        {
            _background = new TweenBackground();

            _cloudLayer1 = new ParallaxLayer1(_velocity);
            _cloudLayer2 = new ParallaxLayer2(_velocity);
            _cloudLayer3 = new ParallaxLayer3(_velocity);
            _cloudLayer4 = new ParallaxLayer4(_velocity);
            _moonLayer = new MoonBackground(_velocity);
            _earthLayer = new EarthBackground();

            _hash.push(_cloudLayer1, _cloudLayer2, _cloudLayer3, _cloudLayer4);
        }

        private function _calculateHeight():void
        {
            var deltaHeight:Number = _statisticsModel.height / 3;
            _darkLayerHeight = _statisticsModel.height - deltaHeight;
            _brightLayerHeight = _darkLayerHeight - deltaHeight;
        }

        private function _checkHeight():void
        {
            if (_model.gameMode == GameMode.ENDLESS)
                return;

            if (_statisticsModel.height <= _darkLayerHeight && _statisticsModel.layer == Layer.COSMOS_LAYER) {
                setDarkLayer();
                _statisticsModel.layer = Layer.DARK_LAYER;
            }

            if (_statisticsModel.height <= _brightLayerHeight && _statisticsModel.layer == Layer.DARK_LAYER) {
                setBrightLayer();
                _statisticsModel.layer = Layer.BRIGHT_LAYER;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _updateVelocity(e:Event):void
        {
            _velocity = _model.velocity;

            for (var i:int = 0; i < _hash.length; i++) {
                _hash[i].velocity = _velocity;
            }
            _moonLayer.velocity = _velocity;
            _checkHeight();
        }

        private function _setupRandomLayer():void
        {
            var random:int = MathHelper.random(0, 2);
            if (random == 1)
                this.setDarkLayer();
            else
                if (random == 2)
                    this.setBrightLayer();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get frontLayer():ParallaxBackgroundLayer
        {
            return _cloudLayer4;
        }

    }

}