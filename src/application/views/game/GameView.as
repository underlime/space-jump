/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 07.12.12
 */

package application.views.game
{
    import application.config.DataPhysics;
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.models.Inventory;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.items.ItemRecord;
    import application.sound.SoundManager;
    import application.sound.libs.PickupSounds;
    import application.sound.libs.UISounds;
    import application.views.game.base.BonusGameView;
    import application.views.game.layers.panel.buffs.BaseBuff;
    import application.views.game.layers.panel.buffs.BoosterBuff;
    import application.views.game.layers.panel.buffs.BuffType;
    import application.views.game.layers.panel.buffs.ImmortalBuff;
    import application.views.game.layers.panel.buffs.MegaRocketBuff;
    import application.views.game.layers.panel.buffs.ParachuteBuff;
    import application.views.game.layers.panel.buffs.ResurrectionBuff;
    import application.views.game.layers.panel.buffs.ResurrectionPlusBuff;
    import application.views.game.layers.panel.buffs.RocketBuff;
    import application.views.game.layers.tips.enum.LabelsEnum;
    import application.views.particular.powerup.PowerupPickupAnimation;
    import application.views.physics.bodies.rocket.BigRocket;
    import application.views.physics.bodies.shield.BigShield;
    import application.views.physics.callbacks.CallbackTypes;

    import flash.events.Event;

    import framework.data.ModelsRegistry;
    import framework.helpers.MathHelper;

    import nape.callbacks.InteractionCallback;
    import nape.dynamics.CollisionArbiter;
    import nape.phys.Body;

    public class GameView extends BonusGameView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameView(model:GameModel)
        {
            super(model);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            super.setup();
            SoundManager.instance.playGameTheme();
            _inventory = ModelsRegistry.getModel(ModelData.INVENTORY_MODEL) as Inventory;
            _items = ModelsRegistry.getModel(ModelData.ITEMS_MODEL) as Items;
        }

        override public function render():void
        {
            super.render();
            _bindButtonsPanel();
            _checkResurrection();
            _setupCounts();
            _checkRocket();
        }

        override public function destroy():void
        {
            _unbindButtonsPanel();
            _buffsPanel.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/


        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _registerCollisions():void
        {
            super._registerCollisions();
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.BIG_SHIELD_TYPE, _heroBigShieldCollision);
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.BIG_ROCKET_TYPE, _heroBoosterCollision);
        }

        override protected function _canDie():Boolean
        {
            if (_simpleResurrectionEnabled && _resurrectionBuff) {
                _enableSimpleResurrection();
                return false;
            }

            if (_resurrectionPlusEnabled) {
                _enableResurrectionPlus();
                return false;
            }

            return true;
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _simpleResurrectionEnabled:Boolean = false;
        private var _resurrectionPlusEnabled:Boolean = false;
        private var _resurrectionBuff:BaseBuff;
        private var _inventory:Inventory;
        private var _items:Items;
        private var _resurrectionPlusBuff:ResurrectionPlusBuff;

        private var _parachuteCount:int = 0;
        private var _resurrectionCount:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _bindButtonsPanel():void
        {
            this.addEventListener(ApplicationEvent.CALL_PARACHUTE, _parachuteCallHandler);
            this.addEventListener(ApplicationEvent.CALL_RESURRECTION, _resurrectionCallHandler);
            this.addEventListener(ApplicationEvent.CALL_PAUSE, _pauseCallHandler);
            _gamePanelLayer.addEventListener(ApplicationEvent.SCREEN_SHOT, _screenShotCallHandler);
        }

        private function _unbindButtonsPanel():void
        {
            this.removeEventListener(ApplicationEvent.CALL_PARACHUTE, _parachuteCallHandler);
            this.removeEventListener(ApplicationEvent.CALL_RESURRECTION, _resurrectionCallHandler);
        }

        private function _checkRocket():void
        {
            var dataRocket:Object = _inventory.getItemsGroupByAction()[Items.ACTION_ROCKET];
            var rocketCount:Number = Number(dataRocket.count);

            var dataMega:Object = _inventory.getItemsGroupByAction()[Items.ACTION_MEGA_ROCKET];
            var megaCount:Number = Number(dataMega.count);

            var item:ItemRecord;

            if (megaCount > 0) {
                _startMegaRocket();
                item = _items.getItemByAction(Items.ACTION_MEGA_ROCKET);
                if (item)
                    _statisticsModel.usedItems.push(item.id.value);

            } else
                if (rocketCount > 0) {
                    _startRocket();
                    item = _items.getItemByAction(Items.ACTION_ROCKET);
                    if (item)
                        _statisticsModel.usedItems.push(item.id.value);
                }
        }

        private function _checkResurrection():void
        {
            var dataResurrection:Object = _inventory.getItemsGroupByAction()[Items.ACTION_RESURRECTION_PLUS];
            var resurrectionCount:Number = Number(dataResurrection.count);

            if (resurrectionCount > 0)
                _setupResurrectionPlus();
        }

        private function _setupResurrectionPlus():void
        {
            _resurrectionPlusBuff = new ResurrectionPlusBuff();
            _buffsPanel.addBuff(_resurrectionPlusBuff);
            _resurrectionPlusEnabled = true;
        }

        private function _startMegaRocket():void
        {
            if (!_cosmonaut.blocked) {
                var seconds:int = DataPhysics.instance.mega_rocket_seconds.value;
                _cosmonaut.addEventListener(Event.COMPLETE, _rocketCompleteHandler);
                _cosmonaut.enableBigRocket(seconds);
                _buffsPanel.addBuff(new MegaRocketBuff(seconds));
                _model.setStaticVelocity(1000);
                super._tipsLayer.addLabel(LabelsEnum.MEGA_START);
            }
        }

        private function _startRocket():void
        {
            if (!_cosmonaut.blocked) {
                var seconds:int = DataPhysics.instance.rocket_seconds.value;
                _cosmonaut.addEventListener(Event.COMPLETE, _rocketCompleteHandler);
                _cosmonaut.enableSmallRocket(seconds);
                _buffsPanel.addBuff(new RocketBuff(seconds));
                _model.setStaticVelocity(1000);
                _tipsLayer.addLabel(LabelsEnum.FAST_START);
            }
        }

        private function _heroBigShieldCollision(cb:InteractionCallback):void
        {
            var shieldBody:Body = cb.int2.castBody;
            var shield:BigShield = (shieldBody.userData.wrapper as BigShield);
            var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;
            _statisticsModel.points++;
            _statisticsModel.power_shield_count++;

            _addImpact(new PowerupPickupAnimation(), carb);

            shield.destroy();

            if (_cosmonaut.buffType == BuffType.NONE || _cosmonaut.buffType == BuffType.IMMORTAL_BUFF) {
                var seconds:int = DataPhysics.instance.defence_field_seconds.value;
                _cosmonaut.enableDefenseShield(seconds);
                _buffsPanel.addBuff(new ImmortalBuff(seconds));
            }

            SoundManager.instance.playPickupSound(PickupSounds.POWERUP_PICK_UP);
            super._tipsLayer.addLabel(LabelsEnum.FORCE_FIELD);
        }

        private function _heroBoosterCollision(cb:InteractionCallback):void
        {
            var rocketBody:Body = cb.int2.castBody;
            var rocket:BigRocket = (rocketBody.userData.wrapper as BigRocket);
            var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;
            _statisticsModel.points++;
            _statisticsModel.booster_count++;

            super._addImpact(new PowerupPickupAnimation(), carb);
            rocket.destroy();

            if (!_cosmonaut.blocked) {
                var seconds:int = DataPhysics.instance.booster_seconds.value;
                _cosmonaut.addEventListener(Event.COMPLETE, _boostComplete);
                _cosmonaut.enableBooster(seconds);
                _model.setStaticVelocity(1000);
                _buffsPanel.addBuff(new BoosterBuff(seconds));
            }

            SoundManager.instance.playPickupSound(PickupSounds.POWERUP_PICK_UP);
            _tipsLayer.addLabel(LabelsEnum.BOOSTER);
        }

        private function _enableSimpleResurrection():void
        {
            _statisticsModel.flushHealth();
            _cosmonaut.resurrectionApply();
            _buffsPanel.deleteBuff(_resurrectionBuff);
            _simpleResurrectionEnabled = false;
            _tipsLayer.addLabel(LabelsEnum.RESURRECTION);
        }

        private function _enableResurrectionPlus():void
        {
            _statisticsModel.flushHealth();
            _cosmonaut.resurrectionPlusApply();
            _buffsPanel.deleteBuff(_resurrectionPlusBuff);
            _resurrectionPlusEnabled = false;
            _tipsLayer.addLabel(LabelsEnum.RESURRECTION_PLUS);

            var resurrectionPlusItem:ItemRecord = _items.getItemByAction(Items.ACTION_RESURRECTION_PLUS);
            if (resurrectionPlusItem)
                _statisticsModel.usedItems.push(resurrectionPlusItem.id.value);
        }

        private function _setupCounts():void
        {
            var parachuteData:Object = _inventory.getItemsGroupByAction()[Items.ACTION_PARACHUTE];
            if (parachuteData && parachuteData.count)
                _parachuteCount = parachuteData.count;
            else
                _parachuteCount = 0;

            var resurrectionData:Object = _inventory.getItemsGroupByAction()[Items.ACTION_RESURRECTION];
            if (resurrectionData && resurrectionData.count)
                _resurrectionCount = resurrectionData.count;
            else
                _resurrectionCount = 0;

            _gamePanelLayer.buttonsLayer.parachuteButton.count = _parachuteCount;
            _gamePanelLayer.buttonsLayer.resurrectionButton.count = _resurrectionCount;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _rocketCompleteHandler(event:Event):void
        {
            _cosmonaut.removeEventListener(Event.COMPLETE, _rocketCompleteHandler);
            _model.removeStaticVelocity();
        }

        private function _boostComplete(event:Event):void
        {
            _cosmonaut.removeEventListener(Event.COMPLETE, _boostComplete);
            _model.removeStaticVelocity();
        }

        private function _pauseCallHandler(event:ApplicationEvent):void
        {
            if (!super.paused)
                super.pause();
        }

        private function _screenShotCallHandler(event:ApplicationEvent):void
        {
            if (super.paused)
                event.stopImmediatePropagation();
        }

        private function _parachuteCallHandler(event:ApplicationEvent):void
        {
            if (!super.paused) {
                event.stopImmediatePropagation();
                if (!_cosmonaut.blocked && _parachuteCount > 0) {
                    _openParachute();
                }
            }
        }

        private function _openParachute():void
        {
            _cosmonaut.openParachute(3);
            _buffsPanel.addBuff(new ParachuteBuff(3));
            _model.velocity = int(MathHelper.reducePercent(_model.velocity, 25));

            _parachuteCount--;
            _gamePanelLayer.buttonsLayer.parachuteButton.count = _parachuteCount;

            var itemParachute:ItemRecord = _items.getItemByAction(Items.ACTION_PARACHUTE);
            if (itemParachute)
                _statisticsModel.usedItems.push(itemParachute.id.value);

            _tipsLayer.addLabel(LabelsEnum.PARACHUTE);
        }

        private function _resurrectionCallHandler(event:ApplicationEvent):void
        {
            if (super.paused)
                return;

            event.stopImmediatePropagation();
            if (_cosmonaut.blocked || _simpleResurrectionEnabled || _resurrectionPlusEnabled)
                return;

            if (_resurrectionCount > 0)
                _enableResurrection();
        }

        private function _enableResurrection():void
        {
            _resurrectionBuff = new ResurrectionBuff(DataPhysics.instance.resurrection_seconds.value);
            _resurrectionBuff.addEventListener(Event.COMPLETE, _simpleResurrectionComplete)

            _resurrectionCount--;
            _gamePanelLayer.buttonsLayer.resurrectionButton.count = _resurrectionCount;

            var itemResurrection:ItemRecord = _items.getItemByAction(Items.ACTION_RESURRECTION);
            if (itemResurrection)
                _statisticsModel.usedItems.push(itemResurrection.id.value);

            _simpleResurrectionEnabled = true;
            _buffsPanel.addBuff(_resurrectionBuff);
            SoundManager.instance.playUISound(UISounds.RESURRECTION_INIT);
        }

        private function _simpleResurrectionComplete(event:Event):void
        {
            _resurrectionBuff.removeEventListener(Event.COMPLETE, _simpleResurrectionComplete)
            _simpleResurrectionEnabled = false;
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
