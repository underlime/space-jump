/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.05.13
 * Time: 16:11
 */
package application.views.game.base.common
{
    import application.config.DataPhysics;
    import application.views.base.ApplicationView;
    import application.views.screen.elements.buttons.UniversalButton;

    import flash.events.Event;
    import flash.events.MouseEvent;

    public class PauseView extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PauseView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBack();
            _setupLabel();
            _setButton();
            _setupExitButton();
        }

        override public function destroy():void
        {
            _resumeButton.destroy();
            _exitButton.destroy();
            _label.destroy();
            graphics.clear();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _resumeButton:UniversalButton;
        private var _exitButton:UniversalButton;

        private var _label:PauseLabel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBack():void
        {
            graphics.beginFill(0x000000, .8);
            graphics.drawRect(0, 0, 800, 700);
            graphics.endFill();
        }

        private function _setupLabel():void
        {
            _label = new PauseLabel();
            addChild(_label);
            _label.y = 235;
            _label.x = int(DataPhysics.WORLD_WIDTH / 2 - _label.width / 2);
        }

        private function _setButton():void
        {
            _resumeButton = new UniversalButton(super.textFactory.getLabel("resume_label").toUpperCase());
            _resumeButton.addEventListener(MouseEvent.CLICK, _clickHandler);

            addChild(_resumeButton);
            _resumeButton.x = 325;
            _resumeButton.y = 338;
        }

        private function _setupExitButton():void
        {
            _exitButton = new UniversalButton(super.textFactory.getLabel('exit_label').toUpperCase(), true)
            _exitButton.addEventListener(MouseEvent.CLICK, _exitHandler);

            addChild(_exitButton);
            _exitButton.x = 325;
            _exitButton.y = 400;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _clickHandler(event:MouseEvent):void
        {
            _resumeButton.removeEventListener(MouseEvent.CLICK, _clickHandler);
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        private function _exitHandler(event:MouseEvent):void
        {
            super.dispatchEvent(new Event(Event.CLOSE));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}

import application.config.ApplicationConfiguration;
import application.views.game.layers.tips.enum.LabelsEnum;

import flash.display.Bitmap;

import framework.view.View;

internal class PauseLabel extends View
{
    private const RU:String = "_RU";
    private const EN:String = "_EN";

    private var _label:Bitmap;

    override public function render():void
    {
        var source:String = LabelsEnum.PAUSE.toString();
        if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
            source += RU;
        else
            source += EN;
        _label = super.source.getBitmap(source);
        addChild(_label);
    }
}
