/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.06.13
 * Time: 10:41
 */
package application.views.game.base.common
{
    import application.views.screen.main.MainScreenData;
    import application.views.screen.main.panel.field.IconTextField;

    import flash.geom.Rectangle;

    public class ChanceScreenGemsField extends IconTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChanceScreenGemsField()
        {
            super(0);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _icon = this.source.getBitmap(MainScreenData.GEMS_ICON);
            _icon.scrollRect = new Rectangle(0, 0, 35, 35);
            super.setup();
        }

        override public function destroy():void
        {
            this.removeChild(_icon);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _setupIcon():void
        {
            this.addChild(_icon);
            _icon.x = _rightEdge.x + _rightEdge.width;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
