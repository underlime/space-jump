/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 16.05.13
 * Time: 21:58
 */
package application.views.game.base.common
{
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.utils.setTimeout;

    import framework.view.View;

    public class ControlsDemoView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const CONTROLS_DATA:String = "CONTROLS_TIPS";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ControlsDemoView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            setTimeout(_setupControlsDemo, 1000);
        }

        override public function destroy():void
        {
            _controls.removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _controls.stop();
            removeChild(_controls);
            _controls = null;

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _controls:MovieClip;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupControlsDemo():void
        {
            _controls = super.source.getMovieClip(CONTROLS_DATA);
            _controls.x = 230;
            _controls.y = 150;

            _controls.addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            addChild(_controls);
            _controls.gotoAndPlay(1);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _enterFrameHandler(event:Event):void
        {
            if (_controls.currentFrame == _controls.totalFrames)
                this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
