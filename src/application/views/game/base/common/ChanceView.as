/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 23.06.13
 * Time: 20:06
 */
package application.views.game.base.common
{
    import application.events.ApplicationEvent;
    import application.views.screen.dialog.ConfirmWindow;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.utils.DestroyUtils;
    import framework.utils.DisplayObjectUtils;
    import framework.view.text.SimpleTextField;

    public class ChanceView extends ConfirmWindow
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        private static const COUNTER:String = "SPACE_COUNTDOWN";
        private static const GEMS_ICO:String = "GEMS_PIC";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChanceView(gems:int)
        {
            var html:String = TextFactory.instance.getLabel("continue_label");
            super(html);
            _gems = gems;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _addCounter();
            _addPrice();
            _addGemsField();
        }

        override public function destroy():void
        {
            if (_counter)
                _destroyCounter();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function removeCounter():void
        {
            _destroyCounter();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _renderLabel():void
        {
            addChild(_txtLabel);
            _txtLabel.x = _background.x;
            _txtLabel.y = _background.y + 87;
            _txtLabel.htmlText = _htmlText;
            _txtLabel.mouseEnabled = true;
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _counter:MovieClip;
        private var _txtPrice:SimpleTextField;

        private var _gems:int = 1;
        private var _gemsField:ChanceScreenGemsField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addCounter():void
        {
            _counter = super.source.getMovieClip(COUNTER, false);
            _counter.x = 328;
            _counter.y = 121;

            addChild(_counter);
            _counter.gotoAndPlay(1);
            _counter.addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        private function _addPrice():void
        {
            var ico:Bitmap = super.source.getBitmap(GEMS_ICO);
            addChild(ico);

            var icoWidth:Number = 45;
            var icoHeight:Number = 45;
            var margin:Number = 10;

            _txtPrice = new SimpleTextField();
            _txtPrice.width = 400;
            _txtPrice.size = 28;
            addChild(_txtPrice);
            _txtPrice.text = _gems.toString();

            ico.width = icoWidth;
            ico.height = icoHeight;

            _txtPrice.y = _txtLabel.y + 40;
            _txtPrice.x = _background.x + _background.width / 2 - (_txtPrice.textWidth + icoWidth + margin) / 2;

            ico.x = _txtPrice.x + _txtPrice.textWidth + margin;
            ico.y = _txtPrice.y - 6;
        }

        private function _destroyCounter():void
        {
            if (_counter) {
                _counter.gotoAndStop(1);
                _counter.removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
                DisplayObjectUtils.removeChild(_counter);
                DestroyUtils.destroy(_counter, false);

                _counter = null;
            }
        }

        private function _addGemsField():void
        {
            _gemsField = new ChanceScreenGemsField();
            _gemsField.x = 745;
            _gemsField.y = 65;
            addChild(_gemsField);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _enterFrameHandler(event:Event):void
        {
            if (_counter.currentFrame >= _counter.totalFrames) {
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLICK_CANCEL));
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get gemsField():ChanceScreenGemsField
        {
            return _gemsField;
        }
    }
}
