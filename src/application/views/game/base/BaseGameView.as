/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 31.01.13
 */
package application.views.game.base
{
    import application.config.DataPhysics;
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;
    import application.models.User;
    import application.models.pattern.PatternsCollection;
    import application.models.skin.SkinsCollection;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.base.ApplicationView;
    import application.views.debug.TimeDebugView;
    import application.views.game.base.common.ControlsDemoView;
    import application.views.game.base.common.PauseView;
    import application.views.game.layers.BackgroundLayer;
    import application.views.game.layers.bonus.BonusLayer;
    import application.views.game.layers.panel.GamePanelLayer;
    import application.views.game.layers.panel.buffs.BuffsPanel;
    import application.views.game.layers.tips.TipsLayer;
    import application.views.game.patterns.PatternsView;
    import application.views.physics.SpaceWorld;
    import application.views.physics.bodies.cosmonaut.Cosmonaut;

    import com.greensock.TweenLite;
    import com.greensock.TweenMax;
    import com.greensock.easing.Linear;

    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import framework.data.ModelsRegistry;
    import framework.helpers.MathHelper;
    import framework.tools.ScreenShot;
    import framework.utils.DestroyUtils;
    import framework.utils.DisplayObjectUtils;

    import nape.geom.Vec2;

    public class BaseGameView extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BaseGameView(model:GameModel)
        {
            _model = model;
            _skins = model.getChildByName(ModelData.SKINS_COLLECTION) as SkinsCollection;
            _patterns = model.getChildByName(ModelData.PATTERNS_COLLECTION) as PatternsCollection;
            _statisticsModel = _model.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;
            _userModel = ModelsRegistry.getModel(ModelData.USER_MODEL) as User;

            _world = new SpaceWorld(_model);
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _background = new BackgroundLayer();
            _gamePanelLayer = new GamePanelLayer();
            _cosmonaut = new Cosmonaut(_world);
            _cosmonaut.skins = _skins;
            _patternsView = new PatternsView(_world, _patterns);
            _tipsLayer = new TipsLayer();

            _statisticsModel.beginCollectAchievements();
            _statisticsModel.addEventListener(Event.CHANGE, _modelChangeHandler);
        }

        override public function render():void
        {
            addChild(_background);
            addChild(_patternsView);
            _addCosmonaut();

            addChild(_background.frontLayer);

            addChild(_tipsLayer);
            addChild(_gamePanelLayer);
            _addBuffsPanel();

            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);

            // добавляем демо контролов
            if (_userModel.jumps_count.value <= 3)
                addChild(new ControlsDemoView());

            _setupTimeDebugView();
        }

        override public function destroy():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);

            if (_pauseView && !_pauseView.destroyed)
                _pauseView.destroy();

            _stopTimer();
            _timeDebugView.destroy();
            _world.destroy();
            _background.destroy();
            _cosmonaut.destroy();
            _gamePanelLayer.destroy();
            _tipsLayer.destroy();
            _patternsView.destroy();
            _statisticsModel.removeEventListener(Event.CHANGE, _modelChangeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function pause():void
        {
            _pauseGame();

            _pauseView = new PauseView();
            _pauseView.addEventListener(Event.COMPLETE, _pauseCompleteHandler);
            _pauseView.addEventListener(Event.CLOSE, _pauseExitHandler);


            addChild(_pauseView);

            if (_musicEnabled)
                SoundManager.instance.musicMute = true;
        }

        public function resume():void
        {
            if (_soundEnabled)
                SoundManager.instance.soundMute = false;
            if (_musicEnabled)
                SoundManager.instance.musicMute = false;

            if (_pauseView && !_pauseView.destroyed) {
                _pauseView.removeEventListener(Event.COMPLETE, _pauseCompleteHandler);
                _pauseView.removeEventListener(Event.CLOSE, _pauseExitHandler);
                _pauseView.destroy();
            }

            _stopTimer();
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _background.resume();
            addChildAt(_background.frontLayer, getChildIndex(_cosmonaut) + 1);

            _paused = false;
        }

        public function continueByResurrect():void
        {
            DisplayObjectUtils.removeChild(_endBitmap);
            DestroyUtils.destroy(_endBitmap, false);

            _statisticsModel.flushHealth();
            _updateParamsEnabled = true;
            _statisticsModel.beginFreeFall();

            resume();
            _model.removeStaticVelocity(3);
        }

        public function endGame():void
        {
            _dispatchEnd();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _model:GameModel;
        protected var _statisticsModel:StatisticsGameModel;
        protected var _userModel:User;

        protected var _world:SpaceWorld;
        protected var _background:BackgroundLayer;
        protected var _bonusLayer:BonusLayer;
        protected var _gamePanelLayer:GamePanelLayer;
        protected var _cosmonaut:Cosmonaut;
        protected var _patternsView:PatternsView;
        protected var _tipsLayer:TipsLayer;
        protected var _buffsPanel:BuffsPanel;

        protected var _skins:SkinsCollection;
        protected var _patterns:PatternsCollection;

        protected var _updatePhysicsEnabled:Boolean = true;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _pauseGame():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);

            _startTimer();
            _background.pause();
            _paused = true;

            SoundManager.instance.destroyLoopSounds();
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _iterations:Number = 0;
        private var _paused:Boolean = false;
        private var _screen:ScreenShot;
        private var _updateParamsEnabled:Boolean = true;

        private var _pauseView:PauseView;
        private var _timer:Timer;

        private var _soundEnabled:Boolean = !SoundManager.instance.soundMute;
        private var _musicEnabled:Boolean = !SoundManager.instance.musicMute;

        private var _endBitmap:Bitmap;

        private var _timeDebugView:TimeDebugView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _addCosmonaut():void
        {
            var impulse:Vec2 = new Vec2(0, _model.velocity * 2);
            impulse.muleq(10);
            addChild(_cosmonaut);
            _cosmonaut.body.position.x = DataPhysics.WORLD_WIDTH / 2 - _cosmonaut.width;
            _cosmonaut.body.position.y = 200;
            _cosmonaut.body.applyWorldImpulse(impulse);
            _cosmonaut.body.rotation = MathHelper.random(0, 10);
        }

        private function _addBuffsPanel():void
        {
            _buffsPanel = new BuffsPanel();
            _buffsPanel.x = DataPhysics.WORLD_WIDTH - 90;
            _buffsPanel.y = 55;

            addChild(_buffsPanel);
        }

        private function _checkComplete():void
        {
            if (_statisticsModel.height <= 0) {

                _statisticsModel.locked = true;
                removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);

                _cosmonaut.blockCosmonaut();
                _patternsView.destroy();
                _cosmonaut.openParachute(15);

                TweenLite.to(_model, 3, {"velocity": 0});

                var tweenParams:Object = {
                    onComplete: _successLanding,
                    ease: Linear.easeNone,
                    y: DataPhysics.WORLD_HEIGHT + 300
                };

                TweenLite.to(_cosmonaut.body.position, 3, tweenParams);
                SoundManager.instance.playUISound(UISounds.SUCCESS);
            }
        }

        private function _successLanding():void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.END_GAME);
            event.data.success = true;
            super.dispatchEvent(event);
        }

        private function _startTimer():void
        {
            _stopTimer();
            _timer = new Timer(1000, int.MAX_VALUE);
            _timer.addEventListener(TimerEvent.TIMER, _updateTime);
            _timer.start();
        }

        private function _updateTime(event:TimerEvent):void
        {
            _statisticsModel.pause_seconds++;
        }

        private function _stopTimer():void
        {
            if (_timer) {
                _timer.stop();
                _timer.removeEventListener(TimerEvent.TIMER, _updateTime);
                _timer = null;
            }
        }

        private function _setupTimeDebugView():void
        {
            _timeDebugView = new TimeDebugView(_statisticsModel);
            addChild(_timeDebugView);

            _timeDebugView.x = 200;
            _timeDebugView.y = 632;
            _timeDebugView.visible = false;

            addEventListener(ApplicationEvent.CALL_DEBUG_TIMER, _callDebugTimerHandler);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _enterFrameHandler(event:Event):void
        {
            if (!_updatePhysicsEnabled)
                return;

            if (_world) {
                _world.update();
            }

            if (_updateParamsEnabled) {
                _updateParams();
            }
        }

        private function _updateParams():void
        {
            _iterations++;
            if (_iterations * DataPhysics.TIME_STEP == 1) {
                _iterations = 0;
                _statisticsModel.physicsTicks++;
                _model.increaseVelocity();
                _statisticsModel.seconds++;
            }
            var shortDistance:int = int(_model.velocity * DataPhysics.TIME_STEP);
            _statisticsModel.distance += shortDistance;
            _statisticsModel.height -= shortDistance;
            _checkComplete();
        }

        protected function _modelChangeHandler(e:Event):void
        {
            if (_statisticsModel.health <= 0 && _canDie()) {
                _updateParamsEnabled = false;
                _statisticsModel.endFreeFall();
                _model.addEventListener(ApplicationEvent.BLOCK, _blockVelocityHandler);
                _model.setStaticVelocity(1, 0.9);
            }
        }

        /**
         * Проверка, есть ли баф на воскрешение (переопределен в GameView)
         * @return
         */
        protected function _canDie():Boolean
        {
            return true;
        }

        private function _blockVelocityHandler(event:ApplicationEvent):void
        {
            _model.removeEventListener(ApplicationEvent.BLOCK, _blockVelocityHandler);
            _setupBitmap();
        }

        private function _setupBitmap():void
        {
            _pauseGame();
            _screen = new ScreenShot();
            _screen.takeShot(this, DataPhysics.WORLD_WIDTH, DataPhysics.WORLD_HEIGHT);

            DisplayObjectUtils.removeChild(_endBitmap);
            DestroyUtils.destroy(_endBitmap);

            _endBitmap = new Bitmap(_screen.rawData);
            addChild(_endBitmap);

            TweenMax.to(_endBitmap, 1, {
                colorMatrixFilter: { saturation: 0 },
                onComplete: _tryResurrect
            });

            SoundManager.instance.playUISound(UISounds.FAIL);
        }

        private function _tryResurrect():void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_CHANCE));
        }

        private function _dispatchEnd():void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.END_GAME);
            event.data.success = false;
            super.dispatchEvent(event);
        }

        private function _pauseCompleteHandler(event:Event):void
        {
            resume();
        }

        private function _pauseExitHandler(event:Event):void
        {
            resume();
            _pauseGame();
            _dispatchEnd();
        }

        private function _callDebugTimerHandler(event:ApplicationEvent):void
        {
            if (!_timeDebugView.visible)
                _timeDebugView.visible = true;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get paused():Boolean
        {
            return _paused;
        }
    }
}
