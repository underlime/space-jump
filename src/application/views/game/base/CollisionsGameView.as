package application.views.game.base
{
    import application.models.GameModel;
    import application.sound.SoundManager;
    import application.sound.libs.CollisionSounds;
    import application.sound.libs.PickupSounds;
    import application.views.game.layers.tips.enum.LabelsEnum;
    import application.views.game.layers.tips.enum.StatusEnum;
    import application.views.particular.coins.CoinsPickupAnimation;
    import application.views.particular.cola.ColaPickupAnimation;
    import application.views.particular.explode.EndingAnimation;
    import application.views.particular.explode.Impact;
    import application.views.particular.heart.HeartPickupAnimation;
    import application.views.particular.pig.PigExplodeAnimation;
    import application.views.particular.powerup.PowerupPickupAnimation;
    import application.views.particular.shield.ShieldPickupAnimation;
    import application.views.physics.bodies.IExplosion;
    import application.views.physics.bodies.IObstacleBody;
    import application.views.physics.bodies.base.SpaceBody;
    import application.views.physics.bodies.coins.GoldCoins;
    import application.views.physics.bodies.coins.ICoin;
    import application.views.physics.bodies.coins.SilverCoins;
    import application.views.physics.bodies.cola.JetCola;
    import application.views.physics.bodies.heart.Heart;
    import application.views.physics.bodies.pig.GreedPig;
    import application.views.physics.bodies.shield.Shield;
    import application.views.physics.callbacks.CallbackTypes;

    import framework.helpers.MathHelper;

    import nape.callbacks.InteractionCallback;
    import nape.callbacks.PreCallback;
    import nape.callbacks.PreFlag;
    import nape.dynamics.CollisionArbiter;
    import nape.geom.Vec2;
    import nape.phys.Body;

    public class CollisionsGameView extends BaseGameView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CollisionsGameView(model:GameModel)
        {
            super(model);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function setup():void
        {
            super.setup();
            _registerCollisions();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function setupBonusLevel():void
        {
            trace("Override me!");
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _registerCollisions():void
        {
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.EXPLOSION_OBSTACLE_TYPE, _cosmonautExplodeTypeCollisionHandler);
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.COIN_CBTYPE, _cosmonautCoinsTypeCollisionHandler);
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.HEART_TYPE, _cosmonautHeartCollisionHandler);
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.SHIELD_TYPE, _cosmonautShieldCollisionHandler);
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.GREED_PIG_TYPE, _cosmonautPigCollisionHandler);
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.COLA_TYPE, _cosmonautColaCollisionHandler);
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.KINEMATIC_TYPE, _cosmonautKinematicCollisionHandler);
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.TROPHY_TYPE, _cosmonautTrophyCollisionHandler);

            _world.collisions.registerPreCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.COIN_CBTYPE, _setupIgnoreFlag);

        }

        protected function _cosmonautExplodeTypeCollisionHandler(cb:InteractionCallback):void
        {
            var body:Body = cb.int2.castBody;
            var explodeBody:IExplosion = body.userData.wrapper as IExplosion;
            explodeBody.explode();

            if (!_cosmonaut.blocked) {
                _statisticsModel.collisionType = explodeBody.collisionType;
                _model.velocity = Math.floor(MathHelper.reducePercent(_model.velocity, explodeBody.speedReduce));
                _cosmonaut.updateGraphic();
                _statisticsModel.reduceHealth(explodeBody.damage);
                _cosmonaut.blink();
                _tipsLayer.reduceHealth(explodeBody.damage, _cosmonaut.body.position.x, _cosmonaut.body.position.y);
            }
            _statisticsModel.endFreeFall();
        }

        protected function _cosmonautKinematicCollisionHandler(cb:InteractionCallback):void
        {
            if (_cosmonaut.blocked)
                return;

            var b1:Body = cb.int1.castBody;
            var b2:Body = cb.int2.castBody;
            var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;
            var position:Vec2 = carb.contacts.at(0).position;
            var impact:Impact = new Impact();
            var obstacleBody:IObstacleBody = b2.userData.wrapper as IObstacleBody;

            impact.x = position.x;
            impact.y = position.y;

            addChild(impact);

            _statisticsModel.collisionType = obstacleBody.collisionType;
            _cosmonaut.updateGraphic();
            _statisticsModel.reduceHealth(obstacleBody.damage);
            _statisticsModel.endFreeFall();
            _cosmonaut.blink();

            _tipsLayer.reduceHealth(obstacleBody.damage, _cosmonaut.body.position.x, _cosmonaut.body.position.y);
            SoundManager.instance.playCollisionSound(CollisionSounds.METAL_BOX_IMPACT);
        }

        protected function _cosmonautTrophyCollisionHandler(cb:InteractionCallback):void
        {
            var body:Body = cb.int2.castBody;
            var trophyBody:SpaceBody = body.userData.wrapper as SpaceBody;
            var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;

            _addImpact(new PowerupPickupAnimation(), carb);
            trophyBody.destroy();

            super._tipsLayer.addLabel(LabelsEnum.TROPHY);
            SoundManager.instance.playPickupSound(PickupSounds.TROPHY_PICKUP);

            _statisticsModel.points++;
            _statisticsModel.trophyFound = true;
        }

        protected function _cosmonautHeartCollisionHandler(cb:InteractionCallback):void
        {
            var body:Body = cb.int2.castBody;
            var heartBody:Heart = body.userData.wrapper as Heart;
            var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;

            _statisticsModel.points++;
            _statisticsModel.health += heartBody.regeneration;
            _statisticsModel.heart_count++;

            _addImpact(new HeartPickupAnimation(), carb);
            heartBody.destroy();

            SoundManager.instance.playPickupSound(PickupSounds.HEART_PICK_UP);
            super._tipsLayer.addStatus(StatusEnum.HP.toString(), _cosmonaut.body.position.x, _cosmonaut.body.position.y);
        }

        protected function _cosmonautShieldCollisionHandler(cb:InteractionCallback):void
        {
            var body:Body = cb.int2.castBody;
            var shieldBody:Shield = body.userData.wrapper as Shield;
            var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;

            _statisticsModel.points++;
            _statisticsModel.armor += shieldBody.armor;
            _statisticsModel.shield_count++;

            _addImpact(new ShieldPickupAnimation(), carb);

            shieldBody.destroy();
            SoundManager.instance.playPickupSound(PickupSounds.SHIELD_PICK_UP);
            super._tipsLayer.addStatus(StatusEnum.SHIELD.toString(), _cosmonaut.body.position.x, _cosmonaut.body.position.y);

        }

        protected function _cosmonautPigCollisionHandler(cb:InteractionCallback):void
        {
            var body:Body = cb.int2.castBody;
            var pigBody:GreedPig = body.userData.wrapper as GreedPig;
            var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;
            _addImpact(new PigExplodeAnimation(), carb);
            pigBody.destroy();

            _statisticsModel.endFreeFall();
            SoundManager.instance.playCollisionSound(CollisionSounds.HOG_IMPACT);

            if (!_cosmonaut.immortal) {
                super._tipsLayer.addStatus(StatusEnum.MINUS_COINS.toString(), _cosmonaut.body.position.x, _cosmonaut.body.position.y);
                _statisticsModel.coins = 0;
            }
        }

        protected function _cosmonautColaCollisionHandler(cb:InteractionCallback):void
        {
            var body:Body = cb.int2.castBody;
            var colaBody:JetCola = body.userData.wrapper as JetCola;
            var carb:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;

            _addImpact(new ColaPickupAnimation(), carb);

            _statisticsModel.points++;
            _statisticsModel.bonus++;

            colaBody.destroy();
            SoundManager.instance.playPickupSound(PickupSounds.COLA_PICK_UP);
            super._tipsLayer.addStatus(StatusEnum.COLA.toString(), _cosmonaut.body.position.x, _cosmonaut.body.position.y);

            if (_statisticsModel.bonus >= _statisticsModel.maxBonus) {
                _statisticsModel.bonus = 0;
                setupBonusLevel();
            }
        }

        protected function _cosmonautCoinsTypeCollisionHandler(cb:InteractionCallback):void
        {
            SoundManager.instance.playPickupSound(PickupSounds.COIN_PICK_UP);

            var body:Body = cb.int2.castBody;
            var coinBody:ICoin = body.userData.wrapper as ICoin;

            _statisticsModel.coins += coinBody.money;
            _statisticsModel.points++;

            if (coinBody is SilverCoins)
                _statisticsModel.silver++;
            else
                if (coinBody is GoldCoins)
                    _statisticsModel.gold++;
                else
                    _statisticsModel.copper++;

            super._tipsLayer.showCoins(coinBody.money, _cosmonaut.body.position.x, _cosmonaut.body.position.y);
            var carb:CollisionArbiter = cb.arbiters.at(0) ? cb.arbiters.at(0).collisionArbiter : null;

            if (carb)
                _addImpact(new CoinsPickupAnimation(), carb);

            coinBody.destroy();
        }

        protected function _addImpact(impact:EndingAnimation, collisionArbiter:CollisionArbiter):void
        {
            var position:Vec2 = collisionArbiter.contacts.at(0).position;
            impact.x = position.x - impact.maxWidth / 2;
            impact.y = position.y - impact.maxHeight / 2;

            addChild(impact);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _setupIgnoreFlag(cb:PreCallback):PreFlag
        {
            return PreFlag.IGNORE;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
