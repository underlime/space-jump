/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 18.04.13
 * Time: 19:59
 */
package application.views.game.base
{
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.sound.SoundManager;
    import application.sound.libs.CollisionSounds;
    import application.views.game.layers.bonus.BonusLayer;
    import application.views.game.layers.panel.buffs.BuffType;
    import application.views.game.layers.tips.enum.LabelsEnum;
    import application.views.physics.callbacks.CallbackTypes;
    import application.views.screen.ScreenData;

    import com.greensock.TweenLite;
    import com.greensock.easing.Linear;

    import flash.display.MovieClip;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import nape.callbacks.InteractionCallback;
    import nape.callbacks.PreCallback;
    import nape.callbacks.PreFlag;
    import nape.dynamics.CollisionArbiter;
    import nape.geom.Vec2;

    public class BonusGameView extends CollisionsGameView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BonusGameView(model:GameModel)
        {
            super(model);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _stopTimer();

            if (_bonusLayer && !_bonusLayer.destroyed)
                _bonusLayer.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        override public function setupBonusLevel():void
        {
            super._pauseGame();
            _startTimer();
            _world.addEventListener(ApplicationEvent.UPDATE, _setupLevel);
            _cosmonaut.removeAllBuffs();
            _buffsPanel.deleteBuffs([BuffType.RESURRECTION_PLUS_BUFF]);
            _statisticsModel.locked = true;
            _tipsLayer.addLabel(LabelsEnum.BONUS);
            SoundManager.instance.playBonusTheme();
        }

        private function _setupLevel(event:ApplicationEvent):void
        {
            _world.removeEventListener(ApplicationEvent.UPDATE, _setupLevel);
            _patternsView.pause();
            _model.setupBonusVelocity();

            _background.pause();
            _bonusLayer = new BonusLayer();
            super.addChildAt(_bonusLayer, 0);
            _setupMask();
        }


        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        override protected function _registerCollisions():void
        {
            super._registerCollisions();
            _world.collisions.registerCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.BLACK_HOLE_TYPE, _cosmonautHoleTypeCollisionHandler);
            _world.collisions.registerPreCallback(CallbackTypes.COSMONAUT_CBTYPE, CallbackTypes.BLACK_HOLE_TYPE, _setupIgnoreFlag);
        }


        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _maskClip:MovieClip;
        private var _timer:Timer;
        private var _seconds:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _startTimer():void
        {
            _timer = new Timer(1000, int.MAX_VALUE);
            _timer.addEventListener(TimerEvent.TIMER, _updateTime);
            _timer.start();
        }

        private function _updateTime(event:TimerEvent):void
        {
            _statisticsModel.bonus_seconds++;
        }

        private function _stopTimer():void
        {
            if (_timer) {
                _timer.stop();
                _timer.removeEventListener(TimerEvent.TIMER, _updateTime);
                _timer = null;
            }
        }

        private function _setupMask():void
        {
            var params:Object = {
                onComplete: _startBonus,
                width: 2000,
                height: 2000,
                x: -620,
                y: -670,
                ease: Linear.easeIn
            }

            _setupDefaultMask();
            _maskClip.x = 380;
            _maskClip.y = 330;

            addChild(_maskClip);
            _bonusLayer.mask = _maskClip;
            super.swapChildren(_background, _bonusLayer);

            TweenLite.to(_maskClip, .5, params);
        }

        private function _startBonus():void
        {
            removeChild(_maskClip);
            super.resume();
            _background.visible = false;
            _bonusLayer.run();

            addChildAt(_bonusLayer.frontLayer, getChildIndex(_cosmonaut) + 1);
            _patternsView.bonusEnabled = true;
            _patternsView.addPattern();
        }

        private function _setupDefaultMask():void
        {
            _maskClip = super.source.getMovieClip(ScreenData.BONUS_LEVEL_MASK);
            _maskClip.width = 20;
            _maskClip.height = 20;
        }

        private function _cosmonautHoleTypeCollisionHandler(cb:InteractionCallback):void
        {
            super._pauseGame();

            _bonusLayer.addChild(_bonusLayer.frontLayer);
            _world.addEventListener(ApplicationEvent.UPDATE, _deleteObjects);

            SoundManager.instance.playCollisionSound(CollisionSounds.HOLE_TOUCH);
            var collisionArbiter:CollisionArbiter = cb.arbiters.at(0).collisionArbiter;
            var position:Vec2 = collisionArbiter.contacts.at(0).position;
            _maskClip.x = position.x - 10;
            _maskClip.y = position.y - 10;
        }

        private function _deleteObjects(event:ApplicationEvent):void
        {
            _world.removeEventListener(ApplicationEvent.UPDATE, _deleteObjects);
            _patternsView.pause();

            _setupDefaultMask();

            var params:Object = {
                onComplete: _resumeGame,
                width: 2000,
                height: 2000,
                x: (-1000 + _maskClip.x),
                y: (-1000 + _maskClip.y),
                ease: Linear.easeIn
            }

            addChild(_maskClip);
            _background.visible = true;
            _background.mask = _maskClip;
            super.swapChildren(_background, _bonusLayer);

            TweenLite.to(_maskClip, .5, params);
            SoundManager.instance.playGameTheme();
        }

        private function _resumeGame():void
        {
            _stopTimer();
            super.resume();
            _model.removeBonusVelocity();
            removeChild(_maskClip);
            _background.mask = null;
            _bonusLayer.destroy();
            _statisticsModel.locked = false;
            _patternsView.bonusEnabled = false;
            _patternsView.addPattern();
            _background.resume();

            addChildAt(_background.frontLayer, getChildIndex(_cosmonaut) + 1);

        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _setupIgnoreFlag(cb:PreCallback):PreFlag
        {
            return PreFlag.IGNORE;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
