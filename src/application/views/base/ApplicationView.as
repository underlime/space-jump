package application.views.base
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;
    import application.views.screen.ScreenData;
    import application.views.screen.elements.PreloaderScreen;
    import application.views.text.TextFactory;

    import com.greensock.TweenLite;
    import com.greensock.easing.Linear;

    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ApplicationView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ApplicationView()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function destroy():void
        {
            var destroyEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.APPLICATION_VIEW_DESTROY);
            this.dispatchEvent(destroyEvent);

            if (_delayedTimer) {
                try {
                    _delayedTimer.stop();
                    _delayedTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, _timerCompleteHandler);
                    _delayedTimer = null;
                }
                catch (e:Error) {
                }
            }

            if (_bgShadow)
                removeChild(_bgShadow);
            _bgShadow = null;

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        /**
         * Установить прелоудер.
         */
        public function setPreloader():void
        {
            if (!_preloader) {
                _preloader = new PreloaderScreen();
                this.addChild(_preloader);
            }
        }

        /**
         * Убрать прелоудер
         */
        public function removePreloader():void
        {
            if (_preloader && this.contains(_preloader)) {
                this.removeChild(_preloader);
                _preloader = null;
            }
        }

        /**
         * Добавить маску
         */
        public function addMaskAnimation():void
        {
            _maskClip = super.source.getMovieClip(ScreenData.TRANSITION_MASK);
            _maskClip.width = 20;
            _maskClip.height = 20;
            _maskClip.x = 390;
            _maskClip.y = 390;

            addChild(_maskClip);
            this.mask = _maskClip;


            var params:Object = {
                onComplete: _maskTweenComplete,
                width: 4000,
                height: 4000,
                x: -1600,
                y: -1650,
                ease: Linear.easeIn
            };

            TweenLite.to(_maskClip, 1.5, params);
        }

        public function delayedDestroy(milliseconds:Number):void
        {
            _delayedTimer = new Timer(milliseconds, 1);
            _delayedTimer.addEventListener(TimerEvent.TIMER_COMPLETE, _timerCompleteHandler);
            _delayedTimer.start();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/

        /**
         * Установить тень за основным фоном
         */
        protected function _setBgShadow():void
        {
            _bgShadow = new Sprite();
            addChild(_bgShadow);
            _bgShadow.graphics.beginFill(0x000000, .5);
            _bgShadow.graphics.drawRect(0, 0, ApplicationConfiguration.APP_WIDTH, ApplicationConfiguration.APP_HEIGHT);
            _bgShadow.graphics.endFill();
            _bgShadow.addEventListener(MouseEvent.CLICK, _onBgShadowClick);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _delayedTimer:Timer;

        private var _preloader:PreloaderScreen;
        private var _maskClip:MovieClip;
        private var _bgShadow:Sprite;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _maskTweenComplete():void
        {
            removeChild(_maskClip);
            _maskClip = null;
            this.mask = null;
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        private function _timerCompleteHandler(event:TimerEvent):void
        {
            _delayedTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, _timerCompleteHandler);
            _delayedTimer.stop();
            _delayedTimer = null;
            destroy();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _onBgShadowClick(e:MouseEvent):void
        {
            if (e.target === e.currentTarget)
                this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get textFactory():TextFactory
        {
            return TextFactory.instance;
        }
    }

}