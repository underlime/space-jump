package application.views.base.scroll 
{
    import framework.view.View;

    /**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class ScrollTrack extends View
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function ScrollTrack(height:Number) 
		{
			_scrollHeight = height;
			super();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function render():void 
		{
			this.update();
		}
		
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		
		public var trackWidth:Number = 12;
		public var trackBackground:uint = 0x054d72;
		public var trackBorder:uint = 0x3aa0cc;
		
		// PUBLIC METHODS ----------------------------------------------------------------------/
		
		public function update():void
		{
			this.graphics.clear();
			this.graphics.beginFill(this.trackBackground);
			this.graphics.lineStyle(1, this.trackBorder);
			this.graphics.drawRect(0, 0, trackWidth, _scrollHeight);
			this.graphics.endFill();
		}
		
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		
		private var _scrollHeight:Number;
		
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
		
		public function set scrollHeight(value:Number):void 
		{
			if (_scrollHeight != value)
			{
				_scrollHeight = value;
				this.update();
			}
		}
	}

}