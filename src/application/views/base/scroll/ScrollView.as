package application.views.base.scroll
{
    import application.views.base.ApplicationView;

    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;
    import framework.view.scroll.Scroll;
    import framework.view.scroll.ScrollEvent;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ScrollView extends ApplicationView
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ScrollView(scrollerRectangle:Rectangle, contentRectangle:Rectangle)
        {
            _scrollerRectangle = scrollerRectangle;
            _contentRectangle = contentRectangle;
            _startY = _contentRectangle.y;

            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            this.addChild(_scrollContent);
        }

        override public function destroy():void
        {
            if (_scrollbar)
                _scrollbar.destroy();

            _scrollContent.destroy();
            _scrollbar.destroy();
            _scrollContent.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        private var _scrollContent:View = new View();
        private var _startY:Number;
        private var _totalHeight:Number;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function setupScroll():void
        {
            _totalHeight = _scrollContent.height;
            _scrollContent.scrollRect = _contentRectangle;
            _createSlider();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _scrollbar:Scroll;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _createSlider():void
        {
            var marker:ScrollMarker = new ScrollMarker();
            var track:ScrollTrack = new ScrollTrack(_scrollerRectangle.height);

            _scrollbar = new Scroll(marker, track, _scrollerRectangle);
            _scrollbar.rectangle.height -= marker.markerHeight;
            _scrollbar.addEventListener(ScrollEvent.CHANGE, _sliderEventHandler);
            this.addChild(_scrollbar);

            _scrollContent.addEventListener(MouseEvent.MOUSE_WHEEL, _wheelEventHandler);
        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _scrollerRectangle:Rectangle;
        private var _contentRectangle:Rectangle;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _wheelEventHandler(e:MouseEvent):void
        {
            _scrollbar.percent += ( -1)*e.delta;
        }

        private function _sliderEventHandler(e:ScrollEvent):void
        {
            var percent:Number = e.percent;
            _contentRectangle.y = ((_totalHeight - _contentRectangle.height)*(percent/100)) + _startY;
            _scrollContent.scrollRect = _contentRectangle;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get scrollContent():View
        {
            return _scrollContent;
        }
    }

}