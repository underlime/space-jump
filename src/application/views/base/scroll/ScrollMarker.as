package application.views.base.scroll 
{
    import flash.display.Bitmap;

    import framework.view.View;

    /**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class ScrollMarker extends View
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function ScrollMarker() 
		{
			super();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function setup():void 
		{
			_marker = this.source.getBitmap(ScrollData.SCROLLER);
		}
		
		override public function render():void 
		{
			this.addChild(_marker);
		}
		
		override public function destroy():void 
		{
			this.removeChild(_marker);
			super.destroy();
		}
		
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		
		public var markerHeight:Number = 186;
		
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		
		private var _marker:Bitmap;
		
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
	}

}