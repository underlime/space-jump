/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.06.13
 * Time: 17:04
 */
package application.views.debug
{
    import application.events.ApplicationEvent;
    import application.models.StatisticsGameModel;

    import framework.view.View;
    import framework.view.text.SimpleTextField;

    public class TimeDebugView extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TimeDebugView(statistics:StatisticsGameModel)
        {
            super();
            _statistics = statistics;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _setupBackground();
            _setupTextField();
            _statistics.addEventListener(ApplicationEvent.TIME_UPDATE, _updateTime);
        }

        override public function destroy():void
        {
            graphics.clear();
            _statistics.removeEventListener(ApplicationEvent.TIME_UPDATE, _updateTime);
            _txtDebug.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _statistics:StatisticsGameModel;
        private var _txtDebug:SimpleTextField;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupBackground():void
        {
            graphics.beginFill(0x000000, 1);
            graphics.drawRect(0, 0, 50, 60);
            graphics.endFill();
        }

        private function _setupTextField():void
        {
            _txtDebug = new SimpleTextField();
            _txtDebug.fontFamily = SimpleTextField.PT_SANS;
            _txtDebug.color = 0xffffff;
            _txtDebug.size = 8;
            addChild(_txtDebug);
        }

        private function _updateText():void
        {
            var text:String = "S: " + _statistics.seconds.toString() + "\n";
            text += "P: " + _statistics.pause_seconds.toString() + "\n";
            text += "B: " + _statistics.bonus_seconds.toString() + "\n";
            text += "PT: " + _statistics.physicsTicks.toString() + "\n";

            _txtDebug.text = text;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _updateTime(event:ApplicationEvent):void
        {
            _updateText();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
