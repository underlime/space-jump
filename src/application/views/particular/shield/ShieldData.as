package application.views.particular.shield
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ShieldData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BIG_SHIELD:String = "SPACE_BigShield";
        public static const BIG_SHIELD_FRAMES:int = 29;

        public static const SHIELD_FRAME:String = "SPACE_SmallShield";
        public static const SHIELD_FRAMES_COUNT:int = 19;

        public static const SHIELD_PICKUP:String = "SPACE_pickUp_shield_0";
        public static const SHIELD_PICKUP_COUNT:int = 11;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}