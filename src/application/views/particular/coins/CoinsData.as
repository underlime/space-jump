/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.12
 */

package application.views.particular.coins
{

    public class CoinsData
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SILVER_COINS_FRAME:String = "SPACE_SilverCoin";
        public static const SILVER_COINS_COUNT:int = 19;

        public static const GOLD_COINS_FRAME:String = "SPACE_GoldCoin";
        public static const GOLD_COINS_COUNT:int = 19;

        public static const COPPER_COINS_FRAME:String = "SPACE_CopperCoin";
        public static const COPPER_COINS_COUNT:int = 19;

        public static const COINS_PICKUP:String = "SPACE_PICKUP_COINS_0";
        public static const COINS_PICKUP_COUNT:int = 12;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
