/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.12
 */

package application.views.particular.coins
{

    import application.views.particular.factory.FramesFactory;

    import framework.view.animation.Animation;

    public class GoldCoinsAnimation extends Animation
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GoldCoinsAnimation()
        {
            super(FramesFactory.getAnimation(CoinsData.GOLD_COINS_FRAME, CoinsData.GOLD_COINS_COUNT));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
