/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.10.12
 */

package application.views.particular.cosmonaut
{

    import flash.display.MovieClip;
    import flash.events.Event;

    import framework.helpers.MathHelper;
    import framework.view.View;

    public class MainScreenCosmonaut extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MainScreenCosmonaut()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._setStayCosmonaut();
        }

        override public function render():void
        {

        }

        override public function destroy():void
        {
            this.removeChild(this._cosmonaut);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function jump():void
        {
            this._cosmonaut.addEventListener(Event.ENTER_FRAME, _checkCosmonautClip);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _cosmonaut:MovieClip;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setStayCosmonaut():void
        {
            if (this._cosmonaut && this.contains(this._cosmonaut)) {
                this.removeChild(this._cosmonaut);
            }
            this._cosmonaut = this.source.getMovieClip(MainScreenCosmonautData.STAY);
            this.addChild(this._cosmonaut);
        }

        private function _replaceCosmonaut():void
        {
            this.removeChild(this._cosmonaut);
            this._cosmonaut = this.source.getMovieClip(MainScreenCosmonautData.JUMP + MathHelper.random(1, MainScreenCosmonautData.JUMP_VARIATIONS).toString());
            this._cosmonaut.gotoAndStop(1);
            this._cosmonaut.addEventListener(Event.ENTER_FRAME, _checkJumpComplete);
            this._cosmonaut.play();
            this.addChild(this._cosmonaut);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _checkCosmonautClip(event:Event):void
        {
            if (this._cosmonaut.currentFrame == this._cosmonaut.totalFrames) {
                this._cosmonaut.removeEventListener(Event.ENTER_FRAME, _checkCosmonautClip);
                this._replaceCosmonaut();
            }
        }

        private function _checkJumpComplete(event:Event):void
        {
            if (this._cosmonaut.currentFrame == this._cosmonaut.totalFrames) {
                this._cosmonaut.removeEventListener(Event.ENTER_FRAME, _checkJumpComplete);
                super.dispatchEvent(new Event(Event.COMPLETE));
                this._setStayCosmonaut();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
