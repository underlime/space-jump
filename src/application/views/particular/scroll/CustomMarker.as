package application.views.particular.scroll
{
    import framework.view.View;

    public class CustomMarker extends View
    {
        public static const BACKGROUND_COLOR:int = 0x1B72A1;
        public static const BORDER_COLOR:int = 0x3AA0CC;

        private var _width:int;
        private var _height:int;

        public function CustomMarker(width:int, height:int)
        {
            super();
            _width = width;
            _height = height;
        }

        override public function render():void
        {
            _setColors();
        }

        private function _setColors():void
        {
            this.graphics.clear();

            this.graphics.beginFill(BORDER_COLOR);
            this.graphics.drawRect(0, 0, _width, _height);
            this.graphics.endFill();

            this.graphics.beginFill(BACKGROUND_COLOR);
            this.graphics.drawRect(1, 1, _width - 2, _height - 2);
            this.graphics.endFill();
        }
    }
}
