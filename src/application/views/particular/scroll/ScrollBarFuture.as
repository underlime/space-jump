package application.views.particular.scroll
{
    import application.views.base.scroll.ScrollTrack;

    import flash.geom.Rectangle;

    import framework.view.View;
    import framework.view.scroll_future.Scroll;

    public class ScrollBarFuture extends Scroll
    {
        public static const SCROLL_BAR_WIDTH:int = 13;

        protected var _height:int;
        private var _scrollerRectangle:Rectangle;

        public function ScrollBarFuture(height:int)
        {
            _height = height;

            var markerHeight:int = 50;
            var marker:View = new CustomMarker(SCROLL_BAR_WIDTH - 2, markerHeight);
            marker.x = 1;
            marker.y = 1;
            var track:ScrollTrack = new ScrollTrack(_height);

            _scrollerRectangle = new Rectangle(1, 1, 0, _height - markerHeight - 1);
            super(marker, track, _scrollerRectangle);
        }
    }
}
