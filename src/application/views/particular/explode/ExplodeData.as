package application.views.particular.explode
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ExplodeData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const TNT_EXPLODE:String = "TNT_EXPLODE";
        public static const TNT_EXPLODE_FRAMES_COUNT:int = 9;

        public static const WOOD_DUST:String = "SPACE_WoodBoxDust";
        public static const WOOD_DUST_COUNT:int = 9;

        public static const IMPACT:String = "SPACE_Impact";
        public static const IMPACT_COUNT:int = 5;


        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}