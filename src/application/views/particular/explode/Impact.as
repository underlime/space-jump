package application.views.particular.explode
{
    import application.views.particular.factory.FramesFactory;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class Impact extends EndingAnimation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Impact()
        {
            super(FramesFactory.getAnimation(ExplodeData.IMPACT, ExplodeData.IMPACT_COUNT));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function set x(value:Number):void
        {
            super.x = value - 25;
        }

        override public function set y(value:Number):void
        {
            super.y = value - 25;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}