/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 11.11.12
 */

package application.views.particular.explode
{

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.view.animation.Animation;

    public class EndingAnimation extends Animation
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EndingAnimation(frames:Vector.<Bitmap>)
        {
            super(frames);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/

        override protected function _enterFrameHandler(e:Event):void
        {
            super._enterFrameHandler(e);

            if (super.currentFrame == super.totalFrames) {
                super.dispatchEvent(new Event(Event.COMPLETE));
                this.destroy();
            }
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
