package application.views.particular.explode
{
    import application.views.particular.birds.BirdData;
    import application.views.particular.factory.FramesFactory;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class WhiteBirdExplodeAnimation extends EndingAnimation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        public function WhiteBirdExplodeAnimation()
        {
            super(FramesFactory.getAnimation(BirdData.WHITE_BIRD_EXPLODE, BirdData.WHITE_BIRD_EXPLODE_COUNT));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}