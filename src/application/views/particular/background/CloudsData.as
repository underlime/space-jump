/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.10.12
 */

package application.views.particular.background
{

    public class CloudsData
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BRIGHT_CLOUD_LAYER_1:String = "SPACE_BrightCloudLayer1";
        public static const BRIGHT_CLOUD_LAYER_1_COUNT:int = 3;

        public static const BRIGHT_CLOUD_LAYER_2:String = "SPACE_BrightCloudLayer2";
        public static const BRIGHT_CLOUD_LAYER_2_COUNT:int = 3;

        public static const BRIGHT_CLOUD_LAYER_3:String = "SPACE_BrightCloudLayer3";
        public static const BRIGHT_CLOUD_LAYER_3_COUNT:int = 3;

        public static const BRIGHT_CLOUD_LAYER_4:String = "SPACE_BrightCloudLayer4";
        public static const BRIGHT_CLOUD_LAYER_4_COUNT:int = 3;

        public static const DARK_CLOUD_LAYER_1:String = "SPACE_DarkCloudLayer1";
        public static const DARK_CLOUD_LAYER_1_COUNT:int = 3;

        public static const DARK_CLOUD_LAYER_2:String = "SPACE_DarkCloudLayer2";
        public static const DARK_CLOUD_LAYER_2_COUNT:int = 3;

        public static const DARK_CLOUD_LAYER_3:String = "SPACE_DarkCloudLayer3";
        public static const DARK_CLOUD_LAYER_3_COUNT:int = 3;

        public static const DARK_CLOUD_LAYER_4:String = "SPACE_DarkCloudLayer4";
        public static const DARK_CLOUD_LAYER_4_COUNT:int = 3;

        public static const ASTEROID_LAYER_1:String = "SPACE_AsteroidLayer1";
        public static const ASTEROID_LAYER_1_COUNT:int = 3;

        public static const ASTEROID_LAYER_2:String = "SPACE_AsteroidLayer2";
        public static const ASTEROID_LAYER_2_COUNT:int = 3;

        public static const ASTEROID_LAYER_3:String = "SPACE_AsteroidLayer3";
        public static const ASTEROID_LAYER_3_COUNT:int = 2;

        public static const ASTEROID_LAYER_4:String = "SPACE_AsteroidLayer4";
        public static const ASTEROID_LAYER_4_COUNT:int = 3;

        public static const STARS_LAYER:String = "SPACE_Stars";

        public static const MOON_LAYER:String = "SPACE_MoonBackground";
        public static const EARTH_LAYER:String = "SPACE_EarthLayer";

        public static const BONUS_LAYER_1_DATA:String = "SPACE_abstrakt_1_";
        public static const BONUS_LAYER_1_DATA_COUNT:int = 7;

        public static const BONUS_LAYER_2_DATA:String = "SPACE_abstrakt_2_";
        public static const BONUS_LAYER_2_DATA_COUNT:int = 7

        public static const BONUS_LAYER_3_DATA:String = "SPACE_abstrakt_3_";
        public static const BONUS_LAYER_3_DATA_COUNT:int = 4

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
