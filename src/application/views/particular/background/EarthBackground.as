package application.views.particular.background
{
    import application.config.DataPhysics;
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.data.ModelsRegistry;
    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class EarthBackground extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function EarthBackground()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _earth = this.source.getBitmap(CloudsData.EARTH_LAYER, true);
            _model = ModelsRegistry.getModel(ModelData.GAME_MODEL) as GameModel;
            _statisticsModel = _model.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;
        }

        override public function render():void
        {
            this.addChild(_earth);
            _earth.x = 0;
            _earth.y = DataPhysics.WORLD_HEIGHT;

            _min = DataPhysics.WORLD_HEIGHT - _earth.height;
        }

        override public function destroy():void
        {
            this.removeEventListener(Event.ENTER_FRAME, _updateEarthPosition);
            if (_earth.parent) {
                this.removeChild(_earth);
            }
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function show():void
        {
            _showing = true;
            this.addEventListener(Event.ENTER_FRAME, _updateEarthPosition);
        }

        public function pause():void
        {
            if (_showing)
                this.removeEventListener(Event.ENTER_FRAME, _updateEarthPosition);
        }

        public function resume():void
        {
            if (_showing)
                this.addEventListener(Event.ENTER_FRAME, _updateEarthPosition);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _earth:Bitmap;
        private var _model:GameModel;
        private var _deltaY:Number = 20;
        private var _statisticsModel:StatisticsGameModel;
        private var _showing:Boolean = false;

        private var _min:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _updateEarthPosition(e:Event):void
        {
            if (_model.velocity == 0)
                return;

            var iterations:int = Math.floor(_statisticsModel.height / (_model.velocity * DataPhysics.TIME_STEP));
            var delta:Number = (_earth.height - _deltaY) / iterations;

            if (_deltaY > _earth.height || iterations <= 0)
                return;

            _earth.y -= delta;
            _deltaY += delta;

            if (_earth.y < _min)
                _earth.y = _min;
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}