/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.10.12
 */

package application.views.particular.background
{
    import application.config.DataPhysics;

    import com.greensock.TweenLite;
    import com.greensock.plugins.HexColorsPlugin;
    import com.greensock.plugins.TweenPlugin;

    import flash.display.Bitmap;

    import framework.helpers.MathHelper;
    import framework.view.View;

    public class TweenBackground extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public const MASK_VARIANTS:Array = ["SPACE_BG_MASK_1", "SPACE_BG_MASK_2", "SPACE_BG_MASK_3"];

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TweenBackground()
        {
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            TweenPlugin.activate([HexColorsPlugin]);
            var maskIndex:int = MathHelper.random(0, MASK_VARIANTS.length - 1);
            _mask = super.source.getBitmap(MASK_VARIANTS[maskIndex]);
            _stars = super.source.getBitmap(CloudsData.STARS_LAYER);
        }

        override public function render():void
        {
            _updateBackground();
            addChild(_mask);
            addChild(_stars);
        }

        override public function destroy():void
        {
            this.graphics.clear();
        }

        private function _tweenBackground():void
        {
            var params:Object = {
                "hexColors": { hex: _hexTargets[_currentColor] },
                "onUpdate": _updateBackground
            };
            TweenLite.to(_backColor, _delay, params);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function setAsteroidLayer():void
        {
            _backColor.hex = 0x000000;
            _currentColor = 0;
            _tweenBackground();
        }

        public function setDarkLayer():void
        {
            _backColor.hex = _hexTargets[0];
            _currentColor = 1;
            _tweenBackground();
        }

        public function setBrightLayer():void
        {
            _backColor.hex = _hexTargets[1];
            _currentColor = 2;
            _tweenBackground();

            var params:Object = {
                "alpha": 0,
                "onComplete": _deleteStars
            };

            TweenLite.to(_stars, _starsDelay, params);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _delay:Number = 2;
        private var _backColor:Object = {"hex": 0x000000};
        private var _currentColor:int = 0;
        private var _hexTargets:Array = [0x09131c, 0x104e9b, 0x73d6ef];
        private var _mask:Bitmap;
        private var _stars:Bitmap;
        private var _starsDelay:Number = 20;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateBackground():void
        {
            this.graphics.clear();
            this.graphics.beginFill(_backColor.hex);
            this.graphics.drawRect(0, 0, DataPhysics.WORLD_WIDTH, DataPhysics.WORLD_HEIGHT);
            this.graphics.endFill();
        }

        private function _deleteStars():void
        {
            _stars.alpha = 1;
            removeChild(_stars);
            _stars = null;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
