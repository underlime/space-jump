package application.views.particular.background.layers
{
    import application.views.particular.background.CloudsData;

    import flash.display.BitmapData;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ParallaxLayer1 extends ParallaxBackgroundLayer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ParallaxLayer1(velocity:Number)
        {
            super(velocity);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _delay = 8;
            _k_velocity = 1 / 7;
            _maxCloudsInLine = 10;
            super.setup();
        }

        override public function setAsteroids():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.ASTEROID_LAYER_1_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.ASTEROID_LAYER_1 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setBrightClouds():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.BRIGHT_CLOUD_LAYER_1_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.BRIGHT_CLOUD_LAYER_1 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setDarkClouds():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.DARK_CLOUD_LAYER_1_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.DARK_CLOUD_LAYER_1 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setBonus():void
        {

        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}