package application.views.particular.background.layers
{
    import application.views.particular.background.CloudsData;

    import flash.display.BitmapData;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ParallaxLayer4 extends ParallaxBackgroundLayer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ParallaxLayer4(velocity:Number)
        {
            super(velocity);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _delay = 2;
            _k_velocity = 4.5;
            _maxCloudsInLine = 3;
            super.setup();
        }

        override public function setAsteroids():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.ASTEROID_LAYER_4_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.ASTEROID_LAYER_4 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setBrightClouds():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.BRIGHT_CLOUD_LAYER_4_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.BRIGHT_CLOUD_LAYER_4 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setDarkClouds():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.DARK_CLOUD_LAYER_4_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.DARK_CLOUD_LAYER_4 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setBonus():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.BONUS_LAYER_3_DATA_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.BONUS_LAYER_3_DATA + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
            _delay = .2;
            _k_velocity = 4;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}