/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.05.13
 * Time: 11:44
 */
package application.views.particular.background.layers
{
    import application.config.DataPhysics;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.helpers.MathHelper;
    import framework.view.View;

    public class ParallaxBackground extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ParallaxBackground(velocity:Number)
        {
            _velocity = velocity;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _createObjects();
        }

        override public function render():void
        {
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function clear():void
        {
            // TODO удалить все объекты
        }

        public function pause():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        public function resume():void
        {
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        public function setBrightClouds():void
        {
            throw new Error("Method must be overriden");
        }

        public function setDarkClouds():void
        {
            throw new Error("Method must be overriden");
        }

        public function setAsteroids():void
        {
            throw new Error("Method must be overriden");
        }

        public function setBonus():void
        {
            throw new Error("Method must be overriden");
        }


        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _maxObjects:int = 3;
        protected var _objects:Vector.<Bitmap> = new Vector.<Bitmap>();
        protected var _cloudSpeed:Number;
        protected var _dataObjects:Vector.<String> = new Vector.<String>();
        protected var _velocity:Number = 0;
        protected var _distance:Number = 0;
        protected var _k_velocity:Number = 1;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _createObjects():void
        {
            var length:int = _dataObjects.length;
            var cy:Number = DataPhysics.WORLD_HEIGHT;

            if (length > 0) {
                for (var i:int = 0; i < _maxObjects; i++) {
                    var random:int = MathHelper.random(0, length - 1);
                    var bitmap:Bitmap = super.source.getBitmap(_dataObjects[random]);
                    bitmap.y = cy;
                    cy += _distance;

                    _objects.push(bitmap);
                    _setHorizontal(bitmap);
                }
            }
        }

        private function _setHorizontal(object:Bitmap):void
        {
            var semi:int = int(object.width / 2);
            object.x = MathHelper.random((-1) * semi, DataPhysics.WORLD_WIDTH - semi);
        }

        private function _moveObject():void
        {

        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _enterFrameHandler(event:Event):void
        {
            _moveObject();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set velocity(value:Number):void
        {
            _velocity = value;
        }
    }
}
