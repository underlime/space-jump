package application.views.particular.background.layers
{
    import application.views.particular.background.CloudsData;

    import flash.display.BitmapData;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ParallaxLayer3 extends ParallaxBackgroundLayer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ParallaxLayer3(velocity:Number)
        {
            super(velocity);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._delay = 2;
            this._k_velocity = 5 / 7;
            this._maxCloudsInLine = 5;
            super.setup();
        }

        override public function setAsteroids():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.ASTEROID_LAYER_3_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.ASTEROID_LAYER_3 + i.toString()));
            }
            this._currentBitmapDataHash = bitmapDataHash;
        }

        override public function setBrightClouds():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.BRIGHT_CLOUD_LAYER_3_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.BRIGHT_CLOUD_LAYER_3 + i.toString()));
            }
            this._currentBitmapDataHash = bitmapDataHash;
        }

        override public function setDarkClouds():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.DARK_CLOUD_LAYER_3_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.DARK_CLOUD_LAYER_3 + i.toString()));
            }
            this._currentBitmapDataHash = bitmapDataHash;
        }

        override public function setBonus():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.BONUS_LAYER_2_DATA_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.BONUS_LAYER_2_DATA + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
            _delay = .5;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}