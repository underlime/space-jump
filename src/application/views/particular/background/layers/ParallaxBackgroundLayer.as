package application.views.particular.background.layers
{
    import application.config.DataPhysics;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.PixelSnapping;
    import flash.events.Event;

    import framework.helpers.MathHelper;
    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ParallaxBackgroundLayer extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ParallaxBackgroundLayer(velocity:Number)
        {
            _velocity = velocity;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            setAsteroids();
            _createCloud();
            _recalcCloudSpeed();
        }

        override public function render():void
        {
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        override public function destroy():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            _deleteClouds();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function clear():void
        {
            var size:int = _clouds.length;
            _deleteClouds();
        }

        public function pause():void
        {
            removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        public function resume():void
        {
            addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
        }

        public function setBrightClouds():void
        {
            throw new Error("Method must be overriden");
        }

        public function setDarkClouds():void
        {
            throw new Error("Method must be overriden");
        }

        public function setAsteroids():void
        {
            throw new Error("Method must be overriden");
        }

        public function setBonus():void
        {
            throw new Error("Method must be overriden");
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _cloudsCount:int = 1;
        protected var _clouds:Vector.<Bitmap> = new Vector.<Bitmap>();
        protected var _velocity:Number;
        protected var _cloudSpeed:Number;
        protected var _data:String;
        protected var _maxCloudsInLine:int = 5;
        protected var _currentBitmapDataHash:Vector.<BitmapData>;

        /**
         * Задержка генерации следующего облака (сек)
         */
        protected var _delay:Number = 0;

        /**
         * Коэффициент ускорения
         */
        protected var _k_velocity:Number = 1;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _currentDelay:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _recalcCloudSpeed():void
        {
            _cloudSpeed = (_velocity * DataPhysics.TIME_STEP) * _k_velocity;
        }

        private function _createCloud():void
        {
            var index:int = MathHelper.random(0, _currentBitmapDataHash.length - 1);
            var cloud:Bitmap = new Bitmap(_currentBitmapDataHash[index], PixelSnapping.AUTO, true);

            _clouds.push(cloud);
            addChild(cloud);
            cloud.y = DataPhysics.WORLD_HEIGHT;
            var semiCloud:Number = cloud.width / 2;
            cloud.x = MathHelper.random((-1) * semiCloud, DataPhysics.WORLD_WIDTH - semiCloud);
        }

        private function _checkCloudPosition():void
        {
            for (var i:int = 0; i < _clouds.length; i++) {
                if (_clouds[i].y + _clouds[i].height + 3 < 0) {
                    _deleteCloud(i);
                }
            }
        }

        private function _deleteCloud(index:int):void
        {
            try {
                removeChild(_clouds[index]);
                _clouds[index] = null;
                _clouds.splice(index, 1);
            } catch (e:Error) {
            }
        }

        private function _moveClouds():void
        {
            var size:int = _clouds.length;
            for (var i:int = 0; i < size; i++) {
                _clouds[i].y -= _cloudSpeed;
            }
        }

        private function _tryGenerateCloud():void
        {
            _currentDelay += DataPhysics.TIME_STEP;
            if (_currentDelay >= _delay) {
                _currentDelay = 0;
                _createCloud();
            }
        }

        private function _recalcDelay(newVelocity:Number):void
        {
            var koeff:Number = newVelocity / _velocity;
            _delay = (_delay) / koeff;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _enterFrameHandler(event:Event):void
        {
            _moveClouds();
            _checkCloudPosition();
            _tryGenerateCloud();
        }

        private function _deleteClouds():void
        {
            var size:int = _clouds.length;
            for (var i:int = 0; i < size; i++) {
                _deleteCloud(i);
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set velocity(value:Number):void
        {
            if (_velocity != value) {
                _recalcDelay(value);
                _velocity = value;
                _recalcCloudSpeed();
            }
        }
    }

}