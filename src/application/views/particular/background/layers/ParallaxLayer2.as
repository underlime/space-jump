package application.views.particular.background.layers
{
    import application.views.particular.background.CloudsData;

    import flash.display.BitmapData;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class ParallaxLayer2 extends ParallaxBackgroundLayer
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ParallaxLayer2(velocity:Number)
        {
            super(velocity);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _delay = 4;
            _k_velocity = 3 / 7;
            _maxCloudsInLine = 8;
            super.setup();
        }

        override public function setAsteroids():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.ASTEROID_LAYER_2_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.ASTEROID_LAYER_2 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setBrightClouds():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.BRIGHT_CLOUD_LAYER_2_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.BRIGHT_CLOUD_LAYER_2 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setDarkClouds():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.DARK_CLOUD_LAYER_2_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.DARK_CLOUD_LAYER_2 + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
        }

        override public function setBonus():void
        {
            var bitmapDataHash:Vector.<BitmapData> = new Vector.<BitmapData>();
            for (var i:int = 1; i <= CloudsData.BONUS_LAYER_1_DATA_COUNT; i++) {
                bitmapDataHash.push(this.source.getBitmapData(CloudsData.BONUS_LAYER_1_DATA + i.toString()));
            }
            _currentBitmapDataHash = bitmapDataHash;
            _delay = 1;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}