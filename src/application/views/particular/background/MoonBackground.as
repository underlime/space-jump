package application.views.particular.background
{
    import application.config.DataPhysics;

    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MoonBackground extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MoonBackground(velocity:Number, yPosition:Number = 150)
        {
            this.velocity = velocity;
            this._startPosition = yPosition;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            this._moon = this.source.getBitmap(CloudsData.MOON_LAYER);
        }

        override public function render():void
        {
            this.addChild(this._moon);
            this._moon.x = DataPhysics.WORLD_WIDTH - this._moon.width - 50;
            this._moon.y = this._startPosition;

            this.addEventListener(Event.ENTER_FRAME, this._updateMoonPosition);
        }

        override public function destroy():void
        {
            this.removeEventListener(Event.ENTER_FRAME, this._updateMoonPosition);
            this.removeChild(this._moon);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function pause():void
        {
            this.removeEventListener(Event.ENTER_FRAME, this._updateMoonPosition);
        }

        public function resume():void
        {
            this.addEventListener(Event.ENTER_FRAME, this._updateMoonPosition);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _velocity:Number;
        private var _moon:Bitmap;
        private var _speed:Number;
        private var _k_velocity:Number = .02;
        private var _startPosition:Number = 150;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _updateMoonPosition(e:Event):void
        {
            this._moon.y -= this._speed;
            if (this._moon.y + this._moon.height + 3 < 0) {
                this.destroy();
            }
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function set velocity(value:Number):void
        {
            this._velocity = value;
            this._speed = this._velocity * DataPhysics.TIME_STEP * this._k_velocity;
        }
    }

}