package application.views.particular.fitil
{
    import application.views.particular.boxes.BoxesData;

    import flash.display.Bitmap;

    import framework.tools.SourceManager;
    import framework.view.animation.Animation;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class FitilAnimation extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function FitilAnimation()
        {
            var frames:Vector.<Bitmap> = new Vector.<Bitmap>();

            var normalFrame:Bitmap = SourceManager.instance.getBitmap(BoxesData.FITIL, true);

            var scaledFrame:Bitmap = SourceManager.instance.getBitmap(BoxesData.FITIL, true);
            scaledFrame.scaleX = 0.8;
            scaledFrame.scaleY = 0.8;
            scaledFrame.x = normalFrame.width / 2 - scaledFrame.width / 2;
            scaledFrame.y = normalFrame.height / 2 - scaledFrame.height / 2;

            frames.push(normalFrame, scaledFrame);

            super(frames);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}