package application.views.particular.boxes
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BoxesData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const WOOD_BOX_1:String = "SPACE_WoodBox1";
        public static const WOOD_BOX_2:String = "SPACE_WoodBox2";
        public static const WOOD_BOX_3:String = "SPACE_WoodBox3";
        public static const WOOD_BOX_4:String = "SPACE_WoodBox4";


        public static const METAL_BOX_1:String = "SPACE_MetalBox1";
        public static const METAL_BOX_2:String = "SPACE_MetalBox2";
        public static const METAL_BOX_3:String = "SPACE_MetalBox3";
        public static const METAL_BOX_4:String = "SPACE_MetalBox4";

        public static const TNT_BOX_1:String = "SPACE_TntBox1";
        public static const TNT_BOX_2:String = "SPACE_TntBox2";
        public static const TNT_BOX_3:String = "SPACE_TntBox3";
        public static const TNT_BOX_4:String = "SPACE_TntBox4";

        public static const FITIL:String = "SPACE_TNTBoxFitil";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}