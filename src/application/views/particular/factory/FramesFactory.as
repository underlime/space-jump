/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 29.10.12
 */

package application.views.particular.factory
{

    import flash.display.Bitmap;

    import framework.tools.SourceManager;

    public class FramesFactory
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static var source:SourceManager = SourceManager.instance;

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getAnimation(alias:String, totalFrames:int):Vector.<Bitmap>
        {
            var frames:Vector.<Bitmap> = new Vector.<Bitmap>();
            for (var i:int = 1; i <= totalFrames; i++) {
                frames.push(FramesFactory.source.getBitmap(alias + i.toString(), true));
            }
            return frames;
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
