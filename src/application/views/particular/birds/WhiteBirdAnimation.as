package application.views.particular.birds
{
    import application.views.particular.factory.FramesFactory;

    import framework.view.animation.Animation;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class WhiteBirdAnimation extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function WhiteBirdAnimation()
        {
            super(FramesFactory.getAnimation(BirdData.WHITE_BIRD, BirdData.WHITE_BIRD_FRAMES));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}