package application.views.particular.birds
{
    import application.views.particular.factory.FramesFactory;

    import framework.view.animation.Animation;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class RedBirdAnimation extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RedBirdAnimation()
        {
            super(FramesFactory.getAnimation(BirdData.RED_BIRD, BirdData.RED_BIRD_FRAMES));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}