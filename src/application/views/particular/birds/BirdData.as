package application.views.particular.birds
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class BirdData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BLACK_BIRD:String = "SPACE_BlackBird";
        public static const BLACK_BIRD_FRAMES:int = 7;
        public static const BLACK_BIRD_EXPLODE:String = "SPACE_BlackBirdExplode";
        public static const BLACK_BIRD_EXPLODE_COUNT:int = 10;


        public static const RED_BIRD:String = "SPACE_RedBird";
        public static const RED_BIRD_FRAMES:int = 7;
        public static const RED_BIRD_EXPLODE:String = "SPACE_RedBirdExplode";
        public static const RED_BIRD_EXPLODE_COUNT:int = 10;

        public static const WHITE_BIRD:String = "SPACE_WhiteBird";
        public static const WHITE_BIRD_FRAMES:int = 7;
        public static const WHITE_BIRD_EXPLODE:String = "SPACE_WhiteBirdExplode";
        public static const WHITE_BIRD_EXPLODE_COUNT:int = 10;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}