/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.02.13
 * Time: 15:06
 */
package application.views.particular.powerup
{
    import application.views.particular.factory.FramesFactory;

    import framework.view.animation.Animation;

    public class SmallRocketFlyAnimation extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/


        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SmallRocketFlyAnimation()
        {
            super(FramesFactory.getAnimation(RocketsData.SMALL_ROCKET_FRAME, RocketsData.SMALL_ROCKET_FRAME_COUNT));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
