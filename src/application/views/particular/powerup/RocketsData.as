/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.02.13
 * Time: 15:07
 */
package application.views.particular.powerup
{
    public class RocketsData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const SMALL_ROCKET_FRAME:String = "SPACE_MEGAROCKET1";
        public static const SMALL_ROCKET_FRAME_COUNT:int = 2;

        public static const SMALL_ROCKET_END:String = "SPACE_MEGAROCKET_END1";
        public static const SMALL_ROCKET_END_COUNT:int = 3;

        public static const BIG_ROCKET_FRAME:String = "SPACE_MEGAROCKET_PLUS";
        public static const BIG_ROCKET_FRAME_COUNT:int = 2;

        public static const BIG_ROCKET_END:String = "SPACE_MEGAROCKET_PLUS_END";
        public static const BIG_ROCKET_END_COUNT:int = 3;

        public static const ROCKET_WIDTH:int = 200;
        public static const ROCKET_HEIGHT:int = 300;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
