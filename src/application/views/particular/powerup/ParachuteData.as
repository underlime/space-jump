/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.02.13
 * Time: 11:41
 */
package application.views.particular.powerup
{
    public class ParachuteData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const INIT_FRAME:String = "SPACE_PARASHUT_INIT"
        public static const INIT_FRAME_COUNT:int = 4;

        public static const OPENED_FRAME:String = "SPACE_PARASHUT_OPENED";

        public static const CLOSE_FRAME:String = "SPACE_PARASHUT_CLOSE";
        public static const CLOSE_FRAME_COUNT:int = 4;

        public static const PARACHUTE_WIDTH:Number = 200;
        public static const PARACHUTE_HEIGHT:Number = 150;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
