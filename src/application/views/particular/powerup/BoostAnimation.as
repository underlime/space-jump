/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 13.02.13
 * Time: 15:16
 */
package application.views.particular.powerup
{
    import application.sound.SoundManager;
    import application.sound.libs.LoopSounds;
    import application.views.particular.factory.FramesFactory;

    import flash.media.SoundChannel;

    import framework.view.animation.BlinkAnimation;

    public class BoostAnimation extends BlinkAnimation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BOOST_FRAME:String = "SPACE_BOOSTER";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BoostAnimation(seconds:int = 10)
        {
            super(FramesFactory.getAnimation(BOOST_FRAME, 2), seconds);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _channel = SoundManager.instance.playLoopSound(LoopSounds.ROCKER_FLY);
        }

        override public function destroy():void
        {
            if (_channel)
                SoundManager.instance.tweenChannelStop(_channel);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _channel:SoundChannel;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
