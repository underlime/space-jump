/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.11.12
 */

package application.views.particular.petarda
{

    public class PetardaData
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const RED_PETARDA_FRAME:String = "SPACE_RedPetarda";
        public static const RED_PETARDA_COUNT:int = 20;

        public static const GREEN_PETARDA_FRAME:String = "SPACE_GreenPetarda";
        public static const GREEN_PETARDA_COUNT:int = 20;

        public static const PURPLE_PETARDA_FRAME:String = "SPACE_PurplePetarda";
        public static const PURPLE_PETARDA_COUNT:int = 20;

        public static const GREEN_PETARDA_EXPLODE:String = "SPACE_FIREWORKS_GREEN0";
        public static const GREEN_PETARDA_EXPLODE_COUNT:int = 20;

        public static const BLUE_PETARDA_EXPLODE:String = "SPACE_FIREWORKS_BLUE00";
        public static const BLUE_PETARDA_EXPLODE_COUNT:int = 20;

        public static const PURPLE_PETARDA_EXPLODE:String = "SPACE_FIREWORKS_PURPLE0";
        public static const PURPLE_PETARDA_EXPLODE_COUNT:int = 20;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
