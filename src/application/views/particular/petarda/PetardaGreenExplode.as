/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 16.02.13
 * Time: 15:23
 */
package application.views.particular.petarda
{
    import application.views.particular.explode.EndingAnimation;
    import application.views.particular.factory.FramesFactory;

    public class PetardaGreenExplode extends EndingAnimation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function PetardaGreenExplode()
        {
            super(FramesFactory.getAnimation(PetardaData.GREEN_PETARDA_EXPLODE, PetardaData.GREEN_PETARDA_EXPLODE_COUNT));
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
