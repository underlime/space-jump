package application.views.particular.heart
{
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class HeartData
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const HEART_FRAMES_COUNT:int = 19;
        public static const HEART_FRAME:String = "SPACE_Hearth";

        public static const HEART_PICKUP:String = "SPACE_PICKUP_HEARTS_0";
        public static const HEART_PICKUP_COUNT:int = 12;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }

}