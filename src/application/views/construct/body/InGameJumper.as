/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.02.13
 * Time: 16:09
 */
package application.views.construct.body
{
    import application.models.skin.SkinModel;

    import framework.helpers.MathHelper;

    public class InGameJumper extends JumperObject
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const COSMONAUT_VARIATION:int = 5;
        public static const COSMONAUT_FALL:String = "SPACE_HERO_FLY_";
        public static const COSMONAUT_JET:String = "SPACE_HERO_FLY_SUPERJET";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function InGameJumper(model:SkinModel)
        {
            super(model);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            this.update();
        }


        override public function destroy():void
        {
            super._deleteAnimation();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function update():void
        {
            super._deleteAnimation();
            super.animation = super.source.getMovieClip(COSMONAUT_FALL + MathHelper.random(1, COSMONAUT_VARIATION).toString());
            addChild(super.animation);
        }

        public function jet():void
        {
            super._deleteAnimation();
            super.animation = super.source.getMovieClip(COSMONAUT_JET);
            addChild(super.animation);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
