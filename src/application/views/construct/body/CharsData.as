/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.02.13
 * Time: 14:45
 */
package application.views.construct.body
{
    import application.models.skin.SkinModel;

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;

    import framework.tools.SourceManager;

    import org.casalib.core.Destroyable;

    public class CharsData extends Destroyable
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function CharsData(model:SkinModel)
        {
            _model = model;
            _setupChars();
            super();
        }

        private function _setupChars():void
        {
            _body = _source.getMovieClip(_model.body.value);
            _leftShoulder = _source.getMovieClip(_model.left_shoulder.value);
            _leftHand = _source.getMovieClip(_model.left_hand.value);
            _leftHip = _source.getMovieClip(_model.left_hip.value);
            _leftLeg = _source.getMovieClip(_model.left_leg.value);
            _leftFeet = _source.getMovieClip(_model.left_feet.value);
            _rightShoulder = _source.getMovieClip(_model.right_shoulder.value);
            _rightHand = _source.getMovieClip(_model.right_hand.value);
            _rightHip = _source.getMovieClip(_model.right_hip.value);
            _rightLeg = _source.getMovieClip(_model.right_leg.value);
            _rightFeet = _source.getMovieClip(_model.right_feet.value);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _model:SkinModel;

        private var _body:DisplayObjectContainer;
        private var _leftShoulder:DisplayObject;
        private var _leftHand:DisplayObject;
        private var _leftHip:DisplayObject;
        private var _leftLeg:DisplayObject;
        private var _leftFeet:DisplayObject;

        private var _rightShoulder:DisplayObject;
        private var _rightHand:DisplayObject;
        private var _rightHip:DisplayObject;
        private var _rightLeg:DisplayObject;
        private var _rightFeet:DisplayObject;

        private var _source:SourceManager = SourceManager.instance;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get body():DisplayObjectContainer
        {
            return _body;
        }

        public function get leftShoulder():DisplayObject
        {
            return _leftShoulder;
        }

        public function get leftHand():DisplayObject
        {
            return _leftHand;
        }

        public function get leftHip():DisplayObject
        {
            return _leftHip;
        }

        public function get leftLeg():DisplayObject
        {
            return _leftLeg;
        }

        public function get leftFeet():DisplayObject
        {
            return _leftFeet;
        }

        public function get rightShoulder():DisplayObject
        {
            return _rightShoulder;
        }

        public function get rightHand():DisplayObject
        {
            return _rightHand;
        }

        public function get rightHip():DisplayObject
        {
            return _rightHip;
        }

        public function get rightLeg():DisplayObject
        {
            return _rightLeg;
        }

        public function get rightFeet():DisplayObject
        {
            return _rightFeet;
        }
    }
}
