/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 21.02.13
 * Time: 15:35
 */
package application.views.construct.body
{
    import application.models.skin.SkinModel;

    import com.greensock.TweenLite;

    import flash.display.Shape;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.filters.GlowFilter;

    import framework.helpers.MathHelper;
    import framework.socnet.SocNet;

    public class MainScreenJumper extends JumperObject
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const STAY:String = "SPACE_HERO_STAY";
        public static const JUMP:String = "SPACE_HERO_JUMP_";
        public static const JUMP_VARIATIONS:int = 4;

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MainScreenJumper(model:SkinModel)
        {
            super(model);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            this.buttonMode = true;
            this.useHandCursor = true;
            _flushFilters();
            _addAnimation();
            _addShape();
            if (SocNet.instance().capabilities['set_avatar'])
                _bindMouse();
            else
                disableHover();
        }

        override public function destroy():void
        {
            this.disableHover();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function jump():void
        {
            super.animation.addEventListener(Event.ENTER_FRAME, _checkJumperClip);
        }

        public function disableHover():void
        {
            this.removeEventListener(MouseEvent.ROLL_OVER, _overHandler);
            this.removeEventListener(MouseEvent.ROLL_OUT, _outHandler);

            this.buttonMode = false;
            this.useHandCursor = false;
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _filterIn:GlowFilter;
        private var _filterOut:GlowFilter;

        private var _filterInTarget:Number = 8;
        private var _filterOutTarget:Number = 12;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _flushFilters():void
        {
            _filterIn = new GlowFilter(0xFFFFFF, 1, 0, 0);
            _filterOut = new GlowFilter(0x00FFFF, 1, 0, 0);
        }

        private function _addAnimation():void
        {
            super.animation = super.source.getMovieClip(STAY);
            addChild(animation);
        }

        private function _addShape():void
        {
            var shape:Shape = new Shape();
            addChild(shape);
            shape.graphics.beginFill(0x000000, 0);
            shape.graphics.drawRect(0, 0, this.width, this.height);
            shape.graphics.endFill();
        }

        private function _bindMouse():void
        {
            this.addEventListener(MouseEvent.ROLL_OVER, _overHandler);
            this.addEventListener(MouseEvent.ROLL_OUT, _outHandler);
        }

        private function _checkJumperClip(event:Event):void
        {
            if (super.animation.currentFrame == super.animation.totalFrames) {
                super.animation.removeEventListener(Event.ENTER_FRAME, _checkJumperClip);
                _replaceCosmonaut();
            }
        }

        private function _replaceCosmonaut():void
        {
            removeChild(super.animation);
            super.animation = super.source.getMovieClip(JUMP + MathHelper.random(1, JUMP_VARIATIONS).toString());
            super.animation.gotoAndStop(1);
            super.animation.addEventListener(Event.ENTER_FRAME, _checkJumpComplete);
            super.animation.play();
            addChild(super.animation);
        }

        private function _checkJumpComplete(event:Event):void
        {
            if (super.animation.currentFrame == super.animation.totalFrames) {
                super.animation.removeEventListener(Event.ENTER_FRAME, _checkJumpComplete);
                super.dispatchEvent(new Event(Event.COMPLETE));
            }
        }

        private function _addFilters():void
        {
            this.filters = [_filterIn, _filterOut];
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _overHandler(event:MouseEvent):void
        {
            var filterInTweenParams:Object = {
                blurX: _filterInTarget,
                blurY: _filterInTarget,
                onUpdate: _addFilters
            };
            TweenLite.to(_filterIn, .2, filterInTweenParams);

            var filterOutTweenParams:Object = {
                blurX: _filterOutTarget,
                blurY: _filterOutTarget,
                onUpdate: _addFilters
            };
            TweenLite.to(_filterOut, .2, filterOutTweenParams);
        }

        private function _outHandler(event:MouseEvent):void
        {
            TweenLite.killTweensOf(_filterIn);
            TweenLite.killTweensOf(_filterOut);
            _flushFilters();
            this.filters = [];
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
