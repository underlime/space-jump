/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.02.13
 * Time: 11:40
 */
package application.views.construct.body
{
    import application.models.skin.SkinModel;

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;

    import framework.view.View;

    public class JumperObject extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const BODY:String = "body";
        public static const LEFT_SHOULDER:String = "left_plecho";
        public static const LEFT_HAND:String = "left_hand";
        public static const LEFT_HIP:String = "left_bedro";
        public static const LEFT_LEG:String = "left_leg";
        public static const LEFT_FEET:String = "left_feet";

        public static const RIGHT_SHOULDER:String = "right_plecho";
        public static const RIGHT_HAND:String = "right_hand";
        public static const RIGHT_HIP:String = "right_bedro";
        public static const RIGHT_LEG:String = "right_leg";
        public static const RIGHT_FEET:String = "right_feet";

        public static const HAT:String = "hat";

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function JumperObject(model:SkinModel)
        {
            _model = model;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            _charsData = new CharsData(_model);
        }

        override public function render():void
        {
            addChild(_animation);
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function construct():void
        {
            _replaceChar(BODY, _charsData.body);
            _replaceChar(LEFT_SHOULDER, _charsData.leftShoulder);
            _replaceChar(LEFT_HAND, _charsData.leftHand);
            _replaceChar(LEFT_HIP, _charsData.leftHip);
            _replaceChar(LEFT_LEG, _charsData.leftLeg);
            _replaceChar(LEFT_FEET, _charsData.leftFeet);

            _replaceChar(RIGHT_SHOULDER, _charsData.rightShoulder);
            _replaceChar(RIGHT_HAND, _charsData.rightHand);
            _replaceChar(RIGHT_HIP, _charsData.rightHip);
            _replaceChar(RIGHT_LEG, _charsData.rightLeg);
            _replaceChar(RIGHT_FEET, _charsData.rightFeet);
        }

        public function addHat(hatData:String):void
        {
            var hat:DisplayObject = super.source.getMovieClip(hatData);

            if (_model.hat_enabled.value) {
                var cont:DisplayObjectContainer = _charsData.body.getChildByName(HAT) as DisplayObjectContainer;
                if (cont) {
                    _clearContainer(cont);
                    cont.addChild(hat);
                }
            }
        }

        public function removeHat():void
        {
            var cont:DisplayObjectContainer = _charsData.body.getChildByName(HAT) as DisplayObjectContainer;
            if (cont) {
                _clearContainer(cont);
            }
        }

        private function _replaceChar(cont:String, char:DisplayObject):void
        {
            var container:DisplayObjectContainer = _animation.getChildByName(cont) as DisplayObjectContainer;
            _clearContainer(container);
            container.addChild(char);
        }

        private function _clearContainer(cont:DisplayObjectContainer):void
        {
            if (cont.numChildren > 0) {
                for (var i:int = 0; i < cont.numChildren; i++) {
                    var object:DisplayObject = cont.getChildAt(i);
                    cont.removeChild(object);
                }
            }
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _animation:MovieClip;
        private var _charsData:CharsData;
        private var _model:SkinModel;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        protected function _deleteAnimation():void
        {
            if (_animation && this.contains(_animation))
                removeChild(_animation);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get animation():MovieClip
        {
            return _animation;
        }

        public function set animation(value:MovieClip):void
        {
            _animation = value;
            this.construct();
        }
    }
}
