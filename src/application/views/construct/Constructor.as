/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 20.02.13
 * Time: 11:02
 */
package application.views.construct
{
    import application.models.skin.SkinModel;
    import application.views.construct.body.InGameJumper;
    import application.views.construct.body.MainScreenJumper;

    public class Constructor
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function getFlyingCosmonaut(model:SkinModel):InGameJumper
        {
            return new InGameJumper(model);
        }

        public static function getMainScreenJumper(model:SkinModel):MainScreenJumper
        {
            return new MainScreenJumper(model);
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/        
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
