package application.controllers
{
    import application.events.ApplicationEvent;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.SuiteParams;
    import application.models.User;
    import application.server_api.ApiMethods;
    import application.views.base.ApplicationView;
    import application.views.screen.repair.RepairScreen;

    import framework.controller.Controller;
    import framework.data.DataBase;
    import framework.interfaces.IBaseController;
    import framework.underquery.ApiRequestEvent;

    public class RepairController extends Controller
    {
        private var _view:RepairScreen;
        private var _preloaderView:ApplicationView;

        private var _suiteParams:SuiteParams;
        private var _itemsModel:Items;
        private var _userModel:User;

        private var _bankController:BankController;

        public function RepairController(baseController:IBaseController)
        {
            super(baseController);
        }

        override public function destroy():void
        {
            if (_view) {
                _view.destroy();
                _view = null;
            }

            if (_preloaderView) {
                _preloaderView.destroy();
                _preloaderView = null;
            }

            if (_bankController) {
                _bankController.destroy();
                _bankController = null;
            }

            super.destroy();
        }

        override public function execute():void
        {
            var dataBase:DataBase = baseController.dataBase;
            _suiteParams = dataBase.getChildByName(ModelData.SUITE_PARAMS) as SuiteParams;
            _itemsModel = dataBase.getChildByName(ModelData.ITEMS_MODEL) as Items;
            _userModel = dataBase.getChildByName(ModelData.USER_MODEL) as User;

            _view = new RepairScreen(_suiteParams, _itemsModel, _userModel);
            baseController.container.addChild(_view);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeRepair);
            _view.addEventListener(ApplicationEvent.CALL_REPAIR, _callRepair);
            _view.addEventListener(ApplicationEvent.CALL_BANK, _callBank);
        }

        private function _closeRepair(e:ApplicationEvent):void
        {
            destroy();
        }

        private function _callRepair(e:ApplicationEvent):void
        {
            _preloaderView = new ApplicationView();
            _preloaderView.setPreloader();
            baseController.container.addChild(_preloaderView);

            var params:Object = {
                'hp_points': _suiteParams.damage,
                'currency': 'gems'
            };
            baseController.serverApi.makeRequest(ApiMethods.SUITE_REPAIR, params, _onRepairSuccess);
        }

        private function _onRepairSuccess(e:ApiRequestEvent):Boolean
        {
            _preloaderView.destroy();
            _preloaderView = null;

            return true;
        }

        private function _callBank(e:ApplicationEvent):void
        {
            _bankController = new BankController(baseController);
            _bankController.addEventListener(ApplicationEvent.CLOSE, _onCloseBank);
            _bankController.execute();
        }

        private function _onCloseBank(e:ApplicationEvent):void
        {
            _bankController.destroy();
            _bankController = null;
        }
    }

}
