package application.controllers
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.SuiteParams;
    import application.models.User;
    import application.models.UserAchievements;
    import application.models.UsersTrophies;
    import application.models.skin.SkinsCollection;
    import application.server_api.ApiMethods;
    import application.views.screen.bonus_window.FriendsBonusesDialog;
    import application.views.screen.main.MainScreenView;
    import application.views.text.TextFactory;

    import flash.events.Event;

    import framework.controller.Controller;
    import framework.data.DataContainer;
    import framework.data.ModelsRegistry;
    import framework.events.GameEvent;
    import framework.interfaces.IBaseController;
    import framework.underquery.ApiRequestEvent;

    public class StartScreenController extends Controller
    {
        private var _view:MainScreenView;
        private var _friendsBonusesDialog:FriendsBonusesDialog;

        private var _userModel:User;
        private var _operateController:Controller;
        private var _bankController:BankController;
        private var _pingEarthController:PingEarthController;
        private var _sessionController:SessionInitController;
        private var _guestController:GuestController;
        private var _userNewsController:UserNewsController;

        public function StartScreenController(baseController:IBaseController)
        {
            super(baseController);
            _setup();
        }

        private function _setup():void
        {
            _userModel = dataBase.getChildByName(ModelData.USER_MODEL) as User;
            var skins:SkinsCollection = (dataBase.getChildByName(ModelData.GAME_MODEL) as DataContainer)
                .getChildByName(ModelData.SKINS_COLLECTION) as SkinsCollection;

            _view = new MainScreenView(_userModel, skins);
            _view.addEventListener(ApplicationEvent.CALL_GAME, _callGameHandler);
            _view.addEventListener(ApplicationEvent.CALL_PING_EARTH, _pingEarthHandler);
            _view.addEventListener(ApplicationEvent.CALL_MISSIONS, _callMissionsHandler);
            _view.addEventListener(ApplicationEvent.CALL_BUY_MONEY, _onBuyMoney);
            _view.addEventListener(ApplicationEvent.CALL_SHOP, _callShopEventHandler);
            _view.addEventListener(ApplicationEvent.CALL_TOP, _callTopHandler);
            _view.addEventListener(ApplicationEvent.CALL_TROPHY, _callTrophyHandler);
            _view.addEventListener(ApplicationEvent.CALL_AVATAR_GENERATOR, _callGenerator);
            _view.addEventListener(ApplicationEvent.CALL_REPAIR, _callRepair);
            _view.addEventListener(ApplicationEvent.SESSION_INIT, _sessionInitHandler);
            _view.addEventListener(ApplicationEvent.TRAINING_COMPLETE, _onTrainingComplete);
            _view.addEventListener(ApplicationEvent.CALL_NEWS, _showNews);

            _view.addMaskAnimation();
            container.addChild(_view);
            _pingEarthController = new PingEarthController(baseController, _view);

            baseController.addEventListener(ApplicationEvent.CALL_GUEST_SCREEN, _onCallGuestScreen);
            if (!_userModel.was_friends_prizes_given.value)
                baseController.socNet.getFriendsInAppList(_onFriendsList);
        }

        private function _onTrainingComplete(e:ApplicationEvent):void
        {
            var userAchievementsModel:UserAchievements = baseController.dataBase.getChildByName(ModelData.USER_ACHIEVEMENTS_MODEL) as UserAchievements;
            if (!userAchievementsModel.checkAchievementByCode('Train_hard'))
                baseController.serverApi.makeRequest(ApiMethods.ACHIEVEMENTS_CHECK_BE_TRAINED);
        }

        private function _callRepair(event:ApplicationEvent):void
        {
            (new RepairController(super.baseController).execute());
        }

        private function _callTopHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();

            _operateController = new RateController(baseController);
            _operateController.addEventListener(GameEvent.DESTROY, _rateControllerDestroyHandler);
            _operateController.execute();
        }

        private function _rateControllerDestroyHandler(event:GameEvent):void
        {
            event.currentTarget.removeEventListener(GameEvent.DESTROY, _rateControllerDestroyHandler);
            if (_view && !_view.destroyed) // необходимо для сборки космонавта заново
                _view.update();
        }

        private function _callShopEventHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();

            _operateController = new ShopController(baseController);
            _operateController.execute();
        }

        private function _callTrophyHandler(event:ApplicationEvent):void
        {
            _destroyOperateController();

            var usersTrophies:UsersTrophies = baseController.dataBase.getChildByName(ModelData.USERS_TROPHIES_MODEL) as UsersTrophies;
            _operateController = new TrophyController(baseController, usersTrophies);
            _operateController.execute();
        }

        private function _onBuyMoney(e:ApplicationEvent):void
        {
            _bankController = new BankController(this.baseController);
            _bankController.addEventListener(ApplicationEvent.COINS_UPDATED, _closeBank);
            _bankController.addEventListener(ApplicationEvent.CLOSE, _closeBank);
            _bankController.execute();
        }

        private function _closeBank(e:Event):void
        {
            _bankController.destroy();
            _bankController = null;
        }

        private function _callMissionsHandler(e:ApplicationEvent):void
        {
            _destroyOperateController();

            var userAchievementsModel:UserAchievements = baseController.dataBase.getChildByName(ModelData.USER_ACHIEVEMENTS_MODEL) as UserAchievements;
            _operateController = new MissionController(baseController, userAchievementsModel);
            _operateController.execute();
        }

        private function _callGameHandler(e:ApplicationEvent):void
        {
            dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_GAME));
        }

        private function _callGenerator(event:ApplicationEvent):void
        {
            _destroyOperateController();

            if (_view.cosmonaut)
                _view.cosmonaut.filters = [];
            _operateController = new AvaGeneratorController(baseController, _view.cosmosLayer);
            _operateController.execute();
        }

        private function _pingEarthHandler(event:ApplicationEvent):void
        {
            _pingEarthController.execute();
        }

        private function _onCallRepair():void
        {
            _operateController = new RepairController(baseController);
            _operateController.execute();
        }

        private function _destroyOperateController():void
        {
            if (_operateController && !_operateController.destroyed)
                _operateController.destroy();
            _operateController = null;
        }

        private function _sessionInitHandler(event:ApplicationEvent):void
        {
            if (_checkSuite()) {
                if (_sessionController && !_sessionController.destroyed)
                    _sessionController.destroy();

                _sessionController = new SessionInitController(super.baseController);
                _sessionController.addEventListener(ApplicationEvent.SESSION_INIT, _callGame);
                _sessionController.addEventListener(ApplicationEvent.SESSION_ERROR, _sessionError);

                _sessionController.execute();
            }
            else {
                (new RepairController(super.baseController)).execute();
            }
        }

        private function _checkSuite():Boolean
        {
            var suiteParams:SuiteParams = ModelsRegistry.getModel(ModelData.SUITE_PARAMS) as SuiteParams;
            return suiteParams.actualHp > 0;
        }

        private function _callGame(event:ApplicationEvent):void
        {
            if (_sessionController && !_sessionController.destroyed)
                _sessionController.destroy();
            _view.startGame();
        }

        private function _sessionError(event:ApplicationEvent):void
        {
            if (_sessionController && !_sessionController.destroyed)
                _sessionController.destroy();
        }

        private function _onCallGuestScreen(e:ApplicationEvent):void
        {
            if (_guestController && !_guestController.destroyed)
                _guestController.destroy();

            var socNetId:String = e.data['soc_net_id'];
            _guestController = new GuestController(baseController, socNetId);
            _guestController.addEventListener(GameEvent.DESTROY, _onGuestScreenDestroy);
            _guestController.execute();
        }

        private function _onGuestScreenDestroy(e:GameEvent):void
        {
            if (_view && !_view.destroyed)
                _view.update();
        }

        override public function destroy():void
        {
            baseController.removeEventListener(ApplicationEvent.CALL_GUEST_SCREEN, _onCallGuestScreen);

            _destroyOperateController();

            if (_view && !_view.destroyed)
                _view.destroy();
            _view = null;

            if (_pingEarthController && !_pingEarthController.destroyed)
                _pingEarthController.destroy();
            _pingEarthController = null;

            if (_sessionController && !_sessionController.destroyed)
                _sessionController.destroy();
            _sessionController = null;

            if (_guestController && !_guestController.destroyed)
                _guestController.destroy();
            _guestController = null;

            if (_friendsBonusesDialog && !_friendsBonusesDialog.destroyed)
                _friendsBonusesDialog.destroy();
            _friendsBonusesDialog = null;

            super.destroy();
        }

        private function _onFriendsList(data:Object):void
        {
            var friendsCount:int = 0;

            var hasFriends:Boolean = data['friends_list'] && data['friends_list']['_add'];
            if (hasFriends) {
                for (var userId:String in data['friends_list']['_add'])
                    ++friendsCount;
            }

            if (friendsCount) {
                _friendsBonusesDialog = new FriendsBonusesDialog(friendsCount);
                _friendsBonusesDialog.addEventListener(ApplicationEvent.CALL_FRIEND_BONUS, _onFriendsBonus);
                _view.addChild(_friendsBonusesDialog);
            }
        }

        private function _onFriendsBonus(e:ApplicationEvent):void
        {
            var bonusType:String = e.data['action'];
            var params:Object = {
                'bonus_type': bonusType,
                'locale': (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU) ? 'ru' : 'en_US'
            };
            baseController.serverApi.makeRequest(ApiMethods.USER_GIVE_FRIENDS_BONUSES, params, _onBonusesSuccess);
        }

        private function _onBonusesSuccess(e:ApiRequestEvent):void
        {
            _friendsBonusesDialog.destroy();
            _friendsBonusesDialog = null;
            this.showMessage(TextFactory.instance.getLabel('gift_sent_message'));
        }

        private function _showNews(e:ApplicationEvent = null):void
        {
            _userNewsController = new UserNewsController(baseController);
            _userNewsController.addEventListener(ApplicationEvent.CALL_GUEST_SCREEN, _onCallGuestScreen);
            _userNewsController.execute();
        }

        public function showMessage(text:String):void
        {
            _view.showMessage(text);
        }
    }
}
