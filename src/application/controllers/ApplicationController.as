/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.10.12
 */
package application.controllers
{
    import application.config.ApplicationConfiguration;
    import application.config.Config;
    import application.config.DataApi;
    import application.controllers.application_controller.OnCallUserInfo;
    import application.controllers.application_controller.OnCallUsersListInfo;
    import application.controllers.game.GameController;
    import application.events.ApplicationEvent;
    import application.events.JsScrollEvent;
    import application.helpers.GemsHelper;
    import application.helpers.WallPostTool;
    import application.models.GameDataBase;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.server_api.ServerApi;
    import application.views.base.ApplicationView;
    import application.views.screen.alert.AlertPosition;
    import application.views.screen.alert.AlertView;
    import application.views.screen.bonus_window.BonusWindow;
    import application.views.screen.dialog.FacebookPhotoWindow;
    import application.views.screen.error.ErrorView;
    import application.views.text.TextFactory;

    import flash.display.DisplayObjectContainer;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.external.ExternalInterface;

    import framework.controller.BaseController;
    import framework.data.ModelsRegistry;
    import framework.error.ApplicationError;
    import framework.interfaces.IController;
    import framework.socnet.SocNet;
    import framework.underquery.ApiRequestEvent;

    public class ApplicationController extends BaseController
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ApplicationController(root:DisplayObjectContainer)
        {
            super(root);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            super.execute();
            _setupLoading();
        }

        override public function destroy():void
        {
            if (_preloaderView && !_preloaderView.destroyed)
                _preloaderView.destroy();
            _preloaderView = null;

            if (_pictureAlert && !_pictureAlert.destroyed)
                _pictureAlert.destroy();
            _pictureAlert = null;

            if (_bonusWindow && !_bonusWindow.destroyed)
                _bonusWindow.destroy();
            _bonusWindow = null;

            super.destroy();
        }

        override public function appError(error:ApplicationError):void
        {
            if (_operateController) {
                _operateController.destroy();
                this.container.addChild(new ErrorView(error));
            }
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _firstLaunch:Boolean = true;
        private var _operateController:IController;
        private var _config:Config = new Config();
        private var _eventsView:AlertView;
        private var _preloaderView:ApplicationView;
        private var _pictureAlert:FacebookPhotoWindow;
        private var _alertPosition:AlertPosition;
        private var _userModel:User;
        private var _bonusWindow:BonusWindow;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupLoading():void
        {
            _destructController();
            _operateController = new LoadController(this);
            _operateController.addEventListener(ApplicationEvent.CONFIG_LOADED, _createInfrastructure);
            _operateController.addEventListener(Event.COMPLETE, _onLoadingComplete);
            _operateController.execute();
        }

        private function _createInfrastructure(e:Event):void
        {
            _dataBase = new GameDataBase();
            ModelsRegistry.dataBase = _dataBase;
            _userModel = _dataBase.getChildByName(ModelData.USER_MODEL) as User;

            _socNet = SocNet.instance(this);

            var dataApiConfig:DataApi = this.config.getChildByName(Config.DATA_API_CONFIG) as DataApi;

            var mock:Boolean = (dataApiConfig.mock.value && ApplicationConfiguration.DEBUG);
            _serverApi = new ServerApi(this, mock);
            _exportServerApi();
            _exportScrollEvent();

            if (_socNet.currentLanguage == SocNet.LANG_RU)
                ApplicationConfiguration.LANGUAGE = ApplicationConfiguration.RU;
            else
                ApplicationConfiguration.LANGUAGE = ApplicationConfiguration.EN;

            _addEventsView();
        }

        private function _addEventsView():void
        {
            _eventsView = new AlertView(this);
            super.root.addChild(_eventsView);
            _eventsView.addEventListener(ApplicationEvent.SHARE, _eventShareHandler);
        }

        private function _eventShareHandler(e:ApplicationEvent):void
        {
            _preloaderView = new ApplicationView();
            _preloaderView.setPreloader();
            super.root.addChild(_preloaderView);

            var alertPosition:AlertPosition = e.target as AlertPosition;
            _alertPosition = alertPosition;

            var shortText:String = TextFactory.instance.getLabel('congratulate_me_label');
            var postTool:WallPostTool = new WallPostTool(baseController, alertPosition.shareText, alertPosition.shareImage, shortText);
            postTool.addEventListener(Event.COMPLETE, _sharingComplete);
            postTool.addEventListener(ErrorEvent.ERROR, _sharingError);
            postTool.addPost();
        }

        private function _sharingComplete(e:Event):void
        {
            e.target.destroy();

            if (baseController.socNet.socNetName == SocNet.FACEBOOK_COM && _alertPosition) {
                _pictureAlert = new FacebookPhotoWindow(container, _alertPosition.shareImage);
                _pictureAlert.addEventListener(Event.COMPLETE, _onShowDialog);
                _pictureAlert.show();
            }
            else {
                if (_preloaderView && !_preloaderView.destroyed)
                    _preloaderView.destroy();
                _preloaderView = null;
            }
        }

        private function _onShowDialog(e:Event):void
        {
            if (_preloaderView && !_preloaderView.destroyed)
                _preloaderView.destroy();
            _preloaderView = null;
        }

        private function _sharingError(e:ErrorEvent):void
        {
            e.target.destroy();

            var err:ApplicationError = new ApplicationError();
            err.type = ApplicationError.SOCIAL_NETWORK_ERROR;
            err.message = e.text;
            this.appError(err);
        }

        private function _onLoadingComplete(event:Event):void
        {
            _setupStartScreen();
            if (_firstLaunch) {
                _checkNews();
                _firstLaunch = false;
            }
        }

        private function _setupStartScreen():void
        {
            _destructController();
            _operateController = new StartScreenController(this);
            _operateController.addEventListener(ApplicationEvent.CALL_GAME, _callGameHandler);
            _operateController.execute();
        }

        private function _checkNews():void
        {
            if (_userModel.day_prize_type.value)
                _showPrize();
        }

        private function _showPrize():void
        {
            var prizeName:String = '';
            var visitsCount:int = _userModel.visits_count.value%7;
            if (visitsCount == 1) {
                prizeName = _userModel.first_day_prize.value + ' $';
            }
            else {
                if (visitsCount >= 2 && visitsCount <= 6) {
                    var _itemsModel:Items = _dataBase.getChildByName(ModelData.ITEMS_MODEL) as Items;
                    var itemAction:String = _userModel.prizes_actions_table.value[visitsCount];
                    var itemRecord:ItemRecord = _itemsModel.getItemByAction(itemAction);
                    if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                        prizeName = itemRecord.name_ru.value;
                    else
                        prizeName = itemRecord.name_en.value;
                }
                else {
                    var prize:int = _userModel.last_day_prize.value;
                    prizeName = prize + ' ' + GemsHelper.getGemsLabel(prize);
                }
            }

            _bonusWindow = new BonusWindow(visitsCount, prizeName);
            container.addChild(_bonusWindow);
            _bonusWindow.render();
        }

        private function _exportServerApi():void
        {
            if (ExternalInterface.available) {
                ExternalInterface.addCallback('callServerApi', _onCallApi);
                ExternalInterface.addCallback('getCurrentUserInfo', _onCallUserInfo);
                ExternalInterface.addCallback('getUsersListInfo', _onCallUsersListInfo);
                ExternalInterface.addCallback('showGuestScreen', _onCallShowGuestScreen);
                ExternalInterface.call('function() { if (onApplicationApiInit) onApplicationApiInit(); }');
            }
        }

        private function _onCallApi(method:String, params:Object, callbackName:String):void
        {
            _serverApi.makeRequest(method, params, function (e:ApiRequestEvent):Boolean
            {
                var res:* = ExternalInterface.call(callbackName, e.data);
                return Boolean(res);
            });
        }

        private function _onCallUserInfo(callbackName:String):void
        {
            OnCallUserInfo.execute(this, callbackName);
        }

        private function _onCallUsersListInfo(socNetIdsList:Array, callbackName:String):void
        {
            OnCallUsersListInfo.execute(this, socNetIdsList, callbackName);
        }

        private function _onCallShowGuestScreen(socNetId:String):void
        {
            var event:ApplicationEvent = new ApplicationEvent(ApplicationEvent.CALL_GUEST_SCREEN);
            event.data['soc_net_id'] = socNetId;
            dispatchEvent(event);
        }

        private function _exportScrollEvent():void
        {
            if (ExternalInterface.available)
                ExternalInterface.addCallback('onJsScroll', _onJsScroll);
            else
                this.container.addEventListener(MouseEvent.MOUSE_WHEEL, _emulateJsScroll);
        }

        private function _onJsScroll(dir:int):void
        {
            var event:JsScrollEvent = new JsScrollEvent(JsScrollEvent.SCROLL);
            event.dir = dir;
            this.dispatchEvent(event);
        }

        private function _emulateJsScroll(e:MouseEvent):void
        {
            var event:JsScrollEvent = new JsScrollEvent(JsScrollEvent.SCROLL);
            event.dir = (e.delta < 0 ? -1 : 1);
            this.dispatchEvent(event);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _callGameHandler(event:ApplicationEvent):void
        {
            _destructController();
            _operateController = new GameController(this);
            _operateController.addEventListener(ApplicationEvent.END_GAME, _gameEndHandler);
            _operateController.addEventListener(ApplicationEvent.CALL_GAME, _callGameHandler);
            _operateController.execute();
        }

        private function _gameEndHandler(event:ApplicationEvent):void
        {
            _setupStartScreen();
        }

        private function _destructController():void
        {
            if (_operateController)
                _operateController.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get config():Config
        {
            return _config;
        }

        public function get eventsView():AlertView
        {
            return _eventsView;
        }
    }
}
