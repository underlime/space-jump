package application.controllers.application_controller
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.User;

    import flash.events.Event;
    import flash.external.ExternalInterface;

    public class OnCallUserInfo
    {
        private static var _callbackName:String;
        private static var _userModel:User;

        public static function execute(appCtrl:ApplicationController, callbackName:String):void
        {
            _callbackName = callbackName;

            _userModel = appCtrl.dataBase.getChildByName(ModelData.USER_MODEL) as User;
            if (_userModel.soc_net_id.value && (_userModel.first_name.value || _userModel.last_name.value))
                ExternalInterface.call(callbackName, _userModel);
            else
                _userModel.addEventListener(Event.CHANGE, _onUserInfo);


        }

        private static function _onUserInfo(e:Event):void
        {
            if (_userModel.soc_net_id.value && (_userModel.first_name.value || _userModel.last_name.value)) {
                _userModel.removeEventListener(Event.CHANGE, _onUserInfo);
                ExternalInterface.call(_callbackName, _userModel);
            }
        }
    }
}
