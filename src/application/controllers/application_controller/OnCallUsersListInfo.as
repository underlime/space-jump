package application.controllers.application_controller
{
    import application.controllers.ApplicationController;
    import application.models.ModelData;
    import application.models.User;
    import application.models.UsersList;
    import application.server_api.ApiMethods;

    import flash.events.Event;
    import flash.external.ExternalInterface;

    public class OnCallUsersListInfo
    {
        private static var _usersListModel:UsersList;
        private static var _socNetIdsList:Array;
        private static var _callbackName:String;

        public static function execute(appCtrl:ApplicationController, socNetIdsList:Array, callbackName:String):void
        {
            for (var i:int = 0; i < socNetIdsList.length; ++i)
                socNetIdsList[i] = socNetIdsList[i].toString();

            _socNetIdsList = socNetIdsList;
            _callbackName = callbackName;

            _usersListModel = appCtrl.dataBase.getChildByName(ModelData.USERS_LIST_MODEL) as UsersList;
            var needList:Array = _usersListModel.checkNotExists(socNetIdsList);
            if (needList.length == 0) {
                ExternalInterface.call(callbackName, _createUsersList());
            }
            else {
                _usersListModel.addEventListener(Event.CHANGE, _onNewUsers);
                var params:Object = {'soc_net_ids_list': needList.join(',')};
                appCtrl.serverApi.makeRequest(ApiMethods.USER_GET_LIST_INFO, params);
            }
        }

        private static function _onNewUsers(e:Event):void
        {
            _usersListModel.removeEventListener(Event.CHANGE, _onNewUsers);
            ExternalInterface.call(_callbackName, _createUsersList());
        }

        private static function _createUsersList():Array
        {
            var resp:Array = [];
            var socNetId:String;
            for each (var userRecord:User in _usersListModel.list.value) {
                socNetId = userRecord.soc_net_id.value.toString();
                if (_socNetIdsList.indexOf(socNetId) != -1)
                    resp.push(userRecord);
            }
            return resp;
        }
    }
}
