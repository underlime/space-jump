package application.controllers
{
    import application.events.ApplicationEvent;
    import application.models.Achievements;
    import application.models.ModelData;
    import application.models.UserAchievements;
    import application.views.screen.missions.MissionView;

    import framework.controller.Controller;
    import framework.interfaces.IBaseController;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class MissionController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function MissionController(baseController:IBaseController, userAchievementsModel:UserAchievements)
        {
            super(baseController);
            _userAchievementsModel = userAchievementsModel;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            var achievementsModel:Achievements = baseController.dataBase.getChildByName(ModelData.ACHIEVEMENTS_MODEL) as Achievements;
            _view = new MissionView(
                    baseController as ApplicationController,
                    achievementsModel,
                    _userAchievementsModel
            );
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            this.container.addChild(_view);
        }

        override public function destroy():void
        {
            if (_view && !_view.destroyed)
                _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _userAchievementsModel:UserAchievements;
        private var _view:MissionView;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(e:ApplicationEvent):void
        {
            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}