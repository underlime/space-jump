/**
 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
 */
package application.controllers.game
{
    import application.config.DataPhysics;
    import application.controllers.*;
    import application.events.ApplicationEvent;
    import application.helpers.WallPostTool;
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;
    import application.models.SuiteParams;
    import application.sound.SoundManager;
    import application.sound.libs.UISounds;
    import application.views.game.GameView;
    import application.views.screen.dialog.FacebookPhotoWindow;
    import application.views.screen.result.ResultScreenView;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.MouseEvent;

    import framework.controller.Controller;
    import framework.data.ModelsRegistry;
    import framework.error.ApplicationError;
    import framework.interfaces.IBaseController;
    import framework.socnet.SocNet;
    import framework.tools.ScreenShot;

    public class GameController extends Controller
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GameController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _startTime = getTimer();
            _setup();
        }

        override public function destroy():void
        {
            if (_wallPostTool) {
                _wallPostTool.destroy();
                _wallPostTool = null;
            }

            if (_sessionController && !_sessionController.destroyed)
                _sessionController.destroy();

            if (_shareJumpController && !_shareJumpController.destroyed)
                _shareJumpController.destroy();

            _screenShot.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _gameView:GameView;
        private var _model:GameModel;
        private var _statistics:StatisticsGameModel;
        private var _resultScreen:ResultScreenView;

        private var _screenShot:ScreenShot = new ScreenShot();
        private var _wallPostTool:WallPostTool;

        private var _sessionController:SessionInitController;
        private var _shareJumpController:ShareJumpResultController;
        private var _finishController:SessionFinishController;
        private var _screenShotImage:Bitmap;
        private var _pictureAlert:FacebookPhotoWindow;

        private var _chanceController:ChanceController;
        private var _startTime:Number = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void
        {
            _model = dataBase.getChildByName(ModelData.GAME_MODEL) as GameModel;
            _statistics = _model.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;

            _gameView = new GameView(_model);
            _gameView.addEventListener(ApplicationEvent.END_GAME, _endGameHandler);
            _gameView.addEventListener(ApplicationEvent.SCREEN_SHOT, _screenShotHandler);

            _gameView.addEventListener(ApplicationEvent.CALL_CHANCE, _chanceHandler);

            container.addChild(_gameView);
        }

        private function _destroyAll():void
        {
            _unbindCommands();
            if (_resultScreen && !_resultScreen.destroyed)
                _resultScreen.destroy();
        }

        private function _checkSuite():Boolean
        {
            var suiteParams:SuiteParams = ModelsRegistry.getModel(ModelData.SUITE_PARAMS) as SuiteParams;
            return suiteParams.actualHp > 0;
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _screenShotHandler(event:ApplicationEvent):void
        {
            _screenShot.takeShot(_gameView, DataPhysics.WORLD_WIDTH, DataPhysics.WORLD_HEIGHT);

            _gameView.pause();
            _gameView.setPreloader();

            var text:String = TextFactory.instance.getLabel('iplaysd');
            var screenShotImage:Bitmap = new Bitmap(_screenShot.rawData.clone());

            if (_wallPostTool) {
                _wallPostTool.destroy();
                _wallPostTool = null;
            }

            var shortText:String = TextFactory.instance.getLabel('i_label');
            _wallPostTool = new WallPostTool(baseController, text, screenShotImage, shortText);
            _screenShotImage = screenShotImage;
            _wallPostTool.addEventListener(Event.COMPLETE, _onScreenShotPosted);
            _wallPostTool.addEventListener(ErrorEvent.ERROR, _onScreenShotError);
            _wallPostTool.addPost();
        }

        private function _onScreenShotPosted(e:Event):void
        {
            if (baseController.socNet.socNetName == SocNet.FACEBOOK_COM && _screenShotImage) {
                _pictureAlert = new FacebookPhotoWindow(_gameView, _screenShotImage);
                _pictureAlert.addEventListener(Event.COMPLETE, _onShowDialog);
                _pictureAlert.show();
            }
            else {
                _gameView.removePreloader();
                if (_wallPostTool && !_wallPostTool.destroyed)
                    _wallPostTool.destroy();
                _wallPostTool = null;
            }
        }

        private function _onShowDialog(e:Event):void
        {
            _gameView.removePreloader();
            if (_wallPostTool && !_wallPostTool.destroyed)
                _wallPostTool.destroy();
            _wallPostTool = null;
        }

        private function _onScreenShotError(e:ErrorEvent):void
        {
            if (_wallPostTool && _wallPostTool.destroyed)
                _wallPostTool.destroy();
            _wallPostTool = null;

            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.SOCIAL_NETWORK_ERROR;
            error.message = e.text;
            this.baseController.appError(error);
        }

        private function _endGameHandler(event:ApplicationEvent):void
        {
            _gameView.removeEventListener(ApplicationEvent.END_GAME, _endGameHandler);
            _gameView.removeEventListener(ApplicationEvent.SCREEN_SHOT, _screenShotHandler);
            _statistics.success = (event.data.success ? event.data.success : false);

            var totalTime:Number = (getTimer() - _startTime)/1000;
            _statistics.realTime = totalTime;

            _setupFinishController();
        }

        private function _setupFinishController():void
        {
            _finishController = new SessionFinishController(super.baseController);
            _finishController.addEventListener(Event.COMPLETE, _finishControllerCompleteHandler);
            _finishController.execute();
        }

        private function _finishControllerCompleteHandler(event:Event):void
        {
            _finishController.removeEventListener(Event.COMPLETE, _finishControllerCompleteHandler);
            _finishController.destroy();

            _gameView.destroy();

            _resultScreen = new ResultScreenView(_model, _statistics.success);

            this.container.addChild(_resultScreen);
            _bindCommands();
        }

        private function _bindCommands():void
        {
            _resultScreen.menuButton.addEventListener(MouseEvent.CLICK, _menuClickHandler);
            _resultScreen.againButton.addEventListener(MouseEvent.CLICK, _againClickHandler);
            _resultScreen.shareButton.addEventListener(MouseEvent.CLICK, _shareClickHandler);
            _resultScreen.addEventListener(ApplicationEvent.CALL_REPAIR, _repairHandler);
        }

        private function _unbindCommands():void
        {
            _resultScreen.menuButton.removeEventListener(MouseEvent.CLICK, _menuClickHandler);
            _resultScreen.againButton.removeEventListener(MouseEvent.CLICK, _againClickHandler);
            _resultScreen.shareButton.removeEventListener(MouseEvent.CLICK, _shareClickHandler);
            _resultScreen.removeEventListener(ApplicationEvent.CALL_REPAIR, _repairHandler);
        }


        private function _menuClickHandler(event:MouseEvent):void
        {
            _destroyAll();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.END_GAME));
        }

        private function _againClickHandler(event:MouseEvent):void
        {
            if (_checkSuite()) {
                if (_sessionController && !_sessionController.destroyed)
                    _sessionController.destroy();

                _sessionController = new SessionInitController(super.baseController);
                _sessionController.addEventListener(ApplicationEvent.SESSION_INIT, _callGame);
                _sessionController.addEventListener(ApplicationEvent.SESSION_ERROR, _sessionError);

                _sessionController.execute();
            }
            else {
                (new RepairController(super.baseController)).execute();
            }
            SoundManager.instance.playUISound(UISounds.BUTTON_CONFIRM);
        }

        private function _callGame(event:ApplicationEvent):void
        {
            _destroyAll();
            if (_sessionController && !_sessionController.destroyed)
                _sessionController.destroy();
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CALL_GAME));
        }

        private function _sessionError(event:ApplicationEvent):void
        {
            if (_sessionController && !_sessionController.destroyed)
                _sessionController.destroy();
        }

        private function _shareClickHandler(event:MouseEvent):void
        {
            var gameStatModel:StatisticsGameModel = _model.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;
            _shareJumpController = new ShareJumpResultController(baseController, gameStatModel);
            _shareJumpController.execute();
        }

        private function _repairHandler(event:ApplicationEvent):void
        {
            (new RepairController(super.baseController)).execute();
        }

        private function _chanceHandler(event:ApplicationEvent):void
        {
            _gameView.removeEventListener(ApplicationEvent.CALL_CHANCE, _chanceHandler);

            _chanceController = new ChanceController(super.baseController, _statistics);
            _chanceController.addEventListener(ApplicationEvent.END_GAME, _chanceEndGameHandler);
            _chanceController.addEventListener(ApplicationEvent.CONTINUE_GAME, _continueGameHandler);

            _chanceController.execute();
        }

        private function _continueGameHandler(event:ApplicationEvent):void
        {
            _chanceController.destroy();

            _gameView.addEventListener(ApplicationEvent.CALL_CHANCE, _chanceHandler);
            _gameView.continueByResurrect();
        }

        private function _chanceEndGameHandler(event:ApplicationEvent):void
        {
            _chanceController.destroy();
            _gameView.endGame();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
