package application.controllers
{
    import application.config.ApplicationConfiguration;
    import application.helpers.WallPostTool;
    import application.models.StatisticsGameModel;
    import application.views.base.ApplicationView;
    import application.views.screen.dialog.FacebookPhotoWindow;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.display.Sprite;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.text.TextFieldAutoSize;

    import flashx.textLayout.formats.TextAlign;

    import framework.controller.Controller;
    import framework.error.ApplicationError;
    import framework.helpers.DateHelper;
    import framework.interfaces.IBaseController;
    import framework.socnet.SocNet;
    import framework.tools.ScreenShot;
    import framework.view.text.SimpleTextField;

    import org.casalib.util.NumberUtil;

    public class ShareJumpResultController extends Controller
    {
        private static const WIDTH:int = 450;
        private static const HEIGHT:int = 370;

        private var _preloaderView:ApplicationView;
        private var _gameStatModel:StatisticsGameModel;
        private var _postTool:WallPostTool;

        [Embed(source='../../../lib/share_pics/RECORDS_RU.png', mimeType='image/png')]
        private var _BackGroundRu:Class;

        [Embed(source='../../../lib/share_pics/RECORDS_EN.png', mimeType='image/png')]
        private var _BackGroundEn:Class;

        private var _tmpImage:Sprite;
        private var _backGround:Bitmap;
        private var _image:Bitmap;
        private var _pictureAlert:FacebookPhotoWindow;

        public function ShareJumpResultController(baseController:IBaseController, gameStatModel:StatisticsGameModel)
        {
            super(baseController);
            _gameStatModel = gameStatModel;
        }

        override public function destroy():void
        {
            if (_postTool) {
                _postTool.destroy();
                _postTool = null;
            }

            if (_preloaderView) {
                _preloaderView.destroy();
                _preloaderView = null;
            }

            super.destroy();
        }

        override public function execute():void
        {
            _preloaderView = new ApplicationView();
            _preloaderView.setPreloader();
            baseController.container.addChild(_preloaderView);

            _createImage();

            var message:String = TextFactory.instance.getLabel('my_records_label');
            message += ': ' + NumberUtil.format(_gameStatModel.points, ' ');

            var shortText:String = TextFactory.instance.getLabel('i_label');

            _postTool = new WallPostTool(baseController, message, _image, shortText);
            _postTool.addEventListener(Event.COMPLETE, _onPostSuccess);
            _postTool.addEventListener(ErrorEvent.ERROR, _onPostError);
            _postTool.addPost();
        }

        private function _createImage():void
        {
            _tmpImage = new Sprite();
            baseController.container.addChild(_tmpImage);

            _createBackground();
            _setRecord();
            _setCoins();
            _setTrophy();
            _setMissions();
            _setVelocity();
            _setSuccess();
            _setTime();

            var _screenShot:ScreenShot = new ScreenShot();
            _screenShot.takeShot(_tmpImage, WIDTH, HEIGHT);
            _image = new Bitmap(_screenShot.rawData.clone());

            _screenShot.destroy();
            baseController.container.removeChild(_tmpImage);
            _tmpImage = null;
        }

        private function _createBackground():void
        {
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                _backGround = new _BackGroundRu();
            else
                _backGround = new _BackGroundEn();
            _tmpImage.addChild(_backGround);
        }

        private function _setRecord():void
        {
            var label:SimpleTextField = _createTextField();
            label.size = 20;
            label.width = _backGround.width;
            label.align = TextAlign.CENTER;
            label.x = 0;
            label.y = 135;
            _tmpImage.addChild(label);
            var text:String = TextFactory.instance.getLabel('my_record_label');
            label.text = text.toUpperCase();

            var value:SimpleTextField = _createTextField();
            value.size = 32;
            value.width = _backGround.width;
            value.align = TextAlign.CENTER;
            value.x = 0;
            value.y = 160;
            _tmpImage.addChild(value);
            value.text = NumberUtil.format(_gameStatModel.points, ' ');
        }

        private function _createTextField():SimpleTextField
        {
            var field:SimpleTextField = new SimpleTextField();
            field.color = 0xFFFFFF;
            field.fontFamily = SimpleTextField.RUSSO_ONE_FONT;
            field.size = 10;
            return field;
        }

        private function _setCoins():void
        {
            var label:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            label.x = 70;
            label.y = 210;
            _tmpImage.addChild(label);
            var text:String = TextFactory.instance.getLabel('coins_collected_label');
            label.text = text.toUpperCase() + ':';

            var value:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                value.x = 162;
            else
                value.x = 170;
            value.y = 210;
            _tmpImage.addChild(value);
            value.text = NumberUtil.format(_gameStatModel.coins, ' ');
        }

        private function _setTrophy():void
        {
            var label:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            label.x = 70;
            label.y = 230;
            _tmpImage.addChild(label);
            var text:String = TextFactory.instance.getLabel('is_trophy_collected_label');
            label.text = text.toUpperCase() + ':';

            var value:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                value.x = 162;
            else
                value.x = 157;
            value.y = 230;
            _tmpImage.addChild(value);

            if (_gameStatModel.trophyFound)
                value.text = TextFactory.instance.getLabel('yes_label');
            else
                value.text = TextFactory.instance.getLabel('no_label');
        }

        private function _setMissions():void
        {
            var label:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            label.x = 70;
            label.y = 250;
            _tmpImage.addChild(label);
            var text:String = TextFactory.instance.getLabel('missions_completed_label');
            label.text = text.toUpperCase() + ':';

            var value:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                value.x = 185;
            else
                value.x = 193;
            value.y = 250;
            _tmpImage.addChild(value);
            value.text = NumberUtil.format(_gameStatModel.missionsComplete, ' ');
        }

        private function _setVelocity():void
        {
            var label:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            label.x = 250;
            label.y = 210;
            _tmpImage.addChild(label);
            var text:String = TextFactory.instance.getLabel('max_velocity_label');
            label.text = text.toUpperCase() + ':';

            var value:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                value.x = 345;
            else
                value.x = 333;
            value.y = 210;
            _tmpImage.addChild(value);
            value.text = NumberUtil.format(_gameStatModel.maxVelocity, ' ');
        }

        private function _setSuccess():void
        {
            var label:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            label.x = 250;
            label.y = 230;
            _tmpImage.addChild(label);
            var text:String = TextFactory.instance.getLabel('landing_success_label');
            label.text = text.toUpperCase() + ':';

            var value:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                value.x = 390;
            else
                value.x = 350;
            value.y = 230;
            _tmpImage.addChild(value);
            if (_gameStatModel.success)
                value.text = TextFactory.instance.getLabel('yes_label');
            else
                value.text = TextFactory.instance.getLabel('no_label');
        }

        private function _setTime():void
        {
            var label:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            label.x = 250;
            label.y = 250;
            _tmpImage.addChild(label);
            var text:String = TextFactory.instance.getLabel('flying_time_label');
            label.text = text.toUpperCase();

            var value:SimpleTextField = _createTextField();
            label.autoSize = TextFieldAutoSize.LEFT;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                value.x = 345;
            else
                value.x = 322;
            value.y = 250;
            _tmpImage.addChild(value);

            var lang:String = (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU) ? DateHelper.LANG_RU : DateHelper.LANG_EN;
            value.text = DateHelper.formatSecondsStamp(_gameStatModel.seconds, lang);
        }

        private function _onPostSuccess(e:Event):void
        {
            if (baseController.socNet.socNetName == SocNet.FACEBOOK_COM) {
                _pictureAlert = new FacebookPhotoWindow(baseController.container, _image);
                _pictureAlert.addEventListener(Event.COMPLETE, _onShowDialog);
                _pictureAlert.show();
            }
            else {
                if (_preloaderView && !_preloaderView.destroyed)
                    _preloaderView.destroy();
                _preloaderView = null;
            }
        }

        private function _onShowDialog(e:Event):void
        {
            if (_preloaderView && !_preloaderView.destroyed)
                _preloaderView.destroy();
            _preloaderView = null;
        }

        private function _onPostError(e:ErrorEvent):void
        {
            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.SOCIAL_NETWORK_ERROR;
            error.message = e.text;
            baseController.appError(error);
        }
    }
}
