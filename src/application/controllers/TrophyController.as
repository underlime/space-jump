/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.03.13
 * Time: 15:02
 */
package application.controllers
{
    import application.events.ApplicationEvent;
    import application.helpers.WallPostTool;
    import application.models.ModelData;
    import application.models.Trophies;
    import application.models.UsersTrophies;
    import application.server_api.ApiMethods;
    import application.views.base.ApplicationView;
    import application.views.screen.dialog.FacebookPhotoWindow;
    import application.views.screen.trophy.TrophyRubric;
    import application.views.screen.trophy.TrophyScreen;
    import application.views.text.TextFactory;

    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.controller.Controller;
    import framework.error.ApplicationError;
    import framework.interfaces.IBaseController;
    import framework.socnet.SocNet;
    import framework.underquery.ApiRequestEvent;

    public class TrophyController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function TrophyController(baseController:IBaseController, usersTrophiesModel:UsersTrophies, showShareButton:Boolean = true, useCachedData:Boolean = false)
        {
            super(baseController);
            _trophiesModel = baseController.dataBase.getChildByName(ModelData.TROPHIES_MODEL) as Trophies;
            _usersTrophiesModel = usersTrophiesModel;
            _showShareButton = showShareButton;
            _useCachedData = useCachedData;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _preloaderView = new ApplicationView();
            _preloaderView.setPreloader();
            super.container.addChild(_preloaderView);

            if (_useCachedData)
                _setupView();
            else
                baseController.serverApi.makeRequest(ApiMethods.TROPHIES_GET_USERS_TROPHIES, null, _onUsersTrophiesComplete);

        }

        override public function destroy():void
        {
            if (_preloaderView) {
                _preloaderView.destroy();
                _preloaderView = null;
            }

            if (_trophyView) {
                _trophyView.destroy();
                _trophyView = null;
            }

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _trophiesModel:Trophies;
        private var _usersTrophiesModel:UsersTrophies;
        private var _dataCompleteCount:int;
        private var _showShareButton:Boolean;
        private var _useCachedData:Boolean;

        private var _trophyView:TrophyScreen;
        private var _preloaderView:ApplicationView;
        private var _currentRubric:TrophyRubric;

        private var _pictureAlert:FacebookPhotoWindow;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _onUsersTrophiesComplete(e:ApiRequestEvent):Boolean
        {
            _dataCompleteCount = 0;
            _trophiesModel.addEventListener(Event.CHANGE, _checkDataComplete);
            _usersTrophiesModel.addEventListener(Event.CHANGE, _checkDataComplete);
            return true;
        }

        private function _checkDataComplete(e:Event):void
        {
            if (++_dataCompleteCount >= 2) {
                _trophiesModel.removeEventListener(Event.CHANGE, _checkDataComplete);
                _usersTrophiesModel.removeEventListener(Event.CHANGE, _checkDataComplete);
                _setupView();
            }
        }

        private function _setupView():void
        {
            if (_preloaderView && !_preloaderView.destroyed)
                _preloaderView.destroy();
            _preloaderView = null;

            if (_pictureAlert && !_pictureAlert.destroyed)
                _pictureAlert.destroy();
            _pictureAlert = null;

            _trophyView = new TrophyScreen(
                baseController as ApplicationController,
                _trophiesModel,
                _usersTrophiesModel,
                _showShareButton
            );
            _trophyView.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _trophyView.addEventListener(ApplicationEvent.SHARE, _shareHandler);
            super.container.addChild(_trophyView);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:ApplicationEvent):void
        {
            destroy();
        }

        private function _shareHandler(e:ApplicationEvent):void
        {
            _preloaderView = new ApplicationView();
            _preloaderView.setPreloader();
            super.container.addChild(_preloaderView);

            var rubric:TrophyRubric = e.target as TrophyRubric;
            _currentRubric = rubric;

            var shortText:String = TextFactory.instance.getLabel('congratulate_me_label');
            var postTool:WallPostTool = new WallPostTool(baseController, rubric.shareText, rubric.shareImage, shortText);
            postTool.addEventListener(Event.COMPLETE, _sharingComplete);
            postTool.addEventListener(ErrorEvent.ERROR, _sharingError);
            postTool.addPost();
        }

        private function _sharingComplete(e:Event):void
        {
            if (baseController.socNet.socNetName == SocNet.FACEBOOK_COM && _currentRubric) {
                _pictureAlert = new FacebookPhotoWindow(_trophyView, _currentRubric.shareImage);
                _pictureAlert.addEventListener(Event.COMPLETE, _onShowDialog);
                _pictureAlert.show();
            }
            else {
                if (_preloaderView && !_preloaderView.destroyed)
                    _preloaderView.destroy();
                _preloaderView = null;
            }
        }

        private function _onShowDialog(e:Event):void
        {
            if (_preloaderView && !_preloaderView.destroyed)
                _preloaderView.destroy();
            _preloaderView = null;
        }

        private function _onAlertOk(e:ApplicationEvent):void
        {
            _pictureAlert.destroy();
            _pictureAlert = null;
        }

        private function _sharingError(e:ErrorEvent):void
        {
            var err:ApplicationError = new ApplicationError();
            err.type = ApplicationError.SOCIAL_NETWORK_ERROR;
            err.message = e.text;
            this.baseController.appError(err);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
