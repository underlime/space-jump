/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 12.01.13
 */

package application.controllers
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.UserAchievements;
    import application.server_api.ApiMethods;
    import application.views.base.ApplicationView;
    import application.views.screen.bank.BankScreen;

    import framework.controller.Controller;
    import framework.error.ApplicationError;
    import framework.interfaces.IBaseController;
    import framework.socnet.SocNet;
    import framework.underquery.ApiRequestEvent;

    public class BankController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        public static const COINS_NAME:String = 'coins';
        public static const GEMS_NAME:String = 'gems';

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BankController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _preloaderView = new ApplicationView();
            _preloaderView.setPreloader();
            baseController.container.addChild(_preloaderView);

            this.baseController.serverApi.makeRequest(ApiMethods.BANK_GET_PRICES, {}, _onBankPrices);
        }

        override public function destroy():void
        {
            if (_preloaderView) {
                _preloaderView.destroy();
                _preloaderView = null;
            }

            if (_view) {
                _view.destroy();
                _view = null;
            }

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:BankScreen;
        private var _preloaderView:ApplicationView;
        private var _closeAfterCoins:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _onBankPrices(e:ApiRequestEvent):void
        {
            var pricesData:Array;
            if (e.data['gems_prices'] && e.data['gems_prices'] is Array) {
                pricesData = e.data['gems_prices'];
            }
            else {
                var err:ApplicationError = new ApplicationError();
                err.type = ApplicationError.SEVER_ERROR;
                err.message = 'Prices format error';
                this.baseController.appError(err);
                return;
            }

            _setupView(pricesData);
        }

        private function _setupView(pricesData:Array):void
        {
            if (_preloaderView) {
                _preloaderView.destroy();
                _preloaderView = null;
            }

            _view = new BankScreen(pricesData, baseController.socNet);
            _view.addEventListener(ApplicationEvent.BUY_QUERY, _buyQueryHandler);
            _view.addEventListener(ApplicationEvent.CLOSE, _onCloseButton);
            this.container.addChild(_view);
        }

        private function _onBuyMoneySuccess(data:Object):void
        {
            if (baseController.socNet.socNetName != SocNet.MY_MAIL_RU)
                _updateCoins();
        }

        private function _updateCoins():void
        {
            baseController.serverApi.makeRequest(ApiMethods.USER_GET_COINS, {}, _onCoinsUpdate);
            var userAchievements:UserAchievements = baseController.dataBase.getChildByName(ModelData.USER_ACHIEVEMENTS_MODEL) as UserAchievements;
            if (!userAchievements.checkAchievementByCode('Banking'))
                baseController.serverApi.makeRequest(ApiMethods.ACHIEVEMENTS_CHECK_BANKING);
        }

        private function _onCoinsUpdate(e:ApiRequestEvent):Boolean
        {
            baseController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onCoinsUpdateSuccess);
            return true;
        }

        private function _onCoinsUpdateSuccess(e:ApiRequestEvent):void
        {
            baseController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onCoinsUpdateSuccess);
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.COINS_UPDATED));
            if (_closeAfterCoins)
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _buyQueryHandler(event:ApplicationEvent):void
        {
            this.baseController.socNet.buyMoney(
                GEMS_NAME,
                parseInt(event.data['money']),
                parseInt(event.data['price']),
                _onBuyMoneySuccess
            );
        }

        private function _onCloseButton(e:ApplicationEvent):void
        {
            if (baseController.socNet.socNetName != SocNet.MY_MAIL_RU) {
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CLOSE));
            }
            else {
                _closeAfterCoins = true;
                _updateCoins();
            }

        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
