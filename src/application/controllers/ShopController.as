/**
 * Created by IntelliJ IDEA.
 * User: antondenikin
 * Date: 19.10.12
 * Time: 21:36
 */
package application.controllers
{
    import application.config.Currency;
    import application.events.ApplicationEvent;
    import application.events.ShopEvent;
    import application.models.Inventory;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.server_api.ApiMethods;
    import application.views.screen.dialog.ConfirmWindow;
    import application.views.screen.shop.ShopScreen;
    import application.views.text.TextFactory;

    import framework.controller.Controller;
    import framework.interfaces.IBaseController;

    public class ShopController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShopController(baseController:IBaseController)
        {
            super(baseController);
            _itemsModel = baseController.dataBase.getChildByName(ModelData.ITEMS_MODEL) as Items;
            _inventoryModel = baseController.dataBase.getChildByName(ModelData.INVENTORY_MODEL) as Inventory;
            _userModel = baseController.dataBase.getChildByName(ModelData.USER_MODEL) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupView();
        }

        override public function destroy():void
        {
            if (_view) {
                _view.destroy();
                _view = null;
            }

            if (_bankController) {
                _bankController.destroy();
                _bankController = null;
            }

            if (_confirmWindow) {
                _confirmWindow.destroy();
                _confirmWindow = null;
            }

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:ShopScreen;
        private var _confirmWindow:ConfirmWindow;
        private var _bankController:BankController;
        private var _itemsModel:Items;
        private var _inventoryModel:Inventory;
        private var _userModel:User;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupView():void
        {
            _view = new ShopScreen(this);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(ApplicationEvent.CALL_BUY_MONEY, _buyMoneyHandler);
            _view.addEventListener(ApplicationEvent.CALL_BANK, _callBankHandler);
            //            _view.addEventListener(ApplicationEvent.CLICK_BUY_ITEM, _clickShopBuyHandler);
            //            _view.addEventListener(ApplicationEvent.CLICK_EQUIP_ITEM, _clickShopEquipHandler);

            _view.addEventListener(ShopEvent.EQUIP, _clickShopEquipHandler);
            _view.addEventListener(ShopEvent.BUY, _clickShopBuyHandler);

            this.container.addChild(_view);
        }

        private function _buyItem(itemRecord:ItemRecord, currency:Currency):void
        {
            _view.setPreloader();

            var data:Object = {
                'item_id': itemRecord.id.value,
                'currency': currency.toString()
            };

            this.baseController.serverApi.makeRequest(ApiMethods.ITEMS_BUY, data, _itemCallback);
        }

        private function _itemCallback(data:Object):Boolean
        {
            _view.removePreloader();
            return true;
        }

        private function _equipItem(itemRecord:ItemRecord):void
        {
            _view.setPreloader();
            var data:Object = {'item_id': itemRecord.id.value};
            this.baseController.serverApi.makeRequest(ApiMethods.ITEMS_EQUIP, data, _itemCallback);
        }

        private function _showBank():void
        {
            _bankController = new BankController(this.baseController);
            _bankController.addEventListener(ApplicationEvent.COINS_UPDATED, _closeBank);
            _bankController.addEventListener(ApplicationEvent.CLOSE, _closeBank);
            _bankController.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        private function _buyMoneyHandler(event:ApplicationEvent):void
        {
            this._showBank();
        }

        private function _callBankHandler(e:ApplicationEvent):void
        {
            var text:String = TextFactory.instance.getLabel('no_money_html_text');
            this._confirmWindow = new ConfirmWindow(text);
            this.container.addChild(_confirmWindow);

            this._confirmWindow.addEventListener(ApplicationEvent.CLICK_CONFIRM, _confirmBuyMoneyHandler);
            this._confirmWindow.addEventListener(ApplicationEvent.CLICK_CANCEL, _cancelBuyMoneyHandler);
        }

        private function _confirmBuyMoneyHandler(e:ApplicationEvent):void
        {
            this._confirmWindow.destroy();
            this._confirmWindow = null;
            this._showBank();
        }

        private function _cancelBuyMoneyHandler(e:ApplicationEvent):void
        {
            this._confirmWindow.destroy();
            this._confirmWindow = null;
        }

        private function _closeBank(event:ApplicationEvent):void
        {
            _bankController.destroy();
        }

        private function _clickShopBuyHandler(event:ShopEvent):void
        {
            //            if (e.data['item_record'])
            //                _buyItem(e.data['item_record']);

            var currency:Currency = event.currency;
            if (event.item)
                _buyItem(event.item, currency);
        }

        private function _clickShopEquipHandler(event:ShopEvent):void
        {
            if (event.item)
                _equipItem(event.item);
            //            if (e.data['item_record'])
            //                _equipItem(e.data['item_record']);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get itemsModel():Items
        {
            return _itemsModel;
        }

        public function get inventoryModel():Inventory
        {
            return _inventoryModel;
        }

        public function get userModel():User
        {
            return _userModel;
        }
    }
}
