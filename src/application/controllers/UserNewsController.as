package application.controllers
{
    import application.config.ApplicationConfiguration;
    import application.events.ApplicationEvent;
    import application.helpers.WallPostTool;
    import application.models.ModelData;
    import application.models.User;
    import application.models.UserNews;
    import application.models.UserNewsRead;
    import application.server_api.ApiMethods;
    import application.views.screen.news.NewsView;

    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.controller.Controller;
    import framework.interfaces.IBaseController;
    import framework.underquery.ApiRequestEvent;

    public class UserNewsController extends Controller
    {
        //TODO: заменить картинку
        [Embed(source="../../../lib/share_pics/MISSION_COMPLETE_RU.png")]
        private var _SharePictureRu:Class;

        [Embed(source="../../../lib/share_pics/MISSION_COMPLETE_EN.png")]
        private var _SharePictureEn:Class;

        private var _userModel:User;
        private var _userNewsModel:UserNews;
        private var _userNewsReadModel:UserNewsRead;
        private var _view:NewsView;
        private var _wallPostTool:WallPostTool;
        private var _shareComplete:Boolean;
        private var _readComplete:Boolean;

        public function UserNewsController(baseController:IBaseController)
        {
            super(baseController);
            _userModel = baseController.dataBase.getChildByName(ModelData.USER_MODEL) as User;
            _userNewsModel = baseController.dataBase.getChildByName(ModelData.USER_NEWS_MODEL) as UserNews;
            _userNewsReadModel = baseController.dataBase.getChildByName(ModelData.USER_NEWS_READ_MODEL) as UserNewsRead;
        }

        override public function execute():void
        {
            var appCtrl:ApplicationController = ApplicationController(baseController);
            _view = new NewsView(appCtrl, _userModel, _userNewsModel, _userNewsReadModel);
            _view.addEventListener(ApplicationEvent.CALL_GUEST_SCREEN, _redispatchEvent);
            _view.addEventListener(ApplicationEvent.CALL_LIKE, _onCallLike);
            baseController.container.addChild(_view);
        }

        private function _redispatchEvent(event:ApplicationEvent):void
        {
            this.dispatchEvent(event);
        }

        private function _onCallLike(e:ApplicationEvent):void
        {
            _view.setPreloader();
            _shareComplete = true;
            if (e.data['need_share'])
                _shareLike(e.data['text']);

            var params:Object = {
                'news_id': e.data['news_id'],
                'is_shared': int(e.data['need_share'])
            };
            _readComplete = false;
            baseController.serverApi.makeRequest(ApiMethods.USER_SET_NEWS_READ, params, _onNewsRead);
        }

        private function _shareLike(text:String):void
        {
            _shareComplete = false;

            var sharePic:Bitmap;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                sharePic = new _SharePictureRu();
            else
                sharePic = new _SharePictureEn();

            _wallPostTool = new WallPostTool(baseController, text, sharePic, text);
            _wallPostTool.addEventListener(Event.COMPLETE, _onShareComplete);
            _wallPostTool.addEventListener(ErrorEvent.ERROR, _onShareError);
            _wallPostTool.addPost();
        }

        private function _onNewsRead(e:ApiRequestEvent):Boolean
        {
            _readComplete = true;
            _checkReadComplete();
            return true;
        }

        private function _onShareComplete(e:Event):void
        {
            _shareComplete = true;
            _checkReadComplete();
        }

        private function _onShareError(e:ErrorEvent):void
        {
            _shareComplete = true;
            _checkReadComplete();
        }

        private function _checkReadComplete():void
        {
            if (_readComplete && _shareComplete)
                _view.removePreloader();
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }
    }
}
