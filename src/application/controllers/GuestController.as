/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com))
 * Date: 22.04.13
 * Time: 19:49
 */
package application.controllers
{
    import application.config.ApplicationConfiguration;
    import application.controllers.guest_screen_controller.UserDataRecord;
    import application.events.ApplicationEvent;
    import application.models.Inventory;
    import application.models.Items;
    import application.models.ModelData;
    import application.models.Trophies;
    import application.models.User;
    import application.models.items.ItemRecord;
    import application.models.skin.SkinsCollection;
    import application.server_api.ApiMethods;
    import application.views.base.ApplicationView;
    import application.views.screen.dialog.AlertWindow;
    import application.views.screen.guest.GuestScreen;
    import application.views.text.TextFactory;

    import flash.events.Event;

    import framework.controller.Controller;
    import framework.data.DataContainer;
    import framework.error.ApplicationError;
    import framework.events.GameEvent;
    import framework.interfaces.IBaseController;
    import framework.underquery.ApiRequestEvent;

    import org.casalib.util.ObjectUtil;

    public class GuestController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function GuestController(baseController:IBaseController, socNetId:String)
        {
            super(baseController);
            _socNetId = socNetId;
            _userModel = baseController.dataBase.getChildByName(ModelData.USER_MODEL) as User;
            _inventoryModel = baseController.dataBase.getChildByName(ModelData.INVENTORY_MODEL) as Inventory;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupSkins();
            if (_userDataCache[_socNetId])
                _setupView();
            else
                _requestInfo();
        }

        private function _setupSkins():void
        {
            _skins = (dataBase.getChildByName(ModelData.GAME_MODEL) as DataContainer)
                .getChildByName(ModelData.SKINS_COLLECTION) as SkinsCollection;
        }

        override public function destroy():void
        {
            if (_view && !_view.destroyed)
                _view.destroy();
            _view = null;

            _destroyPreloader();

            if (_missionsController && !_missionsController.destroyed)
                _missionsController.destroy();
            _missionsController = null;

            if (_trophiesController && !_trophiesController.destroyed)
                _trophiesController.destroy();
            _trophiesController = null;

            if (_profitAlert && !_profitAlert.destroyed)
                _profitAlert.destroy();
            _profitAlert = null;

            var event:GameEvent = new GameEvent(GameEvent.DESTROY);
            dispatchEvent(event);

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private static var _userDataCache:Object = {};

        private var _view:GuestScreen;
        private var _preloaderView:ApplicationView;
        private var _skins:SkinsCollection;
        private var _profitAlert:AlertWindow;
        private var _socNetId:String;

        private var _userModel:User;
        private var _inventoryModel:Inventory;

        private var _missionsController:MissionController;
        private var _trophiesController:TrophyController;

        private var _serverInfoReceived:Boolean = false;
        private var _socNetInfoReceived:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _requestInfo():void
        {
            _setPreloader();

            var params:Object = {
                'soc_net_id': _socNetId,
                'base_info_only': 1
            };
            baseController.serverApi.makeRequest(ApiMethods.USER_GET_INFO, params, _onServerUserInfo);
            baseController.socNet.getUserInfo(_socNetId, _onSocNetUserInfo);
        }

        private function _setPreloader():void
        {
            _preloaderView = new ApplicationView();
            _preloaderView.setPreloader();
            baseController.container.addChild(_preloaderView);
        }

        private function _onServerUserInfo(e:ApiRequestEvent):Boolean
        {
            _serverInfoReceived = true;

            var userData:UserDataRecord;
            if (_userDataCache[_socNetId]) {
                userData = _userDataCache[_socNetId];
            }
            else {
                userData = new UserDataRecord();
                _userDataCache[_socNetId] = userData;
            }

            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.SEVER_ERROR;
            error.message = 'Wrong server data format';

            if (e.data['user'])
                userData.userInfo.importData(e.data['user']);
            else
                baseController.appError(error);

            if (e.data['inventory'])
                userData.inventory.importData(e.data['inventory']);
            else
                baseController.appError(error);

            if (e.data['user_achievements'])
                userData.userAchievements.importData(e.data['user_achievements']);
            else
                baseController.appError(error);

            if (_serverInfoReceived && _socNetInfoReceived)
                _setupView();

            return false;
        }

        private function _onSocNetUserInfo(data:Object):Boolean
        {
            _socNetInfoReceived = true;

            var userData:UserDataRecord;
            if (_userDataCache[_socNetId]) {
                userData = _userDataCache[_socNetId];
            }
            else {
                userData = new UserDataRecord();
                _userDataCache[_socNetId] = userData;
            }

            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.SOCIAL_NETWORK_ERROR;
            error.message = 'Wrong server data format';
            if (data['user_info'])
                userData.userInfo.importData(data['user_info']);
            else
                baseController.appError(error);

            if (_serverInfoReceived && _socNetInfoReceived)
                _setupView();

            return false;
        }

        private function _setupView():void
        {
            _destroyPreloader();

            _view = new GuestScreen(_userDataCache[_socNetId], _skins, _userModel);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(ApplicationEvent.EXPLORE_GUEST, _exploreHandler);
            _view.addEventListener(ApplicationEvent.CALL_MISSIONS, _showMissions);
            _view.addEventListener(ApplicationEvent.CALL_TROPHY, _handleTrophies);
            super.container.addChild(_view);
        }

        private function _destroyPreloader():void
        {
            if (_preloaderView && !_preloaderView.destroyed)
                _preloaderView.destroy();
            _preloaderView = null;
        }

        private function _showMissions(e:ApplicationEvent):void
        {
            _missionsController = new MissionController(baseController, _userDataCache[_socNetId].userAchievements);
            _missionsController.execute();
        }

        private function _handleTrophies(e:ApplicationEvent):void
        {
            var userData:UserDataRecord = _userDataCache[_socNetId];
            if (userData.userTrophies.isEmpty)
                _requestTrophies(userData);
            else
                _showTrophiesScreen(userData);
        }

        private function _requestTrophies(userData:UserDataRecord):void
        {
            _setPreloader();

            var params:Object = {
                'user_id': userData.userInfo.id.value
            };
            baseController.serverApi.makeRequest(ApiMethods.TROPHIES_GET_USERS_TROPHIES, params, _onTrophiesResponse);
        }

        private function _onTrophiesResponse(e:ApiRequestEvent):Boolean
        {
            _destroyPreloader();

            var userData:UserDataRecord = _userDataCache[_socNetId];
            var trophiesModel:Trophies = baseController.dataBase.getChildByName(ModelData.TROPHIES_MODEL) as Trophies;

            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.SEVER_ERROR;
            error.message = 'Wrong server data format';

            if (e.data['trophies_collections'])
                trophiesModel.importData(e.data['trophies_collections']);
            else
                baseController.appError(error);

            if (e.data['users_trophies'])
                userData.userTrophies.importData(e.data['users_trophies']);
            else
                baseController.appError(error);

            _showTrophiesScreen(userData);
            return false;
        }

        private function _showTrophiesScreen(userData:UserDataRecord):void
        {
            _trophiesController = new TrophyController(baseController, userData.userTrophies, false, true);
            _trophiesController.execute();
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:ApplicationEvent):void
        {
            _view.removeEventListener(ApplicationEvent.CLOSE, _closeHandler);
            this.destroy();
        }

        private function _exploreHandler(event:ApplicationEvent):void
        {
            _setPreloader();

            var params:Object = {
                'user_id': event.data['user_id']
            };
            baseController.serverApi.makeRequest(ApiMethods.USER_EXPLORE_CAPSULE, params, _onExploreSuccess, _onExploreError);
        }

        private function _onExploreSuccess(e:ApiRequestEvent):Boolean
        {
            _destroyPreloader();
            var userData:UserDataRecord = _userDataCache[_socNetId];
            userData.userInfo.last_explore_time.value = new Date();

            var itemsModel:Items = baseController.dataBase.getChildByName(ModelData.ITEMS_MODEL) as Items;

            var coins:int = e.data['explore_coins'];

            var invId:String = ObjectUtil.getKeys(e.data['inventory']['_add'])[0];
            var itemId:String = e.data['inventory']['_add'][invId]['item'];
            var itemInfo:ItemRecord = itemsModel.getRecordById(itemId) as ItemRecord;
            var itemName:String;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                itemName = itemInfo.name_ru.value;
            else
                itemName = itemInfo.name_en.value;

            var profitString:String = '<p>' + coins + '$</p><p>' + itemName + '</p>';
            var profitText:String = TextFactory.instance.getLabel('explore_profit_label');
            profitText = profitText.replace('{PROFIT}', profitString);

            _profitAlert = new AlertWindow(profitText);
            _profitAlert.addEventListener(ApplicationEvent.CLICK_OK, _onAlertOk);
            _view.addChild(_profitAlert);

            _view.exploreButton.destroy();

            _inventoryModel.addEventListener(Event.CHANGE, _onNewInventory);
            return true;
        }

        private function _onNewInventory(e:Event):void
        {
            _inventoryModel.removeEventListener(Event.CHANGE, _onNewInventory);
            _view.update();
        }

        private function _onExploreError(e:ApiRequestEvent):Boolean
        {
            _destroyPreloader();
            var userData:UserDataRecord = _userDataCache[_socNetId];
            userData.userInfo.last_explore_time.value = new Date();

            var failText:String = TextFactory.instance.getLabel('explore_fail_label');
            _profitAlert = new AlertWindow(failText);
            _profitAlert.addEventListener(ApplicationEvent.CLICK_OK, _onAlertOk);
            _view.addChild(_profitAlert);

            _view.exploreButton.destroy();

            return false;
        }

        private function _onAlertOk(e:ApplicationEvent):void
        {
            _profitAlert.destroy();
            _profitAlert = null;
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
