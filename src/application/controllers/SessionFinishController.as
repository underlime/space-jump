/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.04.13
 * Time: 15:12
 */
package application.controllers
{
    import application.config.ApplicationConfiguration;
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;
    import application.models.User;
    import application.models.session.GameSession;
    import application.server_api.ApiMethods;
    import application.views.base.ApplicationView;

    import by.blooddy.crypto.MD5;

    import com.hurlant.util.Base64;

    import flash.events.Event;

    import framework.controller.Controller;
    import framework.data.ModelsRegistry;
    import framework.interfaces.IBaseController;
    import framework.tools.SaltTool;
    import framework.underquery.ApiRequestEvent;

    import mx.utils.ObjectUtil;

    public class SessionFinishController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SessionFinishController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _session = ModelsRegistry.getModel(ModelData.GAME_SESSION) as GameSession;
            _gameModel = ModelsRegistry.getModel(ModelData.GAME_MODEL) as GameModel;
            _statistics = _gameModel.getChildByName(ModelData.STATISTICS_GAME_MODEL) as StatisticsGameModel;
            _userModel = ModelsRegistry.getModel(ModelData.USER_MODEL) as User;

            _setupView();
            _setupData();
            _sendData();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _data:Object;
        private var _session:GameSession;

        private var _gameModel:GameModel;
        private var _statistics:StatisticsGameModel;

        private var _separator:String = ":";

        private var _view:ApplicationView;
        private var _userModel:User;

        private var _points:int = 0;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupView():void
        {
            _view = new ApplicationView();
            super.container.addChild(_view);
            _view.setPreloader();
        }

        private function _setupData():void
        {
            _points = int(Math.round(_statistics.points*(1 + _userModel.stars.value/100)));

            _data = {
                "session_id": _session.id.value,
                "win": _statistics.success ? 1 : 0,
                "bronze_coins": _statistics.copper,
                "silver_coins": _statistics.silver,
                "gold_coins": _statistics.gold,
                "coins": _statistics.coins,
                "score": _points,
                "trophy_found": _statistics.trophyFound ? 1 : 0,
                "items_used": _getStingilyItems(_statistics.usedItems),
                "cola": _statistics.cola,
                "distance": _statistics.distance,
                "booster_count": _statistics.booster_count,
                "power_shield_count": _statistics.power_shield_count,
                "heart_count": _statistics.heart_count,
                "shield_count": _statistics.shield_count,
                "max_velocity": int(_statistics.maxVelocity),
                "gems_resurrections_count": _statistics.resurrectionCount,
                "gems_spent": _statistics.gemsTotal,
                "physics_tics": Math.round(_statistics.physicsTicks),
                "real_time" : Math.round(_statistics.realTime),
                "pause_seconds": _statistics.pause_seconds,
                "bonus_seconds": _statistics.bonus_seconds,
                "seconds": _statistics.seconds
            };

            _data["sig"] = MD5.hash(_generateSignature());

            trace(ObjectUtil.toString(_data));

        }

        private function _getStingilyItems(usedItems:Array):Array
        {
            var tmp:Array = [];
            for (var i:int = 0; i < usedItems.length; i++)
                tmp.push(usedItems[i].toString());
            return tmp;
        }

        private function _generateSignature():String
        {
            var tmp:Array = [
                _data.session_id,
                _data.win,
                _data.score,
                _data.bronze_coins,
                _data.silver_coins,
                _data.gold_coins,
                _data.coins,
                _data.trophy_found,
                _data.cola,
                _data.distance,
                _data.booster_count,
                _data.power_shield_count,
                _data.heart_count,
                _data.shield_count,
                _data.max_velocity,
                _data.gems_resurrections_count,
                _data.gems_spent,
                _data.physics_tics,
                _data.real_time,
                _data.pause_seconds,
                _data.bonus_seconds,
                _data.seconds
            ];
            var sig:String = tmp.join(_separator);
            sig += _separator + _getUsedItems();
            sig += SaltTool.decode(ApplicationConfiguration.RESULT_SALT, ApplicationConfiguration.Z);

            return sig;
        }

        private function _getUsedItems():String
        {
            if (_statistics.usedItems.length > 0)
                return _statistics.usedItems.join(",") + _separator;
            else
                return _separator;
        }

        private function _sendData():void
        {
            var strData:String = JSON.stringify(_data);
            var data:Object = {
                "result": Base64.encode(strData)
            };

            super.baseController
                .serverApi
                .addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onSuccessFinish);

            super.baseController
                .serverApi
                .makeRequest(ApiMethods.SESSION_FINISH, data);

        }

        private function _onSuccessFinish(e:ApiRequestEvent):void
        {
            super.baseController
                .serverApi
                .removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onSuccessFinish);

            _view.removePreloader();
            _view.destroy();
            super.dispatchEvent(new Event(Event.COMPLETE));
        }


        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
