/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 28.10.12
 */

package application.controllers
{

    import application.config.ApplicationConfiguration;
    import application.config.Config;
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.coins.CoinsUpgradeCollection;
    import application.models.pattern.PatternsCollection;
    import application.models.skin.SkinsCollection;
    import application.server_api.ApiMethods;
    import application.sound.SoundManager;
    import application.views.base.ApplicationView;

    import com.greensock.events.LoaderEvent;
    import com.greensock.loading.LoaderMax;
    import com.greensock.loading.XMLLoader;

    import flash.display.Loader;
    import flash.events.Event;
    import flash.system.ApplicationDomain;
    import flash.system.LoaderContext;
    import flash.system.SecurityDomain;
    import flash.utils.ByteArray;

    import framework.controller.Controller;
    import framework.error.ApplicationError;
    import framework.helpers.ObjectHelper;
    import framework.interfaces.IBaseController;
    import framework.tools.SourceManager;
    import framework.underquery.ApiRequestEvent;

    public class LoadController extends Controller
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function LoadController(baseController:IBaseController)
        {
            super(baseController);
            _appController = baseController as ApplicationController;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupContext();
            _loadBinaryLibs();
            _loadConfiguration();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../lib/config/skins.json", mimeType="application/octet-stream")]
        private var JsonSkins:Class;

        [Embed(source="../../../lib/config/game_defaults.xml", mimeType="application/octet-stream")]
        private var PhysicsConfig:Class;

        [Embed(source="../../../lib/config/patterns.json", mimeType="application/octet-stream")]
        private var JsonPatterns:Class;

        [Embed(source="../../../lib/config/coins_upgrade.json", mimeType="application/octet-stream")]
        private var JsonCoins:Class;

        [Embed(source="../../../lib/swf/JumperLibs.swf", mimeType="application/octet-stream")]
        private var GraphicsLibrary:Class;

        [Embed(source="../../../lib/tips/TipsAnimations.swf", mimeType="application/octet-stream")]
        private var TipsLibrary:Class;

        [Embed(source="../../../lib/swf/JumperAnim.swf", mimeType="application/octet-stream")]
        private var JumperAnimations:Class;

        [Embed(source="../../../lib/swf/JumperEquip.swf", mimeType="application/octet-stream")]
        private var JumperSkins:Class;

        [Embed(source="../../../lib/swf/SoundLibs.swf", mimeType="application/octet-stream")]
        private var SoundsClass:Class;

        [Embed(source="../../../lib/daily_bonus/DailyBonusShine.swf", mimeType="application/octet-stream")]
        private var BonusShineLib:Class;

        private var _loaderContext:LoaderContext;
        private var _configLoader:XMLLoader;
        private var _config:XML;
        private var _appController:ApplicationController;

        private var _userServerDataLoaded:Boolean = false;
        private var _userSocNetDataLoaded:Boolean = false;
        private var _libsLoaded:Boolean = false;

        private var _securityDomain:SecurityDomain;
        private var _framesDelay:int = 0;

        private var _queue:LoaderMax;

        private var _view:ApplicationView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupView():void
        {
            _view = new ApplicationView();
            super.container.addChild(_view);
            _view.setPreloader();
        }

        private function _setupContext():void
        {
            //TODO разобраться, почему работает только так
            var local:Boolean = ApplicationConfiguration.STAND_ALONE;
            //_securityDomain = local ? null : SecurityDomain.currentDomain;
            _securityDomain = null;
            _loaderContext = new LoaderContext(false, SourceManager.instance.appDomain, _securityDomain);
        }

        private function _loadConfiguration():void
        {
            LoaderMax.defaultContext = new LoaderContext(false, ApplicationDomain.currentDomain, null);

            _queue = new LoaderMax({
                name: "mainQueue",
                onComplete: _configLoaded,
                onError: _configError
            });

            _queue.append(new XMLLoader(ApplicationConfiguration.CONFIG_PATH, { "name": "config" }));
            _queue.load();
        }

        private function _loadBinaryLibs():void
        {
            _loadByteArray(new GraphicsLibrary());
            _loadByteArray(new JumperAnimations());
            _loadByteArray(new JumperSkins());
            _loadByteArray(new TipsLibrary());
            _loadByteArray(new BonusShineLib());
            _loadSoundsByteArray(new SoundsClass());

            _waitFrame();
        }

        private function _loadByteArray(byteArray:ByteArray):void
        {
            var bytesLoader:Loader = new Loader();
            bytesLoader.loadBytes(byteArray, _loaderContext);
        }

        private function _loadSoundsByteArray(byteArray:ByteArray):void
        {
            var bytesLoader:Loader = new Loader();
            _loaderContext.applicationDomain = SoundManager.instance.appDomain;

            bytesLoader.loadBytes(byteArray, _loaderContext);
        }

        private function _waitFrame():void
        {
            super.container.addEventListener(Event.ENTER_FRAME, _enterFrameHandler);
            trace("графические библиотеки загружены");
        }

        private function _enterFrameHandler(event:Event):void
        {
            _framesDelay++;
            if (_framesDelay > 7) {
                super.container.removeEventListener(Event.ENTER_FRAME, _enterFrameHandler);
                _libsLoaded = true;
                _setupView();
                _checkComplete();
            }
        }

        private function _onSocNetSuccess(data:Object):Boolean
        {
            trace('данные соц. сети загружены');
            _userSocNetDataLoaded = true;
            _checkComplete();
            return true;
        }

        private function _checkComplete():void
        {
            if (_userServerDataLoaded && _userSocNetDataLoaded && _libsLoaded) {
                if (_view && !_view.destroyed) {
                    _view.removePreloader();
                    _view.destroy();
                }
                super.dispatchEvent(new Event(Event.COMPLETE));
            }
        }

        private function _setCommonConfig():void
        {
            var config:Config = _appController.config;
            config.invokeDataImport(ObjectHelper.xmlToObject(_config));
        }

        private function _setupSkinsModel():void
        {
            var bytes:ByteArray = new JsonSkins();
            var json:String = bytes.readUTFBytes(bytes.length);
            var data:Object = JSON.parse(json);
            var model:SkinsCollection = new SkinsCollection();
            if (data.skins)
                model.importData(data.skins);
            else
                throw new Error("Skins not found!");

            var rootModel:GameModel = super.dataBase.getChildByName(ModelData.GAME_MODEL) as GameModel;
            rootModel.addChild(model);
            trace("скины прогружена");
        }

        private function _setupPhysicsData():void
        {
            var bytes:ByteArray = new PhysicsConfig();
            var xmlString:String = bytes.readUTFBytes(bytes.length);
            var data:Object = ObjectHelper.xmlToObject(new XML(xmlString));

            var config:Config = _appController.config;
            config.invokeDataImport(data);

            trace("Физические параметры установлены");
        }

        private function _setupPatternsModel():void
        {
            var bytes:ByteArray = new JsonPatterns();
            var json:String = bytes.readUTFBytes(bytes.length);

            var data:Object = JSON.parse(json);

            var rootModel:GameModel = super.dataBase.getChildByName(ModelData.GAME_MODEL) as GameModel;
            var model:PatternsCollection = rootModel.getChildByName(ModelData.PATTERNS_COLLECTION) as PatternsCollection;

            if (data.patterns_collection)
                model.importData(data.patterns_collection);

            trace("Паттерны установлены");
        }

        private function _setupCoinsConfig():void
        {
            var bytes:ByteArray = new JsonCoins();
            var json:String = bytes.readUTFBytes(bytes.length);

            var data:Object = JSON.parse(json);

            var rootModel:GameModel = super.dataBase.getChildByName(ModelData.GAME_MODEL) as GameModel;

            var model:CoinsUpgradeCollection = rootModel.getChildByName(ModelData.COINS_COLLECTION) as CoinsUpgradeCollection;
            if (data.coins_upgrade_collection)
                model.importData(data.coins_upgrade_collection);

            trace("конфиг апргрейда монет готов");
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _configLoaded(event:LoaderEvent):void
        {
            _config = new XML(_queue.getContent("config"));
            _setCommonConfig();
            super.dispatchEvent(new Event(ApplicationEvent.CONFIG_LOADED));
            trace("конфиг загружен");

            _appController.serverApi.makeRequest(ApiMethods.USER_GET_INFO, null, _onServerSuccess, _onServerError);
            _appController.socNet.getCurrentUserFullInfo(_onSocNetSuccess);

            _setupSkinsModel();
            _setupPhysicsData();
            _setupPatternsModel();
            _setupCoinsConfig();
        }

        private function _onServerSuccess(e:ApiRequestEvent):Boolean
        {
            _appController.serverApi.addEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);
            return true;
        }

        private function _onApiComplete(event:ApiRequestEvent):void
        {
            _appController.serverApi.removeEventListener(ApiRequestEvent.SUCCESS_EVENT, _onApiComplete);

            trace('данные пользователя с сервера загружены');
            _userServerDataLoaded = true;
            _checkComplete();
        }

        private function _onServerError(e:ApiRequestEvent):Boolean
        {
            var registerNeed:Boolean = (e.errorType == ApiRequestEvent.ERROR_TYPE_PROGRAM && e.data['code'] == 401);
            if (registerNeed)
                _appController.socNet.getReferrerInfo(_registerUser);
            return !registerNeed;
        }

        private function _registerUser(data:Object):void
        {
            var referrer:String = data['referrer_data']['type'];
            var params:Object = {
                'referrer': referrer
            };
            _appController.serverApi.makeRequest(ApiMethods.USER_REGISTER, params, _onServerSuccess);
        }

        private function _configError(event:LoaderEvent):void
        {
            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.LOAD_ERROR;
            error.message = event.text;
            this.baseController.appError(error);
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
