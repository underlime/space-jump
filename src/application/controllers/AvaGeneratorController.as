/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 05.04.13
 * Time: 11:03
 */
package application.controllers
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.UserAchievements;
    import application.server_api.ApiMethods;
    import application.views.screen.avatar.AvatarGeneratorScreen;

    import framework.controller.Controller;
    import framework.error.ApplicationError;
    import framework.interfaces.IBaseController;
    import framework.view.View;

    public class AvaGeneratorController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function AvaGeneratorController(baseController:IBaseController, mainScreen:View)
        {
            super(baseController);
            _mainScreen = mainScreen;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _view = new AvatarGeneratorScreen(_mainScreen);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(ApplicationEvent.UPLOAD_ERROR, _onUploadError);
            _view.addEventListener(ApplicationEvent.AVATAR_COMPLETE, _onAvatarComplete);
            super.container.addChild(_view);
        }

        override public function destroy():void
        {
            _view.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:AvatarGeneratorScreen;
        private var _mainScreen:View;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _closeHandler(event:ApplicationEvent):void
        {
            this.destroy();
        }

        private function _onUploadError(e:ApplicationEvent):void
        {
            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.SOCIAL_NETWORK_ERROR;
            error.message = 'Photo upload error';
            baseController.appError(error);
        }

        private function _onAvatarComplete(e:ApplicationEvent):void
        {
            var userAchievements:UserAchievements = baseController.dataBase.getChildByName(ModelData.USER_ACHIEVEMENTS_MODEL) as UserAchievements;
            if (!userAchievements.checkAchievementByCode('Picture'))
                baseController.serverApi.makeRequest(ApiMethods.ACHIEVEMENTS_CHECK_PICTURE);
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
