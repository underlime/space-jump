package application.controllers.guest_screen_controller
{
    import application.models.Inventory;
    import application.models.User;
    import application.models.UserAchievements;
    import application.models.UsersTrophies;

    public class UserDataRecord
    {
        public var userInfo:User = new User();
        public var inventory:Inventory = new Inventory();
        public var userAchievements:UserAchievements = new UserAchievements();
        public var userTrophies:UsersTrophies = new UsersTrophies();
    }
}
