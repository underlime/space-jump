package application.controllers
{
    import application.controllers.ping_earth_controller.EarthPinger;
    import application.events.ApplicationEvent;
    import application.helpers.WallPostTool;
    import application.models.ModelData;
    import application.models.UserAchievements;
    import application.models.UsersTrophies;
    import application.server_api.ApiMethods;
    import application.views.screen.dialog.FacebookPhotoWindow;
    import application.views.screen.main.MainScreenView;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;

    import framework.controller.Controller;
    import framework.error.ApplicationError;
    import framework.interfaces.IBaseController;
    import framework.socnet.SocNet;
    import framework.underquery.ApiRequestEvent;

    public class PingEarthController extends Controller
    {
        private var _earthPinger:EarthPinger;
        private var _view:MainScreenView;
        private var _picture:Bitmap;
        private var _wallPostTool:WallPostTool;
        private var _pictureAlert:FacebookPhotoWindow;

        public function PingEarthController(baseController:IBaseController, view:MainScreenView)
        {
            super(baseController);
            _view = view;

            _earthPinger = new EarthPinger(_view.cosmosLayer);
            _earthPinger.addEventListener(ApplicationEvent.COMPLETE, _onPhotoUploaded);
            _earthPinger.addEventListener(IOErrorEvent.IO_ERROR, _onPhotoUploadError);
            _earthPinger.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onPhotoUploadError);
        }

        override public function destroy():void
        {
            if (_earthPinger && _earthPinger.destroyed)
                _earthPinger.destroy();
            _earthPinger = null;

            if (_wallPostTool && _wallPostTool.destroyed)
                _wallPostTool.destroy();
            _wallPostTool = null;

            if (_pictureAlert && !_pictureAlert.destroyed)
                _pictureAlert.destroy();
            _pictureAlert = null;
        }

        override public function execute():void
        {
            _view.setPreloader();

            baseController.serverApi.makeRequest(
                ApiMethods.TROPHIES_GET_USERS_TROPHIES,
                null,
                _onUsersTrophiesComplete
            );
        }

        private function _onUsersTrophiesComplete(e:ApiRequestEvent):Boolean
        {
            var usersTrophiesModel:UsersTrophies = baseController.dataBase.getChildByName(ModelData.USERS_TROPHIES_MODEL) as UsersTrophies;
            usersTrophiesModel.addEventListener(Event.CHANGE, _onUserTrophies);
            return true;
        }

        private function _onUserTrophies(e:Event):void
        {
            e.target.removeEventListener(Event.CHANGE, _onUserTrophies);

            var data:BitmapData = _earthPinger.createBitmapData();
            _picture = new Bitmap(data.clone()); //клонирование здесь обязательно

            var message:String = TextFactory.instance.getLabel('iplaysd');
            var shortText:String = TextFactory.instance.getLabel('i_label');
            _wallPostTool = new WallPostTool(baseController, message, _picture, shortText);
            _wallPostTool.addEventListener(Event.COMPLETE, _onPhotoUploaded);
            _wallPostTool.addEventListener(ErrorEvent.ERROR, _onPhotoUploadError);
            _wallPostTool.addPost();
        }

        private function _onPhotoUploaded(e:Event):void
        {
            if (baseController.socNet.socNetName == SocNet.FACEBOOK_COM) {
                _pictureAlert = new FacebookPhotoWindow(_view, _picture);
                _pictureAlert.addEventListener(Event.COMPLETE, _onShowDialog);
                _pictureAlert.show();
            }
            else {
                _view.removePreloader();
            }

            var userAchievements:UserAchievements = baseController.dataBase.getChildByName(ModelData.USER_ACHIEVEMENTS_MODEL) as UserAchievements;
            if (!userAchievements.checkAchievementByCode('ping_home'))
                baseController.serverApi.makeRequest(ApiMethods.ACHIEVEMENTS_CHECK_PING_EARTH);
        }

        private function _onShowDialog(e:Event):void
        {
            _view.removePreloader();
        }

        private function _onPhotoUploadError(e:ErrorEvent):void
        {
            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.SOCIAL_NETWORK_ERROR;
            error.message = e.text;
            baseController.appError(error);
        }
    }
}
