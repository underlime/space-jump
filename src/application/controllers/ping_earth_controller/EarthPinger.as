package application.controllers.ping_earth_controller
{
    import application.config.ApplicationConfiguration;
    import application.models.ModelData;
    import application.models.User;
    import application.models.UserAchievements;
    import application.models.UsersTrophies;
    import application.views.base.ApplicationView;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.text.TextFieldAutoSize;

    import flashx.textLayout.formats.TextAlign;

    import framework.data.ModelsRegistry;
    import framework.helpers.DateHelper;
    import framework.tools.ScreenShot;
    import framework.tools.SourceManager;
    import framework.view.View;
    import framework.view.text.SimpleTextField;

    import org.casalib.events.RemovableEventDispatcher;
    import org.casalib.util.NumberUtil;

    public class EarthPinger extends RemovableEventDispatcher
    {
        private static const FRAME_RU:String = 'PING_EARTH_RU';
        private static const FRAME_EN:String = 'PING_EARTH_EN';

        private var _view:View;
        private var _frameObject:Bitmap;

        private var _pingPicturePhoto:ScreenShot;
        private var _pingPicture:ApplicationView;

        public function EarthPinger(view:View)
        {
            _view = view;
            _createFrameObject();
            super();
        }

        private function _createFrameObject():void
        {
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                _frameObject = SourceManager.instance.getBitmap(FRAME_RU);
            else
                _frameObject = SourceManager.instance.getBitmap(FRAME_EN);
        }

        override public function destroy():void
        {
            if (_pingPicture) {
                _pingPicture.destroy();
                _pingPicture = null;
            }

            if (_pingPicturePhoto) {
                _pingPicturePhoto.destroy();
                _pingPicturePhoto = null;
            }

            super.destroy();
        }

        public function saveToDisk():void
        {
            _createPicture();
            _pingPicturePhoto.saveToDisk();
        }

        public function createBitmapData():BitmapData
        {
            _createPicture();
            return _pingPicturePhoto.rawData;
        }

        private function _createPicture():void
        {
            if (_pingPicture)
                _pingPicture.destroy();

            _pingPicture = new ApplicationView();
            _view.addChild(_pingPicture);

            _getCosmonautShot();
            _pingPicture.addChild(_frameObject);
            _placeText();
            _takeFramePhoto();

            _view.removeChild(_pingPicture);
        }

        private function _getCosmonautShot():void
        {
            var cosmonautPhoto:ScreenShot = new ScreenShot();

            var startX:int = 120;
            var startY:int = 30;
            cosmonautPhoto.takeShot(
                _view,
                _frameObject.width,
                _frameObject.height,
                startX,
                startY
            );

            var shotData:Bitmap = new Bitmap(cosmonautPhoto.rawData.clone());
            _pingPicture.addChild(shotData);

            cosmonautPhoto.destroy();
        }

        private function _placeText():void
        {
            var textFactory:TextFactory = TextFactory.instance;
            var userModel:User = ModelsRegistry.getModel(ModelData.USER_MODEL) as User;
            var usersTrophiesModel:UsersTrophies = ModelsRegistry.getModel(ModelData.USERS_TROPHIES_MODEL) as UsersTrophies;
            var usersAchievementsModel:UserAchievements = ModelsRegistry.getModel(ModelData.USER_ACHIEVEMENTS_MODEL) as UserAchievements;

            var dltX:Number = 0;
            if (userModel.summary_time.value >= 86400)
                dltX = 60;
            else if (userModel.summary_time.value >= 3600)
                dltX = 40;
            else if (userModel.summary_time.value >= 60)
                dltX = 20;

            
            var field:SimpleTextField = _createTextField();
            field.size = 20;
            field.fontFamily = SimpleTextField.LOBSTER_FONT;
            field.x = 35;
            field.y = 53;
            field.rotation = -3;
            _pingPicture.addChild(field);
            field.text = _getCurrentDateText();

            field = _createTextField();
            field.size = 20;
            field.fontFamily = SimpleTextField.LOBSTER_FONT;
            field.x = 555;
            field.y = 392;
            field.autoSize = TextFieldAutoSize.RIGHT;
            field.align = TextAlign.RIGHT;
            field.rotation = -3;
            _pingPicture.addChild(field);
            field.text = textFactory.getLabel('iamonorbit_label');

            field = _createTextField();
            field.size = 20;
            field.x = 20;
            field.y = 516;
            _pingPicture.addChild(field);
            field.text = textFactory.getLabel('my_record_label').toUpperCase();

            field = _createTextField();
            field.size = 20;
            field.x = 20;
            field.y = 547;
            _pingPicture.addChild(field);
            field.text = NumberUtil.format(userModel.max_score_global.value, ' ');

            field = _createTextField();
            field.x = 306 - dltX;
            field.y = 522;
            _pingPicture.addChild(field);
            field.text = textFactory.getLabel('objectives_label').toUpperCase() + ': ' + NumberUtil.format(userModel.stars.value, ' ');

            field = _createTextField();
            field.x = 465 - dltX;
            field.y = 522;
            _pingPicture.addChild(field);
            field.text = textFactory.getLabel("jump_count_label").toUpperCase() + ' ' + NumberUtil.format(userModel.jumps_count.value, ' ');

            field = _createTextField();
            field.x = 306 - dltX;
            field.y = 539;
            _pingPicture.addChild(field);
            field.text = textFactory.getLabel('trophies_found_label').toUpperCase() + ' ' + NumberUtil.format(usersTrophiesModel.length, ' ');

            field = _createTextField();
            field.x = 465 - dltX;
            field.y = 539;
            _pingPicture.addChild(field);
            field.text = textFactory.getLabel('landing_count_label').toUpperCase() + ' ' + NumberUtil.format(userModel.landings_count.value, ' ');

            field = _createTextField();
            field.x = 306 - dltX;
            field.y = 555;
            _pingPicture.addChild(field);
            field.text = textFactory.getLabel('quest_completed_label').toUpperCase() + ' ' + NumberUtil.format(usersAchievementsModel.length, ' ');

            field = _createTextField();
            field.x = 465 - dltX;
            field.y = 555;
            _pingPicture.addChild(field);
            var timeString:String = DateHelper.formatSecondsStamp(userModel.summary_time.value, _getDateLang());
            field.text = textFactory.getLabel('flying_time_label').toUpperCase() + ' ' + timeString;
        }

        private function _createTextField():SimpleTextField
        {
            var textField:SimpleTextField = new SimpleTextField();
            textField.color = 0xFFFFFF;
            textField.size = 9;
            textField.autoSize = TextFieldAutoSize.LEFT;
            textField.width = 100;

            return textField;
        }

        private function _getCurrentDateText():String
        {
            var format:String;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                format = '%d.%m.%Y';
            else
                format = '%m/%d/%Y';
            return DateHelper.getCurrentDateString(format);
        }

        private function _getDateLang():String
        {
            var lang:String;
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                lang = DateHelper.LANG_RU;
            else
                lang = DateHelper.LANG_EN;
            return lang;
        }

        private function _takeFramePhoto():void
        {
            if (_pingPicturePhoto)
                _pingPicturePhoto.destroy();

            _pingPicturePhoto = new ScreenShot();
            _pingPicturePhoto.takeShot(_pingPicture);
        }
    }

}
