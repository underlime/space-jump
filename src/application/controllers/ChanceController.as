/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 24.06.13
 * Time: 12:13
 */
package application.controllers
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.StatisticsGameModel;
    import application.models.User;
    import application.views.game.base.common.ChanceView;

    import flash.events.Event;

    import framework.controller.Controller;
    import framework.data.ModelsRegistry;
    import framework.interfaces.IBaseController;

    public class ChanceController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ChanceController(baseController:IBaseController, statisticsModel:StatisticsGameModel)
        {
            super(baseController);
            _statisticsModel = statisticsModel;
            _user = ModelsRegistry.getModel(ModelData.USER_MODEL) as User;
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            if (_statisticsModel.resurrectionCount > 0)
                _currentPrice = _statisticsModel.gemsCurrent * _statisticsModel.gemsMultiply;
            else
                _currentPrice = 1;

            _chanceView = new ChanceView(_currentPrice);

            _statisticsModel.gemsCurrent = _currentPrice;

            _chanceView.addEventListener(ApplicationEvent.CLICK_CANCEL, _cancelHandler);
            _chanceView.addEventListener(ApplicationEvent.CLICK_CONFIRM, _confirmHandler);

            super.container.addChild(_chanceView);
            _user.addEventListener(Event.CHANGE, _userModelChangeHandler);
            _updateGemsField();
        }

        override public function destroy():void
        {
            _chanceView.destroy();
            _user.removeEventListener(Event.CHANGE, _userModelChangeHandler);
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _chanceView:ChanceView;
        private var _statisticsModel:StatisticsGameModel;
        private var _user:User;
        private var _currentPrice:int = 1;

        private var _bankController:BankController;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _updateGemsField():void
        {
            _chanceView.gemsField.value = (_user.gems.value - _statisticsModel.gemsTotal);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _confirmHandler(event:ApplicationEvent):void
        {
            _chanceView.removeCounter();
            if (_statisticsModel.gemsTotal + _currentPrice > _user.gems.value) {
                _bankController = new BankController(super.baseController);

                _bankController.addEventListener(ApplicationEvent.CLOSE, _bankCloseHandler);
                _bankController.addEventListener(ApplicationEvent.COINS_UPDATED, _bankCloseHandler);

                _bankController.execute();
            } else {
                _statisticsModel.gemsTotal += _currentPrice;
                _statisticsModel.resurrectionCount++;
                super.dispatchEvent(new ApplicationEvent(ApplicationEvent.CONTINUE_GAME));
            }
        }

        private function _bankCloseHandler(event:ApplicationEvent):void
        {
            _bankController.destroy();
        }


        private function _cancelHandler(event:ApplicationEvent):void
        {
            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.END_GAME));
        }

        private function _userModelChangeHandler(event:Event):void
        {
            _updateGemsField();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
