/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 26.04.13
 * Time: 14:40
 */
package application.controllers
{
    import application.events.ApplicationEvent;
    import application.models.GameModel;
    import application.models.ModelData;
    import application.models.session.GameSession;
    import application.server_api.ApiMethods;
    import application.views.base.ApplicationView;

    import flash.events.Event;

    import framework.controller.Controller;
    import framework.data.ModelsRegistry;
    import framework.interfaces.IBaseController;
    import framework.underquery.ApiRequestEvent;

    public class SessionInitController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SessionInitController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _setupView();
            _sessionInit();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _view:ApplicationView;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupView():void
        {
            _view = new ApplicationView();
            super.container.addChild(_view);
            _view.setPreloader();
        }

        private function _sessionInit():void
        {
            var gameModel:GameModel = ModelsRegistry.getModel(ModelData.GAME_MODEL) as GameModel;
            var data:Object = {
                "difficulty": gameModel.gameMode
            };

            super.baseController
                    .serverApi
                    .makeRequest(ApiMethods.SESSION_INIT, data, _onSuccessInit, _onErrorInit);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _onSuccessInit(e:ApiRequestEvent):Boolean
        {
            var _gameSession:GameSession = ModelsRegistry.getModel(ModelData.GAME_SESSION) as GameSession;
            _gameSession.addEventListener(Event.CHANGE, _importCompleteHandler);
            return true;
        }

        private function _importCompleteHandler(e:Event):void
        {
            e.currentTarget.removeEventListener(Event.CHANGE, _importCompleteHandler);
            _view.removePreloader();
            _view.destroy();

            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.SESSION_INIT));
        }

        private function _onErrorInit(e:ApiRequestEvent):void
        {
            _view.removePreloader();
            _view.destroy();

            super.dispatchEvent(new ApplicationEvent(ApplicationEvent.SESSION_ERROR));
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
