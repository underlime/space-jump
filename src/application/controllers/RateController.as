/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 06.02.13
 * Time: 10:50
 */
package application.controllers
{
    import application.events.ApplicationEvent;
    import application.models.ModelData;
    import application.models.TopDayModel;
    import application.models.TopGlobalModel;
    import application.models.TopMonthModel;
    import application.models.TopWeekModel;
    import application.models.top.GeneralTopModel;
    import application.server_api.ApiMethods;
    import application.views.base.ApplicationView;
    import application.views.screen.rate.RateScreen;

    import flash.events.Event;

    import framework.controller.Controller;
    import framework.interfaces.IBaseController;
    import framework.socnet.models.SocNetInfoRegistry;

    public class RateController extends Controller
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function RateController(baseController:IBaseController)
        {
            super(baseController);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function execute():void
        {
            _getModels();
            _activeModel = _topDayModel;
            _makeServerDataRequest(ApiMethods.TOP_GET_DAY);
        }

        override public function destroy():void
        {
            if (_view) {
                _view.destroy();
                _view = null;
            }

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public static const TAB_DAY:int = 0;
        public static const TAB_WEEK:int = 1;
        public static const TAB_MONTH:int = 2;
        public static const TAB_GLOBAL:int = 3;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _preloadView:ApplicationView;
        private var _view:RateScreen;

        private var _topDayModel:TopDayModel;
        private var _topWeekModel:TopWeekModel;
        private var _topMonthModel:TopMonthModel;
        private var _topGlobalModel:TopGlobalModel;
        private var _activeModel:GeneralTopModel;

        private var _socNetRegistry:SocNetInfoRegistry;
        private var _guestController:GuestController;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _getModels():void
        {
            _topDayModel = this.baseController.dataBase.getChildByName(ModelData.TOP_DAY_MODEL) as TopDayModel;
            _topWeekModel = this.baseController.dataBase.getChildByName(ModelData.TOP_WEEK_MODEL) as TopWeekModel;
            _topMonthModel = this.baseController.dataBase.getChildByName(ModelData.TOP_MONTH_MODEL) as TopMonthModel;
            _topGlobalModel = this.baseController.dataBase.getChildByName(ModelData.TOP_GLOBAL_MODEL) as TopGlobalModel;

            _socNetRegistry = this.baseController.dataBase.getChildByName(ModelData.SOC_NET_INFO_REGISTRY) as SocNetInfoRegistry;
        }

        private function _makeServerDataRequest(method:String):void
        {
            _setPreloader();
            this.baseController.serverApi.makeRequest(method, {}, _onServerResponse);
        }

        private function _setPreloader():void
        {
            _preloadView = new ApplicationView();
            _preloadView.setPreloader();
            this.container.addChild(_preloadView);
        }

        private function _onServerResponse(arg:*):Boolean
        {
            _activeModel.addEventListener(Event.CHANGE, _onTopModelServerData);
            return true;
        }

        private function _onTopModelServerData(e:Event):void
        {
            _activeModel.removeEventListener(Event.CHANGE, _onTopModelServerData);
            _getSocNetInfo();
        }

        private function _getSocNetInfo():void
        {
            var socNetIdsList:Array = _activeModel.getSocNetIdsList();
            var notExistsIds:Array = _socNetRegistry.checkNotExists(socNetIdsList);

            if (notExistsIds.length)
                this.baseController.socNet.getUsersListInfo(notExistsIds, _onSocNetResponse);
            else
                _showResult();
        }

        private function _onSocNetResponse(data:Object):Boolean
        {
            _socNetRegistry.addEventListener(Event.CHANGE, _onSocNetModelData);
            return true;
        }

        private function _onSocNetModelData(e:Event):void
        {
            _socNetRegistry.removeEventListener(Event.CHANGE, _onSocNetModelData);
            _showResult();
        }

        private function _showResult():void
        {
            _removePreloader();
            if (_view)
                _view.showTop();
            else
                _buildView();
        }

        private function _removePreloader():void
        {
            if (_preloadView) {
                _preloadView.destroy();
                _preloadView = null;
            }
        }

        private function _buildView():void
        {
            if (_view)
                _view.destroy();

            _view = new RateScreen(this.baseController as ApplicationController);
            _view.addEventListener(ApplicationEvent.NEED_DATA, _needDataHandler);
            _view.addEventListener(ApplicationEvent.CLOSE, _closeHandler);
            _view.addEventListener(ApplicationEvent.CALL_GUEST_SCREEN, _callGuestHandler);

            this.container.addChild(_view);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _callGuestHandler(event:ApplicationEvent):void
        {
            if (event.data['soc_net_id']) {
                _guestController = new GuestController(super.baseController, event.data['soc_net_id']);
                _guestController.execute();
            }
            else {
                throw new Error("Wrong data");
            }
        }

        private function _needDataHandler(event:ApplicationEvent):void
        {
            var method:String;
            switch (event.data['tab_id']) {
                case TAB_DAY:
                    _activeModel = _topDayModel;
                    method = ApiMethods.TOP_GET_DAY;
                    break;
                case TAB_WEEK:
                    _activeModel = _topWeekModel;
                    method = ApiMethods.TOP_GET_WEEK;
                    break;
                case TAB_MONTH:
                    _activeModel = _topMonthModel;
                    method = ApiMethods.TOP_GET_MONTH;
                    break;
                case TAB_GLOBAL:
                    _activeModel = _topGlobalModel;
                    method = ApiMethods.TOP_GET_GLOBAL;
                    break;
                default:
                    throw new Error('Wrong tab id');
            }

            _makeServerDataRequest(method);
        }

        private function _closeHandler(event:ApplicationEvent):void
        {
            if (_guestController && !_guestController.destroyed)
                _guestController.destroy();
            _guestController = null;

            if (_view && !_view.destroyed)
                _view.destroy();
            _view = null;

            this.destroy();
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
