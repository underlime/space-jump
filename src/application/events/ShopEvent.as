/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 25.06.13
 * Time: 15:03
 */
package application.events
{
    import application.config.Currency;
    import application.models.items.ItemRecord;

    import flash.events.Event;

    public class ShopEvent extends ApplicationEvent
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const BUY:String = "BuyEvent";
        public static const EQUIP:String = "EquipEvent";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ShopEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function clone():Event
        {
            var event:ShopEvent = new ShopEvent(this.type, this.bubbles);
            event.data = this.data;
            event.currency = _currency;
            event.item = _item;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _currency:Currency = Currency.GEMS;
        private var _item:ItemRecord;

        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/

        public function get currency():Currency
        {
            return _currency;
        }

        public function set currency(value:Currency):void
        {
            _currency = value;
        }

        public function get item():ItemRecord
        {
            return _item;
        }

        public function set item(value:ItemRecord):void
        {
            _item = value;
        }
    }
}
