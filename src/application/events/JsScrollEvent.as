package application.events
{
    import flash.events.Event;

    public class JsScrollEvent extends Event
    {
        public static const SCROLL:String = 'jsScroll';

        public var dir:int;

        public function JsScrollEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }
    }
}
