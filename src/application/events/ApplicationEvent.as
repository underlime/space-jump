/**
 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
 */
package application.events
{

    import flash.events.Event;

    import framework.events.GameEvent;

    public class ApplicationEvent extends GameEvent
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const COMPLETE:String = Event.COMPLETE;

        public static const CALL_GAME:String = "CallGameApplicationEvent";
        public static const END_GAME:String = "EndGameApplicationEvent";
        public static const CALL_MISSIONS:String = "CallMissionsEvent";
        public static const VELOCITY_CHANGE:String = "VelocityChangeApplicationEvent";
        public static const LAYER_CHANGE:String = "LayerChangeApplicationEvent";
        public static const CONFIG_LOADED:String = "onConfigLoaded";

        public static const CALL_PING_EARTH:String = "onCallPingEarth";
        public static const CLOSE:String = "CloseApplicationEvent";
        public static const APPLICATION_VIEW_DESTROY:String = 'ApplicationViewClose';
        public static const CALL_BUY_MONEY:String = "onCallBuyMoney";
        public static const CALL_REPAIR:String = "CallRepairEvent";
        public static const CALL_DONT_NEED_REPAIR:String = "CallDontNeedRepairEvent";

        public static const BUY_QUERY:String = "BuyQueryEvent";
        public static const COINS_UPDATED:String = "onCoinsUpdated";

        public static const CALL_SHOP:String = "CallShopEvent";
        public static const CALL_BANK:String = "CallBankEvent";
        public static const CLICK_BUY_ITEM:String = 'onClickShopButtonBuy';
        public static const CLICK_EQUIP_ITEM:String = 'onClickShopButtonEquip';

        public static const CALL_TOP:String = "CallTopEvent";

        public static const UPDATE:String = "ApplciationEventUpdate";
        public static const DESTROY:String = "ApplicationEventDestroy";

        public static const NEED_DATA:String = 'onNeedData';
        public static const BLOCK:String = "BlockEvent";
        public static const CALL_TROPHY:String = "CallTrophyEvent";
        public static const SHARE:String = "ShareApplicationEvent";
        public static const CALL_AVATAR_GENERATOR:String = "CallAvatarGaneratorEvent";
        public static const UPLOAD_ERROR:String = "UploadError";

        public static const CLICK_CANCEL:String = "ClickCancelEvent";
        public static const CLICK_CONFIRM:String = "ClickConfirmEvent";
        public static const CLICK_OK:String = "ClickOkEvent";

        public static const SCREEN_SHOT:String = "ScreenShotEvent";
        public static const CALL_PARACHUTE:String = "CallParachuteEvent";
        public static const CALL_RESURRECTION:String = "CallResurrectionEvent";
        public static const CALL_GUEST_SCREEN:String = "CallGuestScreenEvent";
        public static const EXPLORE_GUEST:String = "ExploreGuestEvent";

        public static const SESSION_INIT:String = "SessionInitEvent";
        public static const SESSION_ERROR:String = "SessionErrorEvent";
        public static const SESSION_FINISH:String = "SessionFinishEvent";

        public static const TRAINING_COMPLETE:String = 'TrainingComplete';
        public static const AVATAR_COMPLETE:String = 'AvatarComplete';
        public static const CALL_PAUSE:String = "CallPauseEvent";

        public static const CALL_CHANCE:String = "CallChanceEvent";
        public static const CONTINUE_GAME:String = "ContinueGameHandler";
        public static const TIME_UPDATE:String = "TimeUpdateEvent";
        public static const CALL_DEBUG_TIMER:String = "CallDebugTimerHandler";
        public static const CALL_FRIEND_BONUS:String = 'CallFriendBonus';
        public static const CALL_NEWS:String = 'CallNews';
        public static const CALL_LIKE:String = 'CallLike';

        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ApplicationEvent(type:String, bubbles:Boolean = false)
        {
            super(type, bubbles);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/


        override public function clone():Event
        {
            var event:ApplicationEvent = new ApplicationEvent(this.type, this.bubbles);
            event.data = this.data;
            return event;
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
