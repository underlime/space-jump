package application.helpers
{
    import application.config.ApplicationConfiguration;
    import application.models.ModelData;
    import application.server_api.ApiMethods;
    import application.views.text.TextFactory;

    import flash.display.Bitmap;

    import framework.controller.BaseController;
    import framework.data.Data;
    import framework.events.GameEvent;
    import framework.interfaces.IBaseController;
    import framework.socnet.SocNet;
    import framework.tools.wall_post.WallPostTool;

    public class WallPostTool extends framework.tools.wall_post.WallPostTool
    {
        private var _baseController:BaseController;

        public function WallPostTool(baseController:IBaseController, text:String, image:Bitmap, shortText:String)
        {
            _baseController = baseController as BaseController;
            var userModel:Data = baseController.dataBase.getChildByName(ModelData.USER_MODEL);

            var link:String = ApplicationConfiguration.getApplicationLink(SocNet.instance().socNetName);
            var gameName:String = TextFactory.instance.getLabel('game_name');
            var linkText:String = TextFactory.instance.getLabel('post_link_text');

            super(text, image, userModel, link, gameName, linkText, shortText);

            addEventListener(GameEvent.ALBUM_CREATED, _onAlbumCreated);
        }

        private function _onAlbumCreated(e:GameEvent):void
        {
            var params:Object = {'photo_album_id': e.data['photo_album_id']};
            _baseController.serverApi.makeRequest(ApiMethods.USER_SET_PHOTO_ALBUM, params);
        }
    }
}
