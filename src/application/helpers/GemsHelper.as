package application.helpers
{
    import application.config.ApplicationConfiguration;

    public class GemsHelper
    {
        public function GemsHelper()
        {
        }

        public static function getGemsLabel(count:int):String
        {
            if (ApplicationConfiguration.LANGUAGE == ApplicationConfiguration.RU)
                return _getNameRu(count);
            else
                return _getNameEn(count);
        }

        private static function _getNameRu(count:int):String
        {
            var mod10:int = count%10;
            var mod100:int = count%100;
            var not11:Boolean = (mod100 <= 10 || mod100 >= 20);

            var currencyName:String;
            if (mod10 >= 2 && mod10 <= 4 && not11)
                currencyName = 'кристалла';
            else
                if (!not11 || mod10 == 0 || not11 && mod10 > 4)
                    currencyName = 'кристаллов';
                else
                    currencyName = 'кристалл';

            return currencyName;
        }

        private static function _getNameEn(count:int):String
        {
            if (count == 1)
                return 'gem';
            else
                return 'gems';
        }
    }

}
