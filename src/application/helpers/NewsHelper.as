package application.helpers
{
    import application.models.UserNews;
    import application.models.user_news.UserNewsRecord;

    import flash.net.SharedObject;

    public class NewsHelper
    {
        private static const NEWS_OBJECT:String = 'news_shared_object';

        public function NewsHelper()
        {
        }

        public static function actualNewsCount(userNewsModel:UserNews):int
        {
            if (userNewsModel.isEmpty)
                return 0;

            var newsObject:SharedObject = SharedObject.getLocal(NEWS_OBJECT, '/');
            var readedNews:Array = newsObject.data.readedNews || [];
            var count:int = 0;
            for each (var rec:UserNewsRecord in userNewsModel.list.value) {
                if (readedNews.indexOf(rec.id.value) == -1)
                    ++count;
            }
            return count;
        }
    }
}
