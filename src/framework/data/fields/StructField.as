package framework.data.fields
{
    public class StructField implements IDataField
    {
        private var _value:Object = {};

        public function StructField()
        {
            super();
        }

        public function get value():Object
        {
            return _value;
        }

        public function set value(value:Object):void
        {
            _value = Object(value);
        }

        public function importValue(value:*):void
        {
            _value = value as Object;
        }
    }
}
