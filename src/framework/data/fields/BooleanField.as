package framework.data.fields
{
    public class BooleanField implements IDataField
    {
        private var _value:Boolean = false;

        public function BooleanField(value:Boolean = false)
        {
			_value = value;
			super();
        }

        public function importValue(value:*):void
        {
            if (value is String && value == '0')
                _value = false;
            else
                _value = Boolean(value);
        }

        public function get value():Boolean
        {
            return _value;
        }

        public function set value(value:Boolean):void
        {
            _value = value;
        }
    }
}
