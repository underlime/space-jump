package framework.data.fields
{
    import framework.helpers.DateHelper;

    public class DateTimeField implements IDataField
    {
        private var _value:Date = null;
        private var _utc:Boolean = true;

        public function DateTimeField(value:Date = null, utc:Boolean=true)
        {
			_value = value;
            _utc = utc;
			super();
        }

        public function get value():Date
        {
            return _value;
        }

        public function set value(value:Date):void
        {
            _value = value;
        }

        public function importValue(value:*):void
        {
            if (value is Date)
                _value = value;
            else if (value is String)
                _value = DateHelper.strToDate(value, _utc);
            else
                _value = null;
        }
    }
}
