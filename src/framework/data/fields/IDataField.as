package framework.data.fields
{
    public interface IDataField
    {
        function importValue(value:*):void;
    }
}
