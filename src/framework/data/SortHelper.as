package framework.data
{
    public class SortHelper
    {
        public static const ASC:String = 'ASC';
        public static const DESC:String = 'DESC';

        public static function getComparison(order:String):Function
        {
            if (order == ASC)
                return _compareFunctionAsc;
            else
                return _compareFunctionDesc;
        }

        private static function _compareFunctionAsc(record1:Object, record2:Object):int
        {
            var res:int = 0;
            if (record1.sort.value > record2.sort.value)
                res = 1;
            else
                if (record1.sort.value < record2.sort.value)
                    res = -1;

            return res;
        }

        private static function _compareFunctionDesc(record1:Object, record2:Object):int
        {
            var res:int = 0;
            if (record1.sort.value < record2.sort.value)
                res = 1;
            else
                if (record1.sort.value > record2.sort.value)
                    res = -1;

            return res;
        }

        public static function getTimeComparison(order:String):Function
        {
            if (order == ASC)
                return _compareTimeFunctionAsc;
            else
                return _compareTimeFunctionDesc;
        }

        private static function _compareTimeFunctionAsc(record1:Object, record2:Object):int
        {
            var res:int = 0;
            if (record1.time.value > record2.time.value)
                res = 1;
            else
                if (record1.time.value < record2.time.value)
                    res = -1;

            return res;
        }

        private static function _compareTimeFunctionDesc(record1:Object, record2:Object):int
        {
            var res:int = 0;
            if (record1.time.value < record2.time.value)
                res = 1;
            else
                if (record1.time.value > record2.time.value)
                    res = -1;

            return res;
        }
    }
}
