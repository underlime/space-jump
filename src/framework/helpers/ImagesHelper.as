package framework.helpers
{
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.PixelSnapping;
    import flash.geom.Matrix;

    public class ImagesHelper
    {

        /**
         * Создать новое изображение с новыми размерами
         * @param bitmap
         * @param maxWidth
         * @param maxHeight
         * @return
         * @param allowSmoothing сглаживание
         */
        public static function resize(bitmap:Bitmap, maxWidth:Number, maxHeight:Number, allowSmoothing:Boolean = false):Bitmap
        {
            var bitmapData:BitmapData = bitmap.bitmapData;
            var newBitmap:Bitmap = new Bitmap(bitmapData, PixelSnapping.AUTO, allowSmoothing);

            var newWidth:Number = newBitmap.width;
            var newHeight:Number = newBitmap.height;

            if (newWidth > maxWidth) {
                newWidth = maxWidth;
                newHeight = newWidth * bitmap.height / bitmap.width;
            }

            if (newHeight > maxHeight) {
                newHeight = maxHeight;
                newWidth = newHeight * bitmap.width / bitmap.height;
            }

            newBitmap.width = newWidth;
            newBitmap.height = newHeight;
            return newBitmap;
        }

        /**
         * Ресайз объекта через матрицу.
         * Отличается от ImagesHelper.resize() тем, что транформируется bitmapData, а не bitmap
         * @param source
         * @param maxWidth
         * @param maxHeight
         * @param allowSmoothing
         * @return
         */
        public static function matrixResize(source:Bitmap, maxWidth:Number, maxHeight:Number, allowSmoothing:Boolean = false):BitmapData
        {
            var rawData:BitmapData;

            var scaleX:Number = maxWidth / source.width;
            var scaleY:Number = maxHeight / source.height;
            var scale:Number = scaleX < scaleY ? scaleX : scaleY;

            var matrix:Matrix = new Matrix();
            matrix.scale(scale, scale);

            rawData = new BitmapData(maxWidth, maxHeight, true, 0x000000);
            rawData.draw(source.bitmapData, matrix, null, null, null, allowSmoothing);

            return rawData;
        }

    }
}
