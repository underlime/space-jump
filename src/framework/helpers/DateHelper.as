package framework.helpers
{
    public class DateHelper
    {
        public static const LANG_RU:String = 'ru';
        public static const LANG_EN:String = 'en_US';

        public static function getCurrentDateString(format:String):String
        {
            var date:Date = new Date();
            return formatDate(date,  format);
        }

        public static function formatDate(date:Date,  format:String):String
        {
            format = format.replace('%Y', date.getFullYear());
            format = format.replace('%m', _extendNumber(date.getMonth()+1));
            format = format.replace('%d', _extendNumber(date.getDate()));
            format = format.replace('%H', _extendNumber(date.getHours()));
            format = format.replace('%i', _extendNumber(date.getMinutes()));
            format = format.replace('%s', _extendNumber(date.getSeconds()));
            return format;
        }

        private static function _extendNumber(num:Number):String
        {
            var strNum:String = num.toString();
            if (num < 10)
                strNum = '0' + strNum;
            return strNum;
        }

        public static function formatSecondsStamp(secondsCount:Number, lang:String):String
        {
            var tmpDate:Date = new Date();
            var tmpTimestamp:Number = secondsCount*1000 + tmpDate.getTimezoneOffset()*60*1000;
            tmpDate.setTime(tmpTimestamp);
            tmpDate.setFullYear(tmpDate.getFullYear() - 1970);

            var format:String = '';
            if (secondsCount >= 31536000)
                format += '%Y' + (lang == LANG_RU ? 'Г' : 'Y') + ' ';
            if (secondsCount >= 2419200)
                format += '%m' + (lang == LANG_RU ? 'М' : 'M') + ' ';
            if (secondsCount >= 86400)
                format += '%d' + (lang == LANG_RU ? 'Д' : 'D') + ' ';
            if (secondsCount >= 3600)
                format += '%H' + (lang == LANG_RU ? 'ч' : 'h') + ' ';
            if (secondsCount >= 60)
                format += '%i' + (lang == LANG_RU ? 'м' : 'm') + ' ';
            format += '%s' + (lang == LANG_RU ? 'с' : 's');

            return formatDate(tmpDate, format);
        }

        public static function strToDate(str:String, utc:Boolean=true):Date
        {
            var dateParts:Object = _getDateParts(str);
            var date:Date = null;

            if (dateParts) {
                date = new Date();
                if (utc) {
                    date.setUTCFullYear(dateParts['year'], dateParts['month']-1, dateParts['day']);
                    date.setUTCHours(dateParts['hours'], dateParts['minutes'], dateParts['seconds']);
                }
                else {
                    date.setFullYear(dateParts['year'], dateParts['month']-1, dateParts['day']);
                    date.setHours(dateParts['hours'], dateParts['minutes'], dateParts['seconds']);
                }
            }

            return date;
        }

        private static function _getDateParts(str:String):Object
        {
            var regexp:RegExp = /^(\d{4})-(\d{2})-(\d{2})(?:\s|T)(\d{2}):(\d{2})(?::(\d{2}))?(?:\.(\d+)Z)?/i;
            var matches:Array = str.match(regexp);
            var data:Object;

            if (matches)
                data = {
                    'year' : int(matches[1]),
                    'month' : int(matches[2]),
                    'day' : int(matches[3]),
                    'hours' : int(matches[4]),
                    'minutes' : int(matches[5]),
                    'seconds' : int(matches[6]),
                    'tzd' : int(matches[7])
                };
            else
                data = null;

            return data;
        }
    }
}
