/**
 * Author: Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 17.10.12
 */
package framework.helpers
{
    import org.casalib.util.ConversionUtil;

    public class MathHelper
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        /**
         * Возвращает случайное число
         * @param    min
         * @param    max
         * @return
         */
        public static function random(min:int, max:int):int
        {
            return Math.random() * (max - min + 1) + min;
        }

        /**
         * Возвращает факт попадания в диапазон вероятности
         * @param    probability (0-100)
         * @return
         */
        public static function chance(probability:int = 50):Boolean
        {
            if (probability <= 0)
                return false;

            if (probability >= 100)
                return true;

            var point:int = MathHelper.random(0, 100);
            return (point <= probability);
        }

        /**
         * Увеличить значение на процент
         * @param    value значение
         * @param    percent процент (> 0)
         * @return
         */
        public static function increasePercent(value:Number, percent:Number):Number
        {
            var delta:Number = value * (percent / 100);
            return (value + delta);
        }

        /**
         * Уменьшить значение на процент
         * @param    value значение
         * @param    percent процент ( > 0)
         * @return
         */
        public static function reducePercent(value:Number, percent:Number):Number
        {
            var delta:Number = value * (percent / 100);
            return (value - delta);
        }

        /**
         * Разбивает секунды на часы/минуты/секунды
         * @param    seconds
         * @return  {"hours":0, "minutes":0, "seconds":0}
         */
        public static function splitSeconds(seconds:Number):Object
        {
            var hours:Number = Math.floor(ConversionUtil.secondsToHours(seconds));
            if (hours > 0) {
                seconds = seconds - (hours * 3600);
            }
            var minutes:Number = Math.floor(ConversionUtil.secondsToMinutes(seconds));
            if (minutes > 0) {
                seconds = seconds - (minutes * 60);
            }
            return {
                "hours": hours,
                "minutes": minutes,
                "seconds": seconds
            };
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/
        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
