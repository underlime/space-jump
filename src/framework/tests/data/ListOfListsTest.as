package framework.tests.data
{
    import flexunit.framework.Assert;

    import framework.tests.data.classes.list_data.DummyListOfLists;
    import framework.tests.data.classes.list_data.DummyListOfRecords;
    import framework.tests.data.classes.list_data.DummyListRecord;

    public class ListOfListsTest
    {
        /*============================================================================*/
        /* Constants                                                                  */
        /*============================================================================*/
        private static const LIST_INIT:Object = {
            '_init': {
                1: {
                    '_init': {
                        1: {
                            'id': 1,
                            'char_field': '#1'
                        }
                    }
                },
                2: {
                    '_init': {
                        2: {
                            'id': 2,
                            'char_field': '#2'
                        }
                    }
                },
                3: {
                    '_init': {
                        3: {
                            'id': 3,
                            'char_field': '#3'
                        }
                    }
                }
            }
        };

        private static const LIST_ADD:Object = {
            '_add': {
                4: {
                    '_init': {
                        4: {
                            'id': 4,
                            'char_field': '#4'
                        }
                    }
                }
            }
        };

        private static const LIST_UPDATE:Object = {
            '_update': {
                2: {
                    '_update': {
                        2: {
                            'id': 2,
                            'char_field': 'Updated'
                        }
                    }
                }
            }
        };

        private static const LIST_DELETE:Object = {
            '_delete': ['1', '3']
        };

        /*============================================================================*/
        /* Private Properties                                                         */
        /*============================================================================*/
        private var _data:DummyListOfLists;

        /*============================================================================*/
        /* Test Setup and Teardown                                                    */
        /*============================================================================*/
        [Before]
        public function before():void
        {
            _data = new DummyListOfLists();
        }

        [After]
        public function after():void
        {
            _data.destroy();
            _data = null;
        }

        /*============================================================================*/
        /* Tests                                                                      */
        /*============================================================================*/
        [Test(order=1)]
        public function initRecords():void
        {
            _data.importData(LIST_INIT);
            var listOfRecords:DummyListOfRecords = _data.getRecordById('1') as DummyListOfRecords;
            var record:DummyListRecord = listOfRecords.getRecordById('1') as DummyListRecord;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(3, _data.length);

            Assert.assertFalse(listOfRecords.isEmpty);
            Assert.assertEquals(1, listOfRecords.length);
            Assert.assertEquals(1, record.id.value);
            Assert.assertEquals('#1', record.char_field.value);
        }

        [Test(order=2)]
        public function addRecord():void
        {
            _data.importData(LIST_INIT);
            _data.importData(LIST_ADD);
            var listOfRecords:DummyListOfRecords = _data.getRecordById('4') as DummyListOfRecords;
            var record:DummyListRecord = listOfRecords.getRecordById('4') as DummyListRecord;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(4, _data.length);

            Assert.assertFalse(listOfRecords.isEmpty);
            Assert.assertEquals(1, listOfRecords.length);
            Assert.assertEquals(4, record.id.value);
            Assert.assertEquals('#4', record.char_field.value);
        }

        [Test(order=3)]
        public function updateRecord():void
        {
            _data.importData(LIST_INIT);
            _data.importData(LIST_UPDATE);
            var listOfRecords:DummyListOfRecords = _data.getRecordById('2') as DummyListOfRecords;
            var record:DummyListRecord = listOfRecords.getRecordById('2') as DummyListRecord;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(3, _data.length);

            Assert.assertFalse(listOfRecords.isEmpty);
            Assert.assertEquals(1, listOfRecords.length);
            Assert.assertEquals(2, record.id.value);
            Assert.assertEquals('Updated', record.char_field.value);
        }

        [Test(order=4)]
        public function deleteRecords():void
        {
            _data.importData(LIST_INIT);
            _data.importData(LIST_DELETE);
            var record1:DummyListRecord = _data.getRecordById('1') as DummyListRecord;
            var record3:DummyListRecord = _data.getRecordById('3') as DummyListRecord;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(1, _data.length);

            Assert.assertNull(record1);
            Assert.assertNull(record3);
        }
    }
}
