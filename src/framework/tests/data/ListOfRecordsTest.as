package framework.tests.data
{
    import flexunit.framework.Assert;

    import framework.data.ModelDataEvent;
    import framework.tests.data.classes.list_data.DummyListOfRecords;
    import framework.tests.data.classes.list_data.DummyListRecord;

    import org.flexunit.async.Async;

    public class ListOfRecordsTest
    {
        /*============================================================================*/
        /* Constants                                                                  */
        /*============================================================================*/
        private static const LIST_INIT:Object = {
            '_init': {
                1: {
                    'id': 1,
                    'char_field': '#1'
                },
                2: {
                    'id': 2,
                    'char_field': '#2'
                },
                3: {
                    'id': 3,
                    'char_field': '#3'
                }
            }
        };

        private static const LIST_ADD:Object = {
            '_add': {
                4: {
                    'id': 4,
                    'char_field': '#4'
                }
            }
        };

        private static const LIST_UPDATE:Object = {
            '_update': {
                2: {
                    'char_field': 'Updated'
                }
            }
        };

        private static const LIST_DELETE:Object = {
            '_delete': ['1', '3']
        };

        /*============================================================================*/
        /* Private Properties                                                         */
        /*============================================================================*/
        private var _data:DummyListOfRecords;

        private var _importFlag:Boolean = false;

        /*============================================================================*/
        /* Test Setup and Teardown                                                    */
        /*============================================================================*/
        [Before]
        public function before():void
        {
            _data = new DummyListOfRecords();
            _importFlag = false;
        }

        [After]
        public function after():void
        {
            _data.destroy();
            _data = null;
        }

        /*============================================================================*/
        /* Tests                                                                      */
        /*============================================================================*/
        [Test(order=1, async)]
        public function initRecords():void
        {
            var asyncHandler:Function = Async.asyncHandler(this, _onImportOperation, 500);

            _data.addEventListener(ModelDataEvent.INIT, asyncHandler);
            _data.importData(LIST_INIT);
            var record:DummyListRecord = _data.getRecordById('1') as DummyListRecord;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(3, _data.length);

            Assert.assertEquals(1, record.id.value);
            Assert.assertEquals('#1', record.char_field.value);
            Assert.assertTrue(_importFlag);

            _data.removeEventListener(ModelDataEvent.INIT, asyncHandler);
        }

        private function _onImportOperation(...args):void
        {
            _importFlag = true;
        }

        [Test(order=2, async)]
        public function addRecord():void
        {
            var asyncHandler:Function = Async.asyncHandler(this, _onImportOperation, 500);

            _data.addEventListener(ModelDataEvent.ADD, asyncHandler);
            _data.importData(LIST_INIT);
            _data.importData(LIST_ADD);
            var record:DummyListRecord = _data.getRecordById('4') as DummyListRecord;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(4, _data.length);

            Assert.assertEquals(4, record.id.value);
            Assert.assertEquals('#4', record.char_field.value);
            Assert.assertTrue(_importFlag);

            _data.removeEventListener(ModelDataEvent.ADD, asyncHandler);
        }

        [Test(order=3, async)]
        public function updateRecord():void
        {
            var asyncHandler:Function = Async.asyncHandler(this, _onImportOperation, 500);

            _data.addEventListener(ModelDataEvent.UPDATE, asyncHandler);
            _data.importData(LIST_INIT);
            _data.importData(LIST_UPDATE);
            var record:DummyListRecord = _data.getRecordById('2') as DummyListRecord;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(3, _data.length);

            Assert.assertEquals(2, record.id.value);
            Assert.assertEquals('Updated', record.char_field.value);
            Assert.assertTrue(_importFlag);

            _data.removeEventListener(ModelDataEvent.UPDATE, asyncHandler);
        }

        [Test(order=4, async)]
        public function deleteRecords():void
        {
            var asyncHandler:Function = Async.asyncHandler(this, _onImportOperation, 500);

            _data.addEventListener(ModelDataEvent.DELETE, asyncHandler);
            _data.importData(LIST_INIT);
            _data.importData(LIST_DELETE);
            var record1:DummyListRecord = _data.getRecordById('1') as DummyListRecord;
            var record3:DummyListRecord = _data.getRecordById('3') as DummyListRecord;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(1, _data.length);

            Assert.assertNull(record1);
            Assert.assertNull(record3);
            Assert.assertTrue(_importFlag);

            _data.removeEventListener(ModelDataEvent.DELETE, asyncHandler);
        }

        [Test(order=5)]
        public function checkNotExists():void
        {
            _data.importData(LIST_INIT);
            var notExistsList:Array = _data.checkNotExists([1, 2, 3, 7, 8, 10]);

            var falseList:Array = ['1', '2', '3'];
            var trueList:Array = ['7', '8', '10'];

            var i:int;
            for (i = 0; i<falseList.length; ++i)
                Assert.assertEquals(-1, notExistsList.indexOf(falseList[i]));
            for (i = 0; i<trueList.length; ++i)
                Assert.assertEquals(i, notExistsList.indexOf(trueList[i]));
        }
    }
}
