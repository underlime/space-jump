package framework.tests.data
{

    import flexunit.framework.Assert;

    import framework.data.Data;
    import framework.data.DataContainer;
    import framework.tests.data.classes.data.DummyData;
    import framework.tests.data.classes.data_container.ChildDummyDataContainer;
    import framework.tests.data.classes.data_container.DummyDataContainer;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class DataContainerTest
    {
        /*============================================================================*/
        /* Private Properties                                                         */
        /*============================================================================*/

        private var _data:DataContainer;
        private var _dummy:DummyDataContainer;

        private var _subData:Data;
        private var _subDataCont:DataContainer;

        /*============================================================================*/
        /* Test Setup and Teardown                                                    */
        /*============================================================================*/

        [Before]
        public function before():void
        {
            this._data = new DataContainer("main");
            this._dummy = new DummyDataContainer();

            var dummyContainer:ChildDummyDataContainer = new ChildDummyDataContainer();
            var dummyData:DummyData = new DummyData();
            dummyContainer.addChild(dummyData);
            this._dummy.addChild(dummyContainer);
        }

        [After]
        public function after():void
        {
            this._data.destroy();
            this._data = null;

            this._dummy.destroy();
            this._dummy = null;
        }

        /*============================================================================*/
        /* Tests                                                                      */
        /*============================================================================*/

        [Test(expects="ArgumentError", order=1)]
        public function addChild_test():void
        {
            this._subData = new Data("subdata");
            this._subDataCont = new DataContainer("subdataCont");

            Assert.assertEquals(this._data.numChildren, 0);
            this._data.addChild(this._subData);
            Assert.assertEquals(this._data.numChildren, 1);
            this._data.addChild(this._subDataCont);
            Assert.assertEquals(this._data.numChildren, 2);
            this._data.addChild(this._data);
            Assert.assertEquals(this._data.numChildren, 2);
            this._data.addChild(this._subData);
            Assert.assertEquals(this._data.numChildren, 2);
            this._data.addChild(this._subDataCont);
            Assert.assertEquals(this._data.numChildren, 2);
            this._data.addChild(new Data("subdata"));
            Assert.assertEquals(this._data.numChildren, 2);

            Assert.assertNotNull(this._subData.parent);
            Assert.assertEquals(this._data, this._data.parent);
        }

        [Test(order=2)]
        public function containsTest():void
        {
            this._subData = new Data("subdata");
            this._data.addChild(this._subData);
            Assert.assertTrue(this._data.contains("subdata"));
            Assert.assertFalse(this._data.contains("subdata1"));
        }

        [Test(order=3)]
        public function removeChild_test():void
        {
            this._subData = new Data("subdata");
            this._data.addChild(this._subData);
            Assert.assertEquals(this._data.numChildren, 1);
            this._data.removeChild(this._subData);
            Assert.assertEquals(this._data.numChildren, 0);
        }

        [Test(order=4)]
        public function getChildByName_test():void
        {
            this._subData = new Data("subdata");
            this._subDataCont = new DataContainer("subdataCont");

            this._data.addChild(this._subData);
            this._data.addChild(this._subDataCont);

            var data:Data = this._data.getChildByName("subdata");
            Assert.assertEquals(data.name, this._subData.name);
        }

        [Test(order=5)]
        public function chilrenKeys():void
        {
            var keysList:Array = _dummy.getChildrenDataKeys();
            Assert.assertTrue(keysList.indexOf('key1') != -1);
            Assert.assertTrue(keysList.indexOf('key2') != -1);
            Assert.assertTrue(keysList.indexOf('key3') != -1);
            Assert.assertTrue(keysList.indexOf('key4') != -1);
        }

        [Test(order=6)]
        public function invokeDataImport():void
        {
            var data:Object = {
                'key1': {
                    'int_field': '23'
                },
                'key2': {
                    'nested_data': {
                        'boolean_field': 1
                    }
                },
                'key3': {
                    'char_field': 'Hello world!'
                }
            };
            _dummy.invokeDataImport(data);

            var childDummyContainer:ChildDummyDataContainer = _dummy.getChildByName('ChildDummyDataContainer') as ChildDummyDataContainer;
            var dummyData:DummyData = childDummyContainer.getChildByName('DummyData') as DummyData;

            Assert.assertEquals(23, _dummy.int_field.value);
            Assert.assertEquals(23, dummyData.int_field.value);
            Assert.assertEquals(true, dummyData.nested_data.boolean_field.value);
            Assert.assertEquals('Hello world!', childDummyContainer.char_field.value);
        }
    }
}
