package framework.tests.data.classes.data_container
{
    import framework.data.DataContainer;
    import framework.data.fields.CharField;
    import framework.data.fields.IntegerField;

    public class DummyDataContainer extends DataContainer
    {
        private var _int_field:IntegerField = new IntegerField();
        private var _char_field:CharField = new CharField();

        public function DummyDataContainer()
        {
            super('DummyDataContainer');
        }

        override public function getModelDataKeys():Array
        {
            return ['key1'];
        }

        public function get int_field():IntegerField
        {
            return _int_field;
        }

        public function get char_field():CharField
        {
            return _char_field;
        }
    }
}
