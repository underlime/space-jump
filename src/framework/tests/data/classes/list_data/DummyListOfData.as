package framework.tests.data.classes.list_data
{
    import framework.data.ListData;
    import framework.tests.data.classes.data.DummyData;

    public class DummyListOfData extends ListData
    {
        public function DummyListOfData()
        {
            _RecordClass = DummyData;
            super('DummyListOfData');
        }
    }
}
