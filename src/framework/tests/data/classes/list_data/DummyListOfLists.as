package framework.tests.data.classes.list_data
{
    import framework.data.ListData;

    public class DummyListOfLists extends ListData
    {
        public function DummyListOfLists()
        {
            _RecordClass = DummyListOfRecords;
            super('DummyListOfData');
        }
    }
}
