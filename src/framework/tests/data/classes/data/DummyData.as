package framework.tests.data.classes.data
{
    import framework.data.Data;
    import framework.data.fields.CharField;
    import framework.data.fields.FloatField;
    import framework.data.fields.IntegerField;

    public class DummyData extends Data
    {
        private var _int_field:IntegerField = new IntegerField();
        private var _float_field:FloatField = new FloatField();
        private var _char_field:CharField = new CharField();

        private var _nested_data:NestedDummyData = new NestedDummyData();

        public function DummyData()
        {
            super('DummyData');
        }

        override public function getModelDataKeys():Array
        {
            return ['key1', 'key2'];
        }

        public function get int_field():IntegerField
        {
            return _int_field;
        }

        public function get float_field():FloatField
        {
            return _float_field;
        }

        public function get char_field():CharField
        {
            return _char_field;
        }

        public function get nested_data():NestedDummyData
        {
            return _nested_data;
        }
    }
}
