package framework.tests.data.classes.data
{
    import framework.data.Data;
    import framework.data.fields.BooleanField;
    import framework.data.fields.DateTimeField;
    import framework.data.fields.StructField;

    public class NestedDummyData extends Data
    {
        private var _boolean_field:BooleanField = new BooleanField();
        private var _struct_field:StructField = new StructField();
        private var _date:DateTimeField = new DateTimeField();

        public function NestedDummyData()
        {
            super('NestedDummyData');
        }

        public function get boolean_field():BooleanField
        {
            return _boolean_field;
        }

        public function get struct_field():StructField
        {
            return _struct_field;
        }

        public function get date():DateTimeField
        {
            return _date;
        }
    }
}
