package framework.tests.data
{
    import flexunit.framework.Assert;

    import framework.tests.data.classes.data.DummyData;
    import framework.tests.data.classes.list_data.DummyListOfData;

    public class ListOfDataTest
    {
        /*============================================================================*/
        /* Constants                                                                  */
        /*============================================================================*/
        private static const LIST_INIT:Object = {
            '_init': {
                1: {
                    'int_field': 1,
                    'char_field': '#1',
                    'nested_data': {
                        'boolean_field': 1
                    }
                },
                2: {
                    'int_field': 2,
                    'char_field': '#2',
                    'nested_data': {
                        'boolean_field': 1
                    }
                },
                3: {
                    'int_field': 3,
                    'char_field': '#3',
                    'nested_data': {
                        'boolean_field': 1
                    }
                }
            }
        };

        private static const LIST_ADD:Object = {
            '_add': {
                4: {
                    'int_field': 4,
                    'char_field': '#4',
                    'nested_data': {
                        'boolean_field': 1
                    }
                }
            }
        };

        private static const LIST_UPDATE:Object = {
            '_update': {
                3: {
                    'char_field': 'Updated',
                    'nested_data': {
                        'boolean_field': 0
                    }
                }
            }
        };

        private static const LIST_DELETE:Object = {
            '_delete': ['1', '3']
        };

        /*============================================================================*/
        /* Private Properties                                                         */
        /*============================================================================*/
        private var _data:DummyListOfData;

        /*============================================================================*/
        /* Test Setup and Teardown                                                    */
        /*============================================================================*/
        [Before]
        public function before():void
        {
            _data = new DummyListOfData();
        }

        [After]
        public function after():void
        {
            _data.destroy();
            _data = null;
        }

        /*============================================================================*/
        /* Tests                                                                      */
        /*============================================================================*/
        [Test(order=1)]
        public function initRecords():void
        {
            _data.importData(LIST_INIT);
            var record:DummyData = _data.getRecordById('1') as DummyData;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(3, _data.length);

            Assert.assertEquals(1, record.int_field.value);
            Assert.assertEquals('#1', record.char_field.value);
            Assert.assertEquals(true, record.nested_data.boolean_field.value);
        }

        [Test(order=2)]
        public function addRecord():void
        {
            _data.importData(LIST_INIT);
            _data.importData(LIST_ADD);
            var record:DummyData = _data.getRecordById('4') as DummyData;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(4, _data.length);

            Assert.assertEquals(4, record.int_field.value);
            Assert.assertEquals('#4', record.char_field.value);
            Assert.assertEquals(true, record.nested_data.boolean_field.value);
        }

        [Test(order=3)]
        public function updateRecord():void
        {
            _data.importData(LIST_INIT);
            _data.importData(LIST_UPDATE);
            var record:DummyData = _data.getRecordById('3') as DummyData;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(3, _data.length);

            Assert.assertEquals(3, record.int_field.value);
            Assert.assertEquals('Updated', record.char_field.value);
            Assert.assertEquals(false, record.nested_data.boolean_field.value);
        }

        [Test(order=4)]
        public function deleteRecords():void
        {
            _data.importData(LIST_INIT);
            _data.importData(LIST_DELETE);
            var record1:DummyData = _data.getRecordById('1') as DummyData;
            var record3:DummyData = _data.getRecordById('3') as DummyData;

            Assert.assertFalse(_data.isEmpty);
            Assert.assertEquals(1, _data.length);

            Assert.assertNull(record1);
            Assert.assertNull(record3);
        }
    }
}
