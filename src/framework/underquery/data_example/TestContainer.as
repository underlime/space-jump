package framework.underquery.data_example
{
    import framework.data.DataContainer;
    import framework.data.fields.DateTimeField;

    import mx.utils.ObjectUtil;

    public class TestContainer extends DataContainer
    {
        private var _register_date:DateTimeField = new DateTimeField();

        public function TestContainer()
        {
            super('TestContainer');
            addChild(new Test1());
            addChild(new Test2());
        }

        override public function getModelDataKeys():Array
        {
            return ['user'];
        }

        override public function invokeDataImport(data:Object):void
        {
            super.invokeDataImport(data);
            trace('TestContainer invoke data import:');
            trace(ObjectUtil.toString(this));
            trace(ObjectUtil.toString(this.getChildByName('Test1')));
            trace(ObjectUtil.toString(this.getChildByName('Test2')));
        }

        public function get register_date():DateTimeField
        {
            return _register_date;
        }

        public function set register_date(value:DateTimeField):void
        {
            _register_date = value;
        }
    }
}
