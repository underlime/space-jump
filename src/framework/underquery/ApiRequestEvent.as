package framework.underquery
{
    import flash.events.Event;

    public class ApiRequestEvent extends Event
    {
        public static const SUCCESS_EVENT:String = 'onSuccessEvent';
        public static const ERROR_EVENT:String = 'onErrorEvent';
        public static const ERROR_TYPE_NONE:String = 'no_error';
        public static const ERROR_TYPE_IO:String = 'io';
        public static const ERROR_TYPE_SECURITY:String = 'security';
        public static const ERROR_TYPE_PROGRAM:String = 'program';
        public static const ERROR_TYPE_CONTENT:String = 'content';

        private var _httpStatus:int = 0;
        private var _errorType:String = ERROR_TYPE_NONE;
        private var _data:Object = null;
        private var _queryID:int = 0;
        private var _successCallback:Function = null;
        private var _errorCallback:Function = null;

        public function ApiRequestEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

        public function set data(value:Object):void
        {
            _data = value;
        }

        public function get data():Object
        {
            return _data;
        }

        public function get httpStatus():int
        {
            return _httpStatus;
        }

        public function set httpStatus(value:int):void
        {
            _httpStatus = value;
        }

        public function get errorType():String
        {
            return _errorType;
        }

        public function set errorType(value:String):void
        {
            _errorType = value;
        }

        public function get successCallback():Function
        {
            return _successCallback;
        }

        public function set successCallback(value:Function):void
        {
            _successCallback = value;
        }

        public function get errorCallback():Function
        {
            return _errorCallback;
        }

        public function set errorCallback(value:Function):void
        {
            _errorCallback = value;
        }

        public function get queryID():int
        {
            return _queryID;
        }

        public function set queryID(value:int):void
        {
            _queryID = value;
        }
    }
}
