package framework.underquery
{
    import flash.events.Event;
    import flash.events.HTTPStatusEvent;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.net.URLVariables;

    import mx.utils.ObjectUtil;

    import org.casalib.events.RemovableEventDispatcher;

    public class ApiRequest extends RemovableEventDispatcher
    {
        private var _url:String = null;
        private var _methodArguments:Object = {};
        private var _token:String = '';

        private var _request:URLRequest;
        private var _urlLoader:URLLoader;
        private var _httpStatus:int;

        private var _successCallback:Function = null;
        private var _errorCallback:Function = null;

        private var _queryID:int = 0;

        public function ApiRequest(methodUrl:String)
        {
            super();
            _url = methodUrl;
        }

        public function send():void
        {
            _methodArguments['token'] = _token;

            var postData:URLVariables = new URLVariables();
            for (var key:String in _methodArguments)
                if (_methodArguments.hasOwnProperty(key) && _methodArguments[key] != null)
                    postData[key] = _methodArguments[key];

            _request = new URLRequest(_url);
            _request.method = URLRequestMethod.POST;
            _request.data = postData;

            _urlLoader = new URLLoader();
            _urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, _onHttpStatus);
            _urlLoader.addEventListener(IOErrorEvent.IO_ERROR, _onIoError);
            _urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onSecurityError);
            _urlLoader.addEventListener(Event.COMPLETE, _onLoad);

            _httpStatus = 0;
            _urlLoader.load(_request);
        }

        private function _onHttpStatus(e:HTTPStatusEvent):void
        {
            _httpStatus = e.status;
        }

        protected function _onLoad(e:Event):void
        {
            var data:Object = _loadData(),
                    event:ApiRequestEvent;

            if (data)
                event = _handleData(data);
            else
                event = _handleNoData();

            event.successCallback = _successCallback;
            event.errorCallback = _errorCallback;
            event.queryID = _queryID;
            event.httpStatus = _httpStatus;
            dispatchEvent(event);
            _destroyInfrastructure();
        }

        private function _handleData(data:Object):ApiRequestEvent
        {
            var eventType:String, event:ApiRequestEvent;

            if (data.hasOwnProperty('error')) {
                eventType = ApiRequestEvent.ERROR_EVENT;
                event = new ApiRequestEvent(eventType);
                event.errorType = ApiRequestEvent.ERROR_TYPE_PROGRAM;
                event.data = data['error'];
            } else {
                eventType = ApiRequestEvent.SUCCESS_EVENT;
                event = new ApiRequestEvent(eventType);
                event.data = data;
            }

            return event;
        }

        private function _handleNoData():ApiRequestEvent
        {
            var eventType:String, event:ApiRequestEvent;

            eventType = ApiRequestEvent.ERROR_EVENT;
            event = new ApiRequestEvent(eventType);
            event.errorType = ApiRequestEvent.ERROR_TYPE_CONTENT;

            return event;
        }

        protected function _loadData():Object
        {
            try {
                return JSON.parse(_urlLoader.data);
            } catch (e:SyntaxError) {
            }

            return null;
        }

        private function _destroyInfrastructure():void
        {
            if (_urlLoader) {
                _urlLoader.removeEventListener(HTTPStatusEvent.HTTP_STATUS, this._onHttpStatus);
                _urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, this._onIoError);
                _urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, this._onSecurityError);
                _urlLoader.removeEventListener(Event.COMPLETE, this._onLoad);
            }

            _request = null;
            _urlLoader = null;
        }

        private function _onSecurityError(e:SecurityErrorEvent):void
        {
            var eventType:String = ApiRequestEvent.ERROR_EVENT, event:ApiRequestEvent = new ApiRequestEvent(eventType);
            event.httpStatus = _httpStatus;
            event.errorType = ApiRequestEvent.ERROR_TYPE_SECURITY;

            event.successCallback = _successCallback;
            event.errorCallback = _errorCallback;

            dispatchEvent(event);
            _destroyInfrastructure();
        }

        private function _onIoError(e:IOErrorEvent):void
        {
            var eventType:String = ApiRequestEvent.ERROR_EVENT, event:ApiRequestEvent = new ApiRequestEvent(eventType);

            event.httpStatus = _httpStatus;
            event.errorType = ApiRequestEvent.ERROR_TYPE_IO;

            event.successCallback = _successCallback;
            event.errorCallback = _errorCallback;

            dispatchEvent(event);
            _destroyInfrastructure();
        }

        public function set methodArguments(value:Object):void
        {
            _methodArguments = ObjectUtil.clone(value);
        }

        public function get methodArguments():Object
        {
            return _methodArguments;
        }

        public function get token():String
        {
            return _token;
        }

        public function set token(value:String):void
        {
            _token = value;
        }

        public function get successCallback():Function
        {
            return _successCallback;
        }

        public function set successCallback(func:Function):void
        {
            _successCallback = func;
        }

        public function get errorCallback():Function
        {
            return _errorCallback;
        }

        public function set errorCallback(func:Function):void
        {
            _errorCallback = func;
        }

        public function get queryID():int
        {
            return _queryID;
        }

        public function set queryID(value:int):void
        {
            _queryID = value;
        }
    }
}
