package framework.tools
{
    import framework.tools.images_manager.ImageLoader;
    import framework.tools.images_manager.ImageRecord;

    public class ImagesManager
    {
        public static const STATUS_ERROR:int = -1;
        public static const STATUS_NOT_LOADED:int = 0;
        public static const STATUS_LOADING:int = 1;
        public static const STATUS_COMPLETE:int = 2;

        private static var _lockInstantiate:Boolean = true;
        private static var _instance:ImagesManager;

        private var _imagesPull:Object = {};

        public function ImagesManager()
        {
            if (_lockInstantiate)
                throw new Error('Use instance property');
        }

        public static function get instance():ImagesManager
        {
            if (!_instance) {
                _lockInstantiate = false;
                _instance = new ImagesManager();
                _lockInstantiate = true;
            }
            return _instance;
        }

        public function getImageByUrl(url:String, onComplete:Function):void
        {
            var imageRecord:ImageRecord = (_imagesPull[url] ? _imagesPull[url] : null);
            if (imageRecord && imageRecord.status == STATUS_COMPLETE)
                onComplete(imageRecord.loader);
            else
                if (imageRecord && imageRecord.status == STATUS_LOADING)
                    imageRecord.addCompleteCallback(onComplete);
                else
                    _loadPhoto(url, onComplete);
        }

        private function _loadPhoto(url:String, onComplete:Function):void
        {
            var imageRecord:ImageRecord = _createImageRecord(url, onComplete);
            var loader:ImageLoader = new ImageLoader(imageRecord);
            loader.load();
        }

        private function _createImageRecord(url:String, onComplete:Function):ImageRecord
        {
            var imageRecord:ImageRecord = new ImageRecord();
            imageRecord.url = url;
            imageRecord.status = STATUS_LOADING;
            imageRecord.addCompleteCallback(onComplete);

            _imagesPull[url] = imageRecord;
            return imageRecord;
        }
    }
}
