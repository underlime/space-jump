package framework.tools.avatar
{
    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.socnet.SocNet;
    import framework.socnet.VkCom;

    import org.casalib.events.RemovableEventDispatcher;

    public class AvatarTool extends RemovableEventDispatcher
    {
        private var _socNet:SocNet;
        private var _uploader:VkAvatarImageUploader;

        public function AvatarTool(image:Bitmap)
        {
            _socNet = SocNet.instance();
            _uploader = new VkAvatarImageUploader(image);
        }

        override public function destroy():void
        {
            if (_uploader) {
                _uploader.destroy();
                _uploader = null;
            }

            super.destroy();
        }

        public function setAvatar():void
        {
            _uploader.addEventListener(Event.COMPLETE, _onImageUploaded);
            _uploader.addEventListener(ErrorEvent.ERROR, _redispatchEvent);
            _uploader.upload();
        }

        private function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }

        private function _onImageUploaded(e:Event):void
        {
            var serverResponse:Object = _uploader.loader.data;
            VkCom(SocNet.instance()).setUserAvatar(serverResponse, _onPhotoSaved);

            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _onPhotoSaved(data:Object):void
        {
        }
    }

}
