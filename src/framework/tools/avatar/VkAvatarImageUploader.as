package framework.tools.avatar
{
    import flash.display.Bitmap;

    import framework.socnet.SocNet;
    import framework.socnet.VkCom;
    import framework.tools.ImageUploader;

    public class VkAvatarImageUploader extends ImageUploader
    {
        public function VkAvatarImageUploader(image:Bitmap)
        {
            super(image);
        }

        override public function upload():void
        {
            VkCom(SocNet.instance()).getUploadProfilePhotoParams(_onParamsSuccess, _onParamsError);
        }

        private function _onParamsSuccess(data:Object):void
        {
            var serverInfo:Object = data['server_info'];
            _uploadPhotoToServer(serverInfo['server_url'], serverInfo['post_params'], serverInfo['file_name']);
        }

        private function _onParamsError(...args):void
        {
            var message:String = 'Unable to get upload server params';
            _dispatchErrorEvent(message);
        }
    }
}
