package framework.tools.images_manager
{
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;
    import flash.net.URLRequest;

    import framework.tools.ImagesManager;

    public class ImageLoader
    {
        private var _imageRecord:ImageRecord;
        private var _loader:Loader;

        public function ImageLoader(imageRecord:ImageRecord)
        {
            _imageRecord = imageRecord;
            _createLoader();
        }

        private function _createLoader():void
        {
            _loader = new Loader();

            _loader.contentLoaderInfo.addEventListener(Event.COMPLETE, _onImageLoaded);
            _loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, _onImageError);
            _loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _onImageError);
        }

        private function _onImageLoaded(e:Event):void
        {
            _imageRecord.loader = _loader;
            _imageRecord.status = ImagesManager.STATUS_COMPLETE;
            _imageRecord.executeCompleteFunctions();
            _cleanLoader();
        }

        private function _onImageError(e:Event):void
        {
            _imageRecord.status = ImagesManager.STATUS_ERROR;
            _cleanLoader();
        }

        private function _cleanLoader():void
        {
            _loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, _onImageLoaded);
            _loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, _onImageError);
            _loader.contentLoaderInfo.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, _onImageError);

            _imageRecord = null;
            _loader = null;
        }

        public function load():void
        {
            var request:URLRequest = new URLRequest(_imageRecord.url);
            _loader.load(request);
        }
    }
}
