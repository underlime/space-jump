package framework.tools.images_manager
{
    import flash.display.Loader;

    import framework.tools.ImagesManager;

    public class ImageRecord
    {
        public var url:String;
        public var status:int = ImagesManager.STATUS_NOT_LOADED;
        public var loader:Loader;

        private var _completeFunctionsList:Vector.<Function> = new Vector.<Function>();

        public function ImageRecord()
        {
        }

        public function addCompleteCallback(callback:Function):void
        {
            if (loader == null)
                _completeFunctionsList.push(callback);
            else
                callback(loader);
        }

        public function executeCompleteFunctions():void
        {
            for (var i:int = 0; i < _completeFunctionsList.length; ++i)
                _completeFunctionsList[i](loader);

            _completeFunctionsList = new Vector.<Function>();
        }
    }
}
