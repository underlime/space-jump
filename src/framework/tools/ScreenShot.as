package framework.tools
{
    import application.events.ApplicationEvent;

    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.events.SecurityErrorEvent;
    import flash.geom.Point;
    import flash.geom.Rectangle;
    import flash.net.FileReference;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;
    import flash.net.URLRequestMethod;

    import framework.helpers.UploadPostHelper;

    import mx.graphics.codec.PNGEncoder;

    import org.casalib.events.RemovableEventDispatcher;

    public class ScreenShot extends RemovableEventDispatcher
    {
        public static function takeScreen(object:DisplayObject, width:int = 0, height:int = 0, startX:int = 0, startY:int = 0):BitmapData
        {
            var shot:ScreenShot = new ScreenShot();
            shot.takeShot(object, width, height, startX, startY);
            return shot.rawData;
        }

        private var _rawData:BitmapData;
        private var _pngEncoder:PNGEncoder;
        private var _loader:URLLoader = new URLLoader();

        public function ScreenShot()
        {
            _pngEncoder = new PNGEncoder();
        }

        public function takeShot(object:DisplayObject, width:int = 0, height:int = 0, startX:int = 0, startY:int = 0):ScreenShot
        {
            if (width < 1)
                width = object.width;
            if (height < 1)
                height = object.height;

            var tempData:BitmapData = new BitmapData(width + startX, height + startY);
            tempData.draw(object);

            var copyRect:Rectangle = new Rectangle(startX, startY, width, height);
            var destPoint:Point = new Point(0, 0);
            _rawData = new BitmapData(width, height);
            _rawData.copyPixels(tempData, copyRect, destPoint);

            return this;
        }

        public function saveToDisk():ScreenShot
        {
            var file:FileReference = new FileReference();
            file.save(_pngEncoder.encode(_rawData), 'screenshot.png');

            return this;
        }

        public function uploadToServer(url:String, fileName:String, postParams:Object = null):void
        {
            if (postParams == null)
                postParams = {};

            var request:URLRequest = new URLRequest(url);
            request.requestHeaders.push(new URLRequestHeader('Cache-Control', 'no-cache'));
            request.requestHeaders.push(new URLRequestHeader('Content-Type', 'multipart/form-data; boundary=' + UploadPostHelper.getBoundary()));
            request.method = URLRequestMethod.POST;
            request.data = UploadPostHelper.getPostData('screenshot.png', _pngEncoder.encode(_rawData), postParams, fileName);

            _loader = new URLLoader();
            _loader.addEventListener(ProgressEvent.PROGRESS, _redispatchCommonEvent);
            _loader.addEventListener(IOErrorEvent.IO_ERROR, _redispatchCommonEvent);
            _loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, _redispatchCommonEvent);
            _loader.addEventListener(Event.COMPLETE, _redispatchComplete);
            _loader.load(request);
        }

        private function _redispatchCommonEvent(e:Event):void
        {
            dispatchEvent(e);
        }

        private function _redispatchComplete(e:Event):void
        {
            var appEvent:ApplicationEvent = new ApplicationEvent(ApplicationEvent.COMPLETE);
            appEvent.data = {'loader': _loader};
            dispatchEvent(appEvent);
        }

        override public function destroy():void
        {
            if (_rawData)
                _rawData.dispose();
            super.destroy();
        }

        public function get loader():URLLoader
        {
            return _loader;
        }

        public function get rawData():BitmapData
        {
            return _rawData;
        }
    }
}
