package framework.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.net.FileReference;
    import flash.system.Capabilities;

    import framework.data.Data;
    import framework.events.GameEvent;
    import framework.socnet.SocNet;
    import framework.socnet.VkCom;
    import framework.tools.ImageUploader;

    import mx.graphics.codec.PNGEncoder;

    public class VkWallImageUploader extends ImageUploader
    {
        private var _userModel:Data;
        private var _albumId:Number;
        private var _gameName:String;

        public function VkWallImageUploader(image:Bitmap, userModel:Data, gameName:String)
        {
            super(image);
            _gameName = gameName;
            _userModel = userModel;
        }

        override public function upload():void
        {
            if (Capabilities.playerType == 'StandAlone')
                _saveToDisk();
            else
                _checkAlbum();
        }

        private function _saveToDisk():void
        {
            var pngEncoder:PNGEncoder = new PNGEncoder();
            var file:FileReference = new FileReference();
            file.save(pngEncoder.encode(_image.bitmapData), 'screenshot.png');

            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _checkAlbum():void
        {
            if (_userModel['photo_album_id'] && _userModel['photo_album_id']['value']) {
                _albumId = parseFloat(_userModel['photo_album_id']['value']);
                VkCom(SocNet.instance()).getAlbumsListInfo(_onAlbumsInfo);
            }
            else {
                _createAlbum();
            }
        }

        private function _onAlbumsInfo(data:Object):void
        {
            var albumsInfo:Array = data['albums_info'] as Array;
            var idsList:Array = [];
            for (var i:int = 0; i < albumsInfo.length; ++i)
                idsList.push(parseFloat(albumsInfo[i]['aid']));

            if (idsList.indexOf(_albumId) != -1) {
                VkCom(SocNet.instance()).getUploadPhotoParams(_albumId, _onParamsSuccess, _onParamsError);
            }
            else {
                _createAlbum();
            }
        }

        private function _createAlbum():void
        {
            var privacy:int = 0;
            VkCom(SocNet.instance()).createAlbum(_gameName, privacy, _onAlbumCreate);
        }

        private function _onAlbumCreate(data:Object):void
        {
            _albumId = parseFloat(data['album_info']['aid']);

            var createEvent:GameEvent = new GameEvent(GameEvent.ALBUM_CREATED);
            createEvent.data['photo_album_id'] = _albumId;
            dispatchEvent(createEvent);

            VkCom(SocNet.instance()).getUploadPhotoParams(_albumId, _onParamsSuccess, _onParamsError);
        }

        private function _onParamsSuccess(data:Object):void
        {
            var serverInfo:Object = data['server_info'];
            _uploadPhotoToServer(serverInfo['server_url'], serverInfo['post_params'], serverInfo['file_name']);
        }

        private function _onParamsError(...args):void
        {
            var message:String = 'Unable to get upload server params';
            _dispatchErrorEvent(message);
        }

        public function get albumId():Number
        {
            return _albumId;
        }
    }
}
