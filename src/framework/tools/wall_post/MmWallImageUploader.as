package framework.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.net.FileReference;
    import flash.system.Capabilities;

    import framework.data.Data;
    import framework.events.GameEvent;
    import framework.socnet.MyMailRu;
    import framework.socnet.SocNet;
    import framework.tools.ImageUploader;
    import framework.underquery.ServerApi;

    import mx.graphics.codec.PNGEncoder;

    public class MmWallImageUploader extends ImageUploader
    {
        private var _gameName:String;
        private var _userModel:Data;
        private var _albumId:Number;
        private var _title:String;
        private var _text:String;

        private var _socNet:MyMailRu;
        private var _photoData:Object;

        public function MmWallImageUploader(title:String, text:String, image:Bitmap, gameName:String, userModel:Data)
        {
            super(image);
            _title = title;
            _text = text;
            _gameName = gameName;
            _userModel = userModel;
            _socNet = SocNet.instance() as MyMailRu;
        }

        override public function upload():void
        {
            if (Capabilities.playerType == 'StandAlone')
                _saveToDisk();
            else
                _checkAlbum();
        }

        private function _saveToDisk():void
        {
            var pngEncoder:PNGEncoder = new PNGEncoder();
            var file:FileReference = new FileReference();
            file.save(pngEncoder.encode(_image.bitmapData), 'screenshot.png');

            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _checkAlbum():void
        {
            if (_userModel['photo_album_id'] && _userModel['photo_album_id']['value']) {
                _albumId = parseFloat(_userModel['photo_album_id']['value']);
                _socNet.getAlbumsListInfo(_onAlbumsInfo);
            }
            else {
                _createAlbum();
            }
        }

        private function _onAlbumsInfo(data:Object):void
        {
            var albumsInfo:Array = data['albums_info'] as Array;
            var idsList:Array = [];
            for (var i:int = 0; i < albumsInfo.length; ++i)
                idsList.push(parseFloat(albumsInfo[i]['aid']));

            if (idsList.indexOf(_albumId) != -1)
                _uploadPhoto();
            else
                _createAlbum();
        }

        private function _uploadPhoto():void
        {
            var serverUrl:String = ServerApi.uploadImageUrl;
            var postParams:Object = _socNet.serverCallParams;
            postParams['token'] = ServerApi.uploadImageToken[SocNet.MY_MAIL_RU];
            _uploadPhotoToServer(serverUrl, postParams, 'file', _onUploadComplete);
        }

        private function _onUploadComplete(e:Event):void
        {
            var data:Object;
            try {
                data = JSON.parse(_loader.data);
            }
            catch (e:Error) {
                _dispatchError('Wrong server response format');
            }

            if (data['error'])
                _dispatchError(data['error']['message']);
            else
                _sendPhotoToMyMailRu(data['url']);
        }

        private function _dispatchError(message:String):void
        {
            var errorEvent:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
            errorEvent.text = message;
            dispatchEvent(errorEvent);
        }

        private function _sendPhotoToMyMailRu(photoUrl:String):void
        {
            _socNet.uploadPhoto(
                _albumId.toString(),
                photoUrl,
                _title,
                _text,
                _gameName,
                _onPhotoUploaded,
                null,
                _onUploadCancel
            );
        }

        private function _onPhotoUploaded(data:Object):void
        {
            if (!data['photo_info']) {
                _dispatchError('Wrong photo info');
                return;
            }

            _photoData = data['photo_info'];
            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _onUploadCancel(...args):void
        {
            var event:Event = new Event(Event.CANCEL);
            dispatchEvent(event);
        }

        private function _createAlbum():void
        {
            _socNet.createAlbum(_gameName, _onAlbumCreate);
        }

        private function _onAlbumCreate(data:Object):void
        {
            _albumId = parseFloat(data['photo_album_id']);

            var createEvent:GameEvent = new GameEvent(GameEvent.ALBUM_CREATED);
            createEvent.data['photo_album_id'] = _albumId;
            dispatchEvent(createEvent);

            _uploadPhoto();
        }

        public function get photoData():Object
        {
            return _photoData;
        }
    }
}
