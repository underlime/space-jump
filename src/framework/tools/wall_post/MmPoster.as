package framework.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.system.Capabilities;

    import framework.data.Data;
    import framework.events.GameEvent;
    import framework.socnet.MyMailRu;
    import framework.socnet.SocNet;

    public class MmPoster extends AbstractWallPoster
    {
        private var _text:String;
        private var _image:Bitmap;
        private var _userModel:Data;
        private var _linkText:String;
        private var _gameName:String;
        private var _title:String;

        private var _socNet:MyMailRu;
        private var _uploader:MmWallImageUploader;

        public function MmPoster(text:String, image:Bitmap, userModel:Data, linkText:String, gameName:String, title:String)
        {
            _text = text;
            _image = image;
            _userModel = userModel;
            _linkText = linkText;
            _gameName = gameName;
            _title = title;

            _socNet = MyMailRu(SocNet.instance());
            _uploader = new MmWallImageUploader(title, text, image, gameName, userModel);
        }

        override public function destroy():void
        {
            if (_uploader) {
                _uploader.destroy();
                _uploader = null;
            }

            super.destroy();
        }

        override public function addPost():void
        {
            _uploader.addEventListener(Event.COMPLETE, _onImageUploaded);
            _uploader.addEventListener(Event.CANCEL, _redispatchEvent);
            _uploader.addEventListener(ErrorEvent.ERROR, _redispatchEvent);
            _uploader.addEventListener(GameEvent.ALBUM_CREATED, _redispatchEvent);
            _uploader.upload();
        }

        private function _onImageUploaded(e:Event):void
        {
            if (Capabilities.playerType != 'StandAlone') {
                _socNet.postPhotoToWall(
                    _text,
                    _linkText,
                    _uploader.photoData['src']
                );
            }

            var event:Event;
            event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }
    }

}
