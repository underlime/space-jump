package framework.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.system.Capabilities;

    import framework.data.Data;
    import framework.events.GameEvent;
    import framework.socnet.OdnoklassnikiRu;
    import framework.socnet.SocNet;

    import org.casalib.util.ObjectUtil;

    public class OkPoster extends AbstractWallPoster
    {
        private var _photosPermission:Boolean;

        private var _text:String;
        private var _uploader:OkWallImageUploader;
        private var _socNet:OdnoklassnikiRu;

        private var _photoText:String;
        private var _linkText:String;
        private var _shortText:String;

        public function OkPoster(text:String, image:Bitmap, userModel:Data, linkText:String, gameName:String, shortText:String)
        {
            _text = text;
            _shortText = shortText;
            _uploader = new OkWallImageUploader(image, gameName, userModel);
            _socNet = OdnoklassnikiRu(SocNet.instance());
            _photoText = text;
            _linkText = linkText;
        }

        override public function destroy():void
        {
            if (_uploader) {
                _uploader.destroy();
                _uploader = null;
            }

            super.destroy();
        }

        override public function addPost():void
        {
            _photosPermission = _socNet.getPhotosPermission();

            if (_photosPermission) {
                _uploader.addEventListener(Event.COMPLETE, _onImageUploaded);
                _uploader.addEventListener(ErrorEvent.ERROR, _redispatchEvent);
                _uploader.addEventListener(GameEvent.ALBUM_CREATED, _redispatchEvent);
                _uploader.upload();
            }
            else {
                _addPlainText();
            }
        }

        private function _onImageUploaded(e:Event):void
        {
            if (Capabilities.playerType == 'StandAlone') {
                var event:Event = new Event(Event.COMPLETE);
                dispatchEvent(event);
            }
            else {
                try {
                    var responseData:Object = JSON.parse(_uploader.loader.data.toString());
                }
                catch (e:SyntaxError) {
                    var errorEvent:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
                    errorEvent.text = 'Wrong server data';
                    dispatchEvent(errorEvent);
                }

                var photoId:String = ObjectUtil.getKeys(responseData['photos'])[0];
                var token:String = responseData['photos'][photoId]['token'];

                _socNet.savePhoto(photoId, token, _photoText, _onPhotoSaved, _onSaveError);
            }
        }

        private function _addPlainText():void
        {
            _socNet.postPhotoToWall(null, _text, _text, _linkText);
            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _onPhotoSaved(data:Object):void
        {
            var photoSrc:String = data['photo_info']['pic640x480'];
            _socNet.postPhotoToWall(photoSrc, _shortText, _text, _linkText);

            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _onSaveError(...args):void
        {
            var message:String = 'Unable to save a photo';
            var event:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
            event.text = message;
            dispatchEvent(event);
        }

        private function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }
    }

}
