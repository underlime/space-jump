package framework.tools.wall_post
{
    import com.facebook.graph.Facebook;

    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.net.URLRequestMethod;

    import framework.data.Data;
    import framework.socnet.SocNet;

    public class FacebookPoster extends AbstractWallPoster
    {
        private var _text:String;
        private var _image:Bitmap;
        private var _userModel:Data;

        public function FacebookPoster(text:String, image:Bitmap, link:String, userModel:Data)
        {
            _text = text;
            _image = image;
            _userModel = userModel;
            _text += '. ' + link;
        }

        override public function destroy():void
        {
            super.destroy();
        }

        override public function addPost():void
        {
            var params:Object = {
                'access_token': SocNet.instance().accessToken,
                'message': _text,
                'image': _image,
                'fileName': 'FILE_NAME'
            };

            var request:String = '/me/photos';
            Facebook.api(request, _onPhotoUploaded, params, URLRequestMethod.POST);
        }

        private function _onPhotoUploaded(...args):void
        {
            if (args.length && args[0] && args[0]['id'])
                _dispatchComplete();
            else
                _dispatchError();
        }

        private function _dispatchComplete():void
        {
            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _dispatchError():void
        {
            var message:String = 'Unable to save a photo';
            var event:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
            event.text = message;
            dispatchEvent(event);
        }
    }

}
