package framework.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.system.Capabilities;

    import framework.data.Data;
    import framework.events.GameEvent;
    import framework.socnet.SocNet;
    import framework.socnet.VkCom;

    public class VkPoster extends AbstractWallPoster
    {
        private var _text:String;
        private var _uploader:VkWallImageUploader;
        private var _socNet:SocNet;
        private var _link:String;

        public function VkPoster(text:String, image:Bitmap, link:String, userModel:Data, gameName:String)
        {
            _text = text;
            _uploader = new VkWallImageUploader(image, userModel, gameName);
            _socNet = SocNet.instance();
            _link = link;
        }

        override public function destroy():void
        {
            if (_uploader) {
                _uploader.destroy();
                _uploader = null;
            }

            super.destroy();
        }

        override public function addPost():void
        {
            _uploader.addEventListener(Event.COMPLETE, _onImageUploaded);
            _uploader.addEventListener(ErrorEvent.ERROR, _redispatchEvent);
            _uploader.addEventListener(GameEvent.ALBUM_CREATED, _redispatchEvent);
            _uploader.upload();
        }

        private function _onImageUploaded(e:Event):void
        {
            if (Capabilities.playerType == 'StandAlone') {
                var event:Event = new Event(Event.COMPLETE);
                dispatchEvent(event);
            }
            else {
                var serverResponse:Object = _uploader.loader.data;
                VkCom(_socNet).savePhoto(_uploader.albumId, serverResponse, _onPhotoSaved, _onSaveError);
            }
        }

        private function _onPhotoSaved(data:Object):void
        {
            var photoId:String = data['photo_info']['photo_id'];
            _addWallPost(photoId);
        }

        private function _onSaveError(...args):void
        {
            var message:String = 'Unable to save a photo';
            var event:ErrorEvent = new ErrorEvent(ErrorEvent.ERROR);
            event.text = message;
            dispatchEvent(event);
        }

        private function _addWallPost(photoId:String):void
        {
            VkCom(_socNet).postPhotoToWall(photoId, _text, _link);
            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }
    }

}
