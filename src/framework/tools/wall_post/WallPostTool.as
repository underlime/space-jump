package framework.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.ErrorEvent;
    import flash.events.Event;

    import framework.data.Data;
    import framework.events.GameEvent;
    import framework.socnet.SocNet;

    import org.casalib.events.RemovableEventDispatcher;

    public class WallPostTool extends RemovableEventDispatcher
    {
        private var _poster:AbstractWallPoster;

        public function WallPostTool(text:String, image:Bitmap, userModel:Data, link:String, gameName:String, linkText:String, shortText:String)
        {
            switch (SocNet.instance().socNetName) {
                case SocNet.VK_COM:
                case SocNet.MOCK:
                    _poster = new VkPoster(text, image, link, userModel, gameName);
                    break;
                case SocNet.FACEBOOK_COM:
                    _poster = new FacebookPoster(text, image, link, userModel);
                    break;
                case SocNet.ODNOKLASSNIKI_RU:
                    _poster = new OkPoster(text, image, userModel, linkText, gameName, shortText);
                    break;
                case SocNet.MY_MAIL_RU:
                    //TODO: вывести title
                    _poster = new MmPoster(text, image, userModel, linkText, gameName, gameName);
                    break;
                default:
                    throw new Error('Unsupported social network');
            }

            _poster.addEventListener(Event.COMPLETE, _redispatchEvent);
            _poster.addEventListener(Event.CANCEL, _dispatchFakeComplete);
            _poster.addEventListener(ErrorEvent.ERROR, _redispatchEvent);
            _poster.addEventListener(GameEvent.ALBUM_CREATED, _redispatchEvent);
        }

        private function _dispatchFakeComplete(e:Event):void
        {
            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _redispatchEvent(e:Event):void
        {
            dispatchEvent(e);
        }

        override public function destroy():void
        {
            if (_poster) {
                _poster.destroy();
                _poster = null;
            }

            super.destroy();
        }

        public function addPost():void
        {
            _poster.addPost();
        }
    }
}
