package framework.tools.wall_post
{
    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.net.FileReference;
    import flash.system.Capabilities;

    import framework.data.Data;
    import framework.events.GameEvent;
    import framework.socnet.OdnoklassnikiRu;
    import framework.socnet.SocNet;
    import framework.tools.ImageUploader;

    import mx.graphics.codec.PNGEncoder;

    public class OkWallImageUploader extends ImageUploader
    {
        private var _gameName:String;
        private var _userModel:Data;
        private var _albumId:Number;

        public function OkWallImageUploader(image:Bitmap, gameName:String, userModel:Data)
        {
            super(image);
            _gameName = gameName;
            _userModel = userModel;
        }

        override public function upload():void
        {
            if (Capabilities.playerType == 'StandAlone')
                _saveToDisk();
            else
                _checkAlbum();
        }

        private function _saveToDisk():void
        {
            var pngEncoder:PNGEncoder = new PNGEncoder();
            var file:FileReference = new FileReference();
            file.save(pngEncoder.encode(_image.bitmapData), 'screenshot.png');

            var event:Event = new Event(Event.COMPLETE);
            dispatchEvent(event);
        }

        private function _checkAlbum():void
        {
            if (_userModel['photo_album_id'] && _userModel['photo_album_id']['value']) {
                _albumId = parseFloat(_userModel['photo_album_id']['value']);
                OdnoklassnikiRu(SocNet.instance()).getAlbumsListInfo(_onAlbumsInfo);
            }
            else {
                _createAlbum();
            }
        }

        private function _onAlbumsInfo(data:Object):void
        {
            var albumsInfo:Array = data['albums_info'] as Array;
            var idsList:Array = [];
            for (var i:int = 0; i < albumsInfo.length; ++i)
                idsList.push(parseFloat(albumsInfo[i]['aid']));

            if (idsList.indexOf(_albumId) != -1) {
                OdnoklassnikiRu(SocNet.instance()).getUploadPhotoParams(_albumId, _onParamsSuccess, _onParamsError);
            }
            else {
                _createAlbum();
            }
        }

        private function _createAlbum():void
        {
            var albumType:String = 'public';
            OdnoklassnikiRu(SocNet.instance()).createAlbum(_gameName, albumType, _onAlbumCreate);
        }

        private function _onAlbumCreate(data:Object):void
        {
            _albumId = parseFloat(data['photo_album_id']);

            var createEvent:GameEvent = new GameEvent(GameEvent.ALBUM_CREATED);
            createEvent.data['photo_album_id'] = _albumId;
            dispatchEvent(createEvent);

            OdnoklassnikiRu(SocNet.instance()).getUploadPhotoParams(_albumId, _onParamsSuccess, _onParamsError);
        }

        private function _onParamsSuccess(data:Object):void
        {
            var serverInfo:Object = data['server_info'];
            _uploadPhotoToServer(serverInfo['server_url'], serverInfo['post_params'], serverInfo['file_name']);
        }

        private function _onParamsError(...args):void
        {
            var message:String = 'Unable to get upload server params';
            _dispatchErrorEvent(message);
        }
    }
}
