package framework.utils
{

    import flash.utils.getQualifiedClassName;

    /**
     * @author SlavaRa
     */
    public class ValidateUtils
    {
        // NAMESPACES --------------------------------------------------------------------------/
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function objectIsNull(object:Object):Boolean
        {
            return !object;
        }

        public static function objectIsNotNull(object:Object):Boolean
        {
            return object !== null;
        }

        public static function objectsIsNotNull(list:Array):Boolean
        {
            for each (var item:* in list) {
                if (!item) {
                    return false;
                }
            }
            return true;
        }

        public static function stringIsEmpty(string:String):Boolean
        {
            return !string || (string.length === 0);
        }

        public static function stringIsNotEmpty(string:String):Boolean
        {
            return string && (string.length > 0);
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ValidateUtils()
        {
            super();
            if ((this as Object).constructor === ValidateUtils) {
                throw new ArgumentError('ArgumentError: ' + getQualifiedClassName(this) + ' class cannot be instantiated.');
            }
        }

        // OVERRIDEN PROPERTIES ----------------------------------------------------------------/
        // OVERRIDEN METHODS -------------------------------------------------------------------/
        // IMPLEMENTED METHODS -----------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
        // DEPRECATED --------------------------------------------------------------------------/
    }
}