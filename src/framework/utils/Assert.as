package framework.utils
{

    import flash.utils.getQualifiedClassName;

    /**
     * @author SlavaRa
     */
    public final class Assert
    {
        // NAMESPACES --------------------------------------------------------------------------/
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function objectIsNull(object:Object, message:String):void
        {
            if (object === null) {
                Error.throwError(TypeError, 2007, message);
            }
        }

        public static function objectIsThis(object:Object, $this:Object):void
        {
            if (object === $this) {
                Error.throwError(ArgumentError, 2024);
            }
        }

        public static function objectNotThis(object:Object, $this:Object):void
        {
            if (object !== $this) {
                Error.throwError(ArgumentError, 2025);
            }
        }

        public static function indexLessThan(index:int, value:int):void
        {
            if (index < value) {
                Error.throwError(RangeError, 2006);
            }
        }

        public static function indexMoreThan(index:int, value:int):void
        {
            if (index > value) {
                Error.throwError(RangeError, 2006);
            }
        }

        public static function indexLessThanOrMoreThan(index:int, value1:int, value2:int):void
        {
            indexLessThan(index, value1);
            indexMoreThan(index, value2);
        }

        public static function stringIsEmpty(string:String, message:String):void
        {
            if (string === null || string.length === 0) {
                Error.throwError(TypeError, 2007);
            }
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Assert()
        {
            super();
            if ((this as Object).constructor === Assert) {
                throw new ArgumentError('ArgumentError: ' + getQualifiedClassName(this) + ' class cannot be instantiated.');
            }
        }

        // OVERRIDEN PROPERTIES ----------------------------------------------------------------/
        // OVERRIDEN METHODS -------------------------------------------------------------------/
        // IMPLEMENTED METHODS -----------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
        // DEPRECATED --------------------------------------------------------------------------/
    }

}