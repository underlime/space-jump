package framework.utils
{

    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.InteractiveObject;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.utils.getQualifiedClassName;

    /**
     * @author SlavaRa
     */
    public class DisplayObjectUtils
    {
        // NAMESPACES --------------------------------------------------------------------------/
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/

        public static function removeChildFrom(child:DisplayObject, container:DisplayObjectContainer):DisplayObject
        {
            if (child && child.parent && child.parent === container) {
                return container.removeChild(child);
            }
            return null;
        }

        /**
         * @author Aleksey Tsvetkov
         * нехороший метод
         */
        public static function removeChild(child:DisplayObject):void
        {
            if (child)
            {
                var parent:DisplayObjectContainer = child.parent;
                if (parent && parent.contains(child))
                    parent.removeChild(child);
            }
        }

        public static function clearContainer(container:DisplayObjectContainer, destroyChildren:Boolean = true, safeMode:Boolean = true):void
        {
            if (container) {
                while (container.numChildren) {
                    const child:DisplayObject = container.removeChildAt(0);
                    if (destroyChildren) {
                        DestroyUtils.destroy(destroyChildren, safeMode);
                    }
                }
            }
        }

        public static function mouseDisable(target:InteractiveObject):void
        {
            if (target) {
                target.mouseEnabled = false;
                if (target is DisplayObjectContainer) {
                    DisplayObjectContainer(target).mouseChildren = false;
                }
                if (target is Sprite) {
                    Sprite(target).buttonMode = false;
                }
            }
        }

        public static function nextFrame(mc:MovieClip, loop:Boolean):void
        {
            if (mc) {
                if (mc.currentFrame !== mc.totalFrames) {
                    mc.nextFrame()
                } else
                    if (loop) {
                        mc.gotoAndStop(0);
                    }
            }
        }

        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function DisplayObjectUtils()
        {
            super();
            if ((this as Object).constructor === DisplayObjectUtils) {
                throw new ArgumentError('ArgumentError: ' + getQualifiedClassName(this) + ' class cannot be instantiated.');
            }
        }

        // OVERRIDEN PROPERTIES ----------------------------------------------------------------/
        // OVERRIDEN METHODS -------------------------------------------------------------------/
        // IMPLEMENTED METHODS -----------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
        // DEPRECATED --------------------------------------------------------------------------/
    }
}