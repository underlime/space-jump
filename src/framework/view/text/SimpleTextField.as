package framework.view.text
{
    import flash.events.Event;
    import flash.text.AntiAliasType;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;

    import org.casalib.display.CasaTextField;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class SimpleTextField extends CasaTextField
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/

        public static const UBUNTU_FONT:String = "UbuntuMono";
        public static const RUSSO_ONE_FONT:String = "Russo_One";
        public static const LOBSTER_FONT:String = "Lobster";
        public static const PT_SANS:String = "Pt_Sans";
        public static const SOURCE_CODE_FONT:String = "SourceCode";


        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function SimpleTextField()
        {
            super();
            this.addEventListener(Event.ADDED_TO_STAGE, this._init);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var size:int = 12;
        public var color:uint = 0xffffff;
        public var align:String = TextFormatAlign.LEFT;
        public var bold:Boolean = false;
        public var italic:Boolean = false;
        public var systemFamily:String;
        public var fontFamily:String = RUSSO_ONE_FONT;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        [Embed(source="../../../../lib/fonts/Russo_One.ttf", embedAsCFF="false", fontName="Russo_One")]
        private var _russoOneFont:Class;

        [Embed(source="../../../../lib/fonts/Lobster.ttf", embedAsCFF="false", fontName="Lobster")]
        private var _lobsterFont:Class;

        [Embed(source="../../../../lib/fonts/PT_Sans.ttf", embedAsCFF="false", fontName="Pt_Sans")]
        private var _ptSansFont:Class;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setupField():void
        {
            this.mouseEnabled = false;
            this.embedFonts = true;
            this.selectable = false;
            this.antiAliasType = AntiAliasType.ADVANCED;

            var format:TextFormat = new TextFormat();

            if (systemFamily) {
                this.embedFonts = false;
                format.font = systemFamily;
            } else {
                format.font = fontFamily;
            }
            format.size = this.size;
            format.align = this.align;
            format.color = this.color;
            format.bold = this.bold;
            format.italic = this.italic;

            this.defaultTextFormat = format;

            this.text = "";
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _init(e:Event):void
        {
            this.removeEventListener(Event.ADDED_TO_STAGE, _init);
            this._setupField();
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }

}