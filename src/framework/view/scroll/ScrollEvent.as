/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 22.12.12
 */

package framework.view.scroll
{
    import flash.events.Event;

    public class ScrollEvent extends Event
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/

        public static const CHANGE:String = "ScrollChangeEvent";

        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ScrollEvent(type:String, percent:Number)
        {
            this.percent = percent;
            super(type);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var percent:Number = 0;

        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // OVERRIDE PROTECTED METHODS ----------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
