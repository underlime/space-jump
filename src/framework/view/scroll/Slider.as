package framework.view.scroll 
{
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;

    /**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class Slider extends View
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function Slider(marker:Sprite, track:Sprite, rectangle:Rectangle) 
		{
			this._marker = marker;
			this._track = track;
			this._rectangle = rectangle;
			super();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		
		override public function render():void 
		{
			this._marker.addEventListener(MouseEvent.MOUSE_DOWN, this._markerDownHandler);
			this._marker.buttonMode = true;
			this._marker.useHandCursor = true;
			
            this.addChild(this._track);
			this.addChild(this._marker);
		}
		
		override public function destroy():void 
		{
			super.destroy();
		}
		
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		
		protected var _marker:Sprite;
		protected var _track:Sprite;
		protected var _rectangle:Rectangle;
		protected var _percent:Number = 0;
		
		// PROTECTED METHODS -------------------------------------------------------------------/
		
		protected function _updatePercent():void 
		{
			
		}
		
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		
		protected function _mouseUpHandler(e:MouseEvent):void 
		{
			this._marker.stopDrag();
            this.stage.removeEventListener( MouseEvent.MOUSE_MOVE, this._mouseMoveHandler );
            this.stage.removeEventListener( MouseEvent.MOUSE_UP, this._mouseUpHandler );
		}
		
		protected function _mouseMoveHandler(e:MouseEvent):void 
		{
			e.updateAfterEvent();
			this._updatePercent();
			super.dispatchEvent(new ScrollEvent(ScrollEvent.CHANGE, this._percent));
		}
		
		protected function _markerDownHandler(e:MouseEvent):void 
		{
			this._marker.startDrag(false, this._rectangle);
            this.stage.addEventListener(MouseEvent.MOUSE_MOVE, this._mouseMoveHandler);
            this.stage.addEventListener(MouseEvent.MOUSE_UP, this._mouseUpHandler);
		}
		
		protected function _updateMarketPosition():void 
		{
			
		}
		
		// ACCESSORS ---------------------------------------------------------------------------/
		
		public function get rectangle():Rectangle 
		{
			return this._rectangle;
		}
		
		public function get percent():Number 
		{
			return this._percent;
		}
		
		public function set percent(value:Number):void 
		{
			if (this._percent != value)
			{
				if (value < 0) {
					this._percent = 0;
				}else if (value > 100) {
					this._percent = 100;
				}else {
					this._percent = value;
				}
				this._updateMarketPosition();
				super.dispatchEvent(new ScrollEvent(ScrollEvent.CHANGE, this._percent));
			}
		}
	}
}