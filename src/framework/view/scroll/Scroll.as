package framework.view.scroll 
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	/**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class Scroll extends Slider
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function Scroll(marker:Sprite, track:Sprite, rectangle:Rectangle) 
		{
			super(marker, track, rectangle);
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		
		override protected function _updatePercent():void 
		{
			var total:Number = this._rectangle.height;
			var current:Number = this._marker.y;
			this._percent = Math.round((current / total) * 100);
		}
		
		override protected function _updateMarketPosition():void 
		{
			var total:Number = this._rectangle.height;
			this._marker.y = Math.round((this._percent / 100) * total);
		}
		
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		// ACCESSORS ---------------------------------------------------------------------------/
	}

}