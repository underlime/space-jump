/**
 * Aleksey Tsvetkov (antondenikin[at]gmail[dot]com)
 * Date: 15.07.13
 * Time: 15:57
 */
package framework.view.scroll_future
{
    import application.controllers.ApplicationController;
    import application.events.JsScrollEvent;

    import flash.geom.Rectangle;

    import framework.view.View;

    import org.casalib.core.Destroyable;

    public class ScrollContentObserver extends Destroyable
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function ScrollContentObserver(appCtrl:ApplicationController, scrollContent:View, scroll:Scroll, scrollRect:Rectangle)
        {
            super();
            _content = scrollContent;
            _contentHeight = scrollContent.height;
            scrollContent.scrollRect = scrollRect;
            appCtrl.addEventListener(JsScrollEvent.SCROLL, _changeByWheel);
            _scrollbar = scroll;
            _scrollbar.addEventListener(ScrollEvent.CHANGE, _updateContent);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function destroy():void
        {
            _content.destroy();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _content:View;
        protected var _scrollbar:Scroll;
        protected var _contentHeight:Number;

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _updateContent(event:ScrollEvent):void
        {
            var scrollable:Number = _contentHeight - _content.scrollRect.height;
            var sr:Rectangle = _content.scrollRect.clone();
            sr.y = scrollable*event.percent;

            _content.scrollRect = sr;
        }

        private function _changeByWheel(e:JsScrollEvent):void
        {
            var percent:Number = _scrollbar.percent - (e.dir*3)/100;
            if (percent < 0)
                percent = 0;

            if (percent > 1)
                percent = 1;

            _scrollbar.percent = percent;
        }

        // ACCESSORS ---------------------------------------------------------------------------/

    }
}
