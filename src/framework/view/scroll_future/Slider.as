package framework.view.scroll_future
{
    import flash.display.DisplayObject;
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;

    import framework.view.View;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class Slider extends View
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Slider(marker:Sprite, track:DisplayObject, rectangle:Rectangle)
        {
            _marker = marker;
            _track = track;
            _rectangle = rectangle;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            _marker.addEventListener(MouseEvent.MOUSE_DOWN, _markerDownHandler);
            _marker.buttonMode = true;
            _marker.useHandCursor = true;

            this.addChild(_track);
            this.addChild(_marker);
        }

        override public function destroy():void
        {
            _marker.removeEventListener(MouseEvent.MOUSE_DOWN, _markerDownHandler);

            super.destroy();
        }


        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/

        protected var _marker:Sprite;
        protected var _track:DisplayObject;
        protected var _rectangle:Rectangle;
        protected var _percent:Number = 0;

        // PROTECTED METHODS -------------------------------------------------------------------/

        protected function _updatePercent():void
        {

        }

        // PRIVATE PROPERTIES ------------------------------------------------------------------/
        // PRIVATE METHODS ---------------------------------------------------------------------/
        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _mouseUpHandler(e:MouseEvent):void
        {
            _marker.stopDrag();
            this.stage.removeEventListener(MouseEvent.MOUSE_MOVE, _mouseMoveHandler);
            this.stage.removeEventListener(MouseEvent.MOUSE_UP, _mouseUpHandler);
        }

        protected function _mouseMoveHandler(e:MouseEvent):void
        {
            e.updateAfterEvent();
            _updatePercent();
            super.dispatchEvent(new ScrollEvent(ScrollEvent.CHANGE, _percent));
        }

        protected function _markerDownHandler(e:MouseEvent):void
        {
            _marker.startDrag(false, _rectangle);
            this.stage.addEventListener(MouseEvent.MOUSE_MOVE, _mouseMoveHandler);
            this.stage.addEventListener(MouseEvent.MOUSE_UP, _mouseUpHandler);
        }

        protected function _updateMarketPosition():void
        {

        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get rectangle():Rectangle
        {
            return _rectangle;
        }

        public function get percent():Number
        {
            return _percent;
        }

        public function set percent(value:Number):void
        {
            if (_percent != value) {
                if (value < 0) {
                    _percent = 0;
                }
                else
                    if (value > 100) {
                        _percent = 100;
                    }
                    else {
                        _percent = value;
                    }
                _updateMarketPosition();
                super.dispatchEvent(new ScrollEvent(ScrollEvent.CHANGE, _percent));
            }
        }
    }
}