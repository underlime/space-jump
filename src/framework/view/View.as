package framework.view
{

    import flash.events.Event;

    import framework.interfaces.IView;
    import framework.tools.SourceManager;

    import org.casalib.display.CasaSprite;

    /**
	 * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
	 */
	public class View extends CasaSprite implements IView
	{
		// CLASS VARIABLES ---------------------------------------------------------------------/
		// CLASS PROPERTIES --------------------------------------------------------------------/
		// CLASS METHODS -----------------------------------------------------------------------/
		// CONSTRUCTOR -------------------------------------------------------------------------/
		
		public function View()
		{
			super();
			this.addEventListener(Event.ADDED_TO_STAGE, this._init);
			this.setup();
		}
		
		// OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
		// PUBLIC PROPERTIES -------------------------------------------------------------------/
		// PUBLIC METHODS ----------------------------------------------------------------------/
		
		/**
		 * Метод вызывается при добавлении объекта на сцену
		 */
		public function render():void
		{
		
		}
		
		/**
		 * Функция вызывается при инициализации объекта
		 */
		public function setup():void
		{
		
		}
		
		/**
		 * Функция отключает интерактивные элементы для вида.
		 * 
		 */
		public function disableInteractive():void
		{
			this.mouseChildren = false;
			this.mouseEnabled = false;
			this.tabChildren = false;
			this.tabEnabled = false;
		}

		// PROTECTED PROPERTIES ----------------------------------------------------------------/
		// PROTECTED METHODS -------------------------------------------------------------------/
		// PRIVATE PROPERTIES ------------------------------------------------------------------/
		// PRIVATE METHODS ---------------------------------------------------------------------/
		// EVENT HANDLERS ----------------------------------------------------------------------/
		
		private function _init(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, this._init);
			this.render();
		}
		
		// ACCESSORS ---------------------------------------------------------------------------/
		
		public function get source():SourceManager
		{
			return SourceManager.instance
		}
	}
}