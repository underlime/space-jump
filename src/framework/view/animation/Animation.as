package framework.view.animation
{
    import flash.display.Bitmap;
    import flash.events.Event;

    import framework.view.View;

    /**
     * Класс для инимирования растровых кадров
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class Animation extends View
    {

        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function Animation(frames:Vector.<Bitmap>)
        {
            this._frames = frames;
            super();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function setup():void
        {
            for (var i:int = 0; i < _frames.length; i++) {
                if (_frames[i].width > maxWidth)
                    maxWidth = _frames[i].width;

                if (_frames[i].height > maxHeight)
                    maxHeight = _frames[i].height;
            }
        }

        override public function render():void
        {
            this.play();
        }

        override public function destroy():void
        {
            this._removeFrame();
            this._disableFrameChange();
            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/

        public var maxWidth:Number = 0;
        public var maxHeight:Number = 0;

        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function play():void
        {
            this._enableFrameChange();
        }

        public function stop():void
        {
            this._disableFrameChange();
        }

        public function gotoAndPlay(frame:int):void
        {
            this._currentFrame = frame;
            this._checkFrame();
            this.play();
        }

        public function gotoAndStop(frame:int):void
        {
            this._currentFrame = frame;
            this._checkFrame();
            this.stop();
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _frames:Vector.<Bitmap> = new Vector.<Bitmap>();
        private var _currentFrame:int = 1;
        private var _totalFrames:int;
        private var _currentBitmap:Bitmap;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _enableFrameChange():void
        {
            if (!this.hasEventListener(Event.ENTER_FRAME)) {
                this.addEventListener(Event.ENTER_FRAME, this._enterFrameHandler);
            }
        }

        private function _disableFrameChange():void
        {
            this.removeEventListener(Event.ENTER_FRAME, this._enterFrameHandler);
        }

        private function _addFrame():void
        {
            this._removeFrame();
            this._currentBitmap = this._frames[this._currentFrame - 1];
            this.addChild(this._currentBitmap);
        }

        private function _removeFrame():void
        {
            if (this._currentBitmap && this.contains(this._currentBitmap)) {
                this.removeChild(this._currentBitmap);
            }
        }

        private function _checkFrame():void
        {
            if (this._currentFrame > this.totalFrames || this._currentFrame < 1) {
                this._currentFrame = 1;
            }
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        protected function _enterFrameHandler(e:Event):void
        {
            this._currentFrame++;
            this._checkFrame();
            this._addFrame();
        }

        // ACCESSORS ---------------------------------------------------------------------------/

        public function get totalFrames():int
        {
            return this._frames.length;
        }

        public function get currentFrame():int
        {
            return this._currentFrame;
        }

        public function get frames():Vector.<Bitmap>
        {
            return this._frames;
        }
    }

}