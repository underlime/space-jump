/**
 * Aleksey Tsvetkov
 * need TweenLite
 */
package framework.view.animation
{
    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    public class BlinkAnimation extends Animation
    {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/

        public function BlinkAnimation(frames:Vector.<Bitmap>, seconds:int = 30)
        {
            _seconds = seconds;
            super(frames);
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/

        override public function render():void
        {
            super.render();
            _createTimer(_seconds);
        }

        override public function destroy():void
        {
            if (_timer)
                _destroyTimer();

            super.destroy();
        }

        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/

        public function updateSeconds(seconds:int):void
        {
            _destroyTimer();
            _createTimer(seconds);
        }

        // PROTECTED PROPERTIES ----------------------------------------------------------------/


        override protected function _enterFrameHandler(e:Event):void
        {
            super._enterFrameHandler(e);
            _blink();
        }

        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _seconds:int = 30;
        private var _timer:Timer;
        private var _timeMargin:Number = 2;
        private var _currentTime:int = 3;
        private var _blinkTime:int = 0;
        private var _blinkEnabled:Boolean = false;

        private var _blinkFramesDelay:int = 10;
        private var _blinkCurrentFramesDelay:int = 0;
        private var _alphaEnabled:Boolean = false;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _blink():void
        {
            if (_blinkEnabled) {
                _blinkCurrentFramesDelay++;
                if (_blinkCurrentFramesDelay >= _blinkFramesDelay) {
                    if (_blinkFramesDelay > 2)
                        _blinkFramesDelay--;

                    _blinkCurrentFramesDelay = 0;
                    _alphaEnabled = !_alphaEnabled;
                    this.alpha = _alphaEnabled ? .5 : 1;
                }
            }
        }

        private function _createTimer(seconds:int):void
        {
            _currentTime = 3;
            _blinkTime = _seconds - _timeMargin;
            _blinkEnabled = false;
            this.alpha = 1;

            _timer = new Timer(1000, seconds);
            _timer.addEventListener(TimerEvent.TIMER, _timerHandler);
            _timer.addEventListener(TimerEvent.TIMER_COMPLETE, _timerCompleteHandler);
            _timer.start();
        }

        private function _destroyTimer():void
        {
            _timer.removeEventListener(TimerEvent.TIMER, _timerHandler);
            _timer.removeEventListener(TimerEvent.TIMER_COMPLETE, _timerCompleteHandler);
            _timer.stop();
            _timer = null;
        }

        private function _completeAnimation():void
        {
            _destroyTimer();
            super.dispatchEvent(new Event(Event.COMPLETE));
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/

        private function _timerHandler(event:TimerEvent):void
        {
            if (_currentTime > _blinkTime) {
                _blinkEnabled = true;
            }
            _currentTime++;
        }

        private function _timerCompleteHandler(event:TimerEvent):void
        {
            _completeAnimation();
        }

        // ACCESSORS ---------------------------------------------------------------------------/
    }
}
