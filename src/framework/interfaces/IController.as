package framework.interfaces {

    import framework.data.DataBase;

    import org.casalib.core.IDestroyable;
    import org.casalib.events.IRemovableEventDispatcher;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public interface IController extends IRemovableEventDispatcher, IExecutable, IDestroyable {

        function get baseController():IBaseController;
		
		
        /**
         * Сcылка на базу данных.
         */
        function get dataBase():DataBase;

    }

}