package framework.interfaces {

    import flash.display.DisplayObjectContainer;

    import framework.error.ApplicationError;
    import framework.socnet.SocNet;
    import framework.underquery.ServerApi;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public interface IBaseController extends IController {
        /**
         * графический контейнер
         */
        function get container():DisplayObjectContainer;
        function get serverApi():ServerApi;
        function get socNet():SocNet;
        function appError(error:ApplicationError):void;
    }

}