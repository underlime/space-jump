package framework.interfaces
{

    import org.casalib.events.IRemovableEventDispatcher;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public interface IView extends IRemovableEventDispatcher
    {
        function render():void;

        function setup():void;
    }

}