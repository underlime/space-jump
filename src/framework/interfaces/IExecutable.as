package framework.interfaces {

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public interface IExecutable {
        function execute():void;
    }

}