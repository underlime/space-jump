package framework.socnet.models
{
    import framework.data.ListData;
    import framework.socnet.models.soc_net_registry.SocNetUserRecord;

    public class SocNetInfoRegistry extends ListData
    {
        public function SocNetInfoRegistry()
        {
            _RecordClass = SocNetUserRecord;
            super('SocNetInfoRegistry');
        }

        override public function getModelDataKeys():Array
        {
            return ['soc_net_users_list'];
        }
    }
}
