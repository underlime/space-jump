package framework.socnet
{
    import by.blooddy.crypto.MD5;

    import flash.external.ExternalInterface;

    import framework.controller.BaseController;

    public class MyMailRu extends SocNet
    {
        public function MyMailRu(baseController:BaseController, privateClass:PrivateClass)
        {
            super(baseController, privateClass);
        }

        override public function createToken(requestUri:String, salt:String):String
        {
            var socNetParams:Object = this.serverCallParams;
            var baseString:String =
                socNetParams['query_params']
                    + requestUri
                    + salt;
            return MD5.hash(baseString);
        }

        override public function buyMoney(currency:String, count:int, price:int, successCallback:Function = null, errorCallback:Function = null, cancelCallback:Function = null):void
        {
            _callMethod('SocNetWrapper.buyMoney', [count, currency, count, price], successCallback, errorCallback, cancelCallback, false);
        }

        public function calcSignature(params:Object):String
        {
            if (params['secure'] !== 0 && params['secure'] !== '0')
                throw new Error('Secure == 0 is required');

            var baseString:String = this.socNetId;

            var paramsNames:Array = [];
            for (var name:String in params) {
                if (name != 'sig' && name != 'img_file')
                    paramsNames.push(name);
            }
            paramsNames.sort();

            for (name in paramsNames)
                baseString += name + '=' + params[name];
            baseString += this.privateKey;

            return MD5.hash(baseString);
        }

        public function getAlbumInfo(albumId:Number, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('MmPostTool.getAlbumInfo', [albumId], successCallback, errorCallback);
        }

        public function getAlbumsListInfo(successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('MmPostTool.getAlbumsListInfo', [], successCallback, errorCallback);
        }

        public function getPhotoInfo(albumId:Number, photoId:Number, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('MmPostTool.getPhotoInfo', [albumId, photoId], successCallback, errorCallback);
        }

        public function createAlbum(name:String, successCallback:Function = null, errorCallback:Function = null):void
        {
            _callMethod('MmPostTool.createAlbum', [name], successCallback, errorCallback);
        }

        public function uploadPhoto(aid:String, photoUrl:String, name:String, description:String, tags:String, successCallback:Function, errorCallback:Function, cancelCallback:Function):void
        {
            _callMethod('MmPostTool.uploadPhoto', [aid, photoUrl, name, description, tags], successCallback, errorCallback, cancelCallback);
        }

        public function postPhotoToWall(text:String, linkText:String, photoSrc:String):void
        {
            _callMethod('MmPostTool.postPhotoToWall', [undefined, text, linkText, photoSrc], callback, null, null, false);
            function callback(data:Object):void
            {
            }
        }

        public function get privateKey():String
        {
            return ExternalInterface.call('SocNetWrapper.getPrivateKey');
        }

        public function get socNetId():String
        {
            return ExternalInterface.call('SocNetWrapper.getCurrentSocNetId');
        }

        public function get appId():String
        {
            return ExternalInterface.call('SocNetWrapper.getAppId');
        }

        public function get sessionKey():String
        {
            return ExternalInterface.call('SocNetWrapper.getSessionKey');
        }
    }
}
