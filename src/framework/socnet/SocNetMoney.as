package framework.socnet
{
    public class SocNetMoney
    {
        private static var _count:int;
        private static var _lang:String;

        public function SocNetMoney()
        {
        }

        public static function getMoneyLabel(count:int, socNet:SocNet):String
        {
            _count = count;
            _lang = socNet.currentLanguage;

            var label:String;
            switch (socNet.socNetName) {
                case SocNet.VK_COM:
                case SocNet.MOCK:
                    label = _getVkComLabel();
                    break;
                case SocNet.FACEBOOK_COM:
                    label = _getFacebookComLabel();
                    break;
                case SocNet.ODNOKLASSNIKI_RU:
                    label = _getOdnoklassnikiRuLabel();
                    break;
                case SocNet.MY_MAIL_RU:
                    label = _getMyMailRuLabel();
                    break;
                default:
                    throw new Error('Wrong social network');
            }

            return label;
        }

        private static function _getVkComLabel():String
        {
            var label:String;

            if (_lang == SocNet.LANG_RU)
                label = _getVkComLabelRu();
            else
                label = _getVkComLabelEn();

            return label;
        }

        private static function _getVkComLabelRu():String
        {
            var currencyName:String = 'голос';

            var mod10:int = _count%10;
            var mod100:int = _count%100;
            var not11:Boolean = (mod100 <= 10 || mod100 >= 20);

            if (mod10 >= 2 && mod10 <= 4 && not11)
                currencyName += 'а';
            else
                if (!not11 || mod10 == 0 || (not11 && mod10 > 4))
                    currencyName += 'ов';

            return currencyName;
        }

        private static function _getVkComLabelEn():String
        {
            var currencyName:String = 'voice';

            if (_count > 1)
                currencyName += 's';

            return currencyName;
        }

        private static function _getFacebookComLabel():String
        {
            var label:String;

            if (_lang == SocNet.LANG_RU)
                label = _getFbComLabelRu();
            else
                label = _getFbComLabelEn();

            return label;
        }

        private static function _getFbComLabelRu():String
        {
            var currencyName:String = 'балл';

            var mod10:int = _count%10;
            var mod100:int = _count%100;
            var not11:Boolean = (mod100 <= 10 || mod100 >= 20);

            if (mod10 >= 2 && mod10 <= 4 && not11)
                currencyName += 'а';
            else
                if (!not11 || mod10 == 0 || (not11 && mod10 > 4))
                    currencyName += 'ов';

            return currencyName;
        }

        private static function _getFbComLabelEn():String
        {
            var currencyName:String = 'credit';

            if (_count > 1)
                currencyName += 's';

            return currencyName;
        }

        private static function _getOdnoklassnikiRuLabel():String
        {
            return 'OK';
        }

        private static function _getMyMailRuLabel():String
        {
            var currencyName:String = 'мэйлик';

            var mod10:int = _count%10;
            var mod100:int = _count%100;
            var not11:Boolean = (mod100 <= 10 || mod100 >= 20);

            if (mod10 >= 2 && mod10 <= 4 && not11)
                currencyName += 'а';
            else
                if (!not11 || mod10 == 0 || (not11 && mod10 > 4))
                    currencyName += 'ов';

            return currencyName;
        }
    }
}
