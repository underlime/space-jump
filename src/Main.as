package
{
    import application.config.ApplicationConfiguration;
    import application.controllers.ApplicationController;
    import application.views.screen.error.ErrorView;

    import com.flashdynamix.utils.SWFProfiler;

    import flash.display.Loader;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.StatusEvent;
    import flash.net.LocalConnection;
    import flash.net.SharedObject;
    import flash.system.Capabilities;
    import flash.utils.ByteArray;

    import framework.error.ApplicationError;

    import org.casalib.util.StageReference;

    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    [Frame(factoryClass="Preloader")]
    [SWF(width='800', height='700', backgroundColor='#000000', frameRate='30')]
    public class Main extends Sprite
    {
        public static const SINGLE_CONNECTION_NAME:String = 'underlime-spacediver-singleconnection';
        public static const CORRECT_URL_REGEXP:RegExp = /^http(?:s?):\/\/[\w\.-]+?\.(?:(?:webkraken)|(?:evast))\.ru\/.+?game\.swf/;
        public static const LAUNCH_SHARED_OBJECT:String = 'launch_data';

        [Embed(source="../lib/swf/intro.swf", mimeType="application/octet-stream")]
        private var _IntroSwfClass:Class;

        private var _singleConnection:LocalConnection;

        private var _introBytesLoader:Loader;
        private var _introFramesCounter:int;
        private var _introMovie:MovieClip;
        private var _launchData:SharedObject;

        public function Main()
        {
            super();
            _launchData = SharedObject.getLocal(LAUNCH_SHARED_OBJECT, '/');
            super.addEventListener(Event.ADDED_TO_STAGE, this.init);
        }

        public function onLocalConnect():void
        {
        }

        private function init(e:Event = null):void
        {
            if (ApplicationConfiguration.DEBUG)
                SWFProfiler.init(stage, this);
            StageReference.setStage(super.stage);

            super.removeEventListener(Event.ADDED_TO_STAGE, arguments.callee);
            ApplicationConfiguration.flashVars = this.root.loaderInfo.parameters;

            var correctDomain:Boolean = true;
            if (!ApplicationConfiguration.DEBUG)
                correctDomain = this._checkDomain();

            if (correctDomain)
                this._checkSingleLaunch();
        }

        private function _checkDomain():Boolean
        {
            var ok:Boolean = CORRECT_URL_REGEXP.test(this.loaderInfo.url);
            if (!ok)
                _showError('Wrong domain');
            return ok;
        }

        private function _checkSingleLaunch():void
        {
            if (LocalConnection.isSupported && !ApplicationConfiguration.DEBUG) {
                this._singleConnection = new LocalConnection();
                this._singleConnection.addEventListener(StatusEvent.STATUS, _onLocalConnectionStatus);
                this._singleConnection.send(SINGLE_CONNECTION_NAME, 'onLocalConnect');
            }
            else {
                this._launchApplication();
            }
        }

        private function _launchApplication():void
        {
            var introDate:Date = _launchData.data.introDate;
            var nowDate:Date = new Date();

            if (introDate && nowDate.getTime() - introDate.getTime() < 86400000) {
                new ApplicationController(this);
            }
            else {
                _launchData.data.introDate = nowDate;
                var introBytes:ByteArray = new _IntroSwfClass();
                _introFramesCounter = 0;
                _introBytesLoader = new Loader();
                _introBytesLoader.loadBytes(introBytes);
                addEventListener(Event.ENTER_FRAME, _waitForIntro);
            }
        }

        private function _waitForIntro(e:Event):void
        {
            if (++_introFramesCounter == 2) {
                removeEventListener(Event.ENTER_FRAME, _waitForIntro);
                _introMovie = _introBytesLoader.content as MovieClip;
                _introMovie.addEventListener(Event.ENTER_FRAME, _onIntroEnterFrame);
                addChild(_introMovie);
            }
        }

        private function _onIntroEnterFrame(e:Event):void
        {
            if (_introMovie.currentFrame == _introMovie.totalFrames) {
                _introMovie.removeEventListener(Event.ENTER_FRAME, _onIntroEnterFrame);
                _introMovie.stop();
                removeChild(_introMovie);
                _introMovie = null;
                new ApplicationController(this);
            }
        }

        private function _onLocalConnectionStatus(e:StatusEvent):void
        {
            this._singleConnection.removeEventListener(StatusEvent.STATUS, _onLocalConnectionStatus);
            if (e.level == 'error') {
                this._singleConnection.client = this;
                this._singleConnection.connect(SINGLE_CONNECTION_NAME);
                this._launchApplication();
            }
            else {
                var errorMessage:String;
                if (Capabilities.language == 'ru')
                    errorMessage = 'Вы уже запустили игру в другом окне';
                else
                    errorMessage = 'You have already launched this game in another window';
                _showError(errorMessage);
            }
        }

        private function _showError(text:String):void
        {
            var error:ApplicationError = new ApplicationError();
            error.type = ApplicationError.LOAD_ERROR;
            error.message = text;

            var errorView:ErrorView = new ErrorView(error);
            super.stage.addChild(errorView);
        }
    }

}