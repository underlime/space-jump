package
{

    import application.views.load.LoadView;

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.utils.getDefinitionByName;

    /**
     * Preloader
     * @author Aleksey Tsvetkov
     */

    public class Preloader extends MovieClip
    {

        private var _loadView:LoadView;

        public function Preloader()
        {
            if (stage) {
                stage.scaleMode = StageScaleMode.NO_SCALE;
                stage.align = StageAlign.TOP_LEFT;
            }
            addEventListener(Event.ENTER_FRAME, checkFrame);
            loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
            loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);

            _setupLoadView();
        }

        private function _setupLoadView():void
        {
            _loadView = new LoadView();
            addChild(_loadView);
        }

        private function ioError(e:IOErrorEvent):void
        {
            trace('Preloader IO error', e.text);
        }

        private function progress(e:ProgressEvent):void
        {
            var percent:Number = (e.bytesLoaded/e.bytesTotal)*100;
            _loadView.percent = percent;
        }

        private function checkFrame(e:Event):void
        {
            if (currentFrame == totalFrames) {
                stop();
                loadingFinished();
            }
        }

        private function loadingFinished():void
        {
            removeEventListener(Event.ENTER_FRAME, checkFrame);
            loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
            loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);

            _loadView.destroy();
            startup();
        }

        private function startup():void
        {
            var mainClass:Class = getDefinitionByName("Main") as Class;
            addChild(new mainClass() as DisplayObject);
        }

    }

}