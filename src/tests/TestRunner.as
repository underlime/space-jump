package tests {

    import org.flexunit.internals.TraceListener;
    import org.flexunit.listeners.CIListener;
    import org.flexunit.runner.FlexUnitCore;

    import tests.suites.EntireTestSuite;
    /**
     * @author Aleksey Tsvetkov (antondenikin[at]gmail.com)
     */
    public class TestRunner {
        // CLASS VARIABLES ---------------------------------------------------------------------/
        // CLASS PROPERTIES --------------------------------------------------------------------/
        // CLASS METHODS -----------------------------------------------------------------------/
        // CONSTRUCTOR -------------------------------------------------------------------------/
        public function TestRunner() {
            this._setup();
        }

        // OVERRIDE PUBLIC METHODS -------------------------------------------------------------/
        // PUBLIC PROPERTIES -------------------------------------------------------------------/
        // PUBLIC METHODS ----------------------------------------------------------------------/
        // PROTECTED PROPERTIES ----------------------------------------------------------------/
        // PROTECTED METHODS -------------------------------------------------------------------/
        // PRIVATE PROPERTIES ------------------------------------------------------------------/

        private var _core:FlexUnitCore;

        // PRIVATE METHODS ---------------------------------------------------------------------/

        private function _setup():void {
            this._core = new FlexUnitCore();
            this._core.addListener(new TraceListener());
            this._core.addListener(new CIListener());

            this._core.run(EntireTestSuite);
        }

        // EVENT HANDLERS ----------------------------------------------------------------------/
        // ACCESSORS ---------------------------------------------------------------------------/
    }
}